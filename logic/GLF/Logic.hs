-- |
-- Module      : Logic
-- Description : Re-exports logical definitions
-- Copyright   : (c) bruno cuconato, 2021–…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic
  ( -- * Defining a new deductive system

    -- | A deductive system is data record specifying a set of rules and some
    -- metadata. To define a new system, one must create a new value of this
    -- type, and add it to the 'systems' list.
    System (..),
    -- | All deductive systems defined by GLF.
    -- minimalImplicationalND,
    -- minimalImplicationalFitch,
    -- minimalND,
    -- minimalFitch,
    -- intuitionisticND,
    -- intuitionisticFitch,
    -- classicalND,
    -- classicalFitch,
    -- modalKFitch,
    -- modalTFitch,
    -- modalS4Fitch,
    -- modalS5Fitch,
    -- modalDFitch,
    -- modalGFitch,
    -- modalK43Fitch,
    -- ialcBND,
    -- claTableaux,
    -- signedClaTableaux,
    -- folFitch,
    -- folBND,
    defaultSystems,

    -- ** Defining deductive rules

    -- | Most of the work in defining a new system consists in
    -- defining its rules. Rules can be defined in ‘raw mode’ by
    -- creating 'DeductiveRule' values, but there is a
    -- domain-specific mini-language that helps define rules (see
    -- below).
    DeductiveRule(..),
    -- | The domain-specific mini-language depends heavily on the
    -- concept of a formula specification. This is used to specify
    -- premises and conclusions of a rule.
    -- | As an example, we define below the rule of implication
    -- elimination in a Natural Deduction system. The rule can be
    -- schematized as:
    --
    -- @
    --      [A]
    --       |
    --       B
    --   --------
    --    A -> B
    -- @
    --
    -- A premise like the one of this rule can be any formula, but
    -- it has to match the consequent of the rule's conclusion. The
    -- conclusion demands a formula specification that requires the
    -- implication as a main connective. All in all, we could write:
    --
    -- @
    --   rule (asciiName "->I")
    --     [ premise "predicate" "B"
    --       `discharges` ("hypothesis", "A")
    --     , concludes ("A" `implies` "B")
    --     ]
    -- @
    --
    -- We will see more about the auxiliary functions (e.g., 'rule',
    -- 'premise', etc.) later. See the files with the definitions
    -- for the various logical systems included in GLF for examples,
    -- or take a look at chapter 5 of my
    -- [thesis](https://doi.org/10.17771/PUCRio.acad.65161).
    MinImp.implication,
    Min.conjunction, Min.disjunction,
    Min.bottomOp,
    --FOL.universal, FOL.existential,
    -- Tactic,
    -- --ruleTactic, -- TODO: add the other combinators here
    -- many,
    -- some,
    -- try,
    -- andThen,
    -- orElse,

  )
where

import qualified Data.Map as Map
import qualified GLF.Logic.LFOL as LFOL
import GLF.Logic.Ultrafilter as Ultra
import GLF.Logic.Filter as Filter
import GLF.Logic.Keisler as Keisler
import GLF.Logic.CTL as CTL
import GLF.Logic.CTLstar as CTLstar
import GLF.Logic.Zp as Zp
import GLF.Logic.Modal as Modal
import qualified GLF.Logic.FOL as FOL
-- import qualified GLF.Logic.Classical as Cla
import qualified GLF.Logic.IALC as IALC
-- import qualified GLF.Logic.Intuitionistic as Intui
import qualified GLF.Logic.Min as Min
import qualified GLF.Logic.MinImp as MinImp
-- import qualified GLF.Logic.Modal as Mod
-- import qualified GLF.Logic.Tableaux.Classical as Tab
-- import qualified GLF.Logic.Tableaux.SignedClassical as STab
import GLF.Rule
import GLF.System
--import GLF.Tactic
import Prelude hiding (min, filter)

-- minimalImplicationalFitch :: System
-- minimalImplicationalFitch =
--   System
--     "M->Fw"
--     [[MinImp.implication]]
--     False
--     [ MinImp.assumptionFw,
--       MinImp.assumptionSameLevelFw,
--       MinImp.qedFw,
--       MinImp.implicationIntroFw,
--       MinImp.implicationElimFw
--     ]
--     [ noLanguageDeductionInput [] ["(-> (-> A (-> B C)) (-> (-> A B) (-> A C)))"]
--     ]
--     []
--     (FitchSys MinImp.assumptionSameLevelFw)

-- minimalImplicationalND :: System
-- minimalImplicationalND =
--   System
--     "M->"
--     [[MinImp.implication]]
--     False
--     [ MinImp.hypothesisDiscard,
--       MinImp.implicationIntroBND,
--       MinImp.implicationElimBND
--     ]
--     [noLanguageDeductionInput ["(-> A1 A2)", "(-> A1 (-> A2 A3))"] ["(-> A1 A3)"]]
--     [("M->", MinImp.tactic)]
--     (BackNDSys hypothesisDiscard)

-- minimalExamples :: [DeductionInput]
-- minimalExamples =
--   [ noLanguageDeductionInput ["(| A B)"] ["(| B A)"],
--     noLanguageDeductionInput ["(& A B)"] ["(& B A)"],
--     noLanguageDeductionInput ["(-> (| A B) (⊥))", "A"] ["(⊥)"]
--   ]

-- minimalND :: System
-- minimalND =
--   System
--     "Min"
--     [ [Min.conjunction],
--       [Min.disjunction],
--       [Min.implication, Min.bottomOp]
--     ]
--     False
--     [ Min.hypothesisDiscard,
--       Min.andIntroBND,
--       Min.andElimLeftBND,
--       Min.andElimRightBND,
--       Min.implicationIntroBND,
--       Min.implicationElimBND,
--       Min.orIntroLeftBND,
--       Min.orIntroRightBND,
--       Min.orElimBND
--     ]
--     minimalExamples
--     [("Min", Min.tactic)]
--     (BackNDSys hypothesisDiscard)

-- minimalFitch :: System
-- minimalFitch =
--   System
--     "MinFw"
--     [ [Min.conjunction],
--       [Min.disjunction],
--       [Min.implication, Min.bottomOp]
--     ]
--     False
--     [ Min.assumptionFw,
--       Min.assumptionSameLevelFw,
--       Min.qedFw,
--       Min.implicationIntroFw,
--       Min.implicationElimFw,
--       Min.andIntroFw,
--       Min.andElimLeftFw,
--       Min.andElimRightFw,
--       Min.orIntroLeftFw,
--       Min.orIntroRightFw,
--       Min.orElimFw
--     ]
--     minimalExamples
--     []
--     (FitchSys Min.assumptionSameLevelFw)

-- intuitionisticND :: System
-- intuitionisticND =
--   System
--     "Int"
--     [ [Min.conjunction],
--       [Min.disjunction],
--       [Min.implication, Min.bottomOp]
--     ] False
--     [ Intui.hypothesisDiscard,
--       Intui.andIntroBND,
--       Intui.andElimLeftBND,
--       Intui.andElimRightBND,
--       Intui.implicationIntroBND,
--       Intui.implicationElimBND,
--       Intui.orIntroLeftBND,
--       Intui.orIntroRightBND,
--       Intui.orElimBND,
--       Intui.exFalsoQuodLibetBND
--     ]
--     [noLanguageDeductionInput ["(& A (-> A (⊥)))"] ["B"]]
--     [("Intui", Intui.tactic)]
--     (BackNDSys hypothesisDiscard)

-- intuitionisticFitch :: System
-- intuitionisticFitch =
--   System
--     "IntFw"
--     [ [Min.conjunction],
--       [Min.disjunction],
--       [Min.implication, Min.bottomOp]
--     ] False
--     [ Intui.assumptionFw,
--       Intui.assumptionSameLevelFw,
--       Intui.qedFw,
--       Intui.implicationIntroFw,
--       Intui.implicationElimFw,
--       Intui.andIntroFw,
--       Intui.andElimLeftFw,
--       Intui.andElimRightFw,
--       Intui.orIntroLeftFw,
--       Intui.orIntroRightFw,
--       Intui.orElimFw,
--       Intui.exFalsoQuodLibetFw
--     ]
--     [noLanguageDeductionInput [] ["(-> (& A (-> A (⊥))) B)"]]
--     []
--     (FitchSys Intui.assumptionSameLevelFw)

-- classicalND :: System
-- classicalND =
--   System
--     "Cla"
--     [ [Min.conjunction],
--       [Min.disjunction],
--       [Min.implication, Min.bottomOp]
--     ] False
--     [ Cla.hypothesisDiscard,
--       Cla.andIntroBND,
--       Cla.andElimLeftBND,
--       Cla.andElimRightBND,
--       Cla.implicationIntroBND,
--       Cla.implicationElimBND,
--       Cla.orIntroLeftBND,
--       Cla.orIntroRightBND,
--       Cla.orElimBND,
--       Cla.exFalsoQuodLibetBND,
--       Cla.reductioAdAbsurdumBND
--     ]
--     [noLanguageDeductionInput ["(-> (-> A (⊥)) (⊥))"] ["A"], noLanguageDeductionInput ["(-> (& p q) (_|_ ))"] ["(| (-> p (_|_ )) (-> q (_|_ )))"]]
--     [("Cla", Cla.tactic)]
--     (BackNDSys hypothesisDiscard)

-- classicalFitch :: System
-- classicalFitch =
--   System
--     "ClaFw"
--     [ [Min.conjunction],
--       [Min.disjunction],
--       [Min.implication, Min.bottomOp]
--     ] False
--     [ Cla.assumptionFw,
--       Cla.assumptionSameLevelFw,
--       Cla.qedFw,
--       Cla.implicationIntroFw,
--       Cla.implicationElimFw,
--       Cla.andIntroFw,
--       Cla.andElimLeftFw,
--       Cla.andElimRightFw,
--       Cla.orIntroLeftFw,
--       Cla.orIntroRightFw,
--       Cla.orElimFw,
--       Cla.exFalsoQuodLibetFw,
--       Cla.reductioAdAbsurdumFw
--     ]
--     [ noLanguageDeductionInput ["(-> (-> A (⊥)) (⊥))"] ["A"],
--       noLanguageDeductionInput ["(-> (& p q) (⊥))"] ["(| (-> p (⊥)) (-> q (⊥)))"]
--     ]
--     []
--     (FitchSys Cla.assumptionSameLevelFw)

-- modalKFitch :: System
-- modalKFitch =
--   System
--     "Modal K"
--     [ [Mod.necessity, Mod.possibility],
--         [Min.conjunction],
--         [Min.disjunction],
--         [MinImp.implication, Min.bottomOp]
--       ] False
--     [ Mod.kFw,
--       Mod.dualLeftFw,
--       Mod.dualRightFw,
--       Mod.implicationElimFw,
--       Mod.necessitationFw,
--       Mod.tautologyFw,
--       Mod.qedFw
--     ]
--     [ noLanguageDeductionInput ["(◻ A)"] ["(◻ (-> B A))"],
--       noLanguageDeductionInput ["(-> (◻ p) (_|_))"] ["(◇ (-> p (_|_)))"]
--     ]
--     []
--     (FitchSys Mod.tautologyFw)

-- modalTFitch :: System
-- modalTFitch =
--   System
--     "Modal T"
--     [ [Mod.necessity, Mod.possibility],
--         [Min.conjunction],
--         [Min.disjunction],
--         [MinImp.implication, Min.bottomOp]
--       ] False
--     [ Mod.kFw,
--       Mod.tFw,
--       Mod.dualLeftFw,
--       Mod.dualRightFw,
--       Mod.implicationElimFw,
--       Mod.necessitationFw,
--       Mod.tautologyFw,
--       Mod.qedFw
--     ]
--     [ noLanguageDeductionInput [] ["(-> (◻ (-> B A)) (◻ A))"],
--       noLanguageDeductionInput [] ["(-> (◇ (-> p (_|_))) (-> (◻ p) (_|_)))"]
--     ]
--     []
--     (FitchSys Mod.tautologyFw)

-- modalS4Fitch :: System
-- modalS4Fitch =
--   System
--     "Modal S4"
--     [ [Mod.necessity, Mod.possibility],
--         [Min.conjunction],
--         [Min.disjunction],
--         [MinImp.implication, Min.bottomOp]
--       ] False
--     [ Mod.kFw,
--       Mod.tFw,
--       Mod.fourFw,
--       Mod.dualLeftFw,
--       Mod.dualRightFw,
--       Mod.implicationElimFw,
--       Mod.necessitationFw,
--       Mod.tautologyFw,
--       Mod.qedFw
--     ]
--     [ noLanguageDeductionInput [] ["(-> (◻ (-> B A)) (◻ A))"],
--       noLanguageDeductionInput [] ["(-> (◇ (-> p (_|_))) (-> (◻ p) (_|_)))"]
--     ]
--     []
--     (FitchSys Mod.tautologyFw)

-- modalS5Fitch :: System
-- modalS5Fitch =
--   System
--     "Modal S5"
--     [ [Mod.necessity, Mod.possibility],
--         [Min.conjunction],
--         [Min.disjunction],
--         [MinImp.implication, Min.bottomOp]
--       ] False
--     [ Mod.kFw,
--       Mod.tFw,
--       Mod.fiveFw,
--       Mod.dualLeftFw,
--       Mod.dualRightFw,
--       Mod.implicationElimFw,
--       Mod.necessitationFw,
--       Mod.tautologyFw,
--       Mod.qedFw
--     ]
--     [ noLanguageDeductionInput [] ["(-> (◻ (-> B A)) (◻ A))"],
--       noLanguageDeductionInput [] ["(-> (◇ (-> p (_|_))) (-> (◻ p) (_|_)))"]
--     ]
--     []
--     (FitchSys Mod.tautologyFw)

-- modalDFitch :: System
-- modalDFitch =
--   System
--     "Modal D"
--     [ [Mod.necessity, Mod.possibility],
--         [Min.conjunction],
--         [Min.disjunction],
--         [MinImp.implication, Min.bottomOp]
--       ] False
--     [ Mod.kFw,
--       Mod.dFw,
--       Mod.dualLeftFw,
--       Mod.dualRightFw,
--       Mod.implicationElimFw,
--       Mod.necessitationFw,
--       Mod.tautologyFw,
--       Mod.qedFw
--     ]
--     [ noLanguageDeductionInput [] ["(-> (◻ (-> B A)) (◻ A))"],
--       noLanguageDeductionInput [] ["(-> (◇ (-> p (_|_))) (-> (◻ p) (_|_)))"]
--     ]
--     []
--     (FitchSys Mod.tautologyFw)

-- modalGFitch :: System
-- modalGFitch =
--   System
--     "Modal G"
--     [ [Mod.necessity, Mod.possibility],
--         [Min.conjunction],
--         [Min.disjunction],
--         [MinImp.implication, Min.bottomOp]
--       ] False
--     [ Mod.kFw,
--       Mod.wFw,
--       Mod.dualLeftFw,
--       Mod.dualRightFw,
--       Mod.implicationElimFw,
--       Mod.necessitationFw,
--       Mod.tautologyFw,
--       Mod.qedFw
--     ]
--     [ noLanguageDeductionInput [] ["(-> (◻ (-> B A)) (◻ A))"],
--       noLanguageDeductionInput [] ["(-> (◇ (-> p (_|_))) (-> (◻ p) (_|_)))"]
--     ]
--     []
--     (FitchSys Mod.tautologyFw)

-- modalK43Fitch :: System
-- modalK43Fitch =
--   System
--     "Modal K4.3"
--     [ [Mod.necessity, Mod.possibility],
--         [Min.conjunction],
--         [Min.disjunction],
--         [MinImp.implication, Min.bottomOp]
--       ] False
--     [ Mod.kFw,
--       Mod.fourFw,
--       Mod.lFw,
--       Mod.dualLeftFw,
--       Mod.dualRightFw,
--       Mod.implicationElimFw,
--       Mod.necessitationFw,
--       Mod.tautologyFw,
--       Mod.qedFw
--     ]
--     [ noLanguageDeductionInput [] ["(-> (◻ (-> B A)) (◻ A))"],
--       noLanguageDeductionInput [] ["(-> (◇ (-> p (_|_))) (-> (◻ p) (_|_)))"]
--     ]
--     []
--     (FitchSys Mod.tautologyFw)



fol :: System
fol =
  System
    "FOL"
    [ FOL.conjunction
    , FOL.universal, FOL.implication, FOL.bottomOp
    ]
    True
    FOL.assumption
    [ FOL.assumption,
      FOL.assumptionNewLevel,
      FOL.implicationIntro,
      FOL.implicationElim,
      FOL.universalIntroduction,
      FOL.universalElimination,
      -- FOL.existentialIntroductionBw,
      -- FOL.existentialEliminationBw,
      FOL.andIntro,
      FOL.andElimLeft,
      FOL.andElimRight
      -- FOL.orIntroLeftBND,
      -- FOL.orIntroRightBND,
      -- FOL.orElimBND,
      -- FOL.exFalsoQuodLibetBND,
      -- FOL.reductioAdAbsurdumBND,
      -- FOL.negationIntroductionBND,
      -- FOL.negationEliminationBND,
      -- FOL.hypothesisDiscard
    ]
    []
    FitchSys
    "First-order logic fragment (only implication, conjunction, and universal quantifier)"


modalK :: System
modalK =
  System
    "K"
    [ Modal.necessity, Modal.possibility,
      Modal.conjunction, Modal.disjunction,
      Modal.implication, Modal.bottomOp
    ]
    False
    Modal.tautology
    [ Modal.k,
      Modal.dualLeft,
      Modal.dualRight,
      Modal.implicationElim,
      Modal.necessitation,
      Modal.tautology
    ]
    []
    FitchSys -- actually axiomatic
    "Propositional Modal K logic system."

lfol :: System
lfol =
  System
    "LFOL"
    [ LFOL.conjunction
    , LFOL.universal, LFOL.implication, LFOL.bottomOp
    , LFOL.consOp, LFOL.emptyOp
    , LFOL.label
    ]
    True
    LFOL.assumption
    [ LFOL.assumption,
      LFOL.assumptionNewLevel,
      LFOL.implicationIntro,
      LFOL.implicationElim,
      LFOL.universalIntroduction,
      LFOL.universalElimination,
      LFOL.conjunctionIntro,
      LFOL.conjunctionElimLeft,
      LFOL.conjunctionElimRight,
      LFOL.reductioAdAbsurdum
      -- FOL.orIntroLeftBND,
      -- FOL.orIntroRightBND,
      -- FOL.orElimBND,
      -- FOL.exFalsoQuodLibetBND,
      -- FOL.reductioAdAbsurdumBND,
      -- FOL.negationIntroductionBND,
      -- FOL.negationEliminationBND,
      -- FOL.hypothesisDiscard
    ]
    [("freevars", LFOL.freevarsFunc),
     ("append", LFOL.appendFunc),
     ("subtract", LFOL.subtractFunc)]
    FitchSys
    "Labelled first-order logic system by Rentería (§2, 2004)"

ultrafilter :: System
ultrafilter =
  System
    "Ultrafilter"
    [ Ultra.markOp, Ultra.conjunction
    , Ultra.universal, Ultra.quasiuniversal
    , Ultra.implication, Ultra.bottomOp
    , Ultra.consOp, Ultra.emptyOp
    , Ultra.label
    ]
    True
    Ultra.assumption
    [ Ultra.assumption,
      Ultra.assumptionNewLevel,
      Ultra.implicationIntro,
      Ultra.implicationElim,
      Ultra.universalIntroduction,
      Ultra.universalIntroductionNoLabelChange,
      Ultra.universalEliminationNoChange,
      Ultra.universalElimination,
      Ultra.universalEliminationUnmarkedLabel,
      Ultra.universalEliminationMarkedLabel,
      Ultra.quasiuniversalIntroduction,
      Ultra.quasiuniversalIntroductionNoLabelChange,
      Ultra.quasiuniversalElimination,
      Ultra.quasiuniversalEliminationNoLabelChange,
      Ultra.conjunctionIntro,
      Ultra.conjunctionElimLeft,
      Ultra.conjunctionElimRight,
      Ultra.reductioAdAbsurdum,
      Ultra.xRule
    ]
    [ ("validLabel", Ultra.validLabelFunc)
    , ("merge", Ultra.mergeFunc)
    , ("keepFreeIn", Ultra.keepFreeInFunc)
    , ("mayMoveUnmarkedToTheLeft", Ultra.mayMoveUnmarkedToTheLeftFunc)
    ]
    FitchSys
    "Labelled ultra-filter logic system by Rentería (§3, 2004)"

filter :: System
filter =
  System
    "Filter"
    [ Filter.markOp, Filter.conjunction
    , Filter.universal, Filter.quasiuniversal
    , Filter.implication, Filter.bottomOp
    , Filter.consOp, Filter.emptyOp
    , Filter.label
    ]
    True
    Filter.assumption
    [ Filter.assumption,
      Filter.assumptionNewLevel,
      Filter.implicationIntro,
      Filter.implicationElim,
      Filter.universalIntroduction,
      Filter.universalIntroductionNoLabelChange,
      Filter.universalEliminationNoChange,
      Filter.universalElimination,
      Filter.universalEliminationUnmarkedLabel,
      Filter.universalEliminationMarkedLabel,
      Filter.quasiuniversalIntroduction,
      Filter.quasiuniversalIntroductionNoLabelChange,
      Filter.quasiuniversalElimination,
      Filter.quasiuniversalEliminationNoLabelChange,
      Filter.conjunctionIntro,
      Filter.conjunctionElimLeft,
      Filter.conjunctionElimRight,
      Filter.reductioAdAbsurdum,
      Filter.xRule
    ]
    [ ("validLabel", Filter.validLabelFunc)
    , ("merge", Filter.mergeFunc)
    , ("keepFreeIn", Filter.keepFreeInFunc)
    , ("mayMoveUnmarkedToTheLeft", Filter.mayMoveUnmarkedToTheLeftFunc)
    , ("onlyUnmarked", Filter.onlyUnmarkedFunc)
    ]
    FitchSys
    "Labelled filter logic system by Rentería (§4, 2004)"

keisler :: System
keisler =
  System
    "Keisler"
    [
      Keisler.bottomOp,
      Keisler.implication,
      Keisler.disjunction,
      Keisler.existential,
      Keisler.uncountablyManyOp,
      Keisler.equality,
      Keisler.label,
      Keisler.consOp,
      Keisler.emptyOp,
      Keisler.markOp
    ]
    True
    Keisler.assumption
    [ Keisler.assumption,
      Keisler.assumptionNewLevel,
      Keisler.ax,
      Keisler.existentialIntroduction,
      Keisler.existentialElimination,
      Keisler.uncountablyManyIntroduction,
      Keisler.uncountablyManyElimination,
      Keisler.implicationIntroduction,
      Keisler.implicationElimination,
      Keisler.disjunctionIntroduction,
      Keisler.disjunctionElimination,
      Keisler.reductioAdAbsurdum,
      Keisler.aleph,
      Keisler.star
    ]
    [ ("onlyUnmarked", Keisler.onlyUnmarkedFunc)
    , ("subsetOf", Keisler.subsetOfFunc)
    ]
    FitchSys
    "Labelled Keisler logic system by Rentería (§6, 2004)"

ctl :: System
ctl =
  System
    "CTL"
    [
      CTL.existsGlobalOp,
      CTL.forallNextOp,
      CTL.untilOp,
      CTL.implication,
      CTL.bottomOp,
      CTL.label,
      CTL.consOp,
      CTL.initialOp
    ]
    False
    CTL.assumption
    [ CTL.assumption,
      CTL.assumptionNewLevel,
      CTL.bottomElimination,
      CTL.implicationIntroduction,
      CTL.implicationElimination,
      CTL.forallNextIntroduction,
      CTL.forallNextElimination,
      CTL.existsGlobalElimination,
      CTL.existsGlobalGrow,
      CTL.existsGlobalPlus,
      CTL.untilIntroduction,
      CTL.untilGrow,
      CTL.untilElimination,
      CTL.untilPlus,
      CTL.forNoneNextNot,
      CTL.existsGlobalEqual
    ]
    [("validLabel", CTL.validLabelFunc)]
    FitchSys
    "Labelled CTL logic system by Rentería (§5, 2004)"

ctlstar :: System
ctlstar =
  System
    "CTL*"
    [ CTLstar.someOp,
      CTLstar.global,
      CTLstar.nextOp,
      CTLstar.consOp,
      CTLstar.label,
      CTLstar.stateOp,
      CTLstar.finiteOp,
      CTLstar.infiniteOp,
      CTLstar.bottomOp
    ]
    False
    CTLstar.assumption
    [ CTLstar.assumption,
      CTLstar.assumptionNewLevel,
      CTLstar.nextIntroduction,
      CTLstar.nextElimination,
      CTLstar.someIntroduction,
      CTLstar.someElimination,
      CTLstar.untilIntroduction,
      CTLstar.untilGrow,
      CTLstar.untilElimination1,
      CTLstar.untilElimination2,
      CTLstar.indD,
      CTLstar.indR,
      CTLstar.globalIntroduction,
      CTLstar.globalElimination,
      CTLstar.p,
      CTLstar.bot,
      CTLstar.star
    ]
    [("validLabel", CTLstar.validLabelFunc)]
    FitchSys
    "Labelled CTL* logic system by Rentería (§7, 2004)"

ialc :: System
ialc =
  System
    "iALC"
    [ IALC.implicationOp, IALC.intersectionOp, IALC.unionOp
    , IALC.isAop, IALC.universal, IALC.existential, IALC.consOp
    , IALC.nilOp, IALC.label
    ]
    True
    IALC.assumption
    [ IALC.assumption,
      IALC.assumptionNewLevel,
      IALC.universalIntroduction,
      IALC.universalElimination,
      IALC.existentialIntroduction,
      IALC.existentialElimination,
      IALC.implicationIntroduction,
      IALC.implicationElimination,
      IALC.intersectionIntroduction,
      IALC.intersectionEliminationLeft,
      IALC.intersectionEliminationRight,
      IALC.unionIntroductionLeft,
      IALC.unionIntroductionRight,
      IALC.unionElimination,
      IALC.exFalsoQuodLibet,
      IALC.gen,
      IALC.chng,
      IALC.dist,
      IALC.join
    ]
    [ ("relatesTo", IALC.roleFunc),
      ("snoc", IALC.snocFunc),
      ("universalsOnly", IALC.universalsOnlyFunc),
      ("existentialsOnly", IALC.existentialsOnlyFunc)
    ]
    FitchSys
    "Labelled logic system for iALC by Alkmim (§3, 2023)"

zp :: System
zp =
  System
    "Zp"
    [ Zp.nonColinearPred, Zp.isClosedPred, Zp.isJordanPred
    , Zp.pointType, Zp.segmentType, Zp.faceType, Zp.volumeType
    , Zp.segmentCons, Zp.unionOp, Zp.intersectionOp, Zp.compatibility
    , Zp.inequality, Zp.leOp, Zp.leqOp, Zp.emptyOp
    , Zp.label
    ]
    False
    Zp.assumption
    [ Zp.s1
    , Zp.s2
    , Zp.f1
    , Zp.f2
    , Zp.v1
    , Zp.v2
    , Zp.emptyRule
    , Zp.e0
    , Zp.e1
    , Zp.e2
    , Zp.leqRule
    , Zp.le1
    , Zp.le2
    , Zp.cmp0
    , Zp.cmp1
    , Zp.cmp2
    , Zp.cmp3
    , Zp.assumption
    ]
    [ ]
    FitchSys
    "Zₚ system by Giovaninni et al. (§5, 2021)"

defaultSystems :: Systems
defaultSystems =
  Systems . Map.fromList $
    (\s -> (systemName s, s))
      <$> [ lfol,
            ultrafilter,
            filter,
            keisler,
            ctl,
            ctlstar,
            ialc,
            zp,
            modalK,
            fol
-- minimalImplicationalND,
--             minimalImplicationalFitch,
--             minimalND,
--             minimalFitch,
--             intuitionisticND,
--             intuitionisticFitch,
--             classicalND,
--             classicalFitch,
--             modalKFitch,
--             modalTFitch,
--             modalS4Fitch,
--             modalS5Fitch,
--             modalDFitch,
--             modalGFitch,
--             modalK43Fitch,
-- --            ialcBND,
--             claTableaux,
--             signedClaTableaux,
--             folBND,
--             folFitch
          ]
