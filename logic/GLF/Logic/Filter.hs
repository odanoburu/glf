-- |
-- Module      : GLF.Logic.Filter
-- Description : Labelled filter logic system
-- Copyright   : (c) bruno cuconato, 2023-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Filter
  ( -- * Operators and formula specifiers
    universal,
    forall,
    quasiuniversal,
    almostall,
    conjunction,
    and,
    implication,
    implies,
    bottom,
    bottomOp,
    negation,
    markOp,
    mark,
    label,
    consOp,
    emptyOp,

    -- * Rules
    assumption,
    assumptionNewLevel,
    universalIntroduction,
    universalIntroductionNoLabelChange,
    universalEliminationNoChange,
    universalElimination,
    universalEliminationUnmarkedLabel,
    universalEliminationMarkedLabel,
    quasiuniversalIntroduction,
    quasiuniversalIntroductionNoLabelChange,
    quasiuniversalElimination,
    quasiuniversalEliminationNoLabelChange,
    implicationIntro,
    implicationElim,
    conjunctionIntro,
    conjunctionElimRight,
    conjunctionElimLeft,
    reductioAdAbsurdum,
    xRule,

    -- * Functions
    validLabelFunc,
    mergeFunc,
    keepFreeInFunc,
    mayMoveUnmarkedToTheLeftFunc,
    onlyUnmarkedFunc,
    onlyUnmarked,

  )
where

import Prelude hiding (and, or)

import GLF.Rule
import GLF.Logic.Base
import GLF.Logic.Ultrafilter hiding (implicationIntro, reductioAdAbsurdum)


-- | The implication introduction rule.
implicationIntro :: DeductiveRule
implicationIntro =
  DeductiveRule
    { name = introductionRuleName implication,
      premises =
        [ Premise
            { hypotheses = [("antecedentHypothesis", "antecedent" .: "l")],
              root = "consequent" .: "m",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = "antecedent" `implies` "consequent" .: onlyUnmarked (merge "consequent" "m" "antecedent" "l"),
      action = EndsSubProof,
      helpText =
        "Apply implication introduction rule."
    }

reductioAdAbsurdum :: DeductiveRule
reductioAdAbsurdum =
  DeductiveRule
    { name = PolyName {unicode = "⊥↯", ascii = "reductioAdAbsurdum", latex = "mathrm{reductioAdAbsurdum}"},
      premises =
        [ Premise
            { hypotheses = [("negativeHypothesis", "conclusion" `implies` bottom .: "l")],
              root = "bottom" <> bottom .: "m",
              role = "absurd"
            }
        ],
      provisos = [],
      conclusion = "conclusion" .: onlyUnmarked "l",
      action = EndsSubProof,
      helpText =
        "Apply reductio ad absurdum rule"
    }

onlyUnmarked :: FormulaSpec -> FormulaSpec
onlyUnmarked l = eval "onlyUnmarked" [l]

onlyUnmarkedFunc :: FormulaFunction
-- l contains only unmarked variables
--- NOTE: also used in Keisler.hs
onlyUnmarkedFunc = (Deterministic fwd, chk)
  where
    unmarkedP (_, marked) = not marked
    fwd =
      \case
        [l] -> [listToLabel . filter unmarkedP $ labelToList l]
        _wrongnumberargs -> []
    chk args res =
      case args of
        [l] -> [ res | l == res && all unmarkedP (labelToList res) ]
        _wrongnumberargs -> []
