 -- |
-- Module      : GLF.Logic.IALC
-- Description : iALC system
-- Copyright   : (c) bruno cuconato, 2020-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.IALC
  ( -- * Rules
  assumption,
  assumptionNewLevel,
  universalIntroduction,
  universalElimination,
  existentialIntroduction,
  existentialElimination,
  implicationIntroduction,
  implicationElimination,
  intersectionIntroduction,
  intersectionEliminationLeft,
  intersectionEliminationRight,
  unionIntroductionLeft,
  unionIntroductionRight,
  unionElimination,
  exFalsoQuodLibet,
  gen,
  chng,
  dist,
  join,

  -- * Operators and predicates
  label,
  implicationOp,
  intersectionOp,
  unionOp,
  isAop,
  universal,
  existential,
  consOp,
  nilOp,

  -- * Formula functions
  roleFunc,
  snocFunc,
  universalsOnlyFunc,
  existentialsOnlyFunc,
  )
  where

import Prelude hiding ((/=))
import GLF.Logic.Base
import GLF.Rule
import GLF.Logic.Min (bottom)
import GLF.Logic.FOL (assumption, assumptionNewLevel)

implicationOp :: Operator
implicationOp = rightAssociativeOp "⟞" "-]" "mathbin{\text{---}\raise0.3ex\\hbox{\\scalebox{0.5}{]}}}" 750

infixr 2 `implies`
implies :: FormulaSpec -> FormulaSpec -> FormulaSpec
-- ^ Specify an implication formula.
implies f g = op implicationOp <> opn 1 f <> opn 2 g

intersectionOp :: Operator
intersectionOp = binaryOp "⊓" "|-|" "sqcap" 800
intersects :: FormulaSpec -> FormulaSpec -> FormulaSpec
intersects f g = op intersectionOp <> opn 1 f <> opn 2 g

unionOp :: Operator
unionOp = binaryOp "⊔" "|_|" "sqcup" 800
union :: FormulaSpec -> FormulaSpec -> FormulaSpec
union f g = op unionOp <> opn 1 f <> opn 2 g

-- negationOp = prefixOp "¬" "~" "neg"
-- precedence = naryOp "≼" "<=" "preceq" 2
isAop :: Operator
isAop = binaryOp "^" "^" "^" 450
-- contextOp = naryOp "^" "^" "string^" 2
universal, existential :: Operator
universal = naryOp "∀" "\\/" "forall" 2
existential = naryOp "∃" "3" "exists" 2
-- forallCtxOp = naryOp "∀^" "\\/^" "forall" 2
-- existsCtxOp = naryOp "∃^" "3^" "exists" 2
-- ctxList = variadicOp "∷" "::" "colon\\colon" 0

consOp :: Operator
consOp = rightAssociativeOp ";" ";" "mathbin{;}" 550

nilOp :: Operator
nilOp = nullaryOp "_" "_" "_"

infix 1 `isA`
isA :: Text -> FormulaSpec -> FormulaSpec
x `isA` restrictions = op isAop <> opn 1 (variable x) <> opn 2 restrictions

forall :: Text -> FormulaSpec -> FormulaSpec
-- ^ Specify an universal role.
forall varname f = op universal <> opn 1 (variable varname) <> opn 2 f

exists :: Text -> FormulaSpec -> FormulaSpec
-- ^ Specify an existential formula.
exists varname f = op existential <> opn 1 (variable varname) <> opn 2 f

infixr 9 .-
(.-) :: FormulaSpec -> FormulaSpec -> FormulaSpec
thelist .- theelem = op consOp <> opn 1 thelist <> opn 2 theelem

universalIntroduction :: DeductiveRule
universalIntroduction =
  rule (introductionRuleName universal)
    [ premise "premise" ("y" `isA` ("α" .: forall "R" "x" .- "l"))
      `discharges` ("roleHypothesis", "x" `roleAssert` "y")
    ---
    , concludes $ "x" `isA` (forall "R" "α" .: "l")
    , provided ("y" `freeIn` "x") -- x ≠ y
    , closesSubProof
    ]

universalElimination :: DeductiveRule
universalElimination =
  rule (eliminationRuleName universal)
    [ premise "premise" ("x" `isA` (forall "R" "α" .: "l"))
    , premise "roleAssertion" ("x" `roleAssert` "y")
    ---
    , concludes $ "y" `isA` ("α" .: forall "R" "x" .- "l")
    ]

existentialIntroduction :: DeductiveRule
existentialIntroduction =
  rule (introductionRuleName existential)
    [ premise "roleAssertion" ("x" `roleAssert` "y")
    , premise "premise" ("y" `isA` ("α" .: exists "R" "x" .- "l"))
    ---
    , concludes ("x" `isA` (exists "R" "α" .: "l"))
    ]

existentialElimination :: DeductiveRule
existentialElimination =
    rule (introductionRuleName existential)
      [ premise "major" ("x" `isA` (exists "R" "α" .: "l"))
      , premise "discharging" ("z" `isA` ("β" .: "m"))
        `discharges` ("roleAssertion", "x" `roleAssert` "y")
        `discharges` ("existential", "y" `isA` ("α" .: exists "R" "x" .- "l"))
      ---
      , provided ("y" `freeIn` "x") -- x ≠ y
      , provided ("x" `freeIn` "z") -- x ≠ z
      , provided ("y" `freeIn` "z") -- z ≠ y
      ---
      , concludes ("z" `isA` ("β" .: "m"))
      , closesSubProof
      ]

implicationIntroduction :: DeductiveRule
implicationIntroduction =
  rule (introductionRuleName implicationOp)
    [ premise "predicate" ("x" `isA` ("β" .: "m"))
      `discharges` ("hypothesis", "x" `isA` ("α" .: "l"))
    , concludes ("x" `isA` (("α" .: "l") `implies` ("β" .: "m")))
    , closesSubProof
    ]

implicationElimination :: DeductiveRule
implicationElimination =
  rule (eliminationRuleName implicationOp)
    [ premise "major" ("x" `isA` (("α" .: "l") `implies` ("β" .: "m")))
    , premise "minor" ("x" `isA` ("α" .: "l"))
    , concludes ("x" `isA` ("β" .: "m"))
    ]

intersectionIntroduction :: DeductiveRule
intersectionIntroduction =
  rule (introductionRuleName intersectionOp)
    [ premise "left" ("x" `isA` ("α" .: "l"))
    , premise "right" ("x" `isA` ("β" .: "l"))
    , concludes ("x" `isA` ("α" `intersects` "β" .: universalsOnly "l"))
    ]

intersectionEliminationLeft, intersectionEliminationRight :: DeductiveRule
intersectionEliminationLeft =
  rule (operatorRuleName intersectionOp (Elimination LeftS))
    [ premise "predicate" ("x" `isA` ("α" `intersects` "β" .: "l"))
    , concludes ("x" `isA` ("α" .: universalsOnly "l"))
    ]

intersectionEliminationRight =
  rule (operatorRuleName intersectionOp (Elimination RightS))
    [ premise "predicate" ("x" `isA` ("α" `intersects` "β" .: "l"))
    , concludes ("x" `isA` ("β" .: universalsOnly "l"))
    ]

unionIntroductionLeft, unionIntroductionRight :: DeductiveRule
unionIntroductionLeft =
  rule (operatorRuleName unionOp (Introduction LeftS))
    [ premise "predicate" ("x" `isA` ("α" .: "l"))
    , concludes ("x" `isA` (("α" `union` "β") .: existentialsOnly "l"))
    ]
unionIntroductionRight =
  rule (operatorRuleName unionOp (Introduction RightS))
    [ premise "predicate" ("x" `isA` ("β" .: "l"))
    , concludes ("x" `isA` (("α" `union` "β") .: existentialsOnly "l"))
    ]

unionElimination :: DeductiveRule
unionElimination =
  rule (eliminationRuleName unionOp)
    [ premise "union" ("x" `isA` (("α" `union` "β") .: existentialsOnly "l"))
    , premise "left" ("z" `isA` ("δ" .: "m"))
      `discharges` ("leftHypothesis", "x" `isA` ("α" .: "l"))
    , premise "right" ("z" `isA` ("δ" .: "m"))
      `discharges` ("rightHypothesis", "x" `isA` ("β" .: "l"))
    , concludes ("z" `isA` ("δ" .: "m"))
    , closesSubProof
    ]

exFalsoQuodLibet :: DeductiveRule
exFalsoQuodLibet =
  rule (asciiName "efq")
    [ premise "absurd" ("x" `isA` (bottom .: existentialsOnly "l"))
    , concludes ("z" `isA` ("δ" .: "m"))
    ]

gen :: DeductiveRule
gen =
  rule (asciiName "Gen")
    [ premise "predicate" ("y" `isA` ("α" .: "l"))
      `discharges` ("role", "x" `relatesTo` "y")
    , concludes ("y" `isA` ("α" .: snoc "l" (forall "R" "x")))
    , provided ("x" `freeIn` "y") -- x ≠ y
    ]

chng :: DeductiveRule
chng =
  rule (asciiName "chng")
    [ premise "predicate" ("y" `isA` (("α" .: snoc "l" (forall "R" "x")) `implies` ("β" .: snoc "m" (forall "R" "x"))))
    , premise "role" ("x" `relatesTo` "y")
    , concludes ("y" `isA` (("α" .: snoc "l" (exists "R" "x")) `implies` ("β" .: snoc "m" (exists "R" "x"))))
    , provided ("x" `freeIn` "y") -- x ≠ y
    ]

dist :: DeductiveRule
dist =
  rule (asciiName "dist")
    [ premise "predicate" ("y" `isA` ("α" `implies` "β" .: snoc (universalsOnly "l") (forall "R" "x")))
    , concludes ("y" `isA` (("α" .: snoc "l" (forall "R" "x")) `implies` ("β" .: snoc "l" (forall "R" "x"))))
    , provided ("x" `freeIn` "y") -- x ≠ y
    ]

join :: DeductiveRule
join =
  rule (asciiName "join")
    [ premise "predicate" ("y" `isA` (("α" .: snoc "l" (exists "R" "x")) `implies` ("β" .: snoc "l" (forall "R" "x"))))
    , concludes ("y" `isA` ("α" `implies` "β" .: snoc (universalsOnly "l") (forall "R" "x")))
    , provided ("x" `freeIn` "y") -- x ≠ y
    ]

roleAssert :: Text -> Text -> FormulaSpec
roleAssert x y = opn 1 (variable x) <> opn 2 (variable y)

snoc :: FormulaSpec -> FormulaSpec -> FormulaSpec
snoc listSpec elemSpec = ToEval "snoc" [listSpec, elemSpec]

snocFunc :: FormulaFunction
snocFunc = (Deterministic fwd, chk)
  where
    fwd =
      \case
        [lst, elm] ->
          case go lst of
            Atom _ -> []
            f -> [f]
          where
            go = \case
              Atom x -> Atom x
              Formula theOp [e, l] | theOp == consOp -> Formula consOp [e, go l]
              Formula theOp [] | theOp == nilOp -> Formula consOp [elm, Formula nilOp []]
              Formula _ _ -> Atom ""
        _wrongargsyntax -> []
    chk [lst, elm] res = [ res | fwd [lst, elm] == [res] ]
    chk _ _ = []

universalsOnly, existentialsOnly :: FormulaSpec -> FormulaSpec
universalsOnly l = ToEval "universalsOnly" [l]
existentialsOnly l = ToEval "existentialsOnly" [l]

universalsOnlyFunc :: FormulaFunction
-- check that label only contains universal roles
universalsOnlyFunc = (Deterministic fwd, chk)
  where
    fwd =
      \case
        [l] ->
          case go l of
            Atom _ -> []
            f -> [f]
          where
            go = \case
              Atom x -> Atom x
              Formula theOp [e, m]
                | theOp == consOp ->
                  case e of
                    Formula elemOp [Atom _, _] | elemOp == universal -> Formula consOp [e, go m]
                    _notProperUniversal -> Atom "" -- fail
              Formula theOp [] | theOp == nilOp -> Formula theOp []
              Formula _ _ -> Atom "" -- fail
        _wrongargsyntax -> []
    chk [l] res = [ res | fwd [l] == [res] ]
    chk _ _ = []

existentialsOnlyFunc :: FormulaFunction
-- check that label only contains existential roles
existentialsOnlyFunc = (Deterministic fwd, chk)
  where
    fwd =
      \case
        [l] ->
          case go l of
            Atom _ -> []
            f -> [f]
          where
            go = \case
              Atom x -> Atom x
              Formula theOp [e, m]
                | theOp == consOp ->
                  case e of
                    Formula elemOp [Atom _, _] | elemOp == existential -> Formula consOp [e, go m]
                    _notProperExistential -> Atom "" -- fail
              Formula theOp [] | theOp == nilOp -> Formula theOp []
              Formula _ _ -> Atom "" -- fail
        _wrongargsyntax -> []
    chk [l] res = [ res | fwd [l] == [res] ]
    chk _ _ = []

relatesTo :: Text -> Text -> FormulaSpec
relatesTo x y = ToEval "relatesTo" [variable x, variable y]

roleFunc :: FormulaFunction
-- FIXME: doesn't really check if the role is the correct one
roleFunc = (NonDeterministic fwd, chk)
  where
    fwd =
      \case
        [Atom l, Atom r] -> [Formula defaultRoleOp [Atom l, Atom r]]
        _wrongargsyntax -> []
      where
        defaultRoleOp :: Operator
        defaultRoleOp = naryOp ".R" ".R" ".R" 2
    chk [Atom x, Atom y] res = [ res | check res ]
      where
        check =
          \case
            Formula theOp [Atom w, Atom z]
              | x == w && y == z  ->
                case theOp of
                  Op{associativity = None (Fixed 2), binds = []} -> True
                  _notProperRoleOp -> False
            _notProperRoleFormula -> False
    chk _ _ = []
