-- |
-- Module      : GLF.Logic.Min
-- Description : Minimal logic definitions
-- Copyright   : (c) bruno cuconato, 2019-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Min
  ( -- * Operators and formula specifiers
    bottom,
    bottomOp,
    negationOf, -- _ → ⊥
    negation, -- ¬
    not,
    implication,
    implies, (==>),
    conjunction,
    and,
    disjunction,
    or,

    -- * Rules

    -- ** Declarative
    implicationIntro,
    implicationElim,
    andIntro,
    andElimLeft,
    andElimRight,
    orIntroLeft,
    orIntroRight,
    orElim,
    negationElimination,
    negationIntroduction,



    -- -- * Tactic
    -- tactic,

    -- * Parser
    parse,

    -- * Helpers
    subFormulas,
  )
where

import GLF.Logic.Base
import GLF.Logic.MinImp
import GLF.Logic.Parser
import GLF.Rule
--import GLF.Tactic
import Prelude hiding (and, or, not)

andIntro :: DeductiveRule
andIntro =
  DeductiveRule
    { name = introductionRuleName conjunction,
      premises =
        [ Premise
            { hypotheses = [],
              root = named "left",
              role = "left"
            },
          Premise
            { hypotheses = [],
              root = named "right",
              role = "right"
            }
        ],
      provisos = [],
      conclusion = "conjunction" <> "left" `and` "right",
      action = NoAction,
      helpText =
        "Apply conjunction introduction rule\n\n\
        \  A    B \n\
        \ -------- \n\
        \  A & B"
    }

andElimLeft :: DeductiveRule
andElimLeft =
  DeductiveRule
    { name = operatorRuleName conjunction (Elimination LeftS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                "conjunction" <> "left" `and` mempty,
              role = "conjunction"
            }
        ],
      provisos = [],
      conclusion = "left",
      action = NoAction,
      helpText =
        "Apply Left conjunction elimination rule\n\n\
        \ A & B \n\
        \ ----- \n\
        \   A "
    }

andElimRight :: DeductiveRule
andElimRight =
  DeductiveRule
    { name = operatorRuleName conjunction (Elimination RightS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                "conjunction" <> mempty `and` "right",
              role = "conjunction"
            }
        ],
      provisos = [],
      conclusion = "right",
      action = NoAction,
      helpText =
        "Apply Right conjunction elimination rule\n\n\
        \ A & B \n\
        \ ----- \n\
        \   B "
    }

orIntroLeft :: DeductiveRule
orIntroLeft =
  DeductiveRule
    { name = operatorRuleName disjunction (Introduction LeftS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                named "left",
              role = "left"
            }
        ],
      provisos = [],
      conclusion = "left" `or` "right",
      action = NoAction,
      helpText =
        "Apply Left disjunction introduction rule\n\n\
        \   A  \n\
        \ ----- \n\
        \ A | B"
    }

orIntroRight :: DeductiveRule
orIntroRight =
  DeductiveRule
    { name = operatorRuleName disjunction (Introduction RightS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                named "right",
              role = "right"
            }
        ],
      provisos = [],
      conclusion = "left" `or` "right",
      action = NoAction,
      helpText =
        "Apply Right disjunction introduction rule\n\n\
        \   B \n\
        \ ----- \n\
        \ A | B"
    }

orElim :: DeductiveRule
orElim =
  DeductiveRule
    { name = operatorRuleName disjunction (Elimination NoS),
      premises =
        [ Premise
            { hypotheses = [],
              root = "disjunction" <> "left" `or` "right",
              role = "disjunction"
            },
          Premise
            { hypotheses = [("leftHypothesis", "left")],
              root = "conclusion",
              role = "resultLeft"
            },
          Premise
            { hypotheses = [("rightHypothesis", "right")],
              root = "conclusion",
              role = "resultRight"
            }
        ],
      provisos = [],
      conclusion = "conclusion",
      action = EndsSubProof,
      helpText =
        "Apply conjunction elimination rule\n\n\
        \        [A]   [B] \n\
        \         |     | \n\
        \A | B    C     C \n\
        \------------------ \n\
        \         C"
    }

negationIntroduction :: DeductiveRule
negationIntroduction =
    DeductiveRule
    { name = PolyName {ascii = "negationIntroduction", unicode = "¬I", latex = "neg{}I"},
      premises =
        [ Premise
            { hypotheses = [("assumption", "assumption")],
              root = bottom,
              role = "absurd"
            }
        ],
      provisos = [],
      conclusion = not "assumption",
      action = EndsSubProof,
      helpText =
        "Apply negation introduction rule."
    }

negationElimination :: DeductiveRule
negationElimination =
    DeductiveRule
    { name = PolyName {ascii = "negationElimination", unicode = "¬E", latex = "neg{}E"},
      premises =
        [ Premise
            { hypotheses = [],
              root = not "negated",
              role = "negation"
            },
          Premise
            { hypotheses = [],
              root = "negated",
              role = "affirmation"
            }
        ],
      provisos = [],
      conclusion = bottom,
      action = NoAction,
      helpText =
        "Apply negation elimination rule."
    }

subFormulas :: Formula -> [Formula]
subFormulas f@(Atom _) = [f]
subFormulas f@(Formula _ fs) = f : concatMap subFormulas fs

conjunction, disjunction :: Operator
conjunction = rightAssociativeOp "∧" "&" "land" 950
disjunction = rightAssociativeOp "∨" "|" "lor" 900

infixr 4 `and`
infixr 3 `or`
and, or :: FormulaSpec -> FormulaSpec -> FormulaSpec
l `and` r = op conjunction <> opn 1 l <> opn 2 r
l `or` r = op disjunction <> opn 1 l <> opn 2 r

bottomOp :: Operator
bottomOp = nullaryOp "⊥" "_|_" "bot"

bottom :: FormulaSpec
bottom = op bottomOp

negationOf :: FormulaSpec -> FormulaSpec
negationOf spec = spec `implies` bottom

negation :: Operator
negation = prefixOp "¬" "~" "neg" 1000

not :: FormulaSpec -> FormulaSpec
not f = op negation <> opn 1 f

parse :: Text -> Fallible Formula
parse = parseLogic atomLabel opTable
  where
    opTable =
      [ conjunction,
        disjunction,
        implication, bottomOp
      ]

-- tactic :: Strategy
-- tactic = noPrelude (many $ oneOf [impIntro, andIntro'])
--   where
--     impIntro = ruleTactic implicationIntroBND
--     andIntro' = ruleTactic andIntroBND
