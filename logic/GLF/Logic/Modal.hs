-- |
-- Module      : GLF.Logic.Modal
-- Description : Modal logic definitions
-- Copyright   : (c) bruno cuconato, 2020-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Modal
  ( -- * Operators and formula specifiers
    necessity,
    necessarily,
    possibility,
    possibly,
    bottomOp,
    implication,
    implies,
    conjunction,
    and,
    disjunction,
    or,

    -- * Rules
    tautology,
    implicationElim,
    necessitation,
    k,
    -- t,
    -- four,
    -- b,
    -- d,
    -- five,
    -- l,
    -- w,
    dualRight,
    dualLeft,

  )
where

import GLF.Logic.Base
import GLF.Logic.Min hiding (parse)
import GLF.Rule
import Prelude hiding (and, or)

tautology :: DeductiveRule
tautology =
  axiom (asciiName "TAUT")
    [ concludes "f"
    , help "Assume tautology to be true. Its validity is not checked."
    ]


necessitation :: DeductiveRule
necessitation =
  rule (operatorRuleName necessity (Introduction NoS))
    [ premise "premise" "p"
    , concludes (necessarily "p")
    ]


k :: DeductiveRule
k =
  axiom (asciiName "K")
    [ concludes (antecedent `implies` consequent) ]
  where
    antecedent = necessarily ("p" `implies` "q")
    consequent = necessarily "p" `implies` necessarily "q"

-- t :: DeductiveRule
-- t =
--   DeductiveRule
--     { name = asciiName "T",
--       premises = [],
--       provisos = [],
--       conclusion = necessarily p `implies` p,
--       action = Axiom,
--       helpText =
--         "Apply T axiom."
--     }
--   where
--     p = named "p"

-- four :: DeductiveRule
-- four =
--   DeductiveRule
--     { name = asciiName "4",
--       premises = [],
--       provisos = [],
--       conclusion =
--         antecedent `implies` consequent,
--       action = Axiom,
--       helpText =
--         "Apply 4 axiom."
--     }
--   where
--     antecedent = necessarily "p"
--     consequent = necessarily antecedent

-- b :: DeductiveRule
-- b =
--   DeductiveRule
--     { name = asciiName "B",
--       premises = [],
--       provisos = [],
--       conclusion =
--         p `implies` consequent,
--       action = Axiom,
--       helpText =
--         "Apply B axiom."
--     }
--   where
--     p = named "p"
--     consequent = necessarily (possibly p)

-- d :: DeductiveRule
-- d =
--   DeductiveRule
--     { name = asciiName "D",
--       premises = [],
--       provisos = [],
--       conclusion = necessarily p `implies` possibly p,
--       action = Axiom,
--       helpText =
--         "Apply D axiom."
--     }
--   where
--     p = named "p"

-- five :: DeductiveRule
-- five =
--   DeductiveRule
--     { name = asciiName "5",
--       premises = [],
--       provisos = [],
--       conclusion =
--         possibleP `implies` necessarily possibleP,
--       action = Axiom,
--       helpText =
--         "Apply 5 axiom\n\n\
--         \  ----------- \n\
--         \     ◇p → ◻◇p"
--     }
--   where
--     possibleP = possibly "p"

-- l :: DeductiveRule
-- l =
--   DeductiveRule
--     { name = asciiName "L",
--       premises = [],
--       provisos = [],
--       conclusion = schema a b' `or` schema b' a,
--       action = Axiom,
--       helpText =
--         "Apply L axiom."
--     }
--   where
--     a = named "a"
--     b' = named "b"
--     schema x y =
--       necessarily
--         (x `and` (necessarily x `implies` y))

-- w :: DeductiveRule
-- w =
--   DeductiveRule
--     { name = asciiName "W",
--       premises = [],
--       provisos = [],
--       conclusion =
--         necessarily (necP `implies` p)
--           `implies` necP,
--       action = Axiom,
--       helpText =
--         "Apply W axiom."
--     }
--   where
--     p = named "p"
--     necP = necessarily p

dualRight :: DeductiveRule
dualRight =
  axiom (asciiName "DUAL-r")
    [ concludes (antecedent `implies` consequent) ]
  where
    antecedent = possibly "p"
    consequent = negationOf (necessarily (negationOf "p"))

dualLeft :: DeductiveRule
dualLeft =
  axiom (asciiName "DUAL-l")
    [ concludes (antecedent `implies` consequent) ]
  where
    antecedent =
      negationOf $
        necessarily (negationOf "p")
    consequent = possibly "p"

necessity :: Operator
necessity = prefixOp "◻" "[]" "square" 1000

possibility :: Operator
possibility = prefixOp "◇" "<>" "lozenge" 1000

necessarily :: FormulaSpec -> FormulaSpec
necessarily f = op necessity <> opn 1 f

possibly :: FormulaSpec -> FormulaSpec
possibly f = op possibility <> opn 1 f
