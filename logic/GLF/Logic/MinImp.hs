-- |
-- Module      : GLF.Logic.MinImp
-- Description : Minimal-implicational logic definitions
-- Copyright   : (c) bruno cuconato, 2019-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.MinImp
  ( -- * Operators and formula specifiers
    implication,
    implies, (==>),

    -- * Rules

    -- ** Declarative
    implicationIntro,
    implicationElim,

    -- * Tactic
    --tactic,

    -- * Formula generator
    fibonacci,
  )
where

import qualified Data.Text as Text
import GLF.Logic.Base
import GLF.Rule
--import GLF.Tactic hiding (some, try)
--import qualified GLF.Tactic as Tactic

-- | The implication introduction rule.
implicationIntro :: DeductiveRule
implicationIntro =
  DeductiveRule
    { name = introductionRuleName implication,
      premises =
        [ Premise
            { hypotheses = [("antecedentHypothesis", "antecedent")],
              root = "consequent",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = "antecedent" `implies` "consequent",
      action = EndsSubProof,
      helpText =
        "Apply implication introduction rule."
    }

-- | The implication elimination rule.
implicationElim :: DeductiveRule
implicationElim =
  DeductiveRule
    { name = eliminationRuleName implication,
      premises =
        [ Premise
            { hypotheses = [],
              root =
                named "implication"
                  <> "antecedent" `implies` "consequent",
              role = "major"
            },
          Premise {hypotheses = [], root = named "antecedent", role = "minor"}
        ],
      provisos = [],
      conclusion = named "consequent",
      action = NoAction,
      helpText =
        "Apply implication elimination rule."
    }

-- -- | Deduction tactic for MIMP.
-- tactic :: Strategy
-- tactic = Strategy (many intro `andThen` Tactic.try discard) (many go)
--   where
--     intro = ruleTactic implicationIntroBND
--     elim = ruleTactic implicationElimBND
--     discard = ruleTactic hypothesisDiscard
--     reachable r = closing r `andThen` discard
--     go =
--       oneOf
--         [ reachable implicationIntro,
--           reachable implicationElim,
--           ( noImmediateCycle intro
--               `orElse` elim
--           )
--             `andThen` Tactic.try discard
--         ]

-- ---
-- -- generators
fibonacci :: Int -> Formula
-- ^ @'fibonacci' n@: generate fibonacci formula of degree @n@.
fibonacci n =
  impl (impl (toAtom 1) (toAtom 2)) $
    go (impl (toAtom 1) (toAtom n))
  where
    impl l r = Formula implication [l, r]
    go acc =
      foldr
        ( \ix a ->
            impl
              ( impl
                  (toAtom $ ix -2)
                  ( impl
                      (toAtom $ ix -1)
                      (toAtom ix)
                  )
              )
              a
        )
        acc
        [3 .. n]
    toAtom = Atom . Text.pack . show

implication :: Operator
-- ^ The implication operator.
implication = rightAssociativeOp "→" "->" "rightarrow" 750

infixr 2 `implies`
infixr 2 ==>
implies, (==>) :: FormulaSpec -> FormulaSpec -> FormulaSpec
-- ^ Specify an implication formula.
implies f g = op implication <> opn 1 f <> opn 2 g
(==>) = implies
