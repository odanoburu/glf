-- |
-- Module      : GLF.Logic.CTLstart
-- Description : System for CTL* logic
-- Copyright   : (c) bruno cuconato, 2020-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.CTLstar
  ( -- * Rules
    assumption,
    assumptionNewLevel,
    nextIntroduction,
    nextElimination,
    someIntroduction,
    someElimination,
    untilIntroduction,
    untilGrow,
    untilElimination1,
    untilElimination2,
    indD,
    indR,
    globalIntroduction,
    globalElimination,
    p,
    bot,
    star,

    -- * Connectives & Operators
    someOp,
    global,
    nextOp,
    label,
    consOp,
    stateOp,
    finiteOp,
    infiniteOp,
    bottomOp,
    -- * Functions
    validLabelFunc,
  ) where

import Prelude hiding (until)

import GLF.Rule hiding (Proviso(..))
import GLF.Logic.Base
import GLF.Logic.Min (bottomOp, bottom)
import GLF.Logic.CTL (untilOp, until)
import GLF.Logic.LFOL (consOp) -- right-associative
--import GLF.Logic.Min ((==>), implication, bottomOp, bottom, negationOf)

assumption :: DeductiveRule
assumption =
  rule (asciiName "Assumption")
    [ concludes $ "A" .: validLabel
    , startsSubProof
    , help "New proof assumption."
    ]


assumptionNewLevel :: DeductiveRule
assumptionNewLevel =
  rule (newLevelName "Assumption")
    [ concludes $ "A" .: validLabel
    , startsSubProofNewLevel
    , help "New proof assumption (at a new level)."
    ]

nextIntroduction, nextElimination :: DeductiveRule
nextIntroduction =
  rule (introductionRuleName nextOp)
    [ premise "predicate" $ "α" .: "l"
    , concludes $ next "α" .: state "a" .- "l"
    ]
nextElimination =
  rule (eliminationRuleName nextOp)
    [ premise "next" $ next "α" .: state "a" .- "l"
    , concludes $ "α" .: "l"
    ]

someIntroduction, someElimination :: DeductiveRule
someIntroduction =
  rule (introductionRuleName someOp)
    [ premise "predicate" $ "α" .: state "a" .- "l"
    , concludes $ some ("α" .: state "a" .- "k")
    ]
someElimination =
  rule (eliminationRuleName someOp)
    [ premise "some" $ some "α" .: state "a" .- "k"
    , premise "discharging" ("γ" .: "m")
      `discharges` ("hypothesis", "α" .: state "a" .- "l")
    , concludes ("γ" .: "m")
    , providedNot (freeIn "l" "m")
    ]

untilIntroduction, untilGrow, untilElimination1, untilElimination2 :: DeductiveRule
untilIntroduction =
  rule (introductionRuleName untilOp)
    [ premise "predicate" $ "β" .: "l"
    , concludes $ "α" `until` "β" .: "l"
    ]
untilGrow =
  rule (polyname "∼∼" "~~" "sim\\sim") -- U
    [ premise "left"  $ "α" .: state "a" .- "l"
    , premise "until" $ "α" `until` "β" .: "l"
    , concludes $ "α" `until` "β" .: state "a" .- "l"
    ]
untilElimination1 =
  rule (version1 $ eliminationRuleName untilOp)
    [ premise "major" $ "α" `until` "β" .: state "a" .- "l"
    , premise "left" "γ"
      `discharges` ("leftHypothesis", "α" .: state "a" .- "l")
      `discharges` ("leftUntilHypothesis", "α" `until` "β" .: "l")
    , premise "right" "γ"
      `discharges` ("rightHypothesis", "β" .: state "a" .- "l")
    , concludes "γ"
    ]
untilElimination2 =
  rule (version2 $ eliminationRuleName untilOp)
    [ premise "major" $ "α" `until` "β" .: finite "x" .- "l"
    , premise "discharging" ("γ" .: "k")
      `discharges` ("hypothesis", "β" .: "l")
    , concludes ("γ" .: "k")
    , providedNot $ "l" `freeIn` "k"
    ]

indD, indR :: DeductiveRule
indD =
  rule (asciiName "IndD")
    [ premise "finite" $ "α" .: finite "x" .- "l"
    , premise "discharging" ("α" .: "l")
      `discharges` ("hypothesis", "α" .: state "a" .- "l")
    , concludes $ "α" .: "l"
    , provided $ allHypothesesDependingOnMatch "discharging" ["α" .: state "a" .- "l"]
    ]
indR =
  rule (asciiName "IndR")
    [ premise "major" $ "α" .: "l"
    , premise "discharging" ("α" .: state "a" .- "l")
      `discharges` ("hypothesis", "α" .: "l")
    , concludes $ "α" .: finite "x" .- "l"
    , provided $ allHypothesesDependingOnMatch "discharging" ["α" .: "l"]
    ]

globalIntroduction, globalElimination :: DeductiveRule
globalIntroduction =
  rule (introductionRuleName global)
    [ premise "someNext" (some (next "α") .: "l")
      `discharges` ("hypothesis", "α" .: "l")
    , concludes $ some (globally "α") .: "l"
    , provided $ allHypothesesDependingOnMatch "someNext" ["α" .: "l"]
    ]
globalElimination =
  rule (eliminationRuleName global)
    [ premise "major" $ some (globally "α") .: "l"
    , premise "discharging" ("β" .: "k")
      `discharges` ("hypothesis", "α" .: "k")
    , concludes $ some (globally "β") .: "l"
    , provided $ allHypothesesDependingOnMatch "discharging" ["α" .: "k"]
    ]

p, bot, star :: DeductiveRule
p =
  rule (asciiName "p")
    [ premise "p" $ ("p" <> atomicFormula) .: state "a" .- "l"
    , concludes $ "p" .: state "a" .- "k"
    ]
bot =
  rule (polyname "⊥" "_|_" "bot{}")
    [ premise "bottom" $ bottom .: "l"
    , concludes $ bottom .: "k"
    ]
star =
  rule (polyname "⋆" "*" "star{}")
    [ premise "predicate" $ "α" .: "l"
    , concludes $ "α" .: "k"
    , help "All hypotheses on which the conclusion depends on have been cancelled."
    ]

globally :: FormulaSpec -> FormulaSpec
globally f = op global <> opn 1 f

global :: Operator
global = prefixOp "□" "[]" "mathcal{G}" 800

some :: FormulaSpec -> FormulaSpec
some f = op someOp <> opn 1 f

someOp :: Operator
someOp = prefixOp "∃" "[-" "mathcal{E}" 800

next :: FormulaSpec -> FormulaSpec
next f = op nextOp <> opn 1 f

nextOp :: Operator
nextOp = prefixOp "⛌" "/\\" "mathcal{X}" 800

state, finite :: FormulaSpec -> FormulaSpec
state f = op stateOp <> opn 1 f
finite f = op finiteOp <> opn 1 f

stateOp, finiteOp, infiniteOp :: Operator
stateOp = postfixOp "^" "^" "hat{}" 1000
finiteOp = postfixOp "+" "+" "+" 1000
infiniteOp = postfixOp "⋆" "*" "star{}" 1000

validLabel :: FormulaSpec
validLabel = ToEval "validLabel" []

infixr 9 .-
(.-) :: FormulaSpec -> FormulaSpec -> FormulaSpec
var .- theLabel = op consOp <> opn 1 var <> opn 2 theLabel

validLabelFunc :: FormulaFunction
-- | A valid label is isomorphic to a list of atoms, with the Cons
-- constructor being 'consOp', and the Nil constructor being an
-- initial label (an atom marked with the 'initialOp' operator)
validLabelFunc = (NonDeterministic fwd, chk)
  where
    fwd =
      -- the fwd function is not very useful, there are infinite
      -- valid labels, the user has to pick one (which we will check
      -- with the chk function)
      \case
        [] -> [Formula infiniteOp [Atom "l"]]
        _wrongargsyntax -> []
    chk (_:_) _ = [] -- too many arguments
    chk [] res = [ res | check res ]
      where
        check =
          \case
            Formula theOp opnds
              | theOp == infiniteOp ->
                  case opnds of
                    [Atom _] -> True
                    _tooManyOrTooFewOperandsOrNotAtomic -> False
            Formula theOp opnds
              | theOp == consOp ->
                case opnds of
                  [restLabel, Formula opndOp [Atom _]] ->
                    (opndOp == finiteOp || opndOp == stateOp)
                    && check restLabel
                  _tooManyOrTooFewOperandsOrNotAtomic -> False
            Formula _ _ -> False -- wrong operator
            Atom _ -> False
