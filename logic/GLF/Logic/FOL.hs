-- |
-- Module      : GLF.Logic.FOL
-- Description : First-order logic system
-- Copyright   : (c) bruno cuconato, 2023-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.FOL
  ( -- * Operators and formula specifiers
    universal,
    forall,
    existential,
    exists,
    conjunction,
    and,
    disjunction,
    or,
    implication,
    implies,
    bottom,
    bottomOp,
    negation,

    -- * Rules
    assumption,
    assumptionNewLevel,
    universalIntroduction,
    universalElimination,
    --existentialIntroduction,
    --existentialElimination,
    implicationIntro,
    implicationElim,
    andIntro,
    andElimRight,
    andElimLeft,

  )
where

import Prelude hiding (and, or, not)

import GLF.Logic.Base
import GLF.Logic.Min
-- import GLF.Logic.Intuitionistic
-- import GLF.Logic.Classical
import GLF.Rule

assumption :: DeductiveRule
assumption =
  DeductiveRule
    { name = asciiName "Assumption",
      premises = [],
      provisos = [],
      conclusion = "f",
      action = StartsSubProof False,
      helpText =
        "New proof assumption."
    }

assumptionNewLevel :: DeductiveRule
assumptionNewLevel =
  DeductiveRule
    { name = newLevelName "Assumption",
      premises = [],
      provisos = [],
      conclusion = "f",
      action = StartsSubProof True,
      helpText =
        "New proof assumption (at a new level)."
    }

-- | The universal introduction rule.
universalIntroduction :: DeductiveRule
universalIntroduction =
  DeductiveRule
    { name = introductionRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = "f",
              role = "predicate"
            }
        ],
      provisos = [Not $ FreeIn "var" (Hypotheses [])],
      conclusion = forall "var" (substituteForInAs "var" "term" "f" "substitutionResult"),
      action = NoAction,
      helpText =
        "Apply universal introduction rule"
    }

-- | The universal introduction rule.
universalElimination :: DeductiveRule
universalElimination =
  DeductiveRule
    { name = eliminationRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = forall "var" "substitutionBody",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = substituteForInAs "term" "var" "substitutionBody" "substitutionResult",
      action = NoAction,
      helpText =
        "Apply universal elimination rule"
    }

-- -- | The existential introduction rule.
-- existentialIntroduction :: DeductiveRule
-- existentialIntroduction =
--   DeductiveRule
--     { name = introductionRuleName existential,
--       premises =
--         [ Branch
--             { hypotheses = [],
--               root = "f",
--               role = "predicate"
--             }
--         ],
--       provisos = [],
--       conclusion = exists "var" (substituteForInAs "var" "term" "f" "_"),
--       action = NoAction,
--       helpText =
--         "Apply existential introduction rule"
--     }

-- -- | The existential elimination rule.
-- existentialElimination :: DeductiveRule
-- existentialElimination =
--   DeductiveRule
--     { name = eliminationRuleName existential,
--       premises =
--         [ Branch
--             { hypotheses = [],
--               root = "existential" <> exists "var" "f",
--               role = "existential"
--             },
--           Branch
--             { hypotheses = [("hypothesis", SubstituteForInAs (variable "constant") "var" "f")],
--               root = "result",
--               role = "predicate"
--             }
--         ],
--       -- TODO: don't use ADT directly, we can fill in information
--       -- like the Hypotheses exceptions for the user
--       provisos = [ Not $ FreeIn "constant" (Hypotheses ["hypothesis"])
--                  , Not $ FreeIn "constant" (FormulaRef "f")
--                  , Not $ FreeIn "constant" (FormulaRef "predicate")
--                  ],
--       conclusion = "result",
--       action = NoAction,
--       helpText =
--         "Apply existential elimination rule"
--     }



universal :: Operator
-- ^ The universal operator.
universal = binaryBinds "∀" "\\/" "forall"

forall :: Text -> FormulaSpec -> FormulaSpec
-- ^ Specify an universal formula.
forall varname f = op universal <> opn 1 (variable varname) <> opn 2 f

existential :: Operator
-- ^ The existential operator.
existential = binaryBinds "∃" "-]" "exists"

exists :: Text -> FormulaSpec -> FormulaSpec
-- ^ Specify an existential formula.
exists varname f = op existential <> opn 1 (variable varname) <> opn 2 f
