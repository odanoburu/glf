-- |
-- Module      : GLF.Logic.CTL
-- Description : System for CTL logic
-- Copyright   : (c) bruno cuconato, 2020-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.CTL
  ( -- * Rules
    assumption,
    assumptionNewLevel,
    bottomElimination,
    implicationIntroduction,
    implicationElimination,
    forallNextIntroduction,
    forallNextElimination,
    existsGlobalElimination,
    existsGlobalGrow,
    existsGlobalPlus,
    untilIntroduction,
    untilGrow,
    untilElimination,
    untilPlus,
    forNoneNextNot,
    existsGlobalEqual,

    -- * Connectives & Operators
    existsGlobalOp,
    forallNextOp,
    implication,
    bottomOp,
    untilOp,
    label,
    consOp,
    initialOp,
    -- * Functions
    validLabelFunc,
    -- * Formula specification
    until,
  ) where

import Prelude hiding ((+), until)

import GLF.Rule hiding (Proviso(..))
import GLF.Logic.Base
import GLF.Logic.Min ((==>), implication, bottomOp, bottom, negationOf)

assumption :: DeductiveRule
assumption =
  rule (asciiName "Assumption")
    [ concludes $ "A" .: validLabel
    , startsSubProof
    , help "New proof assumption."
    ]


assumptionNewLevel :: DeductiveRule
assumptionNewLevel =
  rule (newLevelName "Assumption")
    [ concludes $ "A" .: validLabel
    , startsSubProofNewLevel
    , help "New proof assumption (at a new level)."
    ]

bottomElimination :: DeductiveRule
bottomElimination =
  rule (eliminationRuleName bottomOp)
    [ premise "absurd" (bottom .: "l")
      `discharges` ("negativeHypothesis", "A" ==> bottom .: "k")
    , concludes $ "A" .: "k"
    , closesSubProof
    ]

implicationIntroduction :: DeductiveRule
implicationIntroduction =
  rule (introductionRuleName implication)
    [ premise "conclusion" ("B" .: "l")
      `discharges` ("hypothesis", "A" .: "l")
    , concludes $ "A" ==> "B" .: "l"
    , closesSubProof
    ]

implicationElimination :: DeductiveRule
implicationElimination =
  rule (eliminationRuleName implication)
    [ premise "major" $ "A" ==> "B" .: "l"
    , premise "minor" $ "A" .: "l"
    , concludes $ "B" .: "l"
    ]

forallNextIntroduction, forallNextElimination :: DeductiveRule
forallNextIntroduction =
  rule (introductionRuleName forallNextOp)
    [ premise "next" ("A" .: "l" + "a")
    , concludes $ forallNext "A" .: "l"
    , providedNot $ freeInHypothesesExcept "a" []
    ]
forallNextElimination =
  rule (eliminationRuleName forallNextOp)
    [ premise "forallNext" $ forallNext "A" .: "l"
    , concludes $ "A" .: "l" + "a"
    ]

existsGlobalElimination, existsGlobalGrow, existsGlobalPlus :: DeductiveRule
existsGlobalElimination =
  rule (eliminationRuleName existsGlobalOp)
    [ premise "existsGlobal" $ existsGlobal "A" .: "l"
    , concludes $ "A" .: "l"
    ]
existsGlobalGrow =
  rule (polyname "∃G—" "-]G-" "exists{}G\text{---}")
    [ premise "current" $ "A" .: "l"
    , premise "next" $ existsGlobal "A" .: "l" + "a"
    , concludes $ existsGlobal "A" .: "l"
    ]
existsGlobalPlus =
  rule (polyname "∃G+" "-]G+" "exists{}G+")
    [ premise "existsGlobal" $ existsGlobal "A" .: "l"
    , premise "discharging" ("C" .: "k")
      `discharges` ("existsGlobalHypothesis", existsGlobal "A" .: "l" + "a")
    , concludes $ "C" .: "k"
    , providedNot $ freeInHypothesesExcept "a" ["existsGlobalHypothesis"]
    , providedNot $ freeIn "a" "k"
    , closesSubProof
    ]


untilIntroduction, untilGrow, untilPlus, untilElimination :: DeductiveRule
untilIntroduction =
  rule (introductionRuleName untilOp)
    [ premise "current" $ "A" .: "l"
    , premise "next" $ "B" .: "l" + "a"
    , concludes $ "A" `until` "B" .: "l"
    ]
untilGrow =
  rule (polyname "∼∼" "~~" "sim\\sim") -- ∃—
    [ premise "current" $ "A" .: "l"
    , premise "next" $ "A" `until` "B" .: "l" + "a"
    , concludes $ "A" `until` "B" .: "l"
    ]
untilPlus =
  rule (eliminationRuleName untilOp)
    [ premise "until" $ "A" `until` "B" .: "l"
    , premise "then" ("C" .: "k")
      `discharges` ("currentHypothesisLeft", "A" .: "l")
      `discharges` ("afterHypothesis", "B" .: "l" + "a")
    , premise "before" ("C" .: "k")
      `discharges` ("currentHypothesisRight", "A" .: "l")
      `discharges` ("untilHypothesis", "A" `until` "B" .: "l" + "b")
    , concludes $ "C" .: "k"
    , providedNot $ freeIn "a" "l"
    , providedNot $ freeIn "b" "l"
    , providedNot $ freeIn "a" "k"
    , providedNot $ freeIn "b" "k"
    , providedNot $ freeInHypothesesExcept "a" ["afterHypothesis"]
    , providedNot $ freeInHypothesesExcept "b" ["untilHypothesis"]
    , closesSubProof
    ]
untilElimination =
  rule (eliminationRuleName untilOp)
    [ premise "until" $ "A" `until` "B" .: "l"
    , premise "current" ("C" .: "l")
      `discharges` ("beforeHypothesis", "A" .: "l")
      `discharges` ("conclusionHypothesis", "C" .: "l" + "a")
    , premise "next" ("C" .: "l" + "a")
      `discharges` ("thenHypothesis", "B" .: "l" + "a")
    , concludes $ "C" .: "l"
    , provided $ allHypothesesDependingOnMatch "current" ["A" .: "l", "C" .: "l" + "a"]
    , provided $ allHypothesesDependingOnMatch "next" ["B" .: "l" + "a"]
    , closesSubProof
    ]
forNoneNextNot :: DeductiveRule
forNoneNextNot =
  rule (asciiName "GInd")
    [ premise "notForallNext" $ negationOf (forallNext $ negationOf "A") .: "l"
    , concludes $ existsGlobal "A" .: "l"
    , provided (allHypothesesDependingOnMatch "notForallNext" ["A" .: "l"])
    , help "There is a sub-derivation concluding the premise whose hypotheses are all A : l"
    ]

existsGlobalEqual :: DeductiveRule
existsGlobalEqual =
  rule (asciiName "G=")
    [ premise "existsGlobal" $ existsGlobal "A" .: "l"
    , premise "conclusion" $ "B" .: "l"
    , concludes $ existsGlobal "B" .: "l"
    , provided $ allHypothesesDependingOnMatch "conclusion" ["A" .: "l"]
    , help "There is a sub-derivation concluding B : l whose hypotheses are all in {A : l}"
    ]

until :: FormulaSpec -> FormulaSpec -> FormulaSpec
f `until` g = op untilOp <> opn 1 f <> opn 2 g

untilOp :: Operator
untilOp = binaryOp "∼" "~" "sim" 600

existsGlobal :: FormulaSpec -> FormulaSpec
existsGlobal f = op existsGlobalOp <> opn 1 f

existsGlobalOp :: Operator
existsGlobalOp = prefixOp "∃" "-]" "@gobble{.}[\\exists{}G]" 550


forallNext :: FormulaSpec -> FormulaSpec
forallNext f = op forallNextOp <> opn 1 f

forallNextOp :: Operator
forallNextOp = prefixOp "∀" "\\/" "@gobble{.}[\\forall{} X]" 800

infixl 6 +
(+) :: FormulaSpec -> Text -> FormulaSpec
l + a
  = op consOp <> opn 1 l
  -- not exactly variable, but it is atomic and named
    <> opn 2 (variable a)

consOp :: Operator
consOp = leftAssociativeOp "+" "+" "+" 550

initialOp :: Operator
-- | Marks an initial label/state.
initialOp = postfixOp "|" "|" "mid" 1000

validLabel :: FormulaSpec
validLabel = ToEval "validLabel" []

validLabelFunc :: FormulaFunction
-- | A valid label is isomorphic to a list of atoms, with the Cons
-- constructor being 'consOp', and the Nil constructor being an
-- initial label (an atom marked with the 'initialOp' operator)
validLabelFunc = (NonDeterministic fwd, chk)
  where
    fwd =
      -- the fwd function is not very useful, there are infinite
      -- valid labels, the user has to pick one (which we will check
      -- with the chk function)
      \case
        [] -> [Formula initialOp [Atom "i"]]
        _wrongargsyntax -> []
    chk (_:_) _ = [] -- too many arguments
    chk [] res = [ res | check res ]
      where
        check =
          \case
            Formula theOp opnds
              | theOp == initialOp ->
                  case opnds of
                    [Atom _] -> True
                    _tooManyOrTooFewOperandsOrNotAtomic -> False
            Formula theOp opnds
              | theOp == consOp ->
                case opnds of
                  [restLabel, Atom _] -> check restLabel
                  _tooManyOrTooFewOperandsOrNotAtomic -> False
            Formula _ _ -> False -- wrong operator
            Atom _ -> False
