-- |
-- Module      : GLF.Logic.Classical
-- Description : Classical logic definitions
-- Copyright   : (c) bruno cuconato, 2019-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Classical
  ( -- * Operators and formula specifiers
    bottom,
    bottomOp,
    negationOf,
    implication,
    implies,
    conjunction,
    and,
    disjunction,
    or,

    -- * Rules

    -- ** Declarative
    implicationIntro,
    implicationElim,
    andIntro,
    andElimLeft,
    andElimRight,
    orIntroLeft,
    orIntroRight,
    orElim,
    exFalsoQuodLibet,
    reductioAdAbsurdum,
    reductioAdAbsurdum',
    tertiumNonDatur,

    -- ** Backwards Natural Deduction (compiled)
    implicationIntroBND,
    implicationElimBND,
    andIntroBND,
    andElimLeftBND,
    andElimRightBND,
    orIntroLeftBND,
    orIntroRightBND,
    orElimBND,
    exFalsoQuodLibetBND,
    reductioAdAbsurdumBND,
    reductioAdAbsurdumBND',
    tertiumNonDaturBND,
    hypothesisDiscard,

    -- ** Fitch-style deduction (compiled)
    implicationIntroFw,
    implicationElimFw,
    andIntroFw,
    andElimRightFw,
    andElimLeftFw,
    orIntroLeftFw,
    orIntroRightFw,
    orElimFw,
    exFalsoQuodLibetFw,
    reductioAdAbsurdumFw,
    reductioAdAbsurdumFw',
    assumptionFw,
    assumptionSameLevelFw,
    tertiumNonDaturFw,
    qedFw,

    -- * Tactic
    tactic,

    -- * Parser
    parse,

    -- * Helpers
    subFormulas,
  )
where

import GLF.Logic.Base
import GLF.Logic.Intuitionistic
import GLF.Rule
import Prelude hiding (and, or, not)

reductioAdAbsurdum :: DeductiveRule
reductioAdAbsurdum =
  DeductiveRule
    { name = PolyName {unicode = "⊥↯", ascii = "reductioAdAbsurdum", latex = "mathrm{reductioAdAbsurdum}"},
      premises =
        [ Premise
            { hypotheses = [("negativeHypothesis", named "negativeHypothesis" <> ("conclusion" `implies` bottom))],
              root = named "bottom" <> bottom,
              role = "absurd"
            }
        ],
      provisos = [],
      conclusion = named "conclusion",
      helpText =
        "Apply reductio ad absurdum rule"
    }

reductioAdAbsurdumBND :: CompiledRule
reductioAdAbsurdumBND =
  makeBNDRule reductioAdAbsurdum

reductioAdAbsurdumFw :: CompiledRule
reductioAdAbsurdumFw =
  makeFitchRule EndsSubProof reductioAdAbsurdum

reductioAdAbsurdum' :: DeductiveRule
reductioAdAbsurdum' =
  DeductiveRule
    { name = PolyName {unicode = "⊥↯", ascii = "reductioAdAbsurdum", latex = "mathrm{reductioAdAbsurdum}"},
      premises =
        [ Premise
            { hypotheses = [("negativeHypothesis", named "negativeHypothesis" <> not "conclusion")],
              root = named "bottom" <> bottom,
              role = "absurd"
            }
        ],
      provisos = [],
      conclusion = named "conclusion",
      helpText =
        "Apply reductio ad absurdum rule"
    }

reductioAdAbsurdumBND' :: CompiledRule
reductioAdAbsurdumBND' =
  makeBNDRule reductioAdAbsurdum'

reductioAdAbsurdumFw' :: CompiledRule
reductioAdAbsurdumFw' =
  makeFitchRule EndsSubProof reductioAdAbsurdum'

tertiumNonDatur :: DeductiveRule
tertiumNonDatur = -- excluded middle
  DeductiveRule
    { name = PolyName {unicode = "∨¬", ascii = "tertiumNonDatur", latex = "mathrm{tertiumNonDatur}"},
      premises = [],
      provisos = [],
      conclusion = "A" `or` (not "A"),
      helpText =
        "Tertium non datur axiom"
    }

tertiumNonDaturBND :: CompiledRule
tertiumNonDaturBND = makeBNDRule tertiumNonDatur

tertiumNonDaturFw :: CompiledRule
tertiumNonDaturFw = makeFitchRule NoAction tertiumNonDatur
