-- |
-- Module      : GLF.Logic.LFOL
-- Description : Labelled First-order logic system
-- Copyright   : (c) bruno cuconato, 2023-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.LFOL
  ( -- * Operators and formula specifiers
    universal,
    forall,
    existential,
    exists,
    conjunction,
    and,
    implication,
    implies,
    bottom,
    bottomOp,
    negation,
    label,
    consOp,
    emptyOp,

    -- * Rules
    assumption,
    assumptionNewLevel,
    universalIntroduction,
    universalElimination,
    implicationIntro,
    implicationElim,
    conjunctionIntro,
    conjunctionElimRight,
    conjunctionElimLeft,
    reductioAdAbsurdum,

    -- * Functions
    freevarsFunc,
    appendFunc,
    subtractFunc,

  )
where

import Prelude hiding (and, or, not, subtract)
import qualified Data.Set as Set
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import qualified Data.List as List

import GLF.Logic.Base
import GLF.Logic.Min (conjunction, and, implication, implies, bottom, bottomOp, negation)
import GLF.Logic.FOL (universal, existential, forall, exists)
import GLF.Rule


consOp :: Operator
consOp = rightAssociativeOp ";" ";" "mathbin{;}" 550

emptyOp :: Operator
emptyOp = nullaryOp "_" "_" "_"

assumption :: DeductiveRule
assumption =
  DeductiveRule
    { name = asciiName "Assumption",
      premises = [],
      provisos = [],
      conclusion = "formula" .: freevars "formula",
      action = StartsSubProof False,
      helpText =
        "New proof assumption."
    }

assumptionNewLevel :: DeductiveRule
assumptionNewLevel =
  DeductiveRule
    { name = newLevelName "Assumption",
      premises = [],
      provisos = [],
      conclusion = "formula" .: freevars "formula",
      action = StartsSubProof True,
      helpText =
        "New proof assumption (at a new level)."
    }

-- | The universal introduction rule.
universalIntroduction :: DeductiveRule
universalIntroduction =
  DeductiveRule
    { name = introductionRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = "f" .: "l",
              role = "predicate"
            }
        ],
      provisos = [Not $ FreeIn "v" (FormulaRef "l")],
      conclusion = forall "v" "f" .: "l",
      action = NoAction,
      helpText =
        "Apply universal introduction rule"
    }

-- | The universal introduction rule.
universalElimination :: DeductiveRule
universalElimination =
  DeductiveRule
    { name = eliminationRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = forall "var" "substitutionBody" .: "l",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = substituteForInAs "term" "var" "substitutionBody" "substitutionResult" .: "l",
      action = NoAction,
      helpText =
        "Apply universal elimination rule"
    }

conjunctionIntro :: DeductiveRule
conjunctionIntro =
  DeductiveRule
    { name = introductionRuleName conjunction,
      premises =
        [ Premise
            { hypotheses = [],
              root = "left" .: "l",
              role = "leftConjunct"
            },
          Premise
            { hypotheses = [],
              root = "right" .: "m",
              role = "rightConjunct"
            }
        ],
      provisos = [],
      conclusion = "left" `and` "right" .: append "l" "m",
      action = NoAction,
      helpText =
        "Apply conjunction introduction rule"
    }

conjunctionElimLeft :: DeductiveRule
conjunctionElimLeft =
  DeductiveRule
    { name = operatorRuleName conjunction (Elimination LeftS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                "left" `and` anyFormula .: "l",
              role = "conjunction"
            }
        ],
      provisos = [],
      conclusion = "left" .: "l",
      action = NoAction,
      helpText =
        "Apply left conjunction elimination rule"
    }

conjunctionElimRight :: DeductiveRule
conjunctionElimRight =
  DeductiveRule
    { name = operatorRuleName conjunction (Elimination RightS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                anyFormula `and` "right" .: "l",
              role = "conjunction"
            }
        ],
      provisos = [],
      conclusion = "right" .: "l",
      action = NoAction,
      helpText =
        "Apply right conjunction elimination rule"
    }

-- | The implication introduction rule.
implicationIntro :: DeductiveRule
implicationIntro =
  DeductiveRule
    { name = introductionRuleName implication,
      premises =
        [ Premise
            { hypotheses = [("antecedentHypothesis", "antecedent" .: "l")],
              root = "consequent" .: "m",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = "antecedent" `implies` "consequent" .: subtract "m" "l",
      action = EndsSubProof,
      helpText =
        "Apply implication introduction rule."
    }

-- | The implication elimination rule.
implicationElim :: DeductiveRule
implicationElim =
  DeductiveRule
    { name = eliminationRuleName implication,
      premises =
        [ Premise
            { hypotheses = [],
              root = "antecedent" `implies` "consequent" .: "l",
              role = "major"
            },
          Premise {hypotheses = [], root = "antecedent" .: "m", role = "minor"}
        ],
      provisos = [],
      conclusion = "consequent" .: append "l" "m",
      action = NoAction,
      helpText =
        "Apply implication elimination rule."
    }

reductioAdAbsurdum :: DeductiveRule
reductioAdAbsurdum =
  DeductiveRule
    { name = PolyName {unicode = "⊥↯", ascii = "reductioAdAbsurdum", latex = "mathrm{reductioAdAbsurdum}"},
      premises =
        [ Premise
            { hypotheses = [("negativeHypothesis", "conclusion" `implies` bottom .: "l")],
              root = "bottom" <> bottom .: "m",
              role = "absurd"
            }
        ],
      provisos = [],
      conclusion = "conclusion" .: subtract "m" "l",
      action = EndsSubProof,
      helpText =
        "Apply reductio ad absurdum rule"
    }


freevars :: FormulaSpec -> FormulaSpec
freevars f = eval "freevars" [f]

append :: FormulaSpec -> FormulaSpec -> FormulaSpec
append f g = eval "append" [f, g]

subtract :: FormulaSpec -> FormulaSpec -> FormulaSpec
subtract f g = eval "subtract" [f, g]

freevarsFunc :: FormulaFunction
freevarsFunc = (Deterministic fwd, chk)
  where
    fwd =
      \case
        [f] -> [listToLabel . fmap Atom . Set.toList $ formulaFreeVariables f]
        _wrongargsyntax -> []
    chk args res =
      -- any label is valid as long as all of the formula's free
      -- variables are present (their order doesn't matter)
      case args of
        [f] ->
          -- should we allow variable repetitions?
          let freeVarsFromArg = Set.mapMonotonic Atom $ formulaFreeVariables f
              freeVarsFromRes = Set.fromList $ labelToList res
          in [ res | freeVarsFromArg == freeVarsFromRes ]
        _wrongargsyntax -> []


appendFunc :: FormulaFunction
appendFunc = (Deterministic fwd, chk)
  where
    fwd =
      \case
        [l, m] -> [listToLabel $ labelToList l ++ labelToList m]
        _wrongnumberargs -> []
    chk args res =
      case args of
        [l, m] -> [ res | Map.unionWith (+) (labelToMultiSet l) (labelToMultiSet m) == labelToMultiSet res]
        _wrongnumberargs -> []

subtractFunc :: FormulaFunction
subtractFunc = (Deterministic fwd, chk)
  where
    fwd =
      \case
        [f, g] -> [multiSetToList $ Map.unionWith (-) (labelToMultiSet f) (labelToMultiSet g)]
        _wrongnumberargs -> []
    chk args res =
      case args of
        [l, m] -> [ res | Map.unionWith (-) (labelToMultiSet l) (labelToMultiSet m) == labelToMultiSet res ]
        _wrongnumberargs -> []

listToLabel :: [Formula] -> Formula
listToLabel = foldr (\f acc -> Formula consOp [f, acc]) (Formula emptyOp [])

labelToList :: Formula -> [Formula]
labelToList = go
  where
    go (Atom n) = [Atom n]
    go (Formula theOp [theHead, theTail]) =
      if theOp == consOp
      then theHead : go theTail
      else []
    go (Formula _ _) = []

labelToMultiSet :: Formula -> Map Formula Int
labelToMultiSet =
  List.foldl' (\acc f -> Map.insertWith (+) f 1 acc) Map.empty
  . labelToList

multiSetToList :: Map Formula Int -> Formula
multiSetToList =
  listToLabel . Map.foldlWithKey' (\acc f n -> replicate n f ++ acc) []
