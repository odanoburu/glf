-- |
-- Module      : GLF.Logic.Keisler
-- Description : System for Keisler logic
-- Copyright   : (c) bruno cuconato, 2020-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Keisler
  ( -- * Rules
    assumption,
    assumptionNewLevel,
    ax,
    existentialIntroduction,
    existentialElimination,
    uncountablyManyIntroduction,
    uncountablyManyElimination,
    implicationIntroduction,
    implicationElimination,
    disjunctionIntroduction,
    disjunctionElimination,
    reductioAdAbsurdum,
    aleph,
    star,

    -- * Connectives
    bottomOp,
    implication,
    disjunction,
    existential,
    uncountablyManyOp,
    equality,
    label,
    consOp,
    emptyOp,
    markOp,

    -- * Functions
    onlyUnmarked,
    onlyUnmarkedFunc,
    subsetOfFunc,
  ) where

import Prelude hiding ((==), or)
import Data.Set as Set
import GLF.Rule
import GLF.Logic.Base
import GLF.Logic.Min
import GLF.Logic.FOL hiding (assumptionNewLevel, assumption)
import GLF.Logic.Ultrafilter ((.-), consOp, emptyOp, mark, markOp, labelToList)
import GLF.Logic.Filter (onlyUnmarkedFunc, onlyUnmarked)

assumption :: DeductiveRule
assumption =
  rule (asciiName "Assumption")
    [ concludes $ "φ" .: "L"
    , startsSubProof
    , help "New proof assumption."
    ]


assumptionNewLevel :: DeductiveRule
assumptionNewLevel =
  rule (newLevelName "Assumption")
    [ concludes $ "φ" .: "L"
    , startsSubProofNewLevel
    , help "New proof assumption (at a new level)."
    ]

ax :: DeductiveRule
ax =
  axiom (asciiName "Ax")
    [ concludes $ (negationOf . uncountablyMany "x" $ "x" == "y" `or` "x" == "z") .: emptyLabel ]

existentialIntroduction :: DeductiveRule
existentialIntroduction =
  rule (introductionRuleName existential)
    [ premise "premise" $ "φ" .: "L" .- "x"
    , concludes (exists "x" "φ" .: "L")
    ]

existentialElimination :: DeductiveRule
existentialElimination =
  rule (eliminationRuleName existential)
    [ premise "premise" $ exists "x" "φ" .: "L"
    , concludes $ "φ" .: "L" .- "x"
    ]

uncountablyManyIntroduction :: DeductiveRule
uncountablyManyIntroduction =
  rule (introductionRuleName uncountablyManyOp)
    [ premise "premise" $ "φ" .: "L" .- mark "x"
    , concludes
      $ uncountablyMany
          "y"
          (substituteForInAs "y" "x" "φ" "result" ) -- rename variable
          .: "L"
    ]

uncountablyManyElimination :: DeductiveRule
uncountablyManyElimination =
  rule (eliminationRuleName uncountablyManyOp)
    [ premise "premise" $ uncountablyMany "x" "φ" .: "L"
    , concludes $ "φ" .: "L" .- mark "x"
    ]

implicationIntroduction :: DeductiveRule
implicationIntroduction =
  rule (introductionRuleName implication)
    [ premise "premise" ("ψ" .: "L") `discharges` ("antecedentHypothesis", "φ" .: "L")
    , concludes $ "φ" ==> "ψ" .: onlyUnmarked "L"
    , closesSubProof
    ]

implicationElimination :: DeductiveRule
implicationElimination =
  rule (eliminationRuleName implication)
    [ premise "major" $ "φ" ==> "ψ" .: emptyLabel
    , premise "minor" $ "φ" .: "L"
    , concludes $ "ψ" .: "L"
    -- TODO: make this a proviso
    , help "The free variables of L must not occur free in the hypotheses of φ → ψ"
    ]

disjunctionIntroduction :: DeductiveRule
disjunctionIntroduction =
  rule (introductionRuleName disjunction)
    [ premise "premise" $ "ψ" .: "L"
    , concludes $ "ψ" `or` "φ" .: "L"
    ]

disjunctionElimination :: DeductiveRule
disjunctionElimination =
  rule (eliminationRuleName disjunction)
    [ premise "major" $ "φ" `or` "ψ" .: "L"
    , premise "left" ("γ" .: "K") `discharges`("leftHypothesis", "φ" .: "L")
    , premise "right" ("γ" .: "K") `discharges` ("rightHypothesis", "ψ" .: "L")
    , concludes $ "γ" .: "K"
    ]

reductioAdAbsurdum :: DeductiveRule
reductioAdAbsurdum =
  rule (asciiName "RAA")
    [ premise "absurd" (bottom .: emptyLabel) `discharges` ("negativeHypothesis", negationOf "φ" .: named "L" <> onlyUnmarked "L")
    , concludes $ "φ" .: subsetOf "L"
    , closesSubProof
    , help "Apply reductio ad absurdum rule"
    ]

aleph :: DeductiveRule
aleph =
  rule (polyname "ℵ" "N" "aleph")
    [ premise "major" $ "φ" .: "L" .- mark "y" .- "x"
    , premise "absurd" (bottom .: emptyLabel) `discharges` ("hypothesis", uncountablyMany "x" (exists "y" "φ") .: emptyLabel)
    , concludes $ "φ" .: "L" .- "x" .- mark "y"
    , closesSubProof
    ]

star :: DeductiveRule
star =
  rule (polyname "∗" "*" "star")
    [ premise "major" $ "φ" .: "L"
    , premise "discharging" ("ψ" .: emptyLabel) `discharges` ("hypothesis", "φ" .: emptyLabel)
    , concludes $ "ψ" .: "L"
    , closesSubProof
    , help "The variables in L do not occur free in any of the hypotheses on which ψ depends, with the exception of φ."
    ]

infix 5 ==
(==) :: FormulaSpec -> FormulaSpec -> FormulaSpec
a == b = op equality <> opn 1 a <> opn 2 b

equality :: Operator
equality = binaryOp "=" "=" "=" 540

uncountablyManyOp :: Operator
-- ^ The Keisler quantifier.
uncountablyManyOp = binaryBinds "&" "&" "mathcal{Q}"

uncountablyMany :: Text -> FormulaSpec -> FormulaSpec
uncountablyMany varname f = op uncountablyManyOp <> opn 1 (variable varname) <> opn 2 f

subsetOf :: FormulaSpec -> FormulaSpec
subsetOf l = eval "subsetOf" [l]

emptyLabel :: FormulaSpec
emptyLabel = op emptyOp

subsetOfFunc :: FormulaFunction
-- l contains only unmarked variables
--- NOTE: also used in Keisler.hs
subsetOfFunc = (NonDeterministic fwd, chk)
  where
    fwd =
      \case
        [l] -> [l]
        _wrongnumberargs -> []
    chk args res =
      case args of
        [l] -> [ res | Set.fromList (labelToList res) `isSubsetOf` Set.fromList (labelToList l) ]
        _wrongnumberargs -> []
