-- |
-- Module      : GLF.Logic.Tableaux.Classical
-- Description : Tableaux for Classical logic (unsigned)
-- Copyright   : (c) bruno cuconato, 2022-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Tableaux.Classical
  ( -- * Operators and formula specifiers
    negation,
    negationOf,
    conjunction,
    and,
    disjunction,
    or,

    -- * Rules

    -- ** Declarative
    conjunctionElimination,
    disjunctionElimination,
    doubleNegationElimination,

    -- ** Tableaux-style deduction (compiled)
    conjunctionEliminationTableaux,
    disjunctionEliminationTableaux,
    doubleNegationEliminationTableaux,
    closeTableaux,
  )
where

import GLF.Logic.Base
import GLF.Logic.Min (and, conjunction, disjunction, or)
import GLF.Logic.Tableaux
import GLF.Rule
import Prelude hiding (and, or)

conjunctionEliminationTableaux :: CompiledRule
conjunctionEliminationTableaux =
  tableauxRule conjunctionElimination

conjunctionElimination :: TableauxRule
conjunctionElimination =
  TableauxRule
    { name = operatorRuleName conjunction (Elimination NoS),
      help =
        "Apply conjunction elimination rule\n\n\
        \    A ∧ B\n\
        \  ---------- \n\
        \    A\n\
        \    B",
      premise = "left" `and` "right",
      results = [["left", "right"]]
    }

disjunctionEliminationTableaux :: CompiledRule
disjunctionEliminationTableaux =
  tableauxRule disjunctionElimination

disjunctionElimination :: TableauxRule
disjunctionElimination =
  TableauxRule
    { name = operatorRuleName disjunction (Elimination NoS),
      help =
        "Apply disjunction elimination rule\n\n\
        \    A ∨ B\n\
        \  ---------- \n\
        \    A | B",
      premise = "left" `or` "right",
      results = [["left"], ["right"]]
    }

doubleNegationEliminationTableaux :: CompiledRule
doubleNegationEliminationTableaux =
  tableauxRule doubleNegationElimination

doubleNegationElimination :: TableauxRule
doubleNegationElimination =
  TableauxRule
    { name = PolyName {ascii = "~~E", unicode = "¬¬E", latex = "neg\\neg\\mathrm{E}"},
      help =
        "Apply double negation elimination rule\n\n\
        \   ¬¬ A\n\
        \ --------\n\
        \     A",
      premise = negationOf (negationOf "negated"),
      results = [["negated"]]
    }
