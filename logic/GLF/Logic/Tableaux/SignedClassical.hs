-- |
-- Module      : GLF.Logic.Tableaux.SignedClassical
-- Description : Tableaux for Classical logic (signed)
-- Copyright   : (c) bruno cuconato, 2022-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Tableaux.SignedClassical
  ( -- * Operators and formula specifiers
    true,
    isTrue,
    false,
    isFalse,
    negation,
    negationOf,
    implication,
    implies,
    conjunction,
    and,
    disjunction,
    or,
    equivalence,
    iff,

    -- * Rules

    -- ** Declarative
    trueNegation,
    falseNegation,
    trueImplication,
    falseImplication,
    trueConjunction,
    falseConjunction,
    trueDisjunction,
    falseDisjunction,
    trueEquivalence,
    falseEquivalence,

    -- ** Tableaux-style deduction (compiled)
    trueNegationTableaux,
    falseNegationTableaux,
    trueImplicationTableaux,
    falseImplicationTableaux,
    trueConjunctionTableaux,
    falseConjunctionTableaux,
    trueDisjunctionTableaux,
    falseDisjunctionTableaux,
    trueEquivalenceTableaux,
    falseEquivalenceTableaux,
    closeSignedTableaux,
  )
where

import GLF.Logic.Base
import GLF.Logic.Min
  ( and,
    conjunction,
    disjunction,
    implication,
    implies,
    or,
  )

import GLF.Logic.Tableaux
import GLF.Rule
import Prelude hiding (and, or)

trueNegationTableaux :: CompiledRule
trueNegationTableaux =
  tableauxRule trueNegation

trueNegation :: TableauxRule
trueNegation =
  TableauxRule
    { name = operatorRuleName negation (Elimination TrueV),
      help =
        "Apply negation elimination of true assertion\n\n\
        \    T (¬ A)\n\
        \  --------\n\
        \    F (A)",
      premise = isTrue (negationOf "assertion"),
      results = [[isFalse "assertion"]]
    }

falseNegationTableaux :: CompiledRule
falseNegationTableaux =
  tableauxRule falseNegation

falseNegation :: TableauxRule
falseNegation =
  TableauxRule
    { name = operatorRuleName negation (Elimination FalseV),
      help =
        "Apply negation elimination of false assertion\n\n\
        \    F (¬ A)\n\
        \  --------\n\
        \    T (A)",
      premise = isFalse (negationOf "assertion"),
      results = [[isTrue "assertion"]]
    }

trueImplicationTableaux :: CompiledRule
trueImplicationTableaux =
  tableauxRule trueImplication

trueImplication :: TableauxRule
trueImplication =
  TableauxRule
    { name = operatorRuleName implication (Elimination TrueV),
      help =
        "Apply implication elimination of true assertion\n\n\
        \      T (A → B)\n\
        \  -----------------\n\
        \    F (A) | T (B)",
      premise = isTrue ("left" `implies` "right"),
      results = [[isFalse "left"], [isTrue "right"]]
    }

falseImplicationTableaux :: CompiledRule
falseImplicationTableaux =
  tableauxRule falseImplication

falseImplication :: TableauxRule
falseImplication =
  TableauxRule
    { name = operatorRuleName implication (Elimination FalseV),
      help =
        "Apply implication elimination of false assertion\n\n\
        \    F (A → B)\n\
        \  -------------\n\
        \    T (A)\n\
        \    F (B)",
      premise = isFalse ("left" `implies` "right"),
      results = [[isTrue "left", isFalse "right"]]
    }

trueConjunctionTableaux :: CompiledRule
trueConjunctionTableaux =
  tableauxRule trueConjunction

trueConjunction :: TableauxRule
trueConjunction =
  TableauxRule
    { name = operatorRuleName conjunction (Elimination TrueV),
      help =
        "Apply conjunction elimination of true assertion\n\n\
        \    T (A ∧ B)\n\
        \  --------------\n\
        \    T (A)\n\
        \    T (B)",
      premise = isTrue ("left" `and` "right"),
      results = [[isTrue "left", isTrue "right"]]
    }

falseConjunctionTableaux :: CompiledRule
falseConjunctionTableaux =
  tableauxRule falseConjunction

falseConjunction :: TableauxRule
falseConjunction =
  TableauxRule
    { name = operatorRuleName conjunction (Elimination FalseV),
      help =
        "Apply conjunction elimination of false assertion\n\n\
        \      F (A ∧ B)\n\
        \  -----------------\n\
        \    F (A) | F (B)",
      premise = isFalse ("left" `and` "right"),
      results = [[isFalse "left"], [isFalse "right"]]
    }

trueDisjunctionTableaux :: CompiledRule
trueDisjunctionTableaux =
  tableauxRule trueDisjunction

trueDisjunction :: TableauxRule
trueDisjunction =
  TableauxRule
    { name = operatorRuleName disjunction (Elimination TrueV),
      help =
        "Apply disjunction elimination of true assertion\n\n\
        \     T (A ∨ B)\n\
        \  ----------------\n\
        \    T (A) | T (B)",
      premise = isTrue ("left" `or` "right"),
      results = [[isTrue "left"], [isTrue "right"]]
    }

falseDisjunctionTableaux :: CompiledRule
falseDisjunctionTableaux =
  tableauxRule falseDisjunction

falseDisjunction :: TableauxRule
falseDisjunction =
  TableauxRule
    { name = operatorRuleName disjunction (Elimination FalseV),
      help =
        "Apply disjunction elimination of false assertion\n\n\
        \     F (A ∨ B)\n\
        \  ----------------\n\
        \    F (A)\n\
        \    F (B)",
      premise = isFalse ("left" `or` "right"),
      results = [[isFalse "left", isFalse "right"]]
    }

trueEquivalenceTableaux :: CompiledRule
trueEquivalenceTableaux =
  tableauxRule trueEquivalence

trueEquivalence :: TableauxRule
trueEquivalence =
  TableauxRule
    { name = operatorRuleName equivalence (Elimination TrueV),
      help =
        "Apply equivalence elimination of true assertion\n\n\
        \     T (A ↔ B)\n\
        \  ----------------\n\
        \    T (A) | F (A)\n\
        \    T (B) | F (B)",
      premise = isTrue ("left" `iff` "right"),
      results =
        [ [isTrue "left", isTrue "right"],
          [isFalse "left", isFalse "right"]
        ]
    }

falseEquivalenceTableaux :: CompiledRule
falseEquivalenceTableaux =
  tableauxRule falseEquivalence

falseEquivalence :: TableauxRule
falseEquivalence =
  TableauxRule
    { name = operatorRuleName equivalence (Elimination FalseV),
      help =
        "Apply equivalence elimination of false assertion\n\n\
        \     F (A ↔ B)\n\
        \  ----------------\n\
        \    T (A) | F (A)\n\
        \    F (B) | T (B)",
      premise = isFalse ("left" `iff` "right"),
      results =
        [ [isTrue "left", isFalse "right"],
          [isFalse "left", isTrue "right"]
        ]
    }

equivalence :: Operator
equivalence = leftAssociativeOp "↔" "<->" "iff"

iff :: FormulaSpec -> FormulaSpec -> FormulaSpec
iff l r = op equivalence <> opn 1 l <> opn 2 r
