-- |
-- Module      : GLF.Logic.Ultrafilter
-- Description : Labelled ultrafilter logic system
-- Copyright   : (c) bruno cuconato, 2023-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Ultrafilter
  ( -- * Operators and formula specifiers
    universal,
    forall,
    quasiuniversal,
    almostall,
    conjunction,
    and,
    implication,
    implies,
    bottom,
    bottomOp,
    negation,
    markOp,
    mark,
    label,
    consOp, (.-),
    emptyOp,

    -- * Rules
    assumption,
    assumptionNewLevel,
    universalIntroduction,
    universalIntroductionNoLabelChange,
    universalEliminationNoChange,
    universalElimination,
    universalEliminationUnmarkedLabel,
    universalEliminationMarkedLabel,
    quasiuniversalIntroduction,
    quasiuniversalIntroductionNoLabelChange,
    quasiuniversalElimination,
    quasiuniversalEliminationNoLabelChange,
    implicationIntro,
    implicationElim,
    conjunctionIntro,
    conjunctionElimRight,
    conjunctionElimLeft,
    reductioAdAbsurdum,
    xRule,

    -- * Functions
    validLabelFunc,
    mergeFunc,
    merge,
    keepFreeInFunc,
    mayMoveUnmarkedToTheLeftFunc,
    labelToList,
    listToLabel,

  )
where

--import Debug.Trace (trace)
import Prelude hiding (and, or, subtract)
import Data.Maybe
import Data.Monoid as Monoid
import qualified Data.List as List
import qualified Data.Set as Set
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

import GLF.Logic.Base
import GLF.Logic.Min (conjunction, and, implication, implies, bottom, bottomOp, negation)
import GLF.Logic.FOL (universal, forall)
import GLF.Rule

quasiuniversal :: Operator
-- ^ The universal operator.
quasiuniversal = binaryBinds "∇" "\\`/" "nabla"

almostall :: Text -> FormulaSpec -> FormulaSpec
-- ^ Specify a quasi-universal formula.
almostall varname f = op quasiuniversal <> opn 1 (variable varname) <> opn 2 f

markOp :: Operator
-- | Marks variables in a label.
markOp = postfixOp "⋆" "*" "star" 1000

mark :: Text -> FormulaSpec
mark varname = op markOp <> opn 1 (variable varname)

consOp :: Operator
consOp = leftAssociativeOp ";" ";" "mathbin{;}" 550

emptyOp :: Operator
emptyOp = nullaryOp "_" "_" "_"

infixl 9 .-
(.-) :: FormulaSpec -> FormulaSpec -> FormulaSpec
theLabel .- var = op consOp <> opn 1 theLabel <> opn 2 var

assumption :: DeductiveRule
assumption =
  DeductiveRule
    { name = asciiName "Assumption",
      premises = [],
      provisos = [],
      conclusion = "formula" .: validLabel "formula",
      action = StartsSubProof False,
      helpText =
        "New proof assumption."
    }

assumptionNewLevel :: DeductiveRule
assumptionNewLevel =
  DeductiveRule
    { name = newLevelName "Assumption",
      premises = [],
      provisos = [],
      conclusion = "formula" .: validLabel "formula",
      action = StartsSubProof True,
      helpText =
        "New proof assumption (at a new level)."
    }

-- | The universal introduction rule.
universalIntroduction :: DeductiveRule
universalIntroduction =
  DeductiveRule
    { name = version1 $ introductionRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = "f" .: ("l" .- variable "v"),
              role = "predicate"
            }
        ],
      provisos = [Not $ FreeIn "v" (Hypotheses [])],
      conclusion = forall "v" "f" .: "l",
      action = NoAction,
      helpText =
        "Apply universal introduction rule"
    }

universalIntroductionNoLabelChange :: DeductiveRule
universalIntroductionNoLabelChange =   DeductiveRule
    { name = version2 $ introductionRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = "f" .: "l",
              role = "predicate"
            }
        ],
      provisos = [Not $ FreeIn "v" (FormulaRef "f")],
      conclusion = forall "v" "f" .: "l",
      action = NoAction,
      helpText =
        "Apply universal introduction rule"
    }

universalEliminationNoChange :: DeductiveRule
universalEliminationNoChange = DeductiveRule
    { name = version1 $ eliminationRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = forall "var" "f" .: "l",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = "f" .: "l",
      action = NoAction,
      helpText =
        "Apply version of universal elimination rule with no substitution or change in the labels"
    }

-- | The universal introduction rule.
universalElimination :: DeductiveRule
universalElimination =
  DeductiveRule
    { name = version2 $ eliminationRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = forall "var" "substitutionBody" .: "l",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = substituteForInAs "term" "var" "substitutionBody" "substitutionResult" .: "l",
      action = NoAction,
      helpText =
        "Apply universal elimination rule"
    }

universalEliminationUnmarkedLabel :: DeductiveRule
universalEliminationUnmarkedLabel =
  DeductiveRule
    { name = version3 $ eliminationRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = forall "x" "substitutionBody" .: "l",
              role = "predicate"
            }
        ],
      provisos = [FreeIn "x" (FormulaRef "substitutionBody")],
      conclusion = substituteForInAs "y" "x" "substitutionBody" "substitutionResult" .: "l" .- variable "y", -- rename variable x to y
      action = NoAction,
      helpText =
        "Apply universal elimination rule adding unmarked var to label"
    }

universalEliminationMarkedLabel :: DeductiveRule
universalEliminationMarkedLabel =
  DeductiveRule
    { name = version4 $ eliminationRuleName universal,
      premises =
        [ Premise
            { hypotheses = [],
              root = forall "x" "substitutionBody" .: "l",
              role = "predicate"
            }
        ],
      provisos = [FreeIn "x" (FormulaRef "substitutionBody")],
      conclusion =
        -- rename variable (we don't need to say variable "y" since
        -- mark already establishes that)
        substituteForInAs "y" "x" "substitutionBody" "result"
          .: "l" .- mark "y",
      action = NoAction,
      helpText =
        "Apply universal elimination rule adding marked var to label"
    }

quasiuniversalIntroduction :: DeductiveRule
quasiuniversalIntroduction =
  DeductiveRule
    { name = version1 $ introductionRuleName quasiuniversal,
      premises =
        [ Premise
            { hypotheses = [],
              root = "f" .: "l" .- mark "var",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = almostall "var" "f" .: "l",
      action = NoAction,
      helpText =
        "Apply quasi-universal introduction rule"
    }

quasiuniversalIntroductionNoLabelChange :: DeductiveRule
quasiuniversalIntroductionNoLabelChange =
  DeductiveRule
    { name = version2 $ introductionRuleName quasiuniversal ,
      premises =
        [ Premise
            { hypotheses = [],
              root = "f" .: "l",
              role = "predicate"
            }
        ],
      provisos = [Not $ FreeIn "var" (FormulaRef "f")],
      conclusion = almostall "var" "f" .: "l",
      action = NoAction,
      helpText =
        "Apply quasi-universal introduction rule without changing the label"
    }

quasiuniversalElimination :: DeductiveRule
quasiuniversalElimination =
  DeductiveRule
    { name = version1 $ eliminationRuleName quasiuniversal,
      premises =
        [ Premise
            { hypotheses = [],
              root = almostall "x" "f" .: "l",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = substituteForInAs "y" "x" "f" "substitutionResult" -- rename variable
               .: "l" .- mark "var",
      action = NoAction,
      helpText =
        "Apply quasi-universal elimination rule"
    }

quasiuniversalEliminationNoLabelChange :: DeductiveRule
quasiuniversalEliminationNoLabelChange =
  DeductiveRule
    { name = version2 $ eliminationRuleName quasiuniversal,
      premises =
        [ Premise
            { hypotheses = [],
              root = almostall "x" "f" .: "l",
              role = "predicate"
            }
        ],
      provisos = [Not $ FreeIn "x" (FormulaRef "f")],
      conclusion = "f" .: "l",
      action = NoAction,
      helpText =
        "Apply quasi-universal elimination rule with no label change"
    }

conjunctionIntro :: DeductiveRule
conjunctionIntro =
  DeductiveRule
    { name = introductionRuleName conjunction,
      premises =
        [ Premise
            { hypotheses = [],
              root = "left" .: "l",
              role = "leftConjunct"
            },
          Premise
            { hypotheses = [],
              root = "right" .: "m",
              role = "rightConjunct"
            }
        ],
      provisos = [],
      conclusion = "left" `and` "right" .: merge "left" "l" "right" "m",
      action = NoAction,
      helpText =
        "Apply conjunction introduction rule"
    }

conjunctionElimLeft :: DeductiveRule
conjunctionElimLeft =
  DeductiveRule
    { name = operatorRuleName conjunction (Elimination LeftS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                "left" `and` anyFormula .: "l",
              role = "conjunction"
            }
        ],
      provisos = [],
      conclusion = "left" .: "l" `keepFreeIn` "left",
      action = NoAction,
      helpText =
        "Apply left conjunction elimination rule"
    }

conjunctionElimRight :: DeductiveRule
conjunctionElimRight =
  DeductiveRule
    { name = operatorRuleName conjunction (Elimination RightS),
      premises =
        [ Premise
            { hypotheses = [],
              root =
                anyFormula `and` "right" .: "l",
              role = "conjunction"
            }
        ],
      provisos = [],
      conclusion = "right" .: "l" `keepFreeIn` "right",
      action = NoAction,
      helpText =
        "Apply right conjunction elimination rule"
    }

-- | The implication introduction rule.
implicationIntro :: DeductiveRule
implicationIntro =
  DeductiveRule
    { name = introductionRuleName implication,
      premises =
        [ Premise
            { hypotheses = [("antecedentHypothesis", "antecedent" .: "l")],
              root = "consequent" .: "m",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = "antecedent" `implies` "consequent" .: merge "consequent" "m" "antecedent" "l",
      action = EndsSubProof,
      helpText =
        "Apply implication introduction rule."
    }

-- | The implication elimination rule.
implicationElim :: DeductiveRule
implicationElim =
  DeductiveRule
    { name = eliminationRuleName implication,
      premises =
        [ Premise
            { hypotheses = [],
              root = "antecedent" `implies` "consequent" .: "l",
              role = "major"
            },
          Premise
          { hypotheses = [],
            root = "antecedent" .: "l" `keepFreeIn` "antecedent",
            role = "minor"
          }
        ],
      provisos = [],
      conclusion = "consequent" .: "l" `keepFreeIn` "consequent",
      action = NoAction,
      helpText =
        "Apply implication elimination rule."
    }

reductioAdAbsurdum :: DeductiveRule
reductioAdAbsurdum =
  DeductiveRule
    { name = PolyName {unicode = "⊥↯", ascii = "reductioAdAbsurdum", latex = "mathrm{reductioAdAbsurdum}"},
      premises =
        [ Premise
            { hypotheses = [("negativeHypothesis", "conclusion" `implies` bottom .: "l")],
              root = "bottom" <> bottom .: "m",
              role = "absurd"
            }
        ],
      provisos = [],
      conclusion = "conclusion" .: "l",
      action = EndsSubProof,
      helpText =
        "Apply reductio ad absurdum rule"
    }

xRule :: DeductiveRule
xRule =
  DeductiveRule
    { name = asciiName "X",
      premises =
        [ Premise
            { hypotheses = [],
              root = "f" .: "l",
              role = "predicate"
            }
        ],
      provisos = [],
      conclusion = "f" .: mayMoveUnmarkedToTheLeft "l",
      action = EndsSubProof,
      helpText =
        "Apply reductio ad absurdum rule"
    }


-- * Labels
--
-- Labels in this systems are ordered sets of variables. Each
-- variable must thus occur only once, and these variables must
-- occur free in the formula they annotate.
validLabel :: FormulaSpec -> FormulaSpec
validLabel f = eval "validLabel" [f]

merge :: Text -> Text -> Text -> Text -> FormulaSpec
merge lf ll rf rl = eval "merge" [named lf, named ll, named rf, named rl]

keepFreeIn :: FormulaSpec -> Text -> FormulaSpec
keepFreeIn f ref =  eval "keepFreeIn" [f, named ref]

mayMoveUnmarkedToTheLeft :: Text -> FormulaSpec
mayMoveUnmarkedToTheLeft labelName = eval "mayMoveUnmarkedToTheLeft" [named labelName]

validLabelFunc :: FormulaFunction
validLabelFunc = (NonDeterministic fwd, chk)
  where
    fwd =
      -- the fwd function is not very useful, for any given formula
      -- there might be several valid labels, the user has to pick
      -- one (which we'll check with the chk function)
      \case
        [f] -> [listToLabel . fmap (, True) . Set.toList $ formulaFreeVariables f]
        _wrongargsyntax -> []
    chk args res =
      case args of
        [f] ->
          let boundVarsFromArg = formulaBoundVariables f
              freeVarsFromResCountMap = countMapFromList $ labelToList res
              freeVarsFromRes = Map.keysSet freeVarsFromResCountMap
              -- we can't allow variable repetitions
              freeVarsFromResNotMoreThanOnce =
                Map.foldMapWithKey
                  (\_ (_, n) -> All (n <= 1))
                  freeVarsFromResCountMap
          in [ res | Set.null (freeVarsFromRes `Set.intersection` boundVarsFromArg)
                   && getAll freeVarsFromResNotMoreThanOnce]
        _wrongargsyntax -> []


mergeFunc :: FormulaFunction
-- - Todo termo de < w > está em < u > ou < v >.
--- - Todos os termos de < u > e < v > estão em < w >.
-- - Se y0 e y1 são duas variáveis de < u > (ou < v >) tais que y0
--   ocorre antes de y1 na lista < u > (ou < v >), ou seja, tais que
--   y0 <u y1 (ou y0 <v y1 ), então y0 <w y1 (y0 ocorre antes de y1
--   na lista < w >).
--- - Se x ∈ F V (A) ∩ F V (B) (x variável livre de A e B) então (x
---   ∈ < u > sse x ∈ < v >), (ou seja, não pode pertencer a uma das
---   listas sem pertencer à outra).
mergeFunc = (NonDeterministic fwd, chk)
  where
    freeVarsCheckOut lset lf rset rf  =
      let lFree = formulaFreeVariables lf
          rFree = formulaFreeVariables rf
      in List.all
           (\v -> v `Set.member` lset == v `Set.member` rset)
           (Set.toList $ lFree `Set.intersection` rFree)
    fwd =
      \case
        [lf, ll, rf, rl] -> [ listToLabel $ mergeLabels llist rset rlist | freeVarsCheckOut lset lf rset rf ]
          where
            llist = labelToList ll
            rlist = labelToList rl
            rset = Set.fromList (fmap fst rlist)
            lset = Set.fromList (fmap fst llist)
        _wrongnumberargs -> []
      where
        mergeLabels l rset r = go l r
          where
            -- all sorts of book-keeping are not necessary because
            -- of the conditions on the labels (we assume the input
            -- labels are well-formed), e.g. we don't have to worry
            -- about duplicate variables in the same label
            go [] [] = []
            go [] rest = rest
            go rest [] = rest
            go ((vx, xMarked) : xs) ((vy, yMarked) : ys)
              | vx == vy = (vx, xMarked) : go xs ys
              | vx `Set.member` rset = (vy, yMarked) : goRight (vx, xMarked) xs ys
              | otherwise = (vx, xMarked) : go xs ((vy, yMarked) : ys)
            goRight (vx, xMarked) xs ys =
              -- what happens when the same variables have different
              -- marks?
              let (beforeX, xAndAfter) = List.span (\(vy, _) -> vy /= vx) ys
              -- tail should be safe since we are sure vx exists in both labels
              in beforeX ++ (vx, xMarked) : go xs (tail xAndAfter)
    chk args res =
      case args of
        [lf, ll, rf, rl] -> [ res | labelVarSet reslist == lset `Set.union` rset
                                 && mergeOrderChecksOut
                                 && freeVarsCheckOut lset lf rset rf]
          where
            llist = labelToList ll
            rlist = labelToList rl
            lset = labelVarSet llist
            rset = labelVarSet rlist
            makeOrder l = Map.fromList $ zipWith (\(vn, _) ix -> (vn, ix)) l [0 :: Int ..]
            lmap = makeOrder llist
            rmap = makeOrder rlist
            reslist = labelToList res
            labelVarSet l = Set.fromList $ fmap fst l
            mergeOrderChecksOut =
              go (-1) (-1) $ fmap fst reslist
              where
                go _ _ [] = True
                go lastInL lastInR (x : xs) =
                  let nextInL = x `Map.lookup` lmap
                      nextInR = x `Map.lookup` rmap
                  in maybe True (lastInL <) nextInL
                  && maybe True (lastInR <) nextInR
                  && go (fromMaybe lastInL nextInL) (fromMaybe lastInR nextInR) xs
        _wrongnumberargs -> []

keepFreeInFunc :: FormulaFunction
-- l contém exatamente as variáveis que ocorrem livres em f
keepFreeInFunc = (Deterministic fwd, chk)
  where
    fwd =
      \case
        [l, f] -> [makeLabel l f]
        _wrongnumberargs -> []
    makeLabel l f =
      let fFreeVars = formulaFreeVariables f
      in listToLabel . filter (\(vn, _) -> vn `Set.member` fFreeVars) $ labelToList l
    chk args res =
      case args of
        [l, f] -> [ res | res == makeLabel l f ]
        _wrongnumberargs -> []

mayMoveUnmarkedToTheLeftFunc :: FormulaFunction
mayMoveUnmarkedToTheLeftFunc = (NonDeterministic fwd, chk)
  where
    fwd =
      \case
        [f] -> [f] -- we may keep the formula as is
        _wrongnumberargs -> []
    chk args res =
      case args of
        [f] -> [ res | findDivergingVariable (labelToList f) (labelToList res) ]
        _wrongnumberargs -> []
      where
        findDivergingVariable [] [] = True
        findDivergingVariable [] _ = False
        findDivergingVariable _ [] = False
        findDivergingVariable ((vx, xMarked) : xs) ((vy, yMarked) : ys) =
          if vx /= vy
          then not yMarked
               && checkRest vy ((vx, xMarked) : xs) ys
          else findDivergingVariable xs ys
        checkRest _reorderedVar [] [] = True
        checkRest _reorderedVar _ [] = True
        checkRest _reorderedVar [] _ = True
        checkRest reorderedVar ((vx, xMarked) : xs) ((vy, yMarked) : ys) =
          if vx == vy
          then
            (xMarked == yMarked)
            && checkRest reorderedVar xs ys
          else -- different vars
            vx == reorderedVar
             -- if vx is the reordered var, then we just need the
             -- rest to be the same
            && xs == (vy, yMarked) : ys

listToLabel :: [(Text, Bool)] -> Formula
listToLabel =
  foldr
    (\(vn, marked) acc ->
       Formula consOp [acc, if marked then markedVar vn else Atom vn])
    (Formula emptyOp [])
  where
    markedVar vn = Formula markOp [Atom vn]

labelToList :: Formula -> [(Text, Bool)]
labelToList = go
  where
    go Atom{} = []
    go (Formula theOp [theTail, theHead]) =
      if theOp == consOp
      then varInfo ++ go theTail
      else []
      where
        varInfo =
          case theHead of
            Atom vn -> [(vn, False)]
            Formula headOp [Atom vn]
              | headOp == markOp -> [(vn, True)]
            _wrongelementsyntax -> []
    go (Formula _ _) = []

countMapFromList :: Ord k => [(k, v)] -> Map k (v, Int)
countMapFromList = Map.fromListWith (\(v, n) (_, n') -> (v, n + n'))
  . fmap (\(k, v) -> (k, (v, 1)) )
