-- |
-- Module      : GLF.Logic.Zp
-- Description : Zₚ system
-- Copyright   : (c) bruno cuconato, 2020-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Zp
  ( -- * Rules
  s1,
  s2,
  f1,
  f2,
  v1,
  v2,
  emptyRule,
  e0,
  e1,
  e2,
  leqRule,
  le1,
  le2,
  cmp0,
  cmp1,
  cmp2,
  cmp3,
  assumption,

  -- * Operators and predicates
  nonColinearPred,
  isJordanPred,
  intersectionOp,
  unionOp,
  pointType,
  segmentType,
  faceType,
  volumeType,
  segmentCons,
  inequality,
  isClosedPred,
  leqOp,
  leOp,
  compatibility,
  emptyOp,
  label,
  )
  where

import Prelude hiding ((/=))
import GLF.Logic.Base
import GLF.Rule
import GLF.Logic.FOL (assumption)

s1, s2, f1, f2, v1, v2 :: DeductiveRule
s1 = rule (polyname "s₁" "s1" "empty{}s_{1}")
     [ premise "left" ("m" .: point)
     , premise "right" ("n" .: point)
     , premise "inequality" ("m" /= "n")
     , concludes (twoPointSegment "m" "n")
     ]
s2 = rule (polyname "s₂" "s2" "empty{}s_{2}")
     [ premise "left" ("p" .: segment)
     , premise "right" ("q" .: segment)
     , premise "non-colinearity" (nonColinear "p" "q")
     , premise "pointIntersection" ("p" `intersection` "q" .: point)
     , concludes (polySegment "p" "q")
     ]
f1 = rule (polyname "f₁" "f1" "empty{}f_{1}")
     [ premise "left" ("p" .: segment)
     , premise "right" ("q" .: segment)
     , premise "jordanity" (isJordan ("p" `union` "q"))
     , concludes ("p" `union` "q" .: face)
     ]
f2 = rule (polyname "f₂" "f2" "empty{}f_{2}")
     [ premise "left" ("p" .: face)
     , premise "right" ("q" .: face)
     , premise "segmentIntersection" ("p" `intersection` "q" .: segment)
     , concludes ("p" `union` "q" .: face)
     ]
v1 = rule (polyname "v₁" "v1" "empty{}v_{1}")
     [ premise "left" ("p" .: face)
     , premise "right" ("q" .: face)
     , premise "isClosed" (isClosed ("p" `union` "q"))
     , concludes ("p" `union` "q" .: volume)
     ]
v2 = rule (polyname "v₂" "v2" "empty{}v_{2}")
     [ premise "left" ("p" .: volume)
     , premise "right" ("q" .: volume)
     , premise "faceIntersection" ("p" `intersection` "q" .: face)
     , concludes ("p" `union` "q" .: volume)
     ]


emptyRule :: DeductiveRule
emptyRule = rule (polyname "ε" "e" "varepsilon")
            [ concludes (empty .: "ty")
            , help "The empty object may be considered of any type."
            ]

e0, e1, e2 :: DeductiveRule
e0 = rule (polyname "ε₀" "e0" "varepsilon_{0}")
     [ premise "object" ("p" .: "ty")
     , concludes ("p" `leq` "p")
     ]
e1 = rule (polyname "ε₁" "e1" "varepsilon_{1}")
     [ premise "left" ("p" .: "ty")
     , premise "right" ("q" .: "ty")
     , premise "compatibility" ("p" `compatible` "q")
     , premise "compatibility" ("q" /= empty)
     , concludes ("p" `le` "p" `union` "q")
     ]
e2 = rule (polyname "ε₂" "e2" "varepsilon_{2}")
     [ premise "left" ("p" .: "ty")
     , premise "right" ("q" .: "ty")
     , premise "compatibility" ("p" `compatible` "q")
     , premise "compatibility" ("p" /= empty)
     , concludes ("q" `le` "p" `union` "q")
     ]

leqRule :: DeductiveRule
leqRule = rule (polyname "⪯" "leq" "preceq")
          [ premise "left1" ("p1" .: "ty")
          , premise "left2" ("p2" .: "ty")
          , premise "right1" ("q1" .: "ty")
          , premise "right2" ("q2" .: "ty")
          , premise "leq1" ("p1" `leq` "q1")
          , premise "leq2" ("p2" `leq` "q2")
          , premise "compatibilityp" ("p1" `compatible` "p2")
          , premise "compatibilityq" ("q1" `compatible` "q2")
          , concludes ("p1" `union` "p2" `leq` "q1" `union` "q2")
          ]


le1, le2 :: DeductiveRule
le1 = rule (polyname "≺₁" "le1" "prec_{1}")
      [ premise "left1" ("p1" .: "ty")
      , premise "left2" ("p2" .: "ty")
      , premise "right1" ("q1" .: "ty")
      , premise "right2" ("q2" .: "ty")
      , premise "le" ("p1" `le` "q1")
      , premise "leq" ("p2" `leq` "q2")
      , premise "compatibilityp" ("p1" `compatible` "p2")
      , premise "compatibilityq" ("q1" `compatible` "q2")
      , concludes ("p1" `union` "p2" `le` "q1" `union` "q2")
      ]
le2 = rule (polyname "≺₂" "le2" "prec_{2}")
      [ premise "left1" ("p1" .: "ty")
      , premise "left2" ("p2" .: "ty")
      , premise "right1" ("q1" .: "ty")
      , premise "right2" ("q2" .: "ty")
      , premise "leq" ("p1" `leq` "q1")
      , premise "le" ("p2" `le` "q2")
      , premise "compatibilityp" ("p1" `compatible` "p2")
      , premise "compatibilityq" ("q1" `compatible` "q2")
      , concludes ("p1" `union` "p2" `le` "q1" `union` "q2")
      ]

cmp0, cmp1, cmp2, cmp3 :: DeductiveRule
cmp0 = rule (polyname "?₀" "?0" "?_{0}")
       [ premise "segment" (twoPointSegment "p1" "p2")
       ,  concludes ("p1" `compatible` "p2")
       ]

cmp1 = rule (polyname "?₁" "?1" "?_{1}")
       [ premise "segment" (polySegment "s1" "s2")
       , concludes ("s1" `compatible` "s2")
       ]
cmp2 = rule (polyname "?₂" "?2" "?_{2}")
       [ premise "face" ("p" `union` "q" .: face)
       , concludes ("p" `compatible` "q")
       ]
cmp3 = rule (polyname "?₃" "?3" "?_{3}")
       [ premise "volume" ("p" `union` "q" .: volume)
       , concludes ("p" `compatible` "q")
       ]

empty, point, segment, face, volume :: FormulaSpec
empty = op emptyOp
point = op pointType
segment = op segmentType
face = op faceType
volume = op volumeType

intersection, union :: FormulaSpec ->  FormulaSpec -> FormulaSpec
f `intersection` g = op intersectionOp <> opn 1 f <> opn 2 g
f `union` g = op unionOp <> opn 1 f <> opn 2 g

leq :: FormulaSpec -> FormulaSpec -> FormulaSpec
f `leq` g = op leqOp <> opn 1 f <> opn 2 g

le :: FormulaSpec -> FormulaSpec -> FormulaSpec
f `le` g = op leOp <> opn 1 f <> opn 2 g

compatible :: FormulaSpec -> FormulaSpec -> FormulaSpec
compatible f g = op compatibility <> opn 1 f <> opn 2 g

twoPointSegment, polySegment :: FormulaSpec -> FormulaSpec -> FormulaSpec
twoPointSegment f g = op segmentCons <> opn 1 f <> opn 2 g .: segment
polySegment f g = op segmentCons <> opn 1 f <> opn 2 g .: segment

infix 5 /=
(/=) :: FormulaSpec -> FormulaSpec -> FormulaSpec
a /= b = op inequality <> opn 1 a <> opn 2 b

nonColinear :: FormulaSpec -> FormulaSpec -> FormulaSpec
nonColinear f g = op nonColinearPred <> opn 1 f <> opn 2 g

isJordan :: FormulaSpec -> FormulaSpec
isJordan f = op isJordanPred <> opn 1 f

isClosed :: FormulaSpec -> FormulaSpec
isClosed f = op isClosedPred <> opn 1 f

pointType :: Operator
pointType = nullaryOp "𝓹" "p" "mathfrak{p}"

segmentType :: Operator
segmentType = nullaryOp "𝐬" "s" "mathfrak{s}"

faceType :: Operator
faceType = nullaryOp "𝐟" "f" "mathfrak{f}"

volumeType :: Operator
volumeType = nullaryOp "𝐯" "v" "mathfrak{v}"

segmentCons :: Operator
segmentCons = naryOp "Segment" "Segment" "operatorname{Segment}" 2

inequality :: Operator
inequality = binaryOp "≠" "/=" "neq" 540

nonColinearPred :: Operator
nonColinearPred = naryOp "NonColinear" "NonColinear" "operatorname{NonColinear}" 2

isClosedPred :: Operator
isClosedPred = naryOp "IsClosed" "IsClosed" "operatorname{IsClosed}" 2

intersectionOp :: Operator
intersectionOp = binaryOp "∩" "/\\" "cap" 800

unionOp :: Operator
unionOp = binaryOp "∪" "\\/" "cup" 800

leqOp :: Operator
leqOp = binaryOp "⪯" "<=" "preceq" 600

leOp :: Operator
leOp = binaryOp "≺" "<" "prec" 600

compatibility :: Operator
compatibility = binaryOp "?" "?" "empty{}?" 650

emptyOp :: Operator
emptyOp = nullaryOp "ɛ" "_" "varepsilon"

isJordanPred :: Operator
isJordanPred = naryOp "IsJordan" "IsJordan" "operatorname{IsJordan}" 2

-- -- de Zolt proof

-- t0, t1 :: DeductiveRule
-- t0 =
--   DeductiveRule
--     { name = polyname "t₀" "t0" "t_{0}",
--       premises =
--         [ Premise
--             { hypotheses = [],
--               root = "p" .: "ty",
--               role = "object"
--             },
--           Premise
--             { hypotheses = [],
--               root = "p" /= empty,
--               role = "nonEmpty"
--             }
--         ],
--       provisos = [],
--       conclusion = empty `truncationOf` "p",
--       action = NoAction,
--       helpText = ""
--     }
-- t1 =
--   DeductiveRule
--     { name = polyname "t₁" "t1" "t_{1}",
--       premises =
--         [ Premise
--             { hypotheses = [],
--               root = "p" .: "ty",
--               role = "first"
--             },
--           Premise
--             { hypotheses = [],
--               root = "q" .: "ty",
--               role = "second"
--             },
--           Premise
--             { hypotheses = [],
--               root = "r" .: "ty",
--               role = "third"
--             },
--           Premise
--             { hypotheses = [],
--               root = "p" `compatible` "r",
--               role = "firstCmp"
--             },
--           Premise
--             { hypotheses = [],
--               root = "q" `compatible` "r",
--               role = "secondCmp"
--             },
--           Premise
--             { hypotheses = [],
--               root = "p" `truncationOf` "q",
--               role = "truncation"
--             }
--         ],
--       provisos = [],
--       conclusion = ("p" `union` "r") `truncationOf` ("q" `union` "r"),
--       action = NoAction,
--       helpText = ""
--     }


-- truncationOf :: FormulaSpec -> FormulaSpec -> FormulaSpec
-- truncationOf f g = op truncationOp <> opn 1 f <> opn 2 g

-- truncationOp :: Operator
-- truncationOp = naryOp "TruncationOf" "TruncationOf" "operatorname{TruncationOf}" 2
