-- |
-- Module      : GLF.Logic.Intuitionistic
-- Description : Intuitionistic logic definitions
-- Copyright   : (c) bruno cuconato, 2019-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Intuitionistic
  ( -- * Operators and formula specifiers
    bottom,
    bottomOp,
    negationOf,
    negation, not,
    implication,
    implies,
    conjunction,
    and,
    disjunction,
    or,

    -- * Rules

    -- ** Declarative
    implicationIntro,
    implicationElim,
    andIntro,
    andElimLeft,
    andElimRight,
    orIntroLeft,
    orIntroRight,
    orElim,
    exFalsoQuodLibet,

    -- ** Backwards Natural Deduction (compiled)
    implicationIntroBND,
    implicationElimBND,
    andIntroBND,
    andElimLeftBND,
    andElimRightBND,
    orIntroLeftBND,
    orIntroRightBND,
    orElimBND,
    exFalsoQuodLibetBND,
    hypothesisDiscard,

    -- ** Fitch-style deduction (compiled)
    implicationIntroFw,
    implicationElimFw,
    andIntroFw,
    andElimRightFw,
    andElimLeftFw,
    orIntroLeftFw,
    orIntroRightFw,
    orElimFw,
    exFalsoQuodLibetFw,
    assumptionFw,
    assumptionSameLevelFw,
    qedFw,

    -- * Tactic
    tactic,

    -- * Parser
    parse,

    -- * Helpers
    subFormulas,
  )
where

import GLF.Logic.Base
import GLF.Logic.Min
import GLF.Rule
import Prelude hiding (and, or, not)

exFalsoQuodLibet :: DeductiveRule
exFalsoQuodLibet =
  DeductiveRule
    { name = PolyName {ascii = "exFalsoQuodLibet", unicode = "⊥-💥", latex = "mathrm{exFalsoQuodLibet}"},
      premises =
        [ Premise
            { hypotheses = [],
              root = named "bottom" <> bottom,
              role = "bottom"
            }
        ],
      provisos = [],
      conclusion = named "result",
      helpText =
        "Apply ex falso quod libet rule\n\n\
        \   ⊥ \n\
        \ ----- \n\
        \   A"
    }

exFalsoQuodLibetBND :: CompiledRule
exFalsoQuodLibetBND = makeBNDRule exFalsoQuodLibet

exFalsoQuodLibetFw :: CompiledRule
exFalsoQuodLibetFw =
  makeFitchRule
    NoAction
    exFalsoQuodLibet
