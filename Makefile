CABAL_COMMON_FLAGS=-j10 --ghc-options='+RTS -A512m -n2m -RTS'
CABAL_DEV_FLAGS=$(CABAL_COMMON_FLAGS) --disable-optimization  --ghc-options=-fdefer-typed-holes
CABAL_TEST_FLAGS=--test-show-details=direct

.PHONY: dev
dev:
	cabal v2-build $(CABAL_DEV_FLAGS) lib:glf exe:glf
	cabal v2-run $(CABAL_DEV_FLAGS) -- glf generate-client-api --output=elm-frontend/src/  # will build and output file we need
	cd elm-frontend && $(MAKE) $@

.PHONY: prod
prod:
	cabal v2-run $(CABAL_COMMON_FLAGS) -- glf generate-client-api --output elm-frontend/src/  # will build and output file we need
	cabal v2-install exe:glf --installdir='bin' --install-method=copy --overwrite-policy=always $(CABAL_COMMON_FLAGS)
	git rev-parse HEAD > bin/commit.hash
	cd elm-frontend && $(MAKE) $@

.PHONY: test
test:
	cabal v2-test $(CABAL_DEV_FLAGS) $(CABAL_TEST_FLAGS)
	cd elm-frontend && $(MAKE) $@

.PHONY: test-parser
test-parser:
	cabal v2-test $(CABAL_DEV_FLAGS) $(CABAL_TEST_FLAGS) --test-option=--match --test-option="* Parser tests"

.PHONY: test-strategies
test-strategies:
	cabal v2-test $(CABAL_DEV_FLAGS) $(CABAL_TEST_FLAGS) --test-option=--match --test-option="** Proof Strategies"

.PHONY: docs
docs:
	cabal v2-haddock $(CABAL_DEV_FLAGS) --haddock-hyperlink-source

.PHONY: server
server:
	cabal v2-run $(CABAL_DEV_FLAGS) -- glf serve --log-level=3 # for development
