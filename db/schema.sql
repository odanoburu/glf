DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users (
    user_name    TEXT PRIMARY KEY,
    user_email   TEXT,
    salt         TEXT NOT NULL,
    hashpass     TEXT NOT NULL,
    is_temporary BOOLEAN NOT NULL,
    created_on   timestamp without time zone NOT NULL
);

DROP TABLE IF EXISTS languages CASCADE;
CREATE TABLE languages (
    _id SERIAL PRIMARY KEY,
    -- the data is stored as JSON
    definitions TEXT NOT NULL,
    digest CHAR(56) NOT NULL UNIQUE
);

DROP TABLE IF EXISTS proof CASCADE;
CREATE TABLE proof (
    _id SERIAL PRIMARY KEY,
    in_system TEXT NOT NULL,
    owner TEXT REFERENCES users ON DELETE CASCADE, -- when user is deleted their proofs are deleted too
    language_id integer REFERENCES languages ON DELETE CASCADE
);

DROP TABLE IF EXISTS formula CASCADE;
CREATE TABLE formula (
    _id SERIAL PRIMARY KEY,
    _text TEXT NOT NULL,
    proof integer NOT NULL REFERENCES proof ON DELETE CASCADE,
    UNIQUE(_text, proof)
);

DROP TABLE IF EXISTS ruleapp CASCADE;
CREATE TABLE ruleapp (
    _id SERIAL PRIMARY KEY,
    rule TEXT NOT NULL,
    proof integer NOT NULL REFERENCES proof ON DELETE CASCADE,
    formula integer NOT NULL REFERENCES formula
);

DROP TABLE IF EXISTS assumption CASCADE;
CREATE TABLE assumption (
    _id SERIAL PRIMARY KEY,
    formula integer NOT NULL REFERENCES formula,
    proof integer NOT NULL REFERENCES proof ON DELETE CASCADE,
    introducedBy integer NOT NULL REFERENCES ruleapp ON DELETE CASCADE,
    dischargedBy integer REFERENCES ruleapp
);

DROP TABLE IF EXISTS goal CASCADE;
CREATE TABLE goal (
    _id SERIAL PRIMARY KEY,
    formula integer NOT NULL REFERENCES formula,
    proof integer NOT NULL REFERENCES proof ON DELETE CASCADE
);

DROP TABLE IF EXISTS premise CASCADE;
CREATE TABLE premise (
    _id SERIAL PRIMARY KEY,
    conclusion_of integer NOT NULL REFERENCES ruleapp ON DELETE CASCADE,
    premise_of integer NOT NULL REFERENCES ruleapp ON DELETE CASCADE
);
