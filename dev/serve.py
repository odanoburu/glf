# modify Python HTTP server to serve index.html whenever path does
# not exists (i.e., let Javascript handle Not Found messages, but
# also let single-page app work better)

import http.server
import socketserver
import argparse
import os
import os.path

def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

def init_handler_class(directory):
    class MyHandler(http.server.SimpleHTTPRequestHandler):
        def __init__(self, request, client_address, server):
            super().__init__(request, client_address, server, directory=directory)

        def translate_path(self, path):
            path = super().translate_path(path)
            index_path = os.path.join(directory, 'index.html')
            if os.path.exists(path) or not os.path.exists(index_path):
                print(path)
                return path
            else:
                return index_path

    return MyHandler

def start_server(host=None, port=None, directory=None):
    if host is None:
        host = "0.0.0.0"
    if port is None:
        port = 8000
    if directory is None:
        directory = os.getcwd()
    Handler = init_handler_class(directory)
    with socketserver.TCPServer((host, port), Handler) as httpd:
        print(f"Serving {directory} at port {port}")
        try:
            httpd.serve_forever()
        finally:
            httpd.server_close()

if __name__ == "__main__":
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", metavar="HOST", help="Host address", type=str)
    parser.add_argument("--port", metavar="PORT", help="Port to listen to", type=int)
    parser.add_argument("--directory", metavar="DIR", help="Directory to serve", type=dir_path)
    parser.set_defaults(func=lambda args: start_server(host=args.host, port=args.port, directory=args.directory))
    # parse
    if len(sys.argv) <= 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    args = parser.parse_args()
    # run
    args.func(args)
