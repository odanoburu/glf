module GLF
  ( DeductiveRule(..),
    System (..),
    runApp,
    runAppWithConfig,
  )
where

import GLF.Base
import GLF.GraphBackend
import GLF.Rule
import GLF.System

runApp :: Maybe FilePath -> Systems -> App IO a -> IO a
runApp mfp syss ac =
  readConfig mfp
    >>= \cfg -> runAppWithConfig cfg syss ac

runAppWithConfig :: Config -> Systems -> App IO a -> IO a
runAppWithConfig cfg syss ac = do
  conns <- initConnectionPool cfg
  let env = Env conns syss
      st = initializeState env
  runApp' env st ac
