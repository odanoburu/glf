{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module GLF.Base
  ( App,
    DBio,
    Fallible(..),
    Env (..),
    withConnection,
    withConnectionEnv,
    AppState (..),
    LogLevel(..),
    initializeState,
    Config (..),
    DBConfig(..),
    liftIO,
    logMsg,
    logErr,
    putErrLn,
    readConfig,
    runApp',
    lookupParser,
    addParser,
    decodeBase64,
    encodeBase64
  )
where

--import Debug.Trace (trace)
import Control.Monad.RWS
import Data.Aeson (FromJSON (..), ToJSON (..))
import qualified Data.Aeson as JSON
import qualified Data.ByteArray as ByteArray
import qualified Data.ByteArray.Encoding as ByteArrayEnc
import qualified Database.PostgreSQL.Simple as DB
import Data.Default (Default (..))
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (fromMaybe)
import Data.Pool (Pool)
import qualified Data.Pool as Pool
import GHC.Generics (Generic)
import GLF.System
import GLF.Logic.Base
import GLF.Logic.Parser
import System.Directory
  ( XdgDirectory (XdgConfig),
    canonicalizePath,
    createDirectoryIfMissing,
    doesFileExist,
    getXdgDirectory,
  )
import System.FilePath (takeDirectory, (</>))
import System.IO (hPutStrLn, stderr)

logMsg, logErr, putErrLn :: String -> IO ()

-- | Log a message
logMsg = putErrLn -- probably want to log it somewhere else other
-- than standard error

-- | Log an error message
putErrLn = hPutStrLn stderr
logErr = putErrLn


type DBio a = (DB.Connection -> IO a)

-- | The application's global environment
data Env = Env
  { -- | Connections to the database backend
    connPool :: Pool DB.Connection,
    -- | Collection of deductive systems
    systems :: Systems
  }

data AppState = AppState
  { parsers :: Map (Text, LanguageId) {- system name, language ID -} LogicParser }

type App m = RWST Env [Text] AppState m

withConnection :: DBio a -> App IO a
withConnection ac = do
  pool <- asks connPool
  liftIO $ Pool.withResource pool ac

withConnectionEnv :: Env -> DBio a -> IO a
withConnectionEnv Env{connPool} ac = liftIO $ Pool.withResource connPool ac

-- | Log level of glf server
data LogLevel
  = -- | No logging
    None
  | -- | Detailed logging
    Dev
  | -- | Minimal logging
    Prod
  deriving stock (Eq, Generic, Read, Show)
  deriving anyclass (FromJSON, ToJSON)

data DBConfig = DBConfig
  { dbPort :: Int
  , host :: String
  }
  deriving stock (Generic)
  deriving anyclass (FromJSON, ToJSON)

instance Default DBConfig where
  def = DBConfig {dbPort = 5432, host = "0.0.0.0"}

-- | GLF configuration is the configuration of its components. See 'readConfig'.
data Config = Config
  { port :: Int
  , dbConfig :: DBConfig
  , logLevel :: LogLevel
  }
  deriving stock (Generic)
  deriving anyclass (FromJSON, ToJSON)

instance Default Config where
  def = Config {port = 2487, dbConfig = def, logLevel = Dev}

getDefaultConfigDir :: IO FilePath

-- | Return glf configuration directory. (glf configuration is placed
-- by default in the directory specified by the freedesktop project.)
getDefaultConfigDir = getXdgDirectory XdgConfig "glf"

readConfig' :: FilePath -> IO (Maybe Config)
-- ^ Read GLF configuration.
-- If there are no configuration files available when glf starts, the
-- program will create default configurations, which the administrator
-- can then edit.
readConfig' cfgDir =
  readCfgFile (cfgDir </> glfCfgFileName)
  where
    glfCfgFileName = "glf.cfg"
    readCfgFile :: forall a. (Default a, FromJSON a, ToJSON a) => FilePath -> IO (Maybe a)
    readCfgFile cfgFile = do
      cfgFileExists <- liftIO $ doesFileExist cfgFile
      if cfgFileExists
        then do
          eitherCfg <- liftIO $ JSON.eitherDecodeFileStrict' cfgFile
          case eitherCfg of
            Right cfg -> return $ Just cfg
            Left errorMsg ->
              error
                ( unlines
                    [ "Failed to read configuration file at " ++ cfgFile,
                      "with error message:",
                      errorMsg
                    ]
                ) >> return Nothing
        else
          liftIO $
            writeDefaultCfg
              >> logMsg
                ( unlines
                    [ "No configuration file found at " ++ cfgFile,
                      "Default configuration has been written to help editing"
                    ]
                )
                >> return Nothing
      where
        writeDefaultCfg = do
          createDirectoryIfMissing True $ takeDirectory cfgFile
          cfgFile `JSON.encodeFile` (def :: a)

readConfig :: Maybe FilePath -> IO Config
readConfig mConfigDir' = do
  let mConfigDir = canonicalizePath <$> mConfigDir'
  configDir <- fromMaybe getDefaultConfigDir mConfigDir
  mConfig <- readConfig' configDir
  case mConfig of
    Just config -> do
      putErrLn $ "> Read configuration from " ++ configDir
      return config
    Nothing -> return def

runApp' :: Env -> AppState -> App IO a -> IO a

-- | Read the GLF configuration and then run the 'App' function with
-- it
runApp' env st app = runRWST app env st >>= \(res, _st', _w) -> return res

noLanguageId :: LanguageId
noLanguageId = LanguageId (-1)

initializeState :: Env -> AppState
initializeState Env {systems = Systems { unSystems }} =
  AppState (Map.fromList
            . concatMap
            (\System{systemName, extensible, operators} ->
                [((systemName, noLanguageId), parseLogic atomLabel operators) | not extensible])
            $ Map.elems unSystems)

lookupParser :: Text -> Maybe LanguageId -> Map (Text, LanguageId) LogicParser -> Maybe LogicParser
lookupParser systemName mLangId =
  Map.lookup (systemName, fromMaybe noLanguageId mLangId)

addParser :: Text -> Maybe LanguageId -> LogicParser -> Map (Text, LanguageId) LogicParser -> Map (Text, LanguageId) LogicParser
addParser sysName mLangId = Map.insert (sysName, fromMaybe noLanguageId mLangId)

encodeBase64 :: (ByteArray.ByteArrayAccess bin, ByteArray.ByteArray bout) => bin -> bout
encodeBase64 = ByteArrayEnc.convertToBase ByteArrayEnc.Base64

decodeBase64 :: (ByteArray.ByteArrayAccess bin, ByteArray.ByteArray bout) => bin -> Either String bout
decodeBase64 = ByteArrayEnc.convertFromBase ByteArrayEnc.Base64
