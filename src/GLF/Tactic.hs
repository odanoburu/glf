module GLF.Tactic
  ( Strategy (..),
    Tactic (..),
    -- public API to Tactic
    --ruleTactic,
    many,
    some,
    try,
    oneOf,
    andThen,
    orElse,
--    noImmediateCycle,
    noPrelude,
    --closing,
  )
where

import Data.Text (Text)
import GLF.Rule

-- | Data type for proof strategies.
data Strategy = Strategy
  { -- | This tactic gets run once before the main
    -- tactic
    prelude :: Tactic,
    -- | The main proof strategy which is applied as
    -- many times as possible (until all goals are
    -- proved or there is nothing more to try)
    strategy :: Tactic
  }
  deriving stock (Show)

noPrelude :: Tactic -> Strategy
-- ^ Create strategy with no prelude
noPrelude t = Strategy {prelude = succeed, strategy = t}

-- TODO: use FormulaReference DSL to specify arguments (and in If condition
-- too?) probably needs an extension though, dealing with rule nodes,
-- hypotheses, and such…
--- NOTE: 'FunctionQuery' and 'Text' cypher queries are likely to be substituted
--- by the 'FormulaReference' domain-specific mini-language in the near future.

-- | Data type for proof 'Tactic' specification.
data Tactic
  = RuleTactic DeductiveRule -- if rule has arguments, all are tried
  | -- | Tactic that applies rule with the arguments enumerated by
    -- the text Cypher query.
    --
    -- The query must return a row of results for each possible
    -- combination of arguments; those combinations are tried one by
    -- one automatically until one succeeds. Backtracking is done
    -- automatically.
    --
    -- NOTE: this interface is likely to change in the future.
    RuleWithArgs DeductiveRule Text
  -- | -- | Choose tactic to apply according to the result of 'FunctionQuery'
  --   --
  --   -- NOTE: this interface is likely to change in the future.
  --   If FunctionQuery Tactic Tactic
  | -- | The tactic that always fails without doing anything
    TacticFail
  | -- | Apply first tactic, if unsuccessful apply the
    -- second one
    OrElse Tactic Tactic
  | -- | Apply proof tactic zero or more times without failing
    Many Tactic
  | -- | Apply first tactic, if successful apply the second
    -- one, else fail.
    Then Tactic Tactic
  deriving stock (Show)

-- ruleTactic :: DeductiveRule -> Tactic
-- -- ^ Create 'Tactic' from input 'Rule'.
-- --
-- -- If the rule has enumerable arguments these are tried one-by-one by the tactic
-- -- runner. The user may use employ the 'AtomicWithArgs' constructor directly to
-- -- override the arguments to be used in the proof attempt.
-- --
-- -- Raises an error when an interactive-only rule is supplied as the input
-- -- argument.
-- ruleTactic r@DeductiveRule {arguments} =
--   case arguments of
--     NoArguments -> RuleTactic r
--     Query _ q -> RuleWithArgs r q
--     UserInput {} -> error "Query must not have interactive arguments"

fail :: Tactic
-- ^ The tactic that always fails
fail = TacticFail

succeed :: Tactic
-- ^ The tactic that always succeeds
succeed = Many GLF.Tactic.fail

orElse :: Tactic -> Tactic -> Tactic
-- ^ Apply first tactic, if unsuccessful apply the second one
orElse = OrElse

oneOf :: [Tactic] -> Tactic
-- ^ Attempt application of the input tactics, stopping at the first
-- successful one
oneOf = foldr orElse TacticFail

many :: Tactic -> Tactic
-- ^ Apply proof tactic zero or more times without failing
many = Many

andThen :: Tactic -> Tactic -> Tactic
-- ^ Apply first tactic, if successful apply the second one, else fail.
andThen = Then

try :: Tactic -> Tactic
-- ^ Attempt to apply input tactic, succeed regardless of the result
try t = t `orElse` succeed

some :: Tactic -> Tactic
-- ^ Attempt to apply proof tactic one or more times (fails otherwise)
some t = t `andThen` many t

-- noImmediateCycle :: Tactic -> Tactic
-- noImmediateCycle = If cycles GLF.Tactic.fail
--   where
--     cycles =
--       FunctionQuery
--         "immediateCycle"
--         ( Text.concat
--             [ "MATCH (node:Rule)-[db:DerivedBy]->(goal:Goal)-[:Derives]->(goalFormula:Formula)\n",
--               "WHERE id(goal) = $",
--               goalNodeName,
--               "\nWITH goal, goalFormula, db, node\n",
--               "MATCH (goalFormula)<-[:Derives]-(parent:Rule)-[db2:DerivedBy]->(node)\n",
--               "WHERE all(hyp IN coalesce(db2.introducesHyps, []) WHERE (hyp IN goal.hyps)) AND all(hyp IN coalesce(db.introducesHyps, []) WHERE (hyp IN goal.hyps))\n",
--               "RETURN 1 AS result"
--             ]
--         )

-- closing :: DeductiveRule -> Tactic
-- -- ^ (Backwards Natural Deduction only) Apply the input rule if and
-- -- only if it will achieve the current goal
-- closing r = RuleWithArgs (makeBNDRule r) (bndRuleReachesGoalQuery r)
