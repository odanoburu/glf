{-# LANGUAGE StrictData #-}
module GLF.System
  ( Systems (..),
    System (..),
    SystemWithParser(..),
    systemWithLanguageParser,
    SystemKind (..),
    DeductionInput (..),
    noLanguageDeductionInput,
    getRule,
    getRuleSystem,
--    getStrategy,
    getSystem,
    systemGetRule,
  )
where

import Data.List (find)
import Data.Map (Map)
import qualified Data.Map as Map
import GLF.Logic.Base
import GLF.Logic.Parser
import GLF.Rule
--import GLF.Tactic (Strategy)
import Prelude hiding (min)

-- | Define the style of deduction used by the system. This affects
-- how the deductions are carried out in all interfaces.
data SystemKind
  = -- | Fitch-style deductive system (forward proofs)
    FitchSys
  | -- | Natural-Deduction-style system (backward proofs)
    BackNDSys
  | -- | Tableaux-style system
    TableauxSys
  deriving stock (Show)

-- | Input to a new deduction; a set of assumptions and of
-- goals. Non-emptiness may be required by different kinds of
-- systems (see 'SystemKind').
data DeductionInput = DeductionInput
  { languageInput :: [Text],
    assumptionsInput :: [Text],
    goalsInput :: [Text]
  }

noLanguageDeductionInput :: [Text] -> [Text] -> DeductionInput
noLanguageDeductionInput assumptionsInput goalsInput =
  DeductionInput {languageInput = [], assumptionsInput, goalsInput}

-- | Define a new deductive system
data System = System
  { -- | System name
    systemName :: Text,
    -- | System operators
    operators :: [Operator],
    -- | Whether the system can be extended with a non-logical language
    extensible :: Bool,
    -- | Rule used to introduce a proof's initial assumptions
    assumptionRule :: DeductiveRule,
    -- | System rules
    rules :: [DeductiveRule],
    functions :: [(Text, FormulaFunction)],
    -- -- | Named proof strategies for the system
    -- strategies :: [(Text, Strategy)],
    -- | Style of deduction system uses
    kind :: SystemKind,
    -- | System description (e.g., when it was defined and by whom)
    description :: Text
  }

newtype SystemWithParser = SystemWithParser {unSystemParser :: (System, LogicParser)}

-- | Collection of deductive systems available
newtype Systems = Systems {unSystems :: Map Text System}

-- | Find a system among the collection of systems
getSystem :: Systems -> Text -> Maybe System
getSystem (Systems systems) sysName = Map.lookup sysName systems


-- | Find a rule from a given system among a collection of systems and return it
getRule :: Systems -> Text -> Text -> Maybe DeductiveRule
-- TODO: check that names are unique, or else…
getRule systems sysName theRuleName = snd <$> getRuleSystem systems sysName theRuleName

-- | Find a rule from a given system in the collection of systems and return both
getRuleSystem :: Systems -> Text -> Text -> Maybe (System, DeductiveRule)
getRuleSystem systems sysName theRuleName =
  getSystem systems sysName >>= \sys -> (sys,) <$> systemGetRule sys theRuleName

-- | Lookup a rule in a system
systemGetRule :: System -> Text -> Maybe DeductiveRule
systemGetRule System{rules} theRuleName = find (\DeductiveRule {name} -> theRuleName `samePolyName` name) rules

-- -- | Find a strategy from a given system in the collection of systems
-- getStrategy :: Systems -> Text -> Maybe Text -> Maybe Strategy
-- getStrategy systems sysName mStgName = do
--   system <- getSystem systems sysName
--   case (strategies system, mStgName) of
--     ([], _) -> Nothing
--     ([(_, stg)], _) -> Just stg
--     (stgs, Just stgName) -> lookup stgName stgs
--     (_, Nothing) -> Nothing

systemWithLanguageParser :: System -> LanguageDefinitions -> LogicParser
systemWithLanguageParser System{operators} (LangDefs definitions) =
  parseLogic atomLabel (if null definitions then operators else fmap (uncurry nonLogicalOperator) definitions ++ operators)
