{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module GLF.GraphBackend
  (
    initializeEnv,
    initConnectionPool,
    checkProof,
    DeductionRootRawInfo (..),
    NodeKind (..),
    loadDeduction,
    -- showKind,
    -- rootRoleName,
    -- Params,
    -- ProofNode (..),
    AssumptionInfo(..),
    ProofNodeInfo (..),
    RawProofInfo (..),
    -- Query,
    -- RedisGraph,
    -- Param (..),
    -- tableauxRevertChildrenGoalsParamName,
    -- tableauxRevertDataParentsParamName,
    -- TacticFail (..),
    -- TacticSuccess (..),
    -- addFormula,
    addFormulas,
    -- clear,
    -- initConnectionPool,
    deductionInfoStub,
    deleteDeduction,
    deleteUserAndWork,
    deleteStaleTemporaryUsersAndTheirWork,
    userStaleAfterMilliseconds,
    getFormulaTexts,
    -- loadRootFormula,
    -- loadRootFormula',
    getRawProofInfo,
    -- SubProofRoot (..),
    -- getRawSubproofNodes,
    -- oneResult,
    -- runStrategy,
    -- runStrategyGoals,
    -- proofTree,
    -- maybeResult,
    revertDeduction,
    -- interactiveRuleArgs,
    -- ruleArgs,
    mApplyRuleForwards,
    -- mApplyRule',
    showRoots,
    -- matchingRulesBySystem,
    LanguageInput,
    initializeLanguageParser,
    -- initializeLanguageParser',
    getLanguageInformation,
    printLanguage,
    getLoadSystemLanguageParser
  )
where

import Crypto.Hash
import qualified Data.Maybe as Maybe
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import GHC.Stack (HasCallStack)
import Control.Monad
import Control.Monad.State.Class
import Control.Monad.Reader.Class (asks)
import qualified Data.Aeson as JSON
import qualified Data.Bifunctor as Bifunctor
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import qualified Data.Pool as Pool
import qualified Data.Set as Set
import Data.String.Interpolate (i)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as TextEncoding
import qualified Data.ByteString.Lazy as BS
--import Debug.Trace (trace)
import GHC.Generics (Generic)
--import List.Transformer (ListT (..), Step (..))
--import qualified List.Transformer as ListT
import qualified Database.PostgreSQL.Simple as DB
import Database.PostgreSQL.Simple.FromRow as Row
import Database.PostgreSQL.Simple.FromField (FromField)

import GLF.Base
import GLF.Logic.Base
import GLF.Logic.Parser
import GLF.Rule hiding (op)
import GLF.System
--import GLF.Tactic

-- helpers

queryOne :: (HasCallStack, DB.ToRow q, FromField r) => DB.Connection -> DB.Query -> q -> IO r
queryOne conn query params = DB.query conn query params
  >>= \case
         [] -> error "No results for query"
         [DB.Only r] -> return r
         _ : _ -> error "More than one result for query"

queryOneRow :: (HasCallStack, DB.ToRow q, FromRow r) => DB.Connection -> DB.Query -> q -> IO r
queryOneRow conn query params = DB.query conn query params
  >>= \case
         [] -> error "No results for query"
         [r] -> return r
         _ : _ -> error "More than one result for query"

queryMaybe :: (HasCallStack, DB.ToRow q, FromField r) => DB.Connection -> DB.Query -> q -> IO (Maybe r)
queryMaybe conn query params = DB.query conn query params
  >>= \case
         [] -> return Nothing
         [DB.Only r] -> return r
         _ : _ -> error "More than one result for query"

type UserName = Text

initConnectionPool :: Config -> IO (Pool.Pool DB.Connection)
initConnectionPool Config{dbConfig=DBConfig{host,dbPort}} = do
  mkPool (DB.connectPostgreSQL [i|dbname=glf user=glf host=#{host} port=#{dbPort}|]) DB.close
  where
    mkPool create close = Pool.createPool create close 1 120 {- two minutes -} 10

initializeEnv :: Config -> Systems -> IO Env
initializeEnv cfg systems = do
  conns <- initConnectionPool cfg
  return Env { systems = systems, connPool = conns}

-- clear :: CypherBackend c => GraphName -> c ()
-- clear g = transact $ query_ g "MATCH (n) DETACH DELETE n"

-- (=!) :: Bolt.RecordValue a => Bolt.Record -> Text -> a
-- -- ^ Helper function to extract value from record
-- (=!) r key =
--   either
--     ( error $
--         unwords
--           [ "No key",
--             Text.unpack key,
--             "in map",
--             show r,
--             "or its value has unexpected type"
--           ]
--     )
--     id
--     . Bolt.exactEither
--     $ r Map.! key

-- (=?) :: (Default a, Bolt.RecordValue a) => Bolt.Record -> Text -> a
-- -- ^ Helper function to extract value from record
-- (=?) r key =
--   case r Map.!? key of
--     Nothing -> def
--     Just v ->
--       either
--         ( error $
--             unwords
--               [ "No key",
--                 Text.unpack key,
--                 "in map",
--                 show r,
--                 "or its value has unexpected type"
--               ]
--         )
--         id
--         . Bolt.exactEither
--         $ v

-- boltRecordValueError :: Show a => String -> a -> Either Bolt.UnpackError b
-- boltRecordValueError what x = Left . Bolt.Not . Text.pack $ show x ++ " is not a " ++ what

-- instance Bolt.RecordValue Goal where
--   exactEither (Bolt.M m) =
--     pure $ Goal (m =! "role" :: Text) (m =! "node") (m =? "derives") (m =? "introducesHyps")
--   exactEither x = boltRecordValueError "Goal" x

-- instance (Bolt.RecordValue a, Bolt.RecordValue b) => Bolt.RecordValue (a, b) where
--   exactEither (Bolt.L [a, b]) = liftA2 (,) (Bolt.exactEither a) (Bolt.exactEither b)
--   exactEither x = boltRecordValueError "Pair" x

-- type Params = Map Text Bolt.Value

-- data Param
--   = NodeP Text NodeId
--   | ListP Text [NodeId]
--   deriving stock (Generic)
--   deriving anyclass (ToJSON, FromJSON)

-- paramsToBolt :: [Param] -> Params
-- paramsToBolt = Map.fromList . fmap go
--   where
--     go (NodeP name nodeId) = (name, Bolt.I nodeId)
--     go (ListP name nodeIds) = (name, Bolt.L (fmap Bolt.I nodeIds))

-- data CypherClause = Create | Merge

-- ---
-- -- Cypher/Bolt specific stuff
-- formulaGraph :: CypherBackend c => CypherClause -> RootId -> FormulaId -> Formula -> c ()
-- -- ^ return query that inserts root formula in graph
-- formulaGraph cclause rootId rootFormulaId f = do
--   -- FIXME: we might be able to drop the rootId parameter now
--   --- NOTE: root formula is already there (and is the last in the list) -> not right (probably)
--   (nodeIds :: [NodeId]) <- oneResult "createNodes" (=! "nodeIds") "" createNodes (ps <> Bolt.props ["fs" =: formulasInfosList])
--   let idMap =
--         IntMap.insert rootIx rootFormulaId $
--           IntMap.fromList (zip [0 {- set indices for other nodes -} ..] nodeIds)
--       edgesInfosList =
--         List.map (\(parent, ix, child) -> [idMap IntMap.! parent, ix, idMap IntMap.! child]) $
--           Set.toList (edgesInfos f)
--   queryP_ "" createEdges (ps <> Bolt.props ["es" =: edgesInfosList])
--   where
--     cclauseStr = case cclause of
--       Create -> "CREATE" :: Text
--       Merge -> "MERGE"
--     createNodes =
--       [i|UNWIND $fs AS f\n#{cclauseStr}(n:Formula{formula:f[0],root:$#{rootNodeName}})
-- SET n.op = f[1]
-- RETURN collect(id(n)) AS nodeIds|]
--     createEdges =
--       [i|UNWIND $es AS e
-- MATCH(parent),(child)
-- WHERE id(parent) = e[0] AND id(child) = e[2]\n#{cclauseStr}(child)-[:Builds{opn:e[1]}]->(parent)|]
--     --- we use rootId to differentiate the formula nodes from one graph
--     --- from those of another
--     -- OPTIMIZE: might be able to optimize with
--     -- https://neo4j.com/docs/cypher-manual/current/clauses/call-subquery/
--     -- or maybe just use loadcsv…
--     ps = Bolt.props [rootNodeName =: rootId]
--     formulasInfosList = List.map formulaInfo . Set.toAscList $ strictSubformulas
--     strictSubformulas = Set.delete f $ subformulas f
--     rootIx = -1 :: Int
--     edgesInfos Atom {} = Set.empty
--     edgesInfos n@(Formula _ fs) =
--       Set.unions $
--         Set.fromList
--           ( zipWith
--               ( \ix c ->
--                   ( fromMaybe rootIx (Set.lookupIndex n strictSubformulas), -- HACK:
--                     ix,
--                     Set.findIndex c strictSubformulas
--                   )
--               )
--               [1 :: Int ..] -- operand indices start at 1
--               fs
--           ) :
--         fmap edgesInfos fs
--     formulaInfo form =
--       formulaText form :
--       case form of
--         Atom {} -> []
--         Formula Op {name = PolyName {unicode}} _ -> [unicode]

-- rootRoleName :: Text
-- rootRoleName = "root"

-- loadTableauxDeduction ::
--   CypherBackend cypher =>
--   GraphName ->
--   DeductionRootRawInfo ->
--   Params ->
--   [Formula] ->
--   cypher (RootId, [NodeId])
-- loadTableauxDeduction
--   gr
--   DeductionRootRawInfo {label, systemName}
--   initParams
--   goalFormulas =
--     transact $ do
--       rootId <-
--         oneResult
--           "createRoot"
--           (=! "rootId")
--           gr
--           createRootQuery
--           params
--       fs <- mapM (addFormula' gr rootId) goalFormulas
--       let params' = Bolt.props ["rootId" =: rootId, "formulas" =: fs]
--       initialNodesIds <- oneResult "createInitialGoals" (=! "initialNodesIds") gr createInitialGoals params'
--       return (rootId, initialNodesIds)
--     where
--       createRootQuery =
--         [i|CREATE (crown:Crown)-[:Meta]->(root:Rule:Root {label: $label, system: $system})
-- SET root += $initProps
-- RETURN id(root) AS rootId|]
--       params =
--         Bolt.props
--           [ "label" Bolt.=: label,
--             "system" Bolt.=: systemName,
--             ("initProps", Bolt.M initParams)
--           ]
--       createInitialGoals =
--         [i|MATCH (crown:Crown)-->(r)
-- WHERE id(r) = $rootId
-- UNWIND $formulas AS formulaId
-- MATCH (f)
-- WHERE id(f) = formulaId
-- CREATE (n:Goal:Rule:Initial {name: "Initial"})-[:Derives]->(f)
-- WITH collect(id(n)) AS createdNodesIds, crown
-- WITH crown, createdNodesIds, $rootId + createdNodesIds[..-1] AS parentNodesIds, createdNodesIds[-1] AS lastNodeId
-- UNWIND range(0, size($formulas) - 1) AS ix
-- MATCH (parent)
-- WHERE id(parent) = parentNodesIds[ix]
-- WITH crown, ix, parentNodesIds, createdNodesIds, parent, lastNodeId
-- MATCH (child)
-- WHERE id(child) = createdNodesIds[ix]
-- CREATE (parent)-[:DerivedBy {initial: true}]->(child)
-- WITH crown, collect(child) AS _, lastNodeId, createdNodesIds
-- MATCH (last)
-- WHERE id(last) = lastNodeId
-- CREATE (last)-[:Meta]->(crown)
-- RETURN createdNodesIds AS initialNodesIds|]

-- loadFormula ::
--   CypherBackend cypher =>
--   GraphName ->
--   Maybe CompiledRule ->
--   DeductionRootRawInfo ->
--   Params ->
--   Formula ->
--   [Formula] ->
--   cypher (RootId, GoalId, [NodeId])
-- -- ^ Load formula into graph backend, return the identifiers of the
-- -- root node and the formula goal.
-- --
-- -- Every proof graph is composed of formula nodes and derivation
-- -- nodes; every derivation notes points to the formula node that it
-- -- derives. 'loadFormula' creates the formula nodes and the
-- -- scaffolding for the derivation nodes to be created as the proof is
-- -- carried out.
-- loadFormula
--   gr
--   initialAssumptionsRule
--   DeductionRootRawInfo {label, systemName}
--   initParams
--   rootFormula
--   assumptionFormulas =
--     transact $ do
--       (rootId, goalId, rootFormulaId) <-
--         oneResult
--           "createRoot"
--           (\r -> (r =! "rootId", r =! "goalId", r =! "rootFormulaId"))
--           gr
--           createRootQuery
--           params
--       unless (isAtomic rootFormula) $
--         formulaGraph Create rootId rootFormulaId rootFormula
--       assumptionIds <- mapM (addFormula' gr rootId) assumptionFormulas
--       assumptionNodeIds <- case assumptionIds of
--         [] -> return []
--         _ ->
--           oneResult
--             "addAssumptions"
--             (=! "assumptionNodeIds")
--             gr
--             -- NOTE: keep in sync with assumptionFw
--             [i|MATCH (crown:Crown)-[:Meta]->(root:Root)-[db:DerivedBy]->(goal:Goal)
-- WHERE id(root) = $rootId AND id(goal) = $goalId
-- SET db.introducesHyps = $assumptions, goal.hyps = $assumptions
-- WITH crown, goal
-- UNWIND $assumptions AS assumptionId
-- MATCH (assumption) WHERE id(assumption) = assumptionId
-- CREATE (assumptionNode:Rule:Assumption:Initial {name: "#{maybe "Assumption" (unicode . ruleName) initialAssumptionsRule}"})-[:Meta]->(crown)
-- CREATE (assumptionNode)-[:Derives]->(assumption)
-- CREATE (assumptionNode)-[:Closes]->(assumptionNode)
-- WITH goal, collect(id(assumptionNode)) AS discards
-- SET goal.discards = goal.discards + discards
-- RETURN discards AS assumptionNodeIds|]
--             ( Bolt.props
--                 [ "rootId" Bolt.=: rootId,
--                   "goalId" Bolt.=: goalId,
--                   "assumptions" Bolt.=: assumptionIds
--                 ]
--             )
--       return (rootId, goalId, assumptionNodeIds)
--     where
--       isAtomic (Atom _) = True
--       isAtomic _ = False
--       createRootQuery =
--         [i|CREATE (crown:Crown)-[:Meta]->(root:Rule:Root {label: $label, system: $system})-[:DerivedBy {role: "#{rootRoleName}"}]->(g:Goal {hyps: [], discards: []})-[:Meta]->(crown)
-- SET root += $initProps
-- WITH crown, root, g
-- CREATE (n:Formula {formula: $rootFormula, root: id(root)})
-- SET n.op = $rootOp
-- CREATE (g)-[:Derives]->(n)
-- RETURN id(root) AS rootId, id(g) AS goalId, id(n) AS rootFormulaId|]
--       params =
--         Bolt.props
--           [ "rootFormula" Bolt.=: show rootFormula,
--             "label" Bolt.=: label,
--             "system" Bolt.=: systemName,
--             ("initProps", Bolt.M initParams),
--             "rootOp" =:? fmap operatorUnicodeName (operator rootFormula)
--           ]

type LanguageInput = [Text]

-- languageDefinitionsName, languageAritiesName, languageVariableArityName :: Text
-- languageDefinitionsName = "definitions"
-- languageAritiesName = "arities"
-- languageVariableArityName = "variableArity"

type HashPasswordAlgorithm = SHA3_224

hashText :: Text -> Text
-- encode input text as base64-encoded hash
hashText input = TextEncoding.decodeUtf8 . encodeBase64
  $ hashWith hashPasswordAlgorithm (TextEncoding.encodeUtf8 input)
  where
    hashPasswordAlgorithm :: HashPasswordAlgorithm
    hashPasswordAlgorithm = undefined

loadGetLanguage :: LanguageInput
  -> DBio (Either String (LanguageId, LanguageDefinitions))
-- | Load language specification onto the backend for identification
-- and later retrieval
loadGetLanguage languageInput conn = loadLanguage languageInput
  where
    loadLanguage definitionsInput =
      case Bifunctor.second Map.toList definitions of
        Left err -> return (Left err)
        Right definitionsList -> do
          let jsonText = TextEncoding.decodeUtf8 . BS.toStrict $ JSON.encode definitionsList
              defsDigest = hashText jsonText
          -- Use digest to check for language uniqueness
          mLangId <- queryMaybe conn createLanguageQuery (jsonText, defsDigest)
          langId <- case mLangId of
                      Nothing -> queryOne conn getLanguageQuery (DB.Only defsDigest)
                      Just lId -> return lId
          return $ Right (langId, LangDefs definitionsList)
      where
        getLanguageQuery = [i|SELECT _id FROM languages WHERE digest = ?;|]
        createLanguageQuery = [i|INSERT INTO languages (definitions, digest)
VALUES (?, ?)
ON CONFLICT (digest) DO NOTHING
RETURNING languages._id;|]
        definitions :: Either String (Map Text Arity)
        definitions = -- we use Map to sort and remove duplicates
          List.foldl'
            (\acc defInput ->
               either
                 Left
                 (\m ->
                    case nonLogicalDefinition defInput of
                      Right (n, arityN) -> Right (Map.insert n arityN m)
                      Left err -> Left err)
                 acc)
            (Right Map.empty)
            definitionsInput

printLanguage :: LanguageDefinitions -> LanguageInput
printLanguage (LangDefs langDefs) = fmap go langDefs
  where
    go (n, Fixed arityN) = Text.concat [n, " ", Text.pack $ show arityN]
    go (n, AtLeast arityN) = Text.concat [n, " ", Text.pack $ show arityN, "+"]

-- loadRootFormula' ::
--   CypherBackend c =>
--   SystemWithParser ->
--   Maybe Int ->
--   GraphName ->
--   DeductionRootRawInfo ->
--   Params ->
--   c (Either String ([GoalId], [NodeId], DeductionRootRawInfo))
-- loadRootFormula' (SystemWithParser (System{kind}, parser)) mLanguageId gr dedInfo@DeductionRootRawInfo {assumptionsInput, goalsInput} initParams' = do
--     let initParams = maybe initParams' (\lId -> Map.insert "language" (Bolt.I lId) initParams') mLanguageId
--     Bifunctor.second (\(rootId, goalIds, assumptionNodeIds) -> (goalIds, assumptionNodeIds, dedInfo {root = rootId} :: DeductionRootRawInfo))
--       <$> case kind of
--         TableauxSys -> case (assumptionsInput, mapM parser goalsInput) of
--           ([], Right []) -> return $ Left "No input"
--           ([], Right goalFormulas) -> (\(r, gs) -> Right (r, gs, [])) <$> loadTableauxDeduction gr dedInfo initParams goalFormulas
--           (_ : _, _) -> return $ Left "Tableaux systems should not have assumptions"
--           (_, Left err) -> return $ Left err
--         BackNDSys {} -> case (mapM parser assumptionsInput, mapM parser goalsInput) of
--           (Right assumptionFormulas, Right [goalFormula]) -> (\(r, g, asss) -> Right (r, [g], asss)) <$> loadFormula gr Nothing dedInfo initParams goalFormula assumptionFormulas
--           (Right _, Right []) -> return $ Left "No goals"
--           (Right _, Right _) -> return $ Left "Too many goals"
--           (Left err, _) -> return $ Left err
--           (_, Left err) -> return $ Left err
--         FitchSys initialAssumpRule -> case (mapM parser assumptionsInput, mapM parser goalsInput) of
--           (Right assumptionFormulas, Right [goalFormula]) -> (\(r, g, asss) -> Right (r, [g], asss)) <$> loadFormula gr (Just initialAssumpRule) dedInfo initParams goalFormula assumptionFormulas
--           (Right _, Right _) -> return $ Left "Too many goals"
--           (Left err, _) -> return $ Left err
--           (_, Left err) -> return $ Left err

loadFormula :: RootId -> Text -> DBio FormulaId
loadFormula proofId f conn =
  queryMaybe conn addFormulaQuery (f, proofId)
  >>= \case
       Nothing -> queryOne conn formulaIdQuery (f, proofId)
       Just fId -> return fId

loadFormulas :: RootId -> [Text] -> DBio [FormulaId]
loadFormulas proofId fs conn = mapM (\f -> loadFormula proofId f conn) fs

addFormulaQuery :: DB.Query
addFormulaQuery = [i|INSERT INTO formula(_text, proof)
VALUES (?, ?)
ON CONFLICT (_text, proof) DO NOTHING
RETURNING _id;|]

formulaIdQuery :: DB.Query
formulaIdQuery = [i|SELECT _id
FROM formula
WHERE _text = ? AND proof = ?;|]

unsafeApplyRule :: RootId -> DeductiveRule -> [NodeId] -> FormulaId -> [NodeId] -> DBio NodeId
unsafeApplyRule proofId DeductiveRule{name, action, premises} toBePremisesIds resultFormulaId hypothesesToDiscard conn = do
  ruleappId :: NodeId <- queryOne conn ruleappQuery (unicode name, proofId, resultFormulaId)
  _nPremises <- DB.executeMany conn premisesQuery (fmap (, ruleappId) toBePremisesIds)
  _ <- case premises of
         [] | action /= Axiom -> DB.execute conn assumptionQuery (resultFormulaId, proofId, ruleappId)
         _notAssumption -> return 0
  _ <- DB.executeMany conn dischargeAssumptionQuery (fmap (ruleappId, , proofId) hypothesesToDiscard)
  return ruleappId
  where
    ruleappQuery = [i|INSERT INTO ruleapp(rule, proof, formula) VALUES(?,?,?) RETURNING _id;|]
    premisesQuery = [i|INSERT INTO premise(conclusion_of, premise_of) VALUES(?,?);|]
    assumptionQuery = [i|INSERT INTO assumption(formula, proof, introducedBy, dischargedBy) VALUES (?, ?, ?, null);|]
    dischargeAssumptionQuery = [i|UPDATE assumption
SET dischargedBy = upd.ruleappId
FROM (VALUES (?, ?, ?)) AS upd(ruleappId, introducedId, proofId)
WHERE introducedBy = upd.introducedId AND proof = upd.proofId;|]

loadDeduction :: HasCallStack
  => SystemWithParser -> UserName -> Maybe LanguageId -> DeductionRootRawInfo
  -> DBio (Fallible DeductionRootRawInfo)
-- | Start a new proof.
loadDeduction
  (SystemWithParser (System{systemName, assumptionRule}, parser))
  userName
  mLangId
  dedInfo@DeductionRootRawInfo {assumptionsInput, goalInput}
  conn =
  case (mapM parser assumptionsInput, parser goalInput) of
    (Err err, _) -> return (Err err)
    (_, Err err) -> return (Err err)
    (Ok assumptionFormulas, Ok goalFormula) ->
     DB.withTransaction conn $ do
      dedId <- queryOne conn loadDeductionQuery (systemName, userName, mLangId)
      let assumptionSet = Set.fromList . Vector.toList $ fmap infixFormulaText assumptionFormulas
          goalTextNormalized = infixFormulaText goalFormula
      formulaIds <- loadFormulas dedId (goalTextNormalized : Set.toList (Set.delete goalTextNormalized assumptionSet)) conn
      case formulaIds of
        [] -> error "There should be at least one formula"
        goalId : assumptionIds' -> do
          let assumptionIds = [goalId | goalTextNormalized `Set.member` assumptionSet] ++ assumptionIds'
          assumptionRuleIds <- mapM (\assId -> unsafeApplyRule dedId assumptionRule [] assId [] conn) assumptionIds
          _ <- DB.executeMany conn dischargeInitialAssumptionsQuery (fmap (, dedId) assumptionRuleIds)
          _ <- DB.execute conn addGoalsQuery (dedId, goalId)
          _ <- DB.execute_ conn deleteUnusedLanguagesQuery
          return $ Ok (dedInfo :: DeductionRootRawInfo){root = dedId, goalInput = goalTextNormalized}
  where
    dischargeInitialAssumptionsQuery = [i|UPDATE assumption
SET dischargedBy = upd.assumptionId
FROM (VALUES (?, ?)) AS upd(assumptionId, proofId)
WHERE introducedBy = upd.assumptionId AND proof = upd.proofId;|]
    addGoalsQuery = [i|INSERT INTO goal
(proof, formula) values (?, ?);|]
    loadDeductionQuery = [i|INSERT INTO proof(in_system, owner, language_id)
VALUES (?, ?, ?)
RETURNING proof._id;|]
    deleteUnusedLanguagesQuery = [i|DELETE FROM languages
WHERE NOT EXISTS (SELECT _id FROM proof
                  WHERE language_id = languages._id);|]

-- loadRootFormula :: CypherBackend c => SystemWithParser -> Maybe Int -> GraphName -> DeductionRootRawInfo -> Params
--   -> c (Either String DeductionRootRawInfo)
-- loadRootFormula sys mLangId gr dedInfo initParams =
--   fmap (\(_, _, ded) -> ded) <$> loadRootFormula' sys mLangId gr dedInfo initParams

-- initializeLanguageParser' :: System -> LanguageInput
--   -> DBio (Either String (LogicParser, Maybe Int))
-- -- | Load language specification, create parser for the system plus
-- -- this language
-- initializeLanguageParser' sys@System{extensible} languageInput conn = do
--   mLanguageInfoOr <- if extensible
--                      then Bifunctor.second Just <$> loadGetLanguage gr languageInput
--                      else return (Right Nothing)
--   return $ case mLanguageInfoOr of
--     Left err -> Left err
--     Right mLanguageInfo ->
--       let p = systemWithLanguageParser sys (maybe [] snd mLanguageInfo)
--       in Right (p, fmap fst mLanguageInfo)

initializeLanguageParser :: Text -> LanguageInput
  -> App IO (Either String (SystemWithParser, Maybe LanguageId))
-- | Load language specification, create parser for the system plus
-- this language, and add it to application state.
initializeLanguageParser sysName languageInput = do
  mSys <- flip getSystem sysName <$> asks GLF.Base.systems
  case mSys of
    Nothing -> return $ Left [i|No system named #{sysName}|]
    Just sys@System{extensible} -> do
      mLanguageInfoOr <- if extensible
                         then Bifunctor.second Just <$> withConnection (loadGetLanguage languageInput)
                       else return (Right Nothing)
      case mLanguageInfoOr of
        Left err -> return (Left err)
        Right mLanguageInfo -> Right . (, fmap fst mLanguageInfo) <$> addSystemParser sys mLanguageInfo

addSystemParser :: System -> Maybe (LanguageId, LanguageDefinitions)
  -> App IO SystemWithParser
-- | Create parser for system and the optional input language and
-- add it to the cache.
addSystemParser system@System{systemName} mLanguageInfo = do
  let p = systemWithLanguageParser system (maybe (LangDefs []) snd mLanguageInfo)
      mLangId = fmap fst mLanguageInfo
  modify' (\st@AppState{parsers} -> st{parsers = addParser systemName mLangId p parsers})
  return $ SystemWithParser (system, p)

getLoadSystemLanguageParser :: Text -> Maybe LanguageId
  -> App IO (Either String SystemWithParser)
-- | Given a system name and an optional language ID, lookup the
-- system and its parser, caching the latter if it's not already in
-- cache.
getLoadSystemLanguageParser sysName mLanguageId = do
  mSys <- flip getSystem sysName <$> asks GLF.Base.systems
  case mSys of
    Nothing -> return $ Left [i|No system named #{sysName}|]
    Just sys -> do
      mParser <- lookupParser sysName mLanguageId <$> gets parsers
      case mParser of
        Just p -> return $ Right (SystemWithParser (sys, p))
        Nothing ->
          case mLanguageId of
            Nothing -> return . Left $ "Could not find appropriate parser for system " ++ Text.unpack sysName
            Just langId -> do
              mLanguageInfo <- withConnection $ getLanguageInformation langId
              case mLanguageInfo of
                Nothing -> return . Left $ "Could not find specified language for system " ++ Text.unpack sysName
                Just definitions -> Right <$> addSystemParser sys (Just (langId, definitions))

getLanguageInformation :: LanguageId -> DBio (Maybe LanguageDefinitions)
getLanguageInformation langId conn = do
  queryMaybe conn getLanguageQuery (DB.Only langId)
  where
    -- recoverLanguageInfo r = zipWith (\n arityN -> (n, if n `Set.member` variableArity then AtLeast arityN else Fixed arityN)) definitionNames arities
    --   where
    --     definitionNames = r =! languageDefinitionsName
    --     arities = r =! languageAritiesName
    --     variableArity = Set.fromList (r =! languageVariableArityName)
    getLanguageQuery = [i|SELECT definitions FROM languages WHERE _id = ?;|]

-- addFormula' ::
--   CypherBackend cypher =>
--   GraphName ->
--   RootId ->
--   Formula ->
--   cypher FormulaId
-- addFormula' gr root formula = do
--   (subRoot, existedAlready) <-
--     oneResult
--       "createSubRoot"
--       (\r -> (r =! "subRoot", r =! "existed"))
--       gr
--       createSubRootQuery
--       (Bolt.props ["formula" =: formulaText formula, rootNodeName =: root, "op" =:? fmap operatorUnicodeName (operator formula)])
--   unless existedAlready $ formulaGraph Merge root subRoot formula
--   return subRoot
--   where
--     createSubRootQuery =
--       [i|MERGE (subRoot:Formula{formula:$formula,root:$#{rootNodeName}})
-- ON CREATE SET subRoot.nonGoalSubformula = true, subRoot.op = $op
-- RETURN id(subRoot) as subRoot, subRoot.nonGoalSubformula IS NULL AS existed|]

-- addFormula ::
--   CypherBackend cypher =>
--   LogicParser ->
--   GraphName ->
--   RootId ->
--   Text ->
--   cypher (Either String FormulaId)
-- -- ^ add a formula to an already existing proof in the graph backend
-- addFormula parser gr root input =
--   case parser input of
--     Right formula -> Right <$> addFormula' gr root formula
--     Left err -> return $ Left err

addFormulas ::
  LogicParser ->
  RootId ->
  [Text] ->
  DBio (Fallible [FormulaId])
-- -- ^ add a formula to an existing proof in the graph backend
addFormulas parser proofId inputs conn =
  case mapM parser inputs of
    -- to check if formulas parse, but then use user input
    --- NOTE: this might allow for attacks (i.e., user adding valid
    --- formulas with lots of whitespace just to take up disk space,
    --- but we don't really expect that to happen in our threat
    --- model)
    Ok formulas -> Ok <$> loadFormulas proofId (fmap infixFormulaText formulas) conn
    Err err -> return $ Err err

-- ruleArgs :: CypherBackend cypher => GraphName -> Query -> RuleArgs -> cypher [RuleArgs]
-- ruleArgs gr q args = fmap (fmap go) <$> queryP gr q params
--   where
--     params = Bolt.I <$> args
--     go (Bolt.I nodeId) = nodeId
--     go bv = error $ "Rule argument must be a node ID (integer), not a " ++ show bv

getFormulaTexts :: [FormulaId] -> DBio [(FormulaId, Text)]
-- return formula texts corresponding to the input node ids, which may
-- correspond to formulas nodes or rule node names (in which case we return the
-- text of formula it derives)
getFormulaTexts formulaIds conn = do
  DB.query conn getFormulasQuery (DB.Only $ DB.In formulaIds)
  where
    getFormulasQuery =
      [i|SELECT _id, _text
FROM formula
WHERE _id IN ?|]

-- getNodeFormulas' :: CypherBackend c => GraphName -> LogicParser -> [NodeId] -> c [Formula]
-- getNodeFormulas' _ _ [] = return []
-- getNodeFormulas' gr parser nodeIds = do
--   fsTexts <- getNodeFormulas gr nodeIds
--   return $ rights (fmap parser fsTexts)

-- matchingRulesBySystem :: CypherBackend c => GraphName -> System -> GoalId -> c [Text]
-- -- TODO: if necessary, we could do a deep (instead of a shallow)
-- -- comparison, see 'shallowMatches'. A shallow comparison should
-- -- work for almost all cases though.
-- matchingRulesBySystem gr System {rules, kind} = go
--   where
--     go formula = do
--       (mainOp, childrenOps) <- oneResult "formulaMatches" (\r -> (r =! "mainOp", r =! "childrenOps")) gr q params
--       return . List.nub $
--         concatMap (\(ruleName, goalMatches) -> [ruleName | shallowMatches mainOp childrenOps goalMatches]) ruleMatches
--       where
--         params = Bolt.props ["formula" =: formula]
--     q =
--       [i|MATCH (#{goalNodeName})-[:Derives]->(#{goalDerivesNodeName})
-- WHERE id(#{goalNodeName}) = $formula
-- OPTIONAL MATCH (f)-[b:Builds]->(#{goalDerivesNodeName})
-- WITH #{goalDerivesNodeName}.op AS mainOp, collect(CASE f IS NULL WHEN true THEN null ELSE [b.opn, f.op] END) AS childrenOps
-- RETURN mainOp, childrenOps|]
--     ruleMatches =
--       concatMap
--         ( \CompiledRule {ruleName = PolyName {unicode}, description = RuleDescription {results, premises}} ->
--             (unicode,)
--               <$> case kind of
--                 TableauxSys ->
--                   fmap (snd . fst) premises
--                 BackNDSys _ ->
--                   case results of
--                     rs : _ -> rs
--                     [] -> []
--                 FitchSys _ -> []
--         )
--         rules

-- interactiveRuleArgs :: CypherBackend cypher => GraphName -> Query -> Map Text NodeId -> cypher ([Text], [[(Text, NodeId)]])
-- interactiveRuleArgs gr q args = do
--   possibleArgs <- ruleArgs gr q args
--   case possibleArgs of
--     [] -> return ([], [])
--     args' : argss -> do
--       let argnames = Map.keys args'
--       let nodeArgss = concat (Map.elems <$> args' : argss)
--       fs <- getNodeFormulas gr nodeArgss
--       return (argnames, splitEvery (length argnames) $ zip fs nodeArgss)
--   where
--     splitEvery :: Int -> [a] -> [[a]]
--     splitEvery _ [] = []
--     splitEvery n xs = as : splitEvery n bs
--       where
--         (as, bs) = splitAt n xs

data Constraint = ProvisoConstraint Proviso
  | SameBranch NodeId NodeId Bool {--node discarded--}

mApplyRuleForwards :: SystemWithParser -> RootId -> DeductiveRule -> RuleArgs -> DBio (Fallible RuleApp)
mApplyRuleForwards (SystemWithParser (System{functions}, parser)) proofId theRule@DeductiveRule{name, premises, provisos, conclusion} providedArgs conn = do
  formulas <- Map.traverseMaybeWithKey getFormula providedArgs
  let ok = List.foldl' checkPremise (Ok formulas) expectedPremises
  case ok of
    Ok env ->
      case makeResult env of
        Err err -> return (Err err)
        Ok (env', resultFormula) -> do
          let constraints = fmap ProvisoConstraint provisos -- TODO: add SameBranch constraints based on rule definition
          constraintsOk <- checkConstraints parser functionsMap env' rulePremises constraints conn
          case constraintsOk of
            Err err -> return (Err err)
            Ok _ -> do
              let resultFormulaText = infixFormulaText resultFormula
                  hypothesesToDiscard = mapMaybe (`Map.lookup` providedArgs) $ concatMap (fmap fst . hypotheses) premises
              resultFormulaId <- loadFormula proofId resultFormulaText conn
              Ok <$> applyRule rulePremiseIds resultFormulaId hypothesesToDiscard
    Err err -> return (Err err)
  where
    -- we can use mapMaybe here because we'll check the premises
    -- (and their number) elsewhere
    rulePremiseIds = mapMaybe (\Premise{role} -> Map.lookup role providedArgs) premises
    rulePremises = Map.filterWithKey (\argname _ -> argname `elem` premiseRoles) providedArgs
      where
        premiseRoles = fmap role premises
    makeResult formulas =
      case Map.lookup resultFormulaArgName formulas of
        Just resultFormula ->
          case checkPremise (Ok formulas) (resultFormulaArgName, conclusion) of
            Ok env -> Ok (env, resultFormula)
            Err err -> Err err
        Nothing ->
          maybe (failed ["Could not build result formula from provided information"]) (\f -> Ok (formulas, f))
          $ buildFormulaFromSpec functionsMap formulas conclusion
    -- better to create a data type specifically for checking
    -- unification and constraints like being in the same subproof
    expectedPremises = concatMap (\Premise{root, hypotheses, role} -> hypotheses ++ [(role, root)]) premises -- ordering matters, hence appending last
    getFormula argName argId =
      getFormulas
        parser
        (if argName `elem` fmap fst expectedPremises
         then premiseConclusionQuery
         else formulaQuery)
        (DB.Only argId)
        conn
      >>= \case
             Err parserErr -> error (Text.unpack parserErr)
             Ok [] -> return Nothing
             Ok (f : _) -> return $ Just f
      where
        formulaQuery = [i|SELECT _text FROM formula WHERE formula._id = ?;|]
        premiseConclusionQuery = [i|SELECT _text
FROM ruleapp INNER JOIN formula ON ruleapp.formula = formula._id
WHERE ruleapp._id = ?;|]
    functionsMap = Map.fromList functions
    checkPremise (Err err) _ = Err err
    checkPremise (Ok env) (role, premiseSpec) =
      case Map.lookup role env of
        Nothing -> failed ["Missing premise: ", role, " not provided"]
        Just premiseFormula ->
          case formulaMatchesSpec functionsMap env premiseSpec premiseFormula of
            (env', checkRes) ->
              case checkRes of
                Matches -> Ok env'
                NoMatch err -> Err (Text.pack err)
                MissingInformation -> failed ["Need more information to check premise ", role]
    applyRule toBePremisesIds resultFormulaId hypothesesToDiscard = do
      ruleappId <- unsafeApplyRule proofId theRule toBePremisesIds resultFormulaId hypothesesToDiscard conn
      return RuleApp { ruleApplied = name,
                       node = ruleappId,
                       derives = resultFormulaId,
                       goals = []
                     }

getFormulas :: (DB.ToRow q) => LogicParser -> DB.Query -> q -> DBio (Fallible [Formula])
getFormulas parser q params conn = do
  formulasText :: [DB.Only Text] <- DB.query conn q params
  return
    $ List.foldr (\(DB.Only formulaT) acc ->
                    case acc of
                      Err err -> Err err
                      Ok fs ->
                        case parser formulaT of
                          Err err -> Err err
                          Ok formula -> Ok (formula:fs)
               )
               (Ok [])
               formulasText


checkConstraints :: LogicParser -> FunctionMap -> Map Text Formula -> Map Text NodeId -> [Constraint] -> DBio (Fallible ())
checkConstraints parser functionsMap env premisesIds cs conn =
  foldM checkConstraint (Ok ()) cs
  where
    checkConstraint (Err msg) _ = return $ Err msg
    checkConstraint (Ok ()) constraint =
      case constraint of
        ProvisoConstraint prov -> checkProviso prov
        SameBranch{} -> -- check that formulas are in the same branch
          error "TBI"
    checkProviso =
      \case
        FreeIn varname ref -> getRefCheckFreeIn False varname ref
        Not (FreeIn varname ref) -> getRefCheckFreeIn True varname ref
        Not (Not prov) -> checkProviso prov
        Not (HypothesesMatch premiseRole matches) -> checkHypothesesMatch True premiseRole matches
        HypothesesMatch premiseRole matches -> checkHypothesesMatch False premiseRole matches
      where
        checkHypothesesMatch isNeg premiseRole matches = do
          mAssumptions <- getFormulas parser collectDependantAssumptionsQuery (DB.Only $ DB.In (Maybe.maybeToList $ Map.lookup premiseRole premisesIds)) conn
          let formulaMatches f =
                let check spec =
                      case snd $ formulaMatchesSpec functionsMap env spec f of
                        Matches -> True
                        NoMatch _ -> False
                        MissingInformation -> False
                in
                (if isNeg
                then not
                else id) (List.any check matches)
              errStr = if isNeg then " " else " not "
              assumptionsOk =
                case mAssumptions of
                  Err err -> Err err
                  Ok fs ->
                    foldMap
                      (\f ->
                         if formulaMatches f
                         then Ok ()
                         else failed ["Formula ", infixFormulaText f, " does", errStr, "match ", Text.intercalate "," $ fmap showFormulaSpec matches]
                      )
                      fs
          return assumptionsOk
        checkFreeIn isNeg varname formula errText =
          if (isNeg && notFreeInFormula varname formula) || freeInFormula varname formula
          -- either we want it to be free and it is, or we don't
          -- want it free and it isn't
          then Ok ()
          else Err (Text.concat [varname, " is", if isNeg then "" else " not", " free in ", infixFormulaText formula, " (", errText, ")"])
        getRefCheckFreeIn isNeg var ref =
          case Map.lookup var env of
            Just (Atom varname) ->
              case ref of
                Hypotheses exceptions -> do
                  let exceptionFormulas = mapMaybe (`Map.lookup` env) exceptions
                  mAssumptions <- getFormulas parser collectDependantAssumptionsQuery (DB.Only $ DB.In (Map.elems premisesIds)) conn
                  case mAssumptions of
                    Err err -> return (Err err)
                    Ok fs ->
                      return $ foldMap (\formula -> if formula `elem` exceptionFormulas then Ok () else checkFreeIn isNeg varname formula "hypothesis") fs
                FormulaRef fname ->
                  case Map.lookup fname env of
                    Just formula -> return $ checkFreeIn isNeg varname formula fname
                    Nothing -> return $ Err ("No formula named " <> fname)
            _notAtomOrNotThere -> error $ "Expect variable name under " ++ Text.unpack var
    collectDependantAssumptionsQuery = [i|WITH RECURSIVE parents AS (
SELECT _id, formula
FROM ruleapp
WHERE _id IN ?
UNION
SELECT ruleapp._id AS _id, ruleapp.formula AS formula
FROM premise
  INNER JOIN parents ON parents._id = premise.premise_of
  INNER JOIN ruleapp ON premise.conclusion_of = ruleapp._id
)
SELECT DISTINCT _text
FROM parents
  INNER JOIN formula ON parents.formula = formula._id
WHERE NOT EXISTS
  (SELECT premise._id
   FROM premise
   WHERE premise_of = parents._id)
  AND EXISTS
        (SELECT _id
         FROM assumption
         WHERE dischargedBy IS NULL);|]

-- mApplyRule' :: CypherBackend cypher => SystemWithParser -> GraphName -> RootId -> CompiledRule -> RuleArgs -> [(FormulaSpec, Formula)]
--   -> cypher (Either String RuleApp)
-- mApplyRule' (SystemWithParser (System{kind}, parser)) gr rootId CompiledRule {ruleName, arguments, provisos, description = RuleDescription {premises, results}, query = (formulaArgNames, q)} providedArgs extraFormulas = do
--   let (providedArgsNames, providedArgsIds) =
--         unzip $
--           concatMap (\ArgumentInfo {argumentName} -> maybe [] (\argId -> [(argumentName, argId)]) $ Map.lookup argumentName providedArgs) argumentsInfos
--   providedFormulas <- Map.fromList . zip providedArgsNames <$> getNodeFormulas' gr parser providedArgsIds
--   let (argsSpecsWithFormulas, argsSpecsWithoutFormulas) =
--         List.partition (\(_, mf) -> isJust mf) $
--           fmap
--             ( \ArgumentInfo {argumentName, specification} ->
--                 (\s -> (snd $ namedFormulaSpec argumentName s, Map.lookup argumentName providedFormulas)) $
--                   case findNamedSubSpecs argumentName specification of
--                     [] -> specification
--                     ss -> maximum ss
--             )
--             argumentsInfos
--       (extraFormulasMap, needsChecking) =
--         List.foldl'
--           ( \(formulas, needsFurtherCheck) (spec, f) ->
--               case formulaMatchesSpec formulas spec f of
--                 (acc', Matches) -> (acc', needsFurtherCheck)
--                 (acc', _) -> (acc', (spec, Just f) : needsFurtherCheck)
--           )
--           (providedFormulas, [])
--           (extraFormulas ++ fmap (Bifunctor.second fromJust) argsSpecsWithFormulas)
--       specsOk = checkSpecifications extraFormulasMap (argsSpecsWithoutFormulas ++ needsChecking)
--   case specsOk of
--     Left err -> return (Left err)
--     Right formulas' -> do
--       formulasOk <- foldM evalFormula (Right formulas') formulasToEval
--       case formulasOk of
--         Left err -> return (Left err)
--         Right formulas'' -> do
--           provisosOk <- foldM checkProviso (Right formulas'') provisos
--           case provisosOk of
--             Left err -> return (Left err)
--             Right formulas''' -> do
--               let extraFormulasArgs = fmap (`Map.lookup` formulas''') formulaArgNames
--               if any isNothing extraFormulasArgs
--                 then -- NOTE: shouldn't happen
--                   return (Left "Missing formula")
--                 else do
--                   extraParams <- zipWith (\n fId -> (n, Bolt.I fId)) formulaArgNames <$> mapM (addFormula' gr rootId) (catMaybes extraFormulasArgs)
--                   maybe (Left "Could not apply rule") Right
--                     <$> maybeResult (Text.unpack $ nameAscii ruleName) ruleApp gr q (params <> Bolt.props extraParams)
--   where
--     checkSpecifications :: Map Text Formula -> [(FormulaSpec, Maybe Formula)] -> Either String (Map Text Formula)
--     checkSpecifications initialFormulas initialSpecsAndFormulas =
--       go initialFormulas initial
--       where
--         -- go over list, check things that have no dependencies,
--         -- update dependencies if they do, until list is empty or we
--         -- reach no steady state
--         go formulas specsAndFormulas =
--           case List.foldl' partitionSpecsAndFormulas ([], []) specsAndFormulas of
--             ([], []) -> Right formulas
--             ([], missing) ->
--               Left ("Can't satisfy formula specifications: "
--                     ++ unlines (fmap (\(_dependsOn, spec, mForm) ->
--                                         maybe "No formula matches " (\f -> showFormula (Text.unpack . operatorUnicodeName) f ++ " does not match ") mForm
--                                         ++ Text.unpack (showFormulaSpec spec))
--                                  missing))
--             (noDependencies, continue) ->
--               case List.foldl' checkFormulaMatch (Right formulas) noDependencies of
--                 Right formulas' ->
--                   go formulas' (fmap (\(_, spec, form) -> (formulaSpecDependsOn (Map.keysSet formulas') spec, spec, form)) continue)
--                 Left err -> Left err
--           where
--             partitionSpecsAndFormulas (noDeps, toCont) (specDependencies, spec, mForm) =
--               case Set.partition (`Map.member` formulas) specDependencies of
--                 (_, unsatisfiedDeps)
--                   | Set.null unsatisfiedDeps ->
--                     case maybe (maybeFormulaSpecName spec >>= flip Map.lookup formulas) Just mForm of
--                       Just form -> ((spec, form) : noDeps, toCont)
--                       Nothing -> (noDeps, (Set.empty, spec, Nothing) : toCont)
--                 (_, unsatisfiedDeps) -> (noDeps, (unsatisfiedDeps, spec, mForm) : toCont)
--         checkFormulaMatch (Left err) _ = Left err
--         checkFormulaMatch (Right formulas) (spec, form) =
--           case formulaMatchesSpec formulas spec form of
--             (formulas', Matches) -> Right formulas'
--             (_, NoMatch err) -> Left err
--             (_, MissingInformation) -> Left ("Need more information on " ++ Text.unpack (showFormulaSpec spec))
--         initial = fmap (\(spec, form) -> (formulaSpecDependsOn (Map.keysSet initialFormulas) spec, spec, form)) initialSpecsAndFormulas
--     formulasToEval = List.foldl' go [] formulaSpecsToEval
--       where
--         formulaSpecsToEval =
--           case kind of
--             FitchSys _ -> concat results
--             BackNDSys _ ->
--               concatMap (\((_, spec'), hyps) -> spec':fmap snd hyps) premises
--             TableauxSys -> []
--         go acc fs = formulaSubSpecsToEval fs ++ acc
--     evalFormula (Left err) _ = return (Left err)
--     evalFormula (Right forms) (SubstituteForInAs substitute v n n') = do
--       let (substituteForm, vForm, nForm) = (getFormula . fromMaybe "Substitute in substitution must be named" $ maybeFormulaSpecName substitute, getFormula v, getFormula n)
--           newFormOr = mSubstituteForIn substituteForm vForm nForm
--       case newFormOr of
--         Left err -> return (Left err)
--         Right newForm -> return (Right $ Map.insert n' newForm forms)
--       where
--         getFormula name = fromMaybe (error $ "Missing formula " ++ Text.unpack name) $ Map.lookup name forms
--     evalFormula (Right forms) _ = return (Right forms)
--     argumentsInfos = ruleArgumentsInfo arguments
--     params = Bolt.toValue <$> providedArgs -- lift NodeId/Int to Value
--     ruleApp r = RuleApp ruleName (r =! "ruleId") (r =! "derives") (r =! "goals")
--     checkProviso isOk proviso =
--       case isOk of
--         Left e -> return (Left e)
--         Right forms ->
--           case proviso of
--             -- TODO: could encapsulate this into a function in the
--             -- Rule module
--             SameBranchAs floating subroot -> do
--               ok <- oneResult "sameBranchAs" (=! "ok") gr sameBranchQuery params
--               return $
--                 if ok
--                   then Right forms
--                   else Left (unwords [List.intercalate ", " (fmap Text.unpack floating), "must be in the same branch as", Text.unpack subroot])
--               where
--                 QueryBuilder {querySoFar = sameBranchQuery} =
--                   matchFormulas emptyQB (MatchNodeArg subroot Nothing anyFormula : fmap (\n -> MatchNodeArg n (Just subroot) anyFormula) floating)
--                     `cypherAppend` [i|RETURN true AS ok|]
--             NotFreeIn varname ref -> do
--               let eitherVarId =
--                     maybe
--                       (Left $ "No argument named " ++ Text.unpack varname)
--                       (\case Bolt.I vId -> Right vId; _ -> Left $ "Wrong format for argument named " ++ Text.unpack varname)
--                       (params Map.!? varname)
--               eitherVarArg <-
--                 either
--                   ( \e -> return $
--                       case Map.lookup varname forms of
--                         Just (Atom x) -> Right x
--                         Just _ -> Left (unwords ["Argument", Text.unpack varname, "should be atomic formula"])
--                         Nothing -> Left e
--                   )
--                   (\varId -> Right . head <$> getNodeFormulas gr [varId])
--                   eitherVarId
--               parsedFormulas <-
--                 case ref of
--                   Hypotheses exceptions -> fmap parser <$> getHypotheses exceptions
--                   FormulaRef formulaName ->
--                     let noFormula =
--                           fmap parser
--                             <$> getNodeFormulas
--                               gr
--                               ( maybe
--                                   []
--                                   ( \case
--                                       Bolt.I n -> [n]
--                                       _ -> []
--                                   )
--                                   (Map.lookup formulaName params)
--                               )
--                      in maybe noFormula (\f -> return [Right f]) (Map.lookup formulaName forms)
--               case partitionEithers parsedFormulas of
--                 ([], []) -> return $ Right forms -- can happen in the case of BND, for example
--                 ([], formulas) ->
--                   case eitherVarArg of
--                     Left err -> return $ Left err
--                     Right var ->
--                       case checkNotFreeIn var formulas of
--                         Left err -> return (Left err)
--                         Right _ -> return $ Right (Map.insert varname (Atom var) forms)
--                 (errs, _) ->
--                   return . Left $ List.intercalate "\n\n" (errs :: [String])
--       where
--         checkNotFreeIn :: Text -> [Formula] -> Either String ()
--         checkNotFreeIn varname = List.foldl' checkFormula (Right ())
--           where
--             varFreeIn aFormula = [i|Variable #{varname} is free in #{aFormula}|] :: String
--             checkFormula acc theFormula =
--               if anyFree False theFormula
--                 then Left (varFreeIn theFormula <> otherErrors)
--                 else either Left (\_ -> Right ()) acc
--               where
--                 otherErrors = either id (const "") acc
--                 anyFree isBound =
--                   \case
--                     Atom x | x == varname && not isBound -> True
--                     Atom _ -> False
--                     f@(Formula _ fs) ->
--                       any (anyFree (isBound || varname `elem` operatorBinds f)) fs
--         getHypotheses exceptions =
--           case kind of
--             TableauxSys -> fail "No hypotheses in Tableaux systems"
--             FitchSys _ -> oneResult "getForwardsHypothesesFormulas" (=! hypothesesFormulasReturnName) gr getHypothesesQuery params
--               where
--                 concernedNodes =
--                   concatMap
--                     ( \nodeArgName ->
--                         if nodeArgName `elem` exceptions
--                           then []
--                           else [nodeArgName]
--                     )
--                     nodeArgs
--                   where
--                     nodeArgs = fmap (\((premiseName, _), _) -> premiseName) premises
--                 getHypothesesQuery =
--                   [i|UNWIND [#{Text.intercalate "," (fmap (Text.append "$") concernedNodes)}] AS parentNodeId
-- MATCH (parentNode:Rule)
-- WHERE id(parentNode) = parentNodeId
-- WITH parentNode, CASE NOT exists((parentNode)-[:DerivedBy]->()) WHEN true THEN parentNode ELSE null END AS parentLeaf
-- OPTIONAL MATCH (parentNode)-[:DerivedBy*]->(leafNode)
-- WHERE (NOT exists((leafNode)-[:DerivedBy]->())) AND (NOT (id(leafNode) IN [#{Text.intercalate "," $ fmap (Text.append "$") exceptions}]))
-- WITH coalesce(parentLeaf, []) + collect(leafNode) AS leaves
-- UNWIND leaves AS leafNode
-- MATCH (leafNode)-[:Derives]->(leafFormula)
-- RETURN collect(DISTINCT leafFormula.formula) AS #{hypothesesFormulasReturnName}|]
--             BackNDSys _ -> return [] -- TODO: see issue 42
--           where
--             hypothesesFormulasReturnName = "hypothesesFormulas"

-- data TacticFail
--   = Total
--   | Partial NodeId [GoalId]
--   deriving stock (Eq, Show)

-- data TacticSuccess
--   = Vacuous [GoalId]
--   | Actual NodeId [GoalId]
--   deriving stock (Eq, Show)

-- -- -- for debugging
-- -- listTtoList :: Monad a => ListT a b -> a [b]
-- -- listTtoList xs = do
-- --   step <- next xs
-- --   case step of
-- --     Nil -> return[]
-- --     Cons y ys -> do
-- --       ys' <- listTtoList ys
-- --       return (y : ys')

-- -- listTdropWhile :: Monad m => (a -> Bool) -> ListT m a -> ListT m a
-- -- listTdropWhile p l =
-- --   ListT
-- --     ( do
-- --         n <- next l
-- --         case n of
-- --           Cons x l' | p x -> next $ listTdropWhile p l'
-- --           _ -> return n
-- --     )

-- runStrategyGoals :: CypherBackend c => SystemWithParser -> GraphName -> Strategy -> Maybe RootId -> [GoalId] -> c [Either TacticFail TacticSuccess]
-- runStrategyGoals _ _ _ _ [] = return []
-- runStrategyGoals system gr s mRoot gs@(g : _) = do
--   rootId <- case mRoot of
--     Nothing -> fst <$> getRootAndCrown gr g
--     Just rootId -> return rootId
--   foldM (go rootId) [] gs
--   where
--     f r = runStrategy system gr s r
--     go r rs goal = (: rs) <$> f r goal

-- runStrategy :: CypherBackend c => SystemWithParser -> GraphName -> Strategy -> RootId -> GoalId -> c (Either TacticFail TacticSuccess)
-- -- backtracking is done automatically; we generate a lazy/streaming list (a
-- -- ListT) of all possible results, and whenever an element is produced it
-- -- renders the previous one invalid by backtracking and picking another branch
-- -- in the deduction space, we return the first successful result (if any)
-- runStrategy systemWith@(SystemWithParser (system, _)) gr Strategy {prelude, strategy = tactic} rootId initialGoal =
--   next go >>= findSuccess
--   where
--     findSuccess Nil = return $ Left Total
--     findSuccess (Cons (Right (Actual n [])) _) = return $ Right (Actual n [])
--     findSuccess (Cons _ lr) = next lr >>= findSuccess
--     go = do
--       r <- tacticRun Nothing initialGoal prelude
--       case r of
--         Left Total -> tacticRun Nothing initialGoal tactic
--         Left Partial {} -> return r -- DOUBT: backtrack run?
--         --- if there's no parent node the success was vacuous
--         Right Vacuous {} -> tacticRun Nothing initialGoal tactic
--         Right (Actual _ []) -> return r
--         Right (Actual n goals) -> runGoals tactic n goals
--     runGoals _ _ [] = return $ Left Total
--     runGoals t n [goal] = tacticRun (Just n) goal t
--     runGoals t n goals = runGoals' (\g -> tacticRun Nothing g t) goals
--       where
--         runGoals' f = foldM runGoal (Right $ Vacuous [])
--           where
--             runGoal r goal = combine r <$> f goal
--             combine (Left (Partial _ gs)) r =
--               case r of
--                 Left (Partial _ gs') -> Left (Partial n (gs <> gs'))
--                 Right (Vacuous gs') -> Left (Partial n (gs <> gs'))
--                 Right (Actual _ gs') -> Left (Partial n (gs <> gs'))
--                 Left Total -> Left (Partial n gs)
--             combine r r'@(Left Partial {}) = combine r' r
--             combine (Left Total) (Left Total) = Left Total
--             combine (Left Total) (Right s) = case s of
--               Vacuous gs -> Left (Partial n gs)
--               Actual _ gs -> Left (Partial n gs)
--             combine r r'@(Left Total) = combine r' r
--             combine (Right (Actual _ gs)) (Right s) =
--               case s of
--                 Vacuous gs' -> Right (Actual n (gs <> gs'))
--                 Actual _ gs' -> Right (Actual n (gs <> gs'))
--             combine r@(Right _) r'@(Right Actual {}) = combine r' r
--             combine (Right (Vacuous gs)) (Right (Vacuous gs')) = Right (Vacuous (gs <> gs'))
--     backtrackRun n f =
--       ListT.lift (revertDeduction' system gr n [])
--         >>= f
--     maybeBacktrackRun mn f =
--       maybe ListT.empty (`backtrackRun` f) mn
--     tacticRun mParent (goal :: GoalId) t =
--       case t of
--         TacticFail -> return $ Left Total
--         RuleTactic rule -> do
--           mr <- ListT.lift $ mApplyRule systemWith gr rootId rule (mkArgs goal)
--           return $ case mr of
--             Left {} -> Left Total
--             Right RuleApp {node, goals} -> Right . Actual node $ fmap getGoalId goals
--         RuleWithArgs rule argsQ -> do
--           possibleArgs <- ListT.lift $ ruleArgs gr argsQ (mkArgs goal)
--           case possibleArgs of
--             [] -> return (Left Total) --ListT . return $ Cons (Left Total) (ListT $ return Nil) -- could be simple return (Left Total) ?
--             _ -> goMultiple Nothing possibleArgs
--           where
--             goMultiple _ [] = ListT.empty
--             goMultiple Nothing (args : argss) =
--               applyRule args argss goal
--             goMultiple (Just ruleNode) (args : argss) =
--               backtrackRun ruleNode (applyRule args argss)
--             applyRule args argss theGoal = do
--               appRes <- ListT.lift $ mApplyRule systemWith gr rootId rule (mkArgs theGoal <> args)
--               case appRes of
--                 Left {} -> return (Left Total) <|> goMultiple Nothing argss
--                 Right RuleApp {node, goals} ->
--                   let goals' = fmap getGoalId goals
--                    in return (Right $ Actual node goals') <|> goMultiple (Just node) argss
--         Many t1 ->
--           tacticRun Nothing goal t1 >>= \case
--             Left Total -> return . Right $ Vacuous [goal]
--             r@(Left (Partial n _)) ->
--               return r
--                 -- DOUBT: is this really necessary?
--                 <|> backtrackRun n (\g -> return . Right $ Vacuous [g])
--                 <|> maybeBacktrackRun mParent (\g -> return . Right $ Vacuous [g])
--             Right (Vacuous gs) -> return . Right $ Vacuous gs
--             Right (Actual n []) -> return . Right $ Actual n []
--             Right (Actual n gs) ->
--               runGoals (Many t1) n gs >>= \case
--                 Right (Vacuous gs') -> return . Right $ Actual n gs'
--                 r -> return r
--         Then t1 t2 -> do
--           tr <- tacticRun Nothing goal t1
--           case tr of
--             Right (Vacuous gs) ->
--               tacticRun mParent (Except.assert (gs == [goal]) goal) t2
--             Right (Actual n goals) ->
--               runGoals t2 n goals >>= \case
--                 Left f -> return (Left f)
--                 -- if the second tactic was vacuously successful but
--                 -- the first one was actually successful, the whole
--                 -- thing was successful
--                 Right (Vacuous gs) -> return $ Right (Actual n (Except.assert (Set.fromList gs == Set.fromList goals) gs))
--                 Right s -> return (Right s)
--             Left f -> return $ Left f
--         OrElse t1 t2 -> do
--           tr <- tacticRun Nothing goal t1
--           case tr of
--             Left Total -> tacticRun mParent goal t2
--             Left (Partial n _) ->
--               return tr
--                 <|> backtrackRun n (\g -> tacticRun mParent g t2)
--                 <|> maybeBacktrackRun mParent (\g -> tacticRun mParent g t2)
--             Right s -> return $ Right s
--         If (FunctionQuery _condName condQ) t1 t2 -> do
--           res <-
--             ListT.lift $
--               maybeResult
--                 "if-conditional"
--                 ((=! "result") :: Bolt.Record -> NodeId)
--                 gr
--                 condQ
--                 (Bolt.I <$> mkArgs goal)
--           case res of
--             Just {} -> tacticRun mParent goal t1
--             Nothing -> tacticRun mParent goal t2
--     mkArgs g = Map.fromList [(goalNodeName, g), (rootNodeName, rootId)]

-- maybeResult :: CypherBackend cypher => String -> (Bolt.Record -> a) -> GraphName -> Query -> Params -> cypher (Maybe a)
-- -- ^ Helper function for performing queries over the BOLT protocol
-- -- that return at most one result row
-- maybeResult name f gr queryTxt params = do
--   rs <- queryP gr queryTxt params
--   case rs of
--     [] -> return Nothing
--     [r] -> return . Just $ f r
--     _ ->
--       fail $
--         unwords
--           [ "Internal error: query",
--             name,
--             "with params",
--             show $ Map.toList params,
--             "shouldn't have more than one result"
--           ]

-- oneResult :: CypherBackend cypher => String -> (Bolt.Record -> a) -> GraphName -> Query -> Params -> cypher a
-- -- ^ Helper function for performing queries over the BOLT protocol
-- -- that only return one result row
-- oneResult name f gr queryTxt params = do
--   result <- maybeResult name f gr queryTxt params
--   case result of
--     Nothing ->
--       fail $
--         unwords
--           [ "Internal error: query",
--             concat ["‘", name, "’"],
--             "with params",
--             show $ Map.toList params,
--             "must have exactly one result"
--           ]
--     Just r -> return r

-- | Kinds of proof nodes
data NodeKind
  = LeafNode
  | RuleNode Text
  | GoalNode
  | RootNode
  | CrownNode
  | FormulaNode
  deriving stock (Eq, Show)

-- showKind :: NodeKind -> Text
-- showKind LeafNode = "Leaf"
-- showKind GoalNode = "Goal"
-- showKind (RuleNode n) = n
-- showKind RootNode = "Root"
-- showKind CrownNode = "Crown"
-- showKind FormulaNode = "Formula"

-- -- SIGH: this is so fragile…
-- instance Bolt.RecordValue NodeKind where
--   exactEither (Bolt.T "Leaf") = pure LeafNode
--   exactEither (Bolt.T "Goal") = pure GoalNode
--   exactEither (Bolt.T "Root") = pure RootNode
--   exactEither (Bolt.T "Crown") = pure CrownNode
--   exactEither (Bolt.T "Formula") = pure FormulaNode
--   exactEither (Bolt.T t) = pure $ RuleNode t
--   exactEither x = Left . Bolt.Not . Text.pack $ show x ++ " is not a valid node label"

-- -- instance ToJSON NodeKind where
-- --   toJSON LeafNode = "Leaf"
-- --   toJSON (RuleNode rule) = JSON.object ["rule" .= rule]
-- --   toJSON GoalNode = "Goal"
-- --   toJSON RootNode = "Root"
-- --   toJSON CrownNode = "Crown"
-- --   toJSON FormulaNode = "Formula"

data DeductionRootRawInfo = DeductionRootRawInfo
  { root :: RootId,
    languageInput :: Vector Text,
    goalInput :: Text,
    assumptionsInput :: Vector Text,
    systemName :: Text
  }
  deriving stock (Eq, Generic, Show)

deductionInfoStub :: DeductionRootRawInfo
deductionInfoStub = DeductionRootRawInfo (-1 {-needs to be filled with valid RootId-}) Vector.empty "" Vector.empty ""


showRoots :: Text -> DBio [DeductionRootRawInfo]
-- ^ Return proofs available in the graph backend. The output is a
-- list of pairs (root identifier, formula string).
showRoots username conn = do
  fmap dedFromInfo <$> DB.query conn showProofsQuery [username]
  where
    dedFromInfo :: (RootId, LanguageDefinitions, Text, Vector Text, Text) -> DeductionRootRawInfo
    dedFromInfo (proofId, definitions, goalText, assumptionsText, systemName) =
      DeductionRootRawInfo {
      root = proofId,
      languageInput = Vector.fromList $ printLanguage definitions,
      goalInput = goalText,
      assumptionsInput = assumptionsText,
      systemName = systemName}
    showProofsQuery = [i|SELECT proof._id,
COALESCE(languages.definitions, '[]'),
(SELECT formula._text
 FROM goal, formula
 WHERE goal.proof = proof._id AND goal.formula = formula._id
 LIMIT 1) AS goalId,
(SELECT COALESCE(NULLIF(array_agg(formula._text), '{NULL}'), '{}')
 FROM assumption, formula
 WHERE assumption.proof = proof._id AND assumption.formula = formula._id) AS assumptions,
proof.in_system
FROM proof
  LEFT OUTER JOIN languages ON proof.language_id = languages._id
WHERE proof.owner = ?;|]

-- data ProofNode = -- | Data type for proof nodes
--   ProofNode
--   { -- | node identifier
--     nodeId :: NodeId,
--     -- | subformula derived by this node
--     formula :: Text,
--     -- | node kind
--     kind :: NodeKind,
--     -- | role played by this node
--     role :: Text,
--     -- | optionally: children nodes
--     children :: Maybe [ProofNode]
--   }
--   deriving stock (Show)

-- -- aesonOptions :: JSON.Options
-- -- aesonOptions = JSON.defaultOptions {JSON.omitNothingFields = True}

-- -- instance ToJSON ProofNode where
-- --   toJSON = JSON.genericToJSON aesonOptions

-- proofTree :: CypherBackend cypher => Int -> GraphName -> RootId -> cypher [ProofNode]
-- -- ^ @'proofTree' maxLevel rootId@: Return proof tree rooted at node
-- -- identified by @rootId@, up to @maxLevel@ levels. The root node can
-- -- be the root of any subproof, not necessarily the root of whole
-- -- proof.
-- proofTree maxLevel gr rootId = transact $ go maxLevel rootId
--   where
--     getNodeQuery =
--       [i|MATCH (parent:Rule)-[db:DerivedBy]->(node)-[:Derives]->(f:Formula)
-- WHERE id(parent) = $parentId
-- RETURN id(node) AS nodeId, f.formula AS nodeFormula, db.role AS role|]
--     go :: CypherBackend cypher => Int -> NodeId -> cypher [ProofNode]
--     go level nodeId = do
--       let params = Bolt.props ["parentId" =: nodeId]
--       childrenParts <- fmap getNodeInfo <$> queryP gr getNodeQuery params
--       childrenKinds <- mapM (nodeKind gr . (\(nId, _, _) -> nId)) childrenParts
--       let children = zipWith makeNode childrenParts childrenKinds
--       if level == 0
--         then return children
--         else mapM addChildren children
--       where
--         getNodeInfo r = (r =! "nodeId", r =! "nodeFormula", r =! "role")
--         makeNode (nId, formula, role) kind =
--           ProofNode nId formula kind role Nothing
--         addChildren n@ProofNode {nodeId = _id} = do
--           children <- go (level - 1) _id
--           return n {children = if null children then Nothing else Just children}

-- nodeKind :: CypherBackend cypher => GraphName -> NodeId -> cypher NodeKind
-- nodeKind gr node = oneResult "nodeKindLabel" (=! "kind") gr q ps
--   where
--     q =
--       [i|MATCH (n)
-- WHERE id(n) = $nodeId
-- WITH labels(n) as ls, n
-- RETURN CASE
-- WHEN "Root" IN ls THEN "Root"
-- WHEN "Rule" IN ls THEN n.name
-- WHEN "Goal" IN ls THEN "Goal"
-- WHEN "Leaf" IN ls THEN "Leaf"
-- WHEN "Crown" IN ls THEN "Crown"
-- WHEN "Formula" IN ls THEN "Formula"
-- END AS kind|]
--     ps = Bolt.props ["nodeId" =: node]

deleteDeduction :: RootId -> DBio Bool
-- ^ Delete deduction.
deleteDeduction proofId conn = DB.withTransaction conn $ do
  n <- DB.execute conn deleteQuery (DB.Only proofId)
  return $ n > 0
  where
    deleteQuery =
      [i|DELETE FROM proof WHERE _id = ?;|]

-- deleteSubDeduction :: CypherBackend cypher => GraphName -> NodeId -> cypher Int
-- deleteSubDeduction gr node = oneResult "deleteSubdeduction" (=! "n") gr q ps
--   where
--     ps = Bolt.props ["parentId" =: node]
--     q =
--       [i|MATCH (p)-[:DerivedBy*]->(node)
-- WHERE id(p) = $parentId
-- DETACH DELETE node
-- WITH count(node) AS n, p
-- DETACH DELETE p
-- RETURN n|]

-- tableauxRevertChildrenGoalsParamName, tableauxRevertDataParentsParamName :: Text
-- tableauxRevertChildrenGoalsParamName = "childrenGoals"
-- tableauxRevertDataParentsParamName = "dataParents"

revertDeduction :: System -> NodeId -> DBio GoalId
-- ^ Revert subproof rooted at node with the input identifier. This
-- reverses all the proof steps (rule applications) performed below
-- this node, and it is turned into a goal.
revertDeduction _sys node conn = DB.withTransaction conn $
  DB.execute conn undoDischargeQuery (DB.Only node)
  >> DB.execute conn undoRuleApplicationQuery (DB.Only node)
  >> return node
  where
    undoRuleApplicationQuery = [i|DELETE FROM ruleapp WHERE _id = ?;|]
    undoDischargeQuery = [i|UPDATE assumption
SET dischargedBy = null
FROM (VALUES (?)) AS upd(ruleappId)
WHERE dischargedBy = upd.ruleappId|]


-- collectHypotheses :: CypherBackend cypher => GraphName -> NodeId -> cypher ([NodeId], [NodeId])
-- collectHypotheses gr node = oneResult "collectHypotheses" g gr q ps
--   where
--     g r = (r =! "hyps", r =! "discards")
--     q =
--       [i|MATCH (n)
-- WHERE id(n) = $nodeId
-- MATCH p = (:Root)-[:DerivedBy*]->(n)
-- UNWIND relationships(p) AS db
-- MATCH (parent)-[db]->()
-- WHERE exists(db.introducesHyps)
-- UNWIND db.introducesHyps AS introducesHyp
-- WITH collect(introducesHyp) AS hyps, collect(id(parent)) AS discards
-- RETURN hyps, discards|]
--     ps = Bolt.props ["nodeId" =: node]

-- getRootAndCrown :: CypherBackend cypher => GraphName -> NodeId -> cypher (RootId, NodeId)
-- getRootAndCrown gr node = oneResult "getRootAndCrown" (\r -> (r =! "rootId", r =! "crownId")) gr q ps
--   where
--     q =
--       [i|MATCH (crown:Crown)-[:Meta]->(root:Root)-[:DerivedBy*]->(n)
-- WHERE id(n) = $nodeId
-- RETURN id(root) AS rootId, id(crown) AS crownId
-- LIMIT 1|]
--     ps = Bolt.props ["nodeId" =: node]

data ProofNodeInfo = ProofNodeInfo
  { proofNodeId :: NodeId,
    ruleName :: Text,
    premises :: Vector NodeId,
    derives :: FormulaId
  }
  deriving stock (Generic, Show)

instance DB.FromRow ProofNodeInfo where
    fromRow = ProofNodeInfo <$> Row.field <*> Row.field <*> Row.field <*> Row.field

-- zipWith9 :: (a -> b -> c -> d -> e -> f -> g -> h -> i -> j) -> [a] -> [b] -> [c] -> [d] -> [e] -> [f] -> [g] -> [h] -> [i] -> [j]
-- zipWith9 z (a : as) (b : bs) (c : cs) (d : ds) (e : es) (f : fs) (g : gs) (h : hs) (i' : is) =
--   z a b c d e f g h i' : zipWith9 z as bs cs ds es fs gs hs is
-- zipWith9 _ _ _ _ _ _ _ _ _ _ = []

data AssumptionInfo = AssumptionInfo
  { _id :: NodeId,
    formula :: FormulaId,
    dischargedBy :: Maybe NodeId
  }
  deriving stock (Generic, Show)
  deriving anyclass (DB.FromRow)

data RawProofInfo = RawProofInfo
  { _id :: RootId,
    system :: Text,
    mLanguage :: Maybe LanguageId,
    initialAssumptions :: [AssumptionInfo],
    goals :: [(GoalId, FormulaId)],
    nodesInfo :: [ProofNodeInfo],
    formulas :: [(FormulaId, Text)]
  } deriving stock (Generic)

getRawProofInfo :: RootId -> DBio RawProofInfo
getRawProofInfo root conn = do
  (system, mLanguage) <- queryOneRow conn proofInfoQuery (DB.Only root)
  assumptionInfos <- DB.query conn assumptionInfoQuery (DB.Only root)
  goalInfos <- DB.query conn goalInfoQuery (DB.Only root)
  -- get info we need about all rule nodes in the proof
  nodesInfos <- getRawProofNodes root conn
  formulas <- DB.query conn proofFormulasQuery (DB.Only root)
  return $ RawProofInfo root system mLanguage assumptionInfos goalInfos nodesInfos formulas
  where
    proofInfoQuery = [i|SELECT proof.in_system, proof.language_id
FROM proof
WHERE proof._id = ?;|]
    assumptionInfoQuery = [i|SELECT _id, formula, dischargedBy
FROM assumption
WHERE proof = ?;|]
    goalInfoQuery = [i|SELECT _id, formula
FROM goal
WHERE proof = ?;|]
    proofFormulasQuery = [i|SELECT _id, _text
FROM formula
WHERE proof = ?;|]

getRawProofNodes :: RootId -> DBio [ProofNodeInfo]
getRawProofNodes root conn = do
  DB.query conn ruleAppInfoQuery (DB.Only root)
  where
    ruleAppInfoQuery = [i|SELECT
ruleapp._id,
ruleapp.rule,
COALESCE(NULLIF(array_agg(premise.conclusion_of), '{NULL}'), '{}'),
ruleapp.formula
FROM ruleapp
  LEFT OUTER JOIN premise ON ruleapp._id = premise_of
WHERE ruleapp.proof = ?
GROUP BY ruleapp._id;|]


deleteUserWork :: [DB.Only UserName] -> DBio Bool
-- NOTE: we configured postgresql to delete data belonging to a user
-- when the user is deleted, so this is not actually necessary
deleteUserWork _userNames _conn = do
  --_ <- DB.executeMany conn deleteUserWorkQ userIds
  return True

deleteUserAndWork :: UserName -> DBio Bool
deleteUserAndWork username conn = do
  userName :: [DB.Only UserName] <- DB.query conn deleteUserQ [username]
  case userName of
    [_] -> deleteUserWork userName conn
    [] -> return False
    (_:_:_) -> return False
  where
    deleteUserQ =
      [i|DELETE FROM users
WHERE user_name = ?
RETURNING user_name;|]

deleteStaleTemporaryUsersAndTheirWork :: DBio Bool
deleteStaleTemporaryUsersAndTheirWork conn = do
  -- Delete stale users and their proofs. Stale users are temporary
  -- users who have been created some fixed (but arbitrary) time
  -- before
  _ :: [DB.Only UserName] <- DB.query_ conn deleteUsersQ
  -- TODO: delete their proofs
  -- deletions <- mapM (deleteDeduction gr) roots
  return True
  where
    deleteUsersQ =
      [i|DELETE FROM users
WHERE is_temporary
AND EXTRACT('epoch' from now()) - EXTRACT('epoch' from created_on) > #{userStaleAfterMilliseconds `div` 1000}
RETURNING user_name;|]

userStaleAfterMilliseconds :: Int
userStaleAfterMilliseconds = 30 * 60 * 1000 -- 30 minutes

checkProof :: RootId -> DBio (Fallible ())
-- Assumes no tinkering with the database (i.e., rules were applied
-- legally etc.)
checkProof proofId conn = do
  undischarged :: [FormulaId] <- fmap DB.fromOnly <$> DB.query conn assumptionsDischargedQuery (DB.Only proofId)
  case undischarged of
    [] -> do
      unreached :: [FormulaId] <- fmap DB.fromOnly <$> DB.query conn goalsQuery (DB.Only proofId)
      return $ case unreached of
        [] -> Ok ()
        _ : _ -> Err "There are unreached goals"
    _ : _ -> return (Err "There are undischarged assumptions")
  where
    assumptionsDischargedQuery = [i|SELECT formula
FROM assumption
WHERE proof = ?
  AND dischargedBy IS NULL;|]
    goalsQuery = [i|SELECT goal.formula
FROM goal
WHERE goal.proof = ?
  AND NOT EXISTS (
  SELECT TRUE
  FROM ruleapp
  WHERE ruleapp.formula = goal.formula
    AND ruleapp.proof = goal.proof);|]
