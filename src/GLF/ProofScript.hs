{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module GLF.ProofScript
  (
    ProofScript (..),
    Steps (..),
    proofToLatex,
    --proofScriptFromProof,
    --runProofScript,
--     proofScriptToLatex,
  )
where

import qualified Data.Text as Text
import Data.Aeson (FromJSON (..), ToJSON (..))
import GHC.Generics (Generic)
import GHC.Stack (HasCallStack)
import qualified Data.List as List
import qualified Data.Vector as Vector
import Data.Function (on)
-- import Data.Foldable (foldMap')
-- import Control.Monad
import Data.Maybe
import qualified Data.IntMap as IntMap
import qualified Data.Map.Strict as Map
--import Debug.Trace (trace)

import GLF.Rule
import GLF.System
import GLF.GraphBackend
import GLF.Logic.Latex ((<.>))
import qualified GLF.Logic.Latex as Latex
import GLF.Logic.Base
-- import GLF.Logic.Forwards

data ProofScript = ProofScript
  { system :: Text,
    initialAssumptions :: [(NodeId, Text)],
    language :: LanguageInput,
    steps :: Steps -- proof steps
  }
  deriving stock (Eq, Generic, Show)
  deriving anyclass (FromJSON, ToJSON)

data Steps = Step -- from root to leaves
  { formula :: Text,
    rule :: Text,
    _id :: NodeId,
    discards :: [(Text, NodeId)],
    rulePremises :: [(NodeId {- may beformula or node -}, Text {- corresponding formula -})],
    nextSteps :: [Steps]
  }
  deriving stock (Eq, Generic, Show)
  deriving anyclass (FromJSON, ToJSON)

-- proofScriptFromProof :: HasCallStack => RootId -> DBio ProofScript
-- proofScriptFromProof proofId conn = DB.withTransaction conn $ do
--   -- get info we need about all rule nodes in the proof
--   RawProofInfo {system, mLanguage = mLanguageId, initialAssumptions, nodesInfo} <- getRawProofInfo proofId conn
--   mLanguage <- maybe (return (Just $ LangDefs [])) (`getLanguageInformation` conn) mLanguageId
--   case mLanguage of
--     Nothing -> fail "Failed to find specified language"
--     Just language -> do
--       -- intermediary data structs
--       let rulesPremises = fmap (\ProofNodeInfo{premises} -> premises) <$> nodesInfos
--           --roles = (role :: ProofNodeInfo -> Text) <$> ruleNodeInfos
--           rulesDerives = (derives :: ProofNodeInfo -> FormulaId) <$> nodesInfos
--           -- map from parent id to children information
--           ruleInfoMap = IntMap.fromListWith (<>) . mapMaybe (\(mp, r, rn) -> fmap (,[(r, rn)]) mp) $ zip3 ruleParents roles ruleNodeInfos
--           (initialAssumptionNodeIds, initialAssumptionFormulaIds) = unzip $ fmap (\AssumptionInfo{_id, formula} -> (_id, formula)) initialAssumptions
--           -- and build a map from ids to formula texts
--           formulaMap = IntMap.fromList formulas
--           initialAssumptionTexts = fmap (formulaMap IntMap.!) initialAssumptionFormulaIds
--           -- construct proof steps (from root to leaves)
--           steps = makeSteps ruleInfoMap formulaMap
--       return $ ProofScript {system, language = printLanguage language, initialAssumptions = zip initialAssumptionNodeIds initialAssumptionTexts, steps}
--   where
--     makeSteps childrenMap formulaMap =
--       go root
--       where
--         root =
--           case IntMap.toList $ IntMap.filter (\vs -> _) childrenMap of
--             [r] -> r
--             _ -> fail "Proof has no root"
--         go ProofNodeInfo {proofNodeId = proofNodeId, premises, derives} =
--           Step
--             { formula = formulaOf derives,
--               rule = showKind kind,
--               _id = proofNodeId,
--               discards = discards,
--               rulePremises = zipWith (\n arg -> (n, (arg, formulaOf arg))) ruleArgNames ruleNodeArgs,
--               nextSteps = Bifunctor.second go <$> childrenOf proofNodeId
--             }
--         childrenOf = flip (IntMap.findWithDefault []) childrenMap
--         formulaOf nid = formulaMap IntMap.! nid

-- runProofScript :: forall c. CypherBackend c => System -> ProofScript -> GraphName -> c (Either String RootId)
-- runProofScript System {systemName} ProofScript {system} _
--   | systemName /= system = error "Systems don't match"
-- runProofScript sys@(System {rules, kind}) ProofScript {system, language, initialAssumptions, steps = firstStep@Step {formula = rootFormula}} gr = do
--   languageParserInfoOr <- initializeLanguageParser' gr sys language
--   case languageParserInfoOr of
--     Left err -> return (Left err)
--     Right (logicParser, mLanguageId) -> do
--       let sysWith = SystemWithParser (sys, logicParser)
--       loadRes <-
--         loadRootFormula'
--           sysWith
--           mLanguageId
--           gr
--           deductionInfoStub
--             { goalsInput = case kind of
--                 TableauxSys -> fmap (\Step {formula} -> formula) initialGoals
--                 _ -> [rootFormula],
--               assumptionsInput = initialAssumptionsInput,
--               systemName = system
--             }
--           mempty
--       case loadRes of
--         Left err -> return $ Left err
--         Right ([], _, _) -> return $ Left "No goals"
--         Right (goals@(goal : _), assumptionNodeIds, DeductionRootRawInfo {root}) -> transact $ do
--           case kind of
--             BackNDSys hypothesisDiscardRule ->
--               runBackwardsSteps sysWith hypothesisDiscardRule root firstStep goal
--                 >>= \case
--                   Right {} -> return (Right root)
--                   Left err -> return $ Left err
--             FitchSys _ -> runForwardsSteps sysWith root ([], IntMap.fromList (zip initialAssumptionsFrozenNodeIds assumptionNodeIds)) firstStep
--             TableauxSys ->
--               error "Importing proof script for Tableaux systems not implemented yet" --runTableauxSteps root goals
--   where
--     (initialAssumptionsFrozenNodeIds, initialAssumptionsInput) = unzip initialAssumptions
--     (initialGoals, firstNewSteps) = collectInitialGoals [] ("root", firstStep)
--       where
--         collectInitialGoals initials = \case
--           (_, s@Step {discards = [], args = [], nextSteps}) ->
--             case nextSteps of
--               -- INVARIANT: initial nodes have at most one child
--               [(r, nextStep)] -> collectInitialGoals (s : initials) (r, nextStep)
--               _ -> (initials, nextSteps)
--           s -> (initials, [s])
--     withRule theRuleName cont =
--       case List.find (samePolyName theRuleName . ruleName) rules of
--         Nothing -> return $ Left ("Could not find rule " ++ show theRuleName)
--         Just r -> cont r
--     -- runTableauxSteps root initialGoalsIds = do
--     --   -- Advance until first non-initial node; this node will
--     --   -- indicate its parent, which we have already seen; we then
--     --   -- apply its rule, and derive the current node; Depending on
--     --   -- whether the rule derives goals competitively or
--     --   -- cooperatively, we also derive sibling or descendant nodes,
--     --   -- we save those for later generation/application, and
--     --   -- continue in the same fashion; if a node is a leaf, we apply
--     --   -- close branch to it.
--     --   let initialToBeApplied = Map.fromListWith (++) $ zipWith (\gid step@Step{formula} -> (formula, [step{_id = gid}])) initialGoalsIds initialGoals
--     --   eRes <- go initialToBeApplied firstNewSteps
--     --   return undefined
--     --   where
--     --     go toBeApplied =
--     --       \case
--     --         [] ->
--     --           -- no more steps
--     --           undefined
--     --         currStep@[(_,  Step {args = [(parentArgName, parentFormula)]})] ->
--     --           -- only child: cooperative goal
--     --           case Map.findWithDefault [] parentFormula toBeApplied of
--     --             -- arbitrary pick, as any will yield the same results
--     --             Step {_id, rule} : ss -> withRule rule $ \r ->
--     --               mApplyRule gr r (Map.fromList [(rootNodeName, root), (goalNodeName, _id)])
--     --                 >>= \case
--     --                   Just RuleApp {goals} ->
--     --                     let (toBeApplied', nextSteps') = queueGoals (Map.update (const $ case ss of [] -> Nothing; _ -> Just ss) parentFormula toBeApplied) currStep goals -- add goals to toBeApplied, and skip to the next non-goal
--     --                     in go toBeApplied' nextSteps'
--     --                   Nothing -> return $ Left "Could not apply rule"
--     --               where
--     --                 queueGoals :: Map Text [Steps] -> [(Text, Steps)] -> [Goal] -> (Map Text [Steps], [(Text, Steps)])
--     --                 -- match steps with obtained goals and add them
--     --                 -- to map, waiting to be applied, return the
--     --                 -- first non-matching steps, which are the next
--     --                 -- ones to be applied
--     --                 queueGoals toBeApplied' [(ruleRole, step@Step{formula, nextSteps = nextNextSteps})] goals@(_:_) =
--     --                   case break (\Goal{role=goalRole} -> goalRole == ruleRole) goals of
--     --                     (gs, Goal{node}:gs') -> queueGoals (Map.update (Just . (:) step{_id = node}) formula toBeApplied') nextNextSteps (gs ++ gs')
--     --                     (_, []) -> (toBeApplied', [(ruleRole, step)])
--     --                 queueGoals toBeApplied' nextSteps' _ = (toBeApplied', nextSteps')
--     --             _ -> return $ Left "Tableaux script: no matching parent"
--     --         [(_, Step {args = _})] ->
--     --           -- we expect one argument expliciting the parent node
--     --           -- whose rule application leads to the current step,
--     --           -- so if there's more than one argument or none, the
--     --           -- invariant is broken
--     --           return $ Left "Tableaux script: no parent"
--     --         competing@((_, Step {args = [(parentArgName, parentFormula)]}) : _ : _) ->
--     --           -- more than one sibling step: competitive goals
--     --           if all
--     --             ( \(_, s) -> case s of
--     --                 Step {args = [(parentArgName, f)]} -> f == parentFormula
--     --                 _ -> False
--     --             )
--     --             competing
--     --             then -- the competing goals all have the same
--     --                  -- parent, as expected
--     --             case Map.findWithDefault [] parentFormula toBeApplied of
--     --               Step {_id, rule} : ss -> withRule rule $ \r ->
--     --                 mApplyRule gr r (Map.fromList [(rootNodeName, root), (goalNodeName, _id)])
--     --                   >>= \case
--     --                     Just RuleApp {goals} -> _ (_matchGoalsAndAddToBeApplied (Map.update (const $ case ss of [] -> Nothing; _ -> Just ss) parentFormula toBeApplied))
--     --                     Nothing -> _
--     --               _ -> _
--     --             else -- not ok
--     --               _
--     --         _ -> _ -- err
--     runForwardsSteps sysWith@(SystemWithParser (_, parser)) rootId initial step = do
--       eRes <- go (Right initial) (rootRoleName, step)
--       case eRes of
--         Left err -> return $ Left err
--         Right ([(_, _)], _) ->
--           mApplyRule
--             sysWith
--             gr
--             rootId
--             qedFw
--             ( Map.fromList
--                 [ ( rootNodeName,
--                     rootId
--                   )
--                 ]
--             )
--             >>= \case Left err -> return $ Left ("Proof incomplete\n  " ++ err); Right {} -> return (Right rootId)
--         _ -> return $ Left "Too many results in proofscript execution"
--       where
--         go :: Either String ([(Text, NodeId)], IntMap NodeId) -> (Text, Steps) -> c (Either String ([(Text, NodeId)], IntMap NodeId))
--         go (Left err) _ = return $ Left err
--         go (Right (res, idMap)) (role, Step {_id = _id, rule, discards, formula, nextSteps, args}) = do
--           case IntMap.lookup _id idMap of
--             Nothing ->
--               -- recursively handle children first
--               foldM (\acc steps -> case acc of Left err -> return (Left err); Right {} -> go acc steps) (Right ([], idMap)) nextSteps
--                 -- then on to current step
--                 >>= either (return . Left) doStep
--               where
--                 doStep (args', idMap') = withRule rule $
--                   \theRule@CompiledRule {arguments, description = RuleDescription {results}} -> do
--                     ePreArg <- addFormulaArguments (case arguments of UserInput formulaArgumentInfos _ -> fmap argumentName formulaArgumentInfos; _ -> [])
--                     case ePreArg of
--                       Left err -> return $ Left err
--                       Right preArg -> do
--                         let ruleArguments =
--                               mapMaybe (\(r, dId) -> (r,) <$> (idMap' IntMap.!? dId)) discards
--                                 ++ (rootNodeName, rootId) :
--                               preArg ++ args'
--                         case (results, parser formula) of
--                           ([[rSpec]], Right resultFormula) -> do
--                             mRuleApp <- mApplyRule' sysWith gr rootId theRule (Map.fromList ruleArguments) [(rSpec, resultFormula)]
--                             case mRuleApp of
--                               Left err -> return $ Left ("Could not apply rule " ++ Text.unpack rule ++ " to arguments " ++ show args' ++ " with errors " ++ err)
--                               Right RuleApp {node} -> return $ Right ((role, node) : res, IntMap.insert _id node idMap')
--                           _ -> return (Left "Rule should specify exactly one result")
--                   where
--                     addFormulaArguments argumentNames = do
--                       let (formulaArgumentNames, formulaArguments) =
--                             foldr
--                               (\(n, (_, f)) (ns, fs) -> if n `elem` argumentNames then (n : ns, f : fs) else (ns, fs))
--                               (if resultFormulaArgName `elem` argumentNames then ([resultFormulaArgName], [formula]) else ([], []))
--                               args
--                       fIds <- addFormulas parser gr rootId formulaArguments
--                       return $ fmap (zipWith (\argN (fId, f) -> (argN, fId)) formulaArgumentNames) fIds
--             Just _id' -> return $ Right ((role, _id') : res, idMap)
--     runBackwardsSteps sysWith@(SystemWithParser (_system, parser)) hypothesisDiscardRule root = go
--       where
--         go step@Step{rule = "Leaf"} goal = go step{rule = nameAscii (ruleName hypothesisDiscardRule)} goal
--         go Step {rule, args, nextSteps} goal = do
--           withRule rule $ \theRule -> do
--             -- OPTIMIZE: add all formulas at once, checking for duplicates
--             -- beforehand to avoid this many queries (or assume subformula
--             -- property?)
--             ruleArgsOrErrs <-
--               mapM
--                 ( \(argName, (_, argFormula)) ->
--                     Bifunctor.second (argName,)
--                       <$> addFormula parser gr root argFormula
--                 )
--                 args
--             case sequence ruleArgsOrErrs of
--               Left err -> return $ Left err
--               Right ruleArgsList -> do
--                 mRuleApp <-
--                   mApplyRule
--                     sysWith
--                     gr
--                     root
--                     theRule
--                     ( Map.fromList
--                         ((rootNodeName, root) : (goalNodeName, goal) : ruleArgsList)
--                     )
--                 case mRuleApp of
--                   Left err -> return $ Left ("Could not apply rule " ++ Text.unpack rule ++ " to arguments " ++ show ruleArgsList ++ "with errors " ++ err)
--                   Right RuleApp {goals} -> do
--                     mRes <- mapM (goNextStep goals) nextSteps
--                     case sequence mRes of
--                       Left err -> return (Left err)
--                       Right {} -> return (Right ())
--           where
--             goNextStep goals (goalRole, nextStep) = do
--               let mGoal = List.find ((==) goalRole . (role :: Goal -> Text)) goals
--               case mGoal of
--                 Nothing -> return $ Left ("No goal with matching role " ++ Text.unpack goalRole ++ " found")
--                 Just Goal {node} -> go nextStep node

-- proofScriptToLatex :: SystemWithParser -> ProofScript -> Text
-- -- OPTIMIZE: if Latex is ever too slow we could do the labels from
-- -- Haskell instead of using prfref and friends
-- proofScriptToLatex (SystemWithParser (System{kind, rules = systemRules}, parser)) ProofScript {steps, initialAssumptions} =
--   case kind of
--     TableauxSys ->
--       Latex.toText $
--         tableauxPreamble
--           <.> Latex.environment
--             "document"
--             (Latex.environment "forest" $ tableaux mempty mempty False steps)
--     FitchSys _ ->
--       Latex.toText $
--         fitchPreamble
--           <.> Latex.environment
--             "document"
--             ( Latex.command2 "fitchprf" "" mempty $
--                 fitch (topoSortProofSteps steps)
--             )
--     BackNDSys _ ->
--       Latex.toText $
--         preamble <.> envelope (backwards mempty steps)
--   where
--     getRule' name = fromMaybe (error $ "Unknown rule " ++ Text.unpack name) $ rules Map.!? name
--       where
--         rules = Map.fromList $ concatMap (\r@CompiledRule {ruleName} -> [(ascii ruleName, r), (unicode ruleName, r)]) systemRules
--     ruleLatexName name = latexNameFromRule $ getRule' name
--     latexNameFromRule CompiledRule {ruleName = PolyName {latex}} = "\\" <> Latex.fromText latex
--     formulaLatexText f = case parser f of
--       Left e -> error $ "Could not parse formula:\n  " ++ e
--       Right f' -> Latex.fromText (formulaTextWith (Text.append "\\" . operatorLatexName) f')
--     backwards _ Step {formula, _id, nextSteps = []}
--       | formula `Set.member` initialAssumptionSet =
--         Latex.command "prfbyaxiom" "" "Assumption" <> Latex.braces (formulaLatexText formula)
--     backwards discardedBys Step {formula, _id, nextSteps = []} =
--       maybe "\\prfassumption" (\l -> "\\prfboundedassumption" <> Latex.chevrons l) discardedBy <> Latex.braces (formulaLatexText formula)
--       where
--         discardedBy = IntMap.lookup _id discardedBys
--     backwards discardedBys Step {_id, formula, discards, rule = ruleName, nextSteps} =
--       Latex.command
--         "prftree"
--         "r"
--         ( "$\\scriptstyle " <> ruleLatex
--             <> case discards of
--               [] -> "$"
--               _ -> "_{\\prfref<" <> nId <> ">}$"
--         )
--         <.> mconcat (fmap (Latex.braces . backwards discardedBys' . snd) nextSteps)
--         <.> Latex.braces (formulaLatexText formula)
--       where
--         ruleLatex = ruleLatexName ruleName
--         nId = showLatex _id
--         discardedBys' = discardedBys <> IntMap.fromList ((\(_, dId) -> (dId, nId)) <$> discards)
--     showLatex = Latex.fromText . Text.pack . show
--     initialAssumptionSet = Set.fromList $ fmap snd initialAssumptions
--     fitch orderedSteps = go 1 orderedSteps
--       where
--         go _ [] = mempty
--         go lno (Step {rule, formula, args} : ss) =
--           case action ruleStep of
--             StartsSubProof inNewLevel ->
--               if inNewLevel
--               then
--                 Latex.command2
--                   "subproof"
--                   ""
--                   latexStep
--                   (go (lno + 1) childrenSteps)
--                 <> go (length childrenSteps + lno + 1) followingSteps
--               else -- same level
--                 Latex.command "noalign" "" (Latex.fromText "\\vskip 2mm")
--                 <> latexStep <> ("\\\\" :: Latex.Latex)
--                 <> Latex.fromText "\\ \\\\[-2.5ex] \\cline{1-1}\\\\[-2ex]"
--                 <> go (lno + 1) ss
--             _ -> latexStep <> ("\\\\" :: Latex.Latex) <> go (lno + 1) ss
--           where
--             ruleStep = getRule' rule
--             latexStep = "\n" <>
--               Latex.command "pline" (Text.pack (show lno) <> ".") (formulaLatexText formula)
--                 <> Latex.brackets (Latex.command "formula" "" $ latexNameFromRule ruleStep)
--                 <> foldMap' (\(_argname, (argId, _)) -> Latex.command0 "quad" <> showLatex (nodeIndexMap IntMap.! argId)) args
--             (childrenSteps, followingSteps) = List.span (\Step {rule = ruleName} -> action (getRule' ruleName) /= EndsSubProof) ss
--         nodeIndexMap = IntMap.fromList $ zipWith (\Step {_id} ix -> (_id, ix)) orderedSteps [1 ..]
--     tableaux :: IntMap NodeId -> Map Text (Text, NodeId) -> Bool -> Steps -> Latex.Latex
--     tableaux discardsMap parentInfos noEdge Step {_id, formula, rule = ruleName, args, nextSteps, discards} =
--       Latex.brackets
--         ( Latex.braces
--             ( Latex.command "hypertarget" "" (showLatex _id)
--                 <> Latex.braces (Latex.command "formula" "" (formulaLatexText formula))
--                 <> ruleInfoLatex
--             )
--             <> (if noEdge then ", no edge" else mempty)
--             <.> case nextSteps of
--               [] ->
--                 -- is leaf
--                 case IntMap.lookup _id discardsMap of
--                   Just closesId ->
--                     -- found closing node
--                     Latex.brackets
--                       ( Latex.braces
--                           ( Latex.command "hyperlink" "" (showLatex closesId)
--                               <> Latex.braces (Latex.command "formula" "" $ Latex.command "bigotimes" "" mempty)
--                           )
--                           <> ", no edge"
--                       )
--                   Nothing -> mempty
--               _ ->
--                 let updatedDiscardsMap = case discards of
--                       [] -> discardsMap
--                       _ -> List.foldl' (\acc (_, leafId) -> IntMap.insert leafId _id acc) discardsMap discards
--                  in foldMap' (tableaux updatedDiscardsMap (Map.insert formula (ruleName, _id) parentInfos) (isNothing mParentInfo)) (fmap snd nextSteps)
--         )
--       where
--         -- if links are not working well,
--         -- https://tex.stackexchange.com/questions/411987/how-to-fix-hypertargets-directing-links-to-the-line-after-the-target-it-looks-l

--         ruleInfoLatex =
--           maybe
--             mempty
--             ( \(parentRuleName, parentId) ->
--                 Latex.command0 "quad"
--                   <> Latex.command "hyperlink" "" (showLatex parentId)
--                   <> Latex.braces (Latex.command "formula" "" $ ruleLatexName parentRuleName)
--             )
--             mParentInfo
--         mParentInfo = (\(_, f) -> parentInfos Map.! f) <$> List.lookup parentArgName args
--     tweakingComments =
--       Latex.comment "Some proofs (e.g., large proofs, or proofs with large formulas) might require manual tweaking;"
--       <> Latex.comment "Be sure to read the documentation of the packages below."
--       <> Latex.comment "Packages are sorted by importance (the most relevant ones come first)."
--     tableauxPreamble =
--       Latex.comment "This PDF uses hyperlinks, which are not supported by all PDF readers"
--         <> Latex.comment "Adjust size and import packages as needed."
--         <> Latex.comment "You may need to install additional packages, such as forest."
--         <> tweakingComments
--         <.> Latex.command "documentclass" "a3paper" "article"
--         <.> Latex.command "usepackage" "" "forest"
--         <.> Latex.command "usepackage" "" "amssymb"
--         <.> Latex.command "usepackage" "" "geometry"
--         <.> Latex.command "usepackage" "" "hyperref"
--         <.> (Latex.command "newcommand*" "" "\\formula"
--              <> "[1]" <> (Latex.braces $ Latex.command "ensuremath" "" "#1")) -- \newcommand*{\formula}[1]{\ensuremath{\sf{#1}}}
--     fitchPreamble =
--       Latex.comment "Adjust size and import packages as needed."
--         <> Latex.comment "You may need to install additional packages, such as lplfitch."
--         <> tweakingComments
--         <.> Latex.command "documentclass" "a3paper" "article"
--         <.> Latex.command "usepackage" "" "lplfitch"
--         <.> Latex.command "usepackage" "" "amssymb"
--         <.> Latex.command "usepackage" "" "geometry"
--     preamble =
--       Latex.comment "Adjust size and import packages as needed."
--         <> Latex.comment "You may need to install additional packages, such as prftree."
--         <> tweakingComments
--         <.> Latex.command "documentclass" "a3paper" "article"
--         <.> Latex.command "usepackage" "" "prftree"
--         <.> Latex.command "usepackage" "" "amssymb"
--         <.> Latex.command "usepackage" "landscape" "geometry"
--     envelope l =
--       Latex.environment "document" $
--         Latex.environment "displaymath" $
--           Latex.environment "prfenv" l

-- topoSortProofSteps :: Steps -> [Steps]
-- topoSortProofSteps steps =
--   let (initialPrecedes, initialPrecededBy, initialLeaves, nodesMap) = initialState mempty steps
--       orderedSteps = topoSort initialPrecedes initialPrecededBy initialLeaves Set.empty []
--    in reverse $ fmap (nodesMap IntMap.!) orderedSteps
--   where
--     argumentPrecedences :: IntMap [NodeId]
--     argumentPrecedences = getArgumentPrecedences steps
--       where
--         getArgumentPrecedences Step{args,nextSteps} = IntMap.unionsWith (++) (thisStep : fmap (\(_, stp) -> getArgumentPrecedences stp) nextSteps)
--           where
--             thisStep = go mempty (fmap (\(_, (nId, _)) -> nId) args)
--             go acc [] = acc
--             go acc (a:as) = go (IntMap.insertWith (++) a as acc) as
--     sortOnArgument :: NodeId -> NodeId -> Ordering
--     sortOnArgument n m =
--       case IntMap.lookup n argumentPrecedences of
--         Nothing -> def
--         Just later ->
--           if m `elem` later
--           then LT
--           else def
--       where
--         def =
--           case IntMap.lookup m argumentPrecedences of
--             Nothing -> EQ
--             Just later ->
--               if n `elem` later
--               then
--                 GT
--               else
--                 EQ
--     topoSort :: IntMap [NodeId] -> IntMap [NodeId] -> [NodeId] -> Set.Set NodeId -> [NodeId] -> [NodeId]
--     topoSort precedes precededBy leaves seen ordered =
--       case List.sortBy sortOnArgument leaves of
--         [] -> ordered
--         l : ls ->
--           let preceded = IntMap.findWithDefault [] l precedes
--               (newPrecededBy, newLeaves) =
--                 List.foldl'
--                   ( \(pBy, newLs) m ->
--                       let (mPrecededBy, updatedPrecededBy) = IntMap.updateLookupWithKey (\_ -> Just . filter (l /=)) m pBy
--                        in (updatedPrecededBy,) $ case filter (l /=) <$> mPrecededBy of
--                             Just (_ : _) -> newLs
--                             _ -> m : newLs
--                   )
--                   (precededBy, [])
--                   preceded
--            in if l `Set.member` seen
--                 then topoSort precedes precededBy ls seen ordered
--                 else topoSort precedes newPrecededBy (seq newPrecededBy (newLeaves ++ ls)) (Set.insert l seen) (l : ordered)
--     initialState :: (IntMap [NodeId], IntMap [NodeId], [NodeId], IntMap Steps) -> Steps -> (IntMap [NodeId], IntMap [NodeId], [NodeId], IntMap Steps)
--     initialState (precedes, precededBy, leaves, nodesMap) step@Step {_id, nextSteps} =
--       let stepPrecededBy = fmap (\(_, Step {_id}) -> _id) nextSteps
--           (updatedPrecedes, updatedPrecededBy, updatedLeaves) =
--             case stepPrecededBy of
--               [] -> (precedes, precededBy, _id : leaves)
--               _ ->
--                 let precedes' = List.foldl' (flip $ IntMap.alter (maybe (Just [_id]) (Just . (:) _id))) precedes stepPrecededBy
--                     precededBy' = IntMap.insert _id stepPrecededBy precededBy
--                 in
--                 ( precedes',
--                   precededBy',
--                   leaves
--                 )
--        in List.foldl'
--             (\acc (_, nextStep) -> initialState acc nextStep)
--             (updatedPrecedes, updatedPrecededBy, updatedLeaves, IntMap.insert _id (step {nextSteps = []}) nodesMap)
--             nextSteps


proofToLatex :: HasCallStack => SystemWithParser -> RawProofInfo -> Text
-- OPTIMIZE: if Latex is ever too slow we could do the labels from
-- Haskell instead of using prfref and friends
proofToLatex (SystemWithParser (System{kind, rules = systemRules}, parser)) RawProofInfo{nodesInfo, formulas} =
  case kind of
    TableauxSys ->
      error "TBI"
      -- Latex.toText $
      --   tableauxPreamble
      --     <.> Latex.environment
      --       "document"
      --       (Latex.environment "forest" $ tableaux mempty mempty False steps)
    FitchSys ->
      Latex.toText $
        fitchPreamble
          <.> Latex.environment
            "document"
            ( Latex.command2 "fitchprf" "" mempty $
                fitch (List.sortBy (compare `on` proofNodeId) nodesInfo)
            )
    BackNDSys ->
      error "TBI"
      -- Latex.toText $
      --   preamble <.> envelope (backwards mempty steps)
  where
    formulasMap = IntMap.fromList formulas
    getRule' name = fromMaybe (error $ "Unknown rule " ++ Text.unpack name) $ rules Map.!? name
      where
        rules = Map.fromList $ concatMap (\r@DeductiveRule {name = ruleName} -> [(ascii ruleName, r), (unicode ruleName, r)]) systemRules
    --ruleLatexName name = latexNameFromRule $ getRule' name
    latexNameFromRule DeductiveRule {name = PolyName {latex}} = "\\" <> Latex.fromText latex
    formulaLatexText f = case parser f of
      Err e -> error $ "Could not parse formula:\n  " ++ Text.unpack e
      Ok f' -> Latex.fromText (infixFormulaTextWith (Text.append "\\" . operatorLatexName) f')
    -- backwards _ Step {formula, _id, nextSteps = []}
    --   | formula `Set.member` initialAssumptionSet =
    --     Latex.command "prfbyaxiom" "" "Assumption" <> Latex.braces (formulaLatexText formula)
    -- backwards discardedBys Step {formula, _id, nextSteps = []} =
    --   maybe "\\prfassumption" (\l -> "\\prfboundedassumption" <> Latex.chevrons l) discardedBy <> Latex.braces (formulaLatexText formula)
    --   where
    --     discardedBy = IntMap.lookup _id discardedBys
    -- backwards discardedBys Step {_id, formula, discards, rule = ruleName, nextSteps} =
    --   Latex.command
    --     "prftree"
    --     "r"
    --     ( "$\\scriptstyle " <> ruleLatex
    --         <> case discards of
    --           [] -> "$"
    --           _ -> "_{\\prfref<" <> nId <> ">}$"
    --     )
    --     <.> mconcat (fmap (Latex.braces . backwards discardedBys' . snd) nextSteps)
    --     <.> Latex.braces (formulaLatexText formula)
    --   where
    --     ruleLatex = ruleLatexName ruleName
    --     nId = showLatex _id
    --     discardedBys' = discardedBys <> IntMap.fromList ((\(_, dId) -> (dId, nId)) <$> discards)
    showLatex :: Int -> Latex.Latex
    showLatex = Latex.fromText . Text.pack . show
    -- initialAssumptionSet = Set.fromList $ fmap snd initialAssumptions
    fitch orderedRuleApps = go (1 :: Int) orderedRuleApps
      where
        go _ [] = mempty
        go lno (ProofNodeInfo{ruleName, premises, derives} : nextRuleApps) =
          case action ruleStep of
            StartsSubProof inNewLevel ->
              if inNewLevel
              then
                Latex.command2
                  "subproof"
                  ""
                  latexStep
                  (go (lno + 1) childrenSteps)
                <> go (length childrenSteps + lno + 1) followingSteps
              else -- same level
                Latex.command "noalign" "" (Latex.fromText "\\vskip 2mm")
                <> latexStep <> ("\\\\" :: Latex.Latex)
                <> Latex.fromText "\\ \\\\[-2.5ex] \\cline{1-1}\\\\[-2ex]"
                <> go (lno + 1) nextRuleApps
            _doesntstartsubproof ->
              latexStep <> ("\\\\" :: Latex.Latex) <> go (lno + 1) nextRuleApps
          where
            ruleStep = getRule' ruleName
            latexStep = "\n" <>
              Latex.command "pline" (Text.pack (show lno) <> ".") (formulaLatexText . fromMaybe "?" $ IntMap.lookup derives formulasMap)
                <> Latex.brackets (Latex.command "formula" "" $ latexNameFromRule ruleStep)
                <> Vector.foldMap' (\premiseId -> Latex.command0 "quad" <> showLatex (nodeIndexMap IntMap.! premiseId)) premises
            (childrenSteps, followingSteps) = List.span (\ProofNodeInfo {ruleName = thisRuleName} -> action (getRule' thisRuleName) /= EndsSubProof) nextRuleApps
        nodeIndexMap = IntMap.fromList $ zipWith (\ProofNodeInfo {proofNodeId} ix -> (proofNodeId, ix)) orderedRuleApps [1 ..]
    -- tableaux :: IntMap NodeId -> Map Text (Text, NodeId) -> Bool -> Steps -> Latex.Latex
    -- tableaux discardsMap parentInfos noEdge Step {_id, formula, rule = ruleName, args, nextSteps, discards} =
    --   Latex.brackets
    --     ( Latex.braces
    --         ( Latex.command "hypertarget" "" (showLatex _id)
    --             <> Latex.braces (Latex.command "formula" "" (formulaLatexText formula))
    --             <> ruleInfoLatex
    --         )
    --         <> (if noEdge then ", no edge" else mempty)
    --         <.> case nextSteps of
    --           [] ->
    --             -- is leaf
    --             case IntMap.lookup _id discardsMap of
    --               Just closesId ->
    --                 -- found closing node
    --                 Latex.brackets
    --                   ( Latex.braces
    --                       ( Latex.command "hyperlink" "" (showLatex closesId)
    --                           <> Latex.braces (Latex.command "formula" "" $ Latex.command "bigotimes" "" mempty)
    --                       )
    --                       <> ", no edge"
    --                   )
    --               Nothing -> mempty
    --           _ ->
    --             let updatedDiscardsMap = case discards of
    --                   [] -> discardsMap
    --                   _ -> List.foldl' (\acc (_, leafId) -> IntMap.insert leafId _id acc) discardsMap discards
    --              in foldMap' (tableaux updatedDiscardsMap (Map.insert formula (ruleName, _id) parentInfos) (isNothing mParentInfo)) (fmap snd nextSteps)
    --     )
    --   where
    --     -- if links are not working well,
    --     -- https://tex.stackexchange.com/questions/411987/how-to-fix-hypertargets-directing-links-to-the-line-after-the-target-it-looks-l

    --     ruleInfoLatex =
    --       maybe
    --         mempty
    --         ( \(parentRuleName, parentId) ->
    --             Latex.command0 "quad"
    --               <> Latex.command "hyperlink" "" (showLatex parentId)
    --               <> Latex.braces (Latex.command "formula" "" $ ruleLatexName parentRuleName)
    --         )
    --         mParentInfo
    --     mParentInfo = (\(_, f) -> parentInfos Map.! f) <$> List.lookup parentArgName args
    tweakingComments =
      Latex.comment "Some proofs (e.g., large proofs, or proofs with large formulas) might require manual tweaking;"
      <> Latex.comment "Be sure to read the documentation of the packages below."
      <> Latex.comment "Packages are sorted by importance (the most relevant ones come first)."
    -- tableauxPreamble =
    --   Latex.comment "This PDF uses hyperlinks, which are not supported by all PDF readers"
    --     <> Latex.comment "Adjust size and import packages as needed."
    --     <> Latex.comment "You may need to install additional packages, such as forest."
    --     <> tweakingComments
    --     <.> Latex.command "documentclass" "a3paper" "article"
    --     <.> Latex.command "usepackage" "" "forest"
    --     <.> Latex.command "usepackage" "" "amssymb"
    --     <.> Latex.command "usepackage" "" "geometry"
    --     <.> Latex.command "usepackage" "" "hyperref"
    --     <.> (Latex.command "newcommand*" "" "\\formula"
    --          <> "[1]" <> Latex.braces (Latex.command "ensuremath" "" "#1")) -- \newcommand*{\formula}[1]{\ensuremath{\sf{#1}}}
    fitchPreamble =
      Latex.comment "Adjust size and import packages as needed."
        <> Latex.comment "You may need to install additional packages, such as lplfitch."
        <> tweakingComments
        <.> Latex.command "documentclass" "a3paper" "article"
        <.> Latex.command "usepackage" "" "lplfitch"
        <.> Latex.command "usepackage" "" "amsmath"
        <.> Latex.command "usepackage" "" "amssymb"
        <.> Latex.command "usepackage" "" "geometry"
    -- preamble =
    --   Latex.comment "Adjust size and import packages as needed."
    --     <> Latex.comment "You may need to install additional packages, such as prftree."
    --     <> tweakingComments
    --     <.> Latex.command "documentclass" "a3paper" "article"
    --     <.> Latex.command "usepackage" "" "prftree"
    --     <.> Latex.command "usepackage" "" "amssymb"
    --     <.> Latex.command "usepackage" "landscape" "geometry"
    -- envelope l =
    --   Latex.environment "document" $
    --     Latex.environment "displaymath" $
    --       Latex.environment "prfenv" l

--- no needed anymore since we can order rule application by the node id
-- topoSortProofSteps :: Steps -> [Steps]
-- topoSortProofSteps steps =
--   let (initialPrecedes, initialPrecededBy, initialLeaves, nodesMap) = initialState mempty steps
--       orderedSteps = topoSort initialPrecedes initialPrecededBy initialLeaves Set.empty []
--    in reverse $ fmap (nodesMap IntMap.!) orderedSteps
--   where
--     argumentPrecedences :: IntMap [NodeId]
--     argumentPrecedences = getArgumentPrecedences steps
--       where
--         getArgumentPrecedences Step{args,nextSteps} = IntMap.unionsWith (++) (thisStep : fmap (\(_, stp) -> getArgumentPrecedences stp) nextSteps)
--           where
--             thisStep = go mempty (fmap (\(_, (nId, _)) -> nId) args)
--             go acc [] = acc
--             go acc (a:as) = go (IntMap.insertWith (++) a as acc) as
--     sortOnArgument :: NodeId -> NodeId -> Ordering
--     sortOnArgument n m =
--       case IntMap.lookup n argumentPrecedences of
--         Nothing -> def
--         Just later ->
--           if m `elem` later
--           then LT
--           else def
--       where
--         def =
--           case IntMap.lookup m argumentPrecedences of
--             Nothing -> EQ
--             Just later ->
--               if n `elem` later
--               then
--                 GT
--               else
--                 EQ
--     topoSort :: IntMap [NodeId] -> IntMap [NodeId] -> [NodeId] -> Set.Set NodeId -> [NodeId] -> [NodeId]
--     topoSort precedes precededBy leaves seen ordered =
--       case List.sortBy sortOnArgument leaves of
--         [] -> ordered
--         l : ls ->
--           let preceded = IntMap.findWithDefault [] l precedes
--               (newPrecededBy, newLeaves) =
--                 List.foldl'
--                   ( \(pBy, newLs) m ->
--                       let (mPrecededBy, updatedPrecededBy) = IntMap.updateLookupWithKey (\_ -> Just . filter (l /=)) m pBy
--                        in (updatedPrecededBy,) $ case filter (l /=) <$> mPrecededBy of
--                             Just (_ : _) -> newLs
--                             _ -> m : newLs
--                   )
--                   (precededBy, [])
--                   preceded
--            in if l `Set.member` seen
--                 then topoSort precedes precededBy ls seen ordered
--                 else topoSort precedes newPrecededBy (seq newPrecededBy (newLeaves ++ ls)) (Set.insert l seen) (l : ordered)
--     initialState :: (IntMap [NodeId], IntMap [NodeId], [NodeId], IntMap Steps) -> Steps -> (IntMap [NodeId], IntMap [NodeId], [NodeId], IntMap Steps)
--     initialState (precedes, precededBy, leaves, nodesMap) step@Step {_id, nextSteps} =
--       let stepPrecededBy = fmap (\(_, Step {_id}) -> _id) nextSteps
--           (updatedPrecedes, updatedPrecededBy, updatedLeaves) =
--             case stepPrecededBy of
--               [] -> (precedes, precededBy, _id : leaves)
--               _ ->
--                 let precedes' = List.foldl' (flip $ IntMap.alter (maybe (Just [_id]) (Just . (:) _id))) precedes stepPrecededBy
--                     precededBy' = IntMap.insert _id stepPrecededBy precededBy
--                 in
--                 ( precedes',
--                   precededBy',
--                   leaves
--                 )
--        in List.foldl'
--             (\acc (_, nextStep) -> initialState acc nextStep)
--             (updatedPrecedes, updatedPrecededBy, updatedLeaves, IntMap.insert _id (step {nextSteps = []}) nodesMap)
--             nextSteps
