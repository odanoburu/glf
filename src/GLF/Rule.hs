{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE QuasiQuotes #-}

module GLF.Rule
  ( (.:),
    AnchorStatus(..),
    formulaSpecAnchorable,
    DeductiveRule (..),
    axiom,
    rule,
    premise,
    discharges,
    provided,
    providedNot,
    freeInHypothesesExcept,
    freeIn,
    concludes,
    startsSubProof, startsSubProofNewLevel, closesSubProof,
    help,
    forwardsHypothesesNames,
    premiseNames,
    Premise (..),
    Proviso (..),
    allHypothesesDependingOnMatch,
    FormulaFunction,
    FunctionMap,
    FunctionInfo(..),
    getFunction,
    showProviso,
    Reference (..),
    FormulaId,
    Goal (..),
    GoalId,
    NodeId,
    RootId,
    ArgumentInfo (..),
    NodeArgument (..),
    nodeArgumentInfos,
    RuleArgumentsKind (..),
    ruleArgumentsInfo,
    --TableauxRule (..),
    --TableauxChildren (..),
    RuleApp (..),
    RuleArgs,
    RuleAction (..),
    OpSpec,
    op,
    opn,
    list,
    FormulaSpec (..),
    maybeFormulaSpecName,
    FormulaMatchResult (..),
    formulaMatchesSpec,
    buildFormulaFromSpec,
    showFormulaSpec,
    formulaSubSpecsToEval,
    named,
    substituteForInAs,
    anyFormula,
    eval,
    atomicFormula,
    variable,
    getGoalId,
    resultFormulaArgName,
  )
where

import Control.Applicative
import Data.Aeson (ToJSON (..))
import qualified Data.Aeson as JSON
import Data.Foldable
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.String (IsString (..))
import Data.String.Interpolate (i)
import qualified Data.Text as Text
--import Debug.Trace (trace)
import GHC.Generics (Generic)
import GLF.Logic.Base

type NodeId = Int

type FormulaId = Int

type GoalId = Int

type RootId = Int

data ArgumentInfo = ArgumentInfo
  { argumentName :: Text,
    specification :: FormulaSpec
  }
  deriving stock (Eq, Generic, Ord, Show)

instance ToJSON ArgumentInfo where
  toJSON ArgumentInfo {argumentName, specification} =
    JSON.object ["argumentName" JSON..= argumentName, "specification" JSON..= showFormulaSpec specification]

data NodeArgument = Sole ArgumentInfo | BranchArg ArgumentInfo ArgumentInfo
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (ToJSON)

nodeArgumentInfos :: NodeArgument -> [ArgumentInfo]
nodeArgumentInfos =
  \case
    Sole ai -> [ai]
    BranchArg ai ai' -> [ai, ai']

data RuleArgumentsKind
  = -- NOTE: to get a simple proviso just collect a single dummy argument
    Query
      [ArgumentInfo]
      Text -- query collecting possible arguments, must return any number of
  | -- integer arguments
    UserInput [ArgumentInfo] [NodeArgument] -- formula arguments, rule node arguments
  | NoArguments
  deriving stock (Eq, Generic, Show)
  deriving anyclass (ToJSON)

ruleArgumentsInfo :: RuleArgumentsKind -> [ArgumentInfo]
ruleArgumentsInfo =
  \case
    (Query as _) -> as
    (UserInput as bs) -> as ++ concatMap nodeArgumentInfos bs
    NoArguments -> []

data RuleAction
  = StartsSubProof Bool {- new level? -}
  | EndsSubProof
  -- Continue subproof
  | NoAction
  -- If marked as axiom no hypotheses are made (otherwise a rule
  -- with no premises is considered a hypothesis)
  | Axiom
  deriving stock (Eq, Generic, Show)
  deriving anyclass (ToJSON)

data OperandSpec
  = IndexedOpn Int FormulaSpec
  | OpnList FormulaSpec
  deriving stock (Eq, Ord, Show)

isIndexedOpn :: OperandSpec -> Bool
isIndexedOpn IndexedOpn {} = True
isIndexedOpn OpnList {} = False

operandSpec :: OperandSpec -> FormulaSpec
operandSpec (IndexedOpn _ fs) = fs
operandSpec (OpnList fs) = fs

list :: FormulaSpec -> FormulaSpec
list fs = F (OpSpec Nothing [OpnList fs])

data OpSpec = OpSpec
  { mOp :: Maybe Operator,
    operands :: [OperandSpec]
  }
  deriving stock (Eq, Ord, Show)

instance Semigroup OpSpec where
  OpSpec {mOp = Just {}} <> OpSpec {mOp = Just {}} =
    error "Cannot specify formula operator twice"
  OpSpec {mOp, operands} <> OpSpec {mOp = mOp', operands = operands'} =
    OpSpec (mOp <|> mOp') (operands <> operands')

instance Monoid OpSpec where
  mempty = OpSpec Nothing []

data FormulaSpec
  = Atomic
  | Whatever
  | SubstituteForInAs FormulaSpec {- substitute -} Text {- substituendum -} Text {- in formula -}
  | F OpSpec
  | ToEval Text {- function name -} [FormulaSpec] {- function arguments (name, spec) -} {- TODO: result spec? -}
  | Named Text FormulaSpec
  deriving stock (Eq, Show)

instance Ord FormulaSpec where
  compare Whatever _ = LT
  compare _ Whatever = GT
  compare Atomic Atomic = EQ
  compare Atomic _ = LT
  compare _ Atomic = GT
  compare SubstituteForInAs {} _spec' = GT
  compare _spec SubstituteForInAs {} = LT
  compare (F opSpec) (F opSpec') =
    compare opSpec opSpec'
  compare ToEval{} _spec = GT
  compare _spec ToEval{} = LT
  compare (Named _ spec) (Named _ spec') = compare spec spec'
  compare (Named _ spec) spec' =
    case compare spec spec' of
      EQ -> LT
      x -> x
  compare spec (Named _ spec') =
    case compare spec spec' of
      EQ -> GT
      x -> x

instance IsString FormulaSpec where
  fromString x = named (Text.pack x)

instance Semigroup FormulaSpec where
  Named n spec <> spec' = Named n (spec <> spec')
  spec <> Named n spec' = Named n (spec <> spec')
  Whatever <> fs = fs
  fs <> Whatever = fs
  Atomic {} <> _ = error "FormulaSpec: can't merge atomic specification"
  _ <> Atomic {} = error "FormulaSpec: can't merge atomic specification"
  SubstituteForInAs {} <> _ = error "FormulaSpec: can't merge substitution specification"
  _ <> SubstituteForInAs {} = error "FormulaSpec: can't merge substitution specification"
  F ops <> F ops' = F (ops <> ops')
  ToEval {} <> _ = error "FormulaSpec: can't merge function specification"
  _ <> ToEval {} = error "FormulaSpec: can't merge function specification"


instance Monoid FormulaSpec where
  mempty = Whatever

infix 1 .:
(.:) :: FormulaSpec -> FormulaSpec -> FormulaSpec
formula .: theLabel = op label <> opn 1 formula <> opn 2 theLabel


formulaSubSpecsToEval :: FormulaSpec -> [FormulaSpec]
formulaSubSpecsToEval =
  \case
    Whatever -> []
    Atomic {} -> []
    fs@SubstituteForInAs {} -> [fs]
    F OpSpec {operands} -> foldMap (formulaSubSpecsToEval . operandSpec) operands
    f@(ToEval _ args) -> f : foldMap formulaSubSpecsToEval args
    Named _ f -> formulaSubSpecsToEval f

showFormulaSpec :: FormulaSpec -> Text
-- Show formula representing formula spec
showFormulaSpec = getFormulaText . formulaFromSpec 1
  where
    getFormulaText :: (Int, Formula) -> Text
    getFormulaText (_, f) = infixFormulaText f
    freshName ci = "F_" <> Text.pack (show ci)
    formulaFromSpec ci = \case
      Named n spec ->
        case spec of
          Atomic -> (ci, Atom n)
          Whatever -> (ci, Atom n)
          F opSpec@OpSpec {mOp = Just _} -> showOpSpec ci (Just n) opSpec
          F OpSpec {mOp = Nothing} -> (ci, Atom n)
          SubstituteForInAs{} -> formulaFromSpec ci spec
          ToEval{} -> formulaFromSpec ci spec
          Named {} -> formulaFromSpec ci spec
      Atomic -> (ci + 1, Atom (freshName ci))
      Whatever -> (ci + 1, Atom (freshName ci))
      SubstituteForInAs substitute substituendum inF -> (ci, Atom [i|#{inF}[#{substituendum} ← #{showFormulaSpec substitute}]|])
      F opSpec -> showOpSpec ci Nothing opSpec
      ToEval n fs -> showOpSpec ci Nothing OpSpec{mOp = Just (nonLogicalOperator n $ Fixed (length fs)), operands = zipWith IndexedOpn [1..] fs}
    showOpSpec ci maybeName OpSpec {mOp, operands} =
      case mOp of
        Nothing -> maybe (ci + 1, Atom $ freshName ci) (\n -> (ci, Atom n)) maybeName
        Just theOp@Op {associativity} -> (ci', Formula theOp operands')
          where
            (ci', operands') = case getArity associativity of
              Fixed n -> showOperands n
              AtLeast n -> showOperands n
            showOperands minOperands = go ci 1 operands
              where
                go charI ix remOperands =
                  if ix > minOperands
                    then (charI, []) -- over
                    else case remOperands of
                      [] ->
                        let (charI', rest) = go (charI + 1) (ix + 1) []
                         in (charI', Atom (freshName charI) : rest)
                      (IndexedOpn specIx spec) : remOpnds ->
                        if ix == specIx
                          then
                            let (charI', f) = formulaFromSpec charI spec
                                (charI'', rest) = go charI' (ix + 1) remOpnds
                             in (charI'', f : rest)
                          else
                            let (charI', rest) = go (charI + 1) (ix + 1) remOpnds
                             in (charI', Atom (freshName charI) : rest)
                      OpnList spec : remOpnds ->
                        let (charI', f) = formulaFromSpec charI spec
                            (charI'', rest) = go charI' (ix + 1) remOpnds
                         in (charI'', f : Atom "…" : rest)

maybeFormulaSpecName :: FormulaSpec -> Maybe Text
maybeFormulaSpecName (Named n _) = Just n
maybeFormulaSpecName Atomic = Nothing
maybeFormulaSpecName Whatever = Nothing
maybeFormulaSpecName SubstituteForInAs{} = Nothing
maybeFormulaSpecName (ToEval _ _) = Nothing
maybeFormulaSpecName (F _) = Nothing

allNames :: FormulaSpec -> Set Text
allNames = go Set.empty
  where
    go acc s = case s of
      Atomic -> acc
      Whatever -> acc
      SubstituteForInAs substitute substituendum inF -> List.foldl' (flip Set.insert) (acc `Set.union` allNames substitute) [substituendum, inF]
      F OpSpec {operands} -> foldMap' (go Set.empty) (fmap operandSpec operands) `Set.union` acc
      ToEval _ args -> foldMap' (go Set.empty) args `Set.union` acc
      Named n spec -> go (Set.insert n acc) spec

data FunctionInfo a b = Deterministic (a -> b) | NonDeterministic (a -> b)

getFunction :: FunctionInfo a b -> (a -> b)
getFunction (Deterministic f) = f
getFunction (NonDeterministic f) = f

type FormulaFunction =
  (FunctionInfo [Formula] [Formula], -- calculate
   [Formula] -> Formula -> [Formula]) -- check/unify (return [] if doesn't check, [f] if it does)

type FunctionMap = Map Text FormulaFunction

applyForward :: FunctionMap -> Text -> [Formula] -> [Formula]
applyForward env n args = maybe [] (\(fw, _) -> getFunction fw args) $ Map.lookup n env

checkResult :: FunctionMap -> Text -> [Formula] -> Formula -> [Formula]
checkResult env n args res = maybe []  (\(_, chk) -> chk args res) $ Map.lookup n env

data AnchorStatus = Unanchorable Text
  | Anchorable (Set Text) {- in scope -} (Map Text FormulaSpec) {- to anchor -}
  deriving stock (Show)

instance Semigroup AnchorStatus where
  Unanchorable why <> _ = Unanchorable why
  _ <> Unanchorable why = Unanchorable why
  Anchorable ns xs <> Anchorable ns' xs' = Anchorable (Set.union ns ns') (Map.unionWith max xs xs')

instance Monoid AnchorStatus where
  mempty = triviallyAnchorable Set.empty

triviallyAnchorable :: Set Text -> AnchorStatus
triviallyAnchorable = flip Anchorable Map.empty

formulaSpecAnchorable :: FunctionMap -> Set Text -> FormulaSpec -> AnchorStatus
-- a formula spec is anchored if there's at most one possible
-- formula that satisfies it
formulaSpecAnchorable funcs = go
  where
    go anchors spec =
      case spec of
        Named n spec' ->
          let otherAnchors = -- like those needed by function application
                additionalAnchors (triviallyAnchorable anchors) spec' in
          if n `Set.member` anchors
          then otherAnchors
          else
            case go anchors spec' of
              Anchorable ns as | Map.null as -> Anchorable ns as <> otherAnchors
              _ -> Anchorable (allNames spec') (Map.singleton n spec) <> otherAnchors
          where
            -- mostly for the case of 'SubstituteForInAs'
            additionalAnchors acc =
              \case
                 Named n' spec'' -> additionalAnchors (acc <> triviallyAnchorable (Set.singleton n')) spec''
                 Whatever -> acc
                 Atomic -> acc <> triviallyAnchorable Set.empty
                 SubstituteForInAs substitution substituendum inF ->
                   goSubstitution acc substitution substituendum inF (Just n)
                 F OpSpec{operands} ->
                   List.foldl'
                     (\acc' opnd -> additionalAnchors acc' $ operandSpec opnd)
                     (acc <> triviallyAnchorable Set.empty)
                     operands
                 ToEval fn args -> toEval fn args
        Atomic -> Unanchorable "Can't achor unnamed unspecified formula"
        Whatever -> Unanchorable "Can't anchor unspecified formula"
        SubstituteForInAs substitute substituendum inF ->
          case maybeFormulaSpecName substitute of
            Just _substituteName -> goSubstitution (triviallyAnchorable anchors) substitute substituendum inF Nothing
            Nothing -> Unanchorable "Can't achor unnamed unspecified formula"
        ToEval fn args -> toEval fn args
        F OpSpec {mOp = Nothing} ->
          Unanchorable "Can't anchor formula with missing operator specification"
        F OpSpec {mOp = Just Op{associativity}, operands} ->
           let (oneOpns, listOpns) = List.partition isIndexedOpn operands
               opnsAnchors opns =
                 case foldMap (go anchors . operandSpec) opns of
                   Unanchorable why -> Unanchorable why
                   Anchorable ns xs | Map.null xs -> Anchorable ns xs
                   -- FIXME: this is what we need for subformula
                   -- principle, but not so when we don't need
                   -- the subformula principle
                   Anchorable ns xs -> Anchorable ns $ fmap (const spec) xs
            in ( case getArity associativity of
                   Fixed n ->
                     case compare (length oneOpns) n of
                       EQ -> opnsAnchors oneOpns -- all (if any) list patterns are empty
                       GT -> Unanchorable "More operands specified than operator allows" -- something wrong with the pattern, it will never match
                       LT ->
                         if null listOpns
                           then Unanchorable "Less operands specified than operator allows"
                           else -- some list patterns must be non-empty
                             opnsAnchors operands
                   AtLeast n ->
                     case compare (length oneOpns) n of
                       EQ -> opnsAnchors oneOpns
                       GT -> opnsAnchors operands
                       LT ->
                         if null listOpns
                           then Unanchorable "Less operands specified than operator allows, and no list operand"
                           else opnsAnchors operands
               )
        where
          toEval name args =
              case Map.lookup name funcs of
                Nothing -> Unanchorable ("No known function named " <> name)
                Just (NonDeterministic _, _) -> Unanchorable ("Function " <> name <> "is non-deterministic")
                Just (Deterministic _, _) -> foldMap (go anchors) args
          goSubstitution anchorStatus substitute substituendum inF maybeOutF =
            case (maybeFormulaSpecName substitute, anchorStatus) of
              (Just substituteName, Anchorable anchoredNames' anchors') ->
                anchorStatus <>
                case maybeOutF of
                  Just outF ->
                    if outF `Set.member` anchoredNames
                    then
                      anchorSubstitutionWithFormula inF
                    else
                      if inF `Set.member` anchoredNames
                      then
                        anchorSubstitutionWithFormula outF
                      else
                        Anchorable (Set.singleton outF)
                                   (Map.fromList [(substituteName, substitute),
                                                  (substituendum, named substituendum),
                                                  (inF, named inF)])
                  Nothing ->
                    Anchorable Set.empty
                      (Map.fromList [(substituteName, substitute),
                                      (substituendum, named substituendum),
                                      (inF, named inF)])
                where
                  anchorSubstitutionWithFormula formulaName =
                    case (substituteName `Set.member` anchoredNames, substituendum `Set.member` anchoredNames, formulaName `Set.member` anchoredNames) of
                      (True, True, True) -> Anchorable Set.empty Map.empty
                      (False, True, True) -> Anchorable (Set.singleton substituteName) Map.empty
                      (True, False, True) -> Anchorable (Set.singleton substituendum) Map.empty
                      (True, True, False) -> Anchorable (Set.singleton formulaName) Map.empty
                      (True, False, False) -> Anchorable (Set.singleton formulaName) (Map.fromList [(substituendum, named substituendum)])
                      (False, False, True) -> Anchorable (Set.singleton substituteName) (Map.fromList [(substituendum, named substituendum)])
                      (False, True, False) -> Anchorable (Set.singleton formulaName) (Map.fromList [(substituteName, substitute)])
                      (False, False, False) ->
                        Anchorable (Set.fromList [substituteName, substituendum]) (Map.fromList [ (formulaName, anyFormula) ])
                  anchoredNames = anchoredNames' `Set.union` Map.keysSet anchors'
              (_, Unanchorable err) -> Unanchorable err
              (Nothing, _) -> Unanchorable "Can't achor unnamed unspecified formula"

substituteForInAs :: Text -> Text -> Text -> Text -> FormulaSpec
substituteForInAs substituteName substituendum inF outF = Named outF (SubstituteForInAs (Named substituteName Whatever) substituendum inF)

named :: Text -> FormulaSpec
named n = Named n Whatever

atomicFormula :: FormulaSpec
atomicFormula = Atomic

variable :: Text -> FormulaSpec
variable n = Named n Atomic

anyFormula :: FormulaSpec
anyFormula = Whatever

eval :: Text -> [FormulaSpec] -> FormulaSpec
eval = ToEval

opn :: Int -> FormulaSpec -> FormulaSpec
opn ix fs = F (OpSpec Nothing [IndexedOpn ix fs])

op :: Operator -> FormulaSpec
op o = F $ OpSpec (Just o) []


-- -- checks whether a formula specification is just a name
-- -- or if it actually specifies something
-- formulaSpecMatters :: FormulaSpec -> Bool
-- formulaSpecMatters = \case
--   Atomic {} -> True
--   Ref _ s -> case s of
--     Atomic (Just _) -> True
--     Ref _ _ -> True
--     F (Just _) _ -> True
--     Whatever -> False
--     Atomic Nothing -> True
--     SubstituteForInAs {} -> True
--     F Nothing _ -> formulaSpecMatters s
--   Whatever -> False
--   SubstituteForInAs {} -> True
--   F _ (OpSpec Nothing []) -> False
--   F _ _ -> True

data Premise = Premise
   { root :: FormulaSpec,
     hypotheses :: [(Text, FormulaSpec)],
     role :: Text
   }
  deriving stock (Show)

branchListFormulaSpecs :: Premise -> [FormulaSpec]
branchListFormulaSpecs Premise {root, hypotheses} = root : fmap snd hypotheses

premiseNames :: DeductiveRule -> Set Text
premiseNames DeductiveRule{premises} = List.foldl' (\acc fs -> acc <> allNames fs) Set.empty $ concatMap branchListFormulaSpecs premises

--data TableauxChildren = Competing | Cooperating

-- data TableauxRule = TableauxRule
--   { name :: PolyName,
--     help :: Text,
--     premise :: FormulaSpec,
--     results :: [[FormulaSpec]]
--   }

data Reference
  = FormulaRef Text {- the formula referred to by name -}
  | Hypotheses {- all hypotheses, except those named -} [Text]
  deriving stock (Show)

data Proviso
  = FreeIn Text {- var -} Reference
  | Not Proviso
  -- all hypotheses of premise are of the form of these formulas
  | HypothesesMatch Text [FormulaSpec]
  --  | FreeForIn Text {- term -} Text {- var -} Text {- formula ref -}
  deriving stock (Show)

freeInHypothesesExcept :: Text -> [Text] -> Proviso
freeInHypothesesExcept var except = FreeIn var (Hypotheses except)

freeIn :: Text -> Text -> Proviso
freeIn var formulaName = FreeIn var (FormulaRef formulaName)

allHypothesesDependingOnMatch :: Text -> [FormulaSpec] -> Proviso
allHypothesesDependingOnMatch = HypothesesMatch

showProviso :: Proviso -> Text
showProviso = go False
  where
    negStr isNeg = if isNeg then " not " :: Text else " "
    go isNeg (FreeIn var (Hypotheses _)) = [i|#{var} does#{negStr isNeg}occur free in any hypothesis or undischarged assumption|]
    go isNeg (FreeIn var (FormulaRef formula)) = [i|#{var} does#{negStr isNeg}occur free in #{formula}|]
    --go isNeg (SameBranchAs formulas formulaBranch) = [i|#{Text.intercalate ", " formulas} are#{negStr isNeg}in the same proof branch as #{formulaBranch}|]
    go _ (Not theProviso) = go True theProviso
    go isNeg (HypothesesMatch premiseRole matches) = [i|All hypotheses that depend on #{premiseRole} are #{negStr isNeg}of the form of #{Text.intercalate ", " $ fmap showFormulaSpec matches}|]


data RuleCons
  = PremiseCons Premise
  | ProvisoCons Proviso
  | ConclusionCons FormulaSpec
  | ActionCons RuleAction
  | HelpCons Text

axiom :: PolyName -> [RuleCons] -> DeductiveRule
axiom name ruleConstructors =
  rule name (ActionCons Axiom : ruleConstructors')
  where
    ruleConstructors' = filter (\case
                                   PremiseCons _ -> error "Axiom must not have premises"
                                   _other -> True
                               )
                        ruleConstructors

rule :: PolyName -> [RuleCons] -> DeductiveRule
rule name ruleConstructors =
  case categorizeCons ruleConstructors of
    (_, _, _, _:_:_, _) -> error "Rule must not have more than one action"
    (_, _, [], _, _) -> error "Rule must have a conclusion"
    (_, _, _:_:_, _, _) -> error "Rule must have exactly one conclusion"
    (premises, provisos, [conclusion], acs, helps) ->
      DeductiveRule {
      name,
      premises = reverse premises,
      provisos = reverse provisos,
      conclusion,
      action =
      case acs of
        [] -> NoAction
        ac : _ -> ac,
      helpText = Text.intercalate "\n"  helps
      }

  where
    categorizeCons = go ([], [], [], [], [])
      where
        go acc [] = acc
        go (prems, provs, concs, acs, helps) (cons : conss) =
          flip go conss $
          case cons of
            PremiseCons prem -> (prem:prems, provs, concs, acs, helps)
            ProvisoCons prov -> (prems, prov:provs, concs, acs, helps)
            ConclusionCons conc -> (prems, provs, conc:concs, acs, helps)
            ActionCons ac -> (prems, provs, concs, ac:acs, helps)
            HelpCons h -> (prems, provs, concs, acs, h:helps)

premise :: Text -> FormulaSpec -> RuleCons
premise role root = PremiseCons (Premise { root, role, hypotheses = []})

discharges :: RuleCons -> (Text, FormulaSpec) -> RuleCons
(PremiseCons prem@Premise{hypotheses}) `discharges` hyp = PremiseCons (prem {hypotheses = hyp : hypotheses})
_ `discharges` _ = error "Can only discharge from premise. Use as:\\n  premise <premise name> <premise formula spec> `discharges` (<hypothesisName>, <hypothesis formula specification>)"

provided :: Proviso -> RuleCons
provided = ProvisoCons

providedNot :: Proviso -> RuleCons
providedNot prov = ProvisoCons (Not prov)

concludes :: FormulaSpec -> RuleCons
concludes = ConclusionCons


startsSubProof, startsSubProofNewLevel, closesSubProof :: RuleCons
startsSubProof = ActionCons (StartsSubProof False)
startsSubProofNewLevel = ActionCons (StartsSubProof True)
closesSubProof = ActionCons EndsSubProof

help :: Text -> RuleCons
help = HelpCons

data DeductiveRule = DeductiveRule
  { name :: PolyName,
    premises :: [Premise],
    provisos :: [Proviso],
    conclusion :: FormulaSpec,
    action :: RuleAction,
    helpText :: Text -- TODO: generate automatically
  }
  deriving stock (Show)

forwardsHypothesesNames :: DeductiveRule -> [Text]
forwardsHypothesesNames DeductiveRule {premises} = concatMap go premises
  where
    go Premise {hypotheses} = fmap fst hypotheses

-- findNamedSubSpecs :: Text -> FormulaSpec -> [FormulaSpec]
-- findNamedSubSpecs needle = go
--   where
--     go s = case s of
--       Whatever -> []
--       Ref n s' -> if n == needle then [s] else go s'
--       Atomic (Just n) -> [s | n == needle]
--       Atomic Nothing -> []
--       SubstituteForInAs _ _ _ outF -> [s | outF == needle]
--       F Nothing OpSpec {operands} -> concatMap (go . operandSpec) operands
--       F (Just n) OpSpec {operands} -> if n == needle then [s] else concatMap (go . operandSpec) operands

resultFormulaArgName :: Text
resultFormulaArgName = "Result formula"

data Goal = Goal
  { goalRole :: Text,
    node :: GoalId,
    derives :: FormulaId,
    introducedHyps :: [FormulaId]
  }
  -- NOTE: any changes here must reflect on the queries returning goals (type
  -- system does not catch these errors)
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON)

getGoalId :: Goal -> GoalId
getGoalId Goal {node} = node

type RuleArgs = Map Text NodeId

data RuleApp = RuleApp
  { ruleApplied :: PolyName,
    node :: NodeId,
    derives :: FormulaId,
    goals :: [Goal] -- NOTE: no goals ↔ is leaf
  }

-- optionallyMatchSpecsCypher :: [FormulaSpec] -> (Text, Text)
-- optionallyMatchSpecsCypher specs = ("matchingSpecifications", querySoFar)
--   where
--     QueryBuilder{querySoFar} = finish $ List.foldl' go (0 :: Int, [], q) specs
--     go (ix, names, qb@QueryBuilder{withVars}) spec =
--       (ix + 1, nameToReturn:names,
--        qb'{withVars = Set.insert nameToReturn withVars} `cypherWith` [UpdateIfExists [i|(id(#{nameToReturn})=id(#{goalDerivesNodeName}) OR null) AND true|] nameToReturn])
--       where
--         qb' = formulaSpecCypher' Optional (qb `cypherAppend` [i|MATCH (#{nameToReturn}) WHERE #{nameToReturn}.root = #{goalDerivesNodeName}.root\n|]
--                                               `cypherWith` [Plus nameToReturn nameToReturn]) spec'
--         def = [i|_spec_#{ix}|]
--         (spec', nameToReturn) = mkSpec spec
--         mkSpec = \case
--           Whatever -> (F (Just def) mempty, def)
--           Ref n spec'' ->
--             let (spec''', name) = mkSpec spec''
--             in (Ref n spec''', name)
--           F (Just n) opSpec -> (F (Just n) opSpec, n)
--           F Nothing opSpec -> (F (Just def) opSpec, def)
--     finish (_, namesToReturn, qb) =
--       qb `cypherAppend`
--         [i|RETURN [#{Text.intercalate "," . fmap (<> " IS NOT NULL") $ reverse namesToReturn}] AS matchingSpecifications|]
--     q =
--       QueryBuilder
--         { referenceVar = goalDerivesNodeName,
--           withVars = Set.fromList [goalDerivesNodeName],
--           querySoFar =
--             [i|MATCH (#{goalNodeName}:Goal)-[:Derives]->(#{goalDerivesNodeName})
-- WHERE id(#{goalNodeName}) = $#{goalNodeName}\n|]
--         }

-- shallowMatches :: Maybe Text -> [(Int, Maybe Text)] -> FormulaSpec -> Bool
-- shallowMatches mainOp childrenOps =
--   \case
--     Whatever -> True
--     Atomic _ -> isNothing mainOp && null childrenOps
--     Ref {} -> False
--     SubstituteForInAs {} -> False
--     F _ OpSpec {mOp, operands} ->
--       matchesOp mainOp mOp && fst (List.foldl' matchesOperand (True, List.sort childrenOps) operands)
--       where
--         matchesOperand (doesMatch, actualOps) (IndexedOpn ix spec) =
--           case dropWhile (\(ix', _) -> ix' < ix) actualOps of
--             (ix', mChildOp) : restOps | ix == ix' -> (doesMatch && opMatches, restOps)
--               where
--                 opMatches = case spec of
--                   Atomic {} -> isNothing mChildOp
--                   Whatever -> True
--                   Ref {} -> False
--                   SubstituteForInAs {} -> False
--                   F _ OpSpec {mOp = childOpSpec} -> matchesOp mChildOp childOpSpec
--             -- no child matching spec
--             _ -> (False, [])
--         -- TODO: it's always ok to return True, but ofc this defeats
--         -- the purpose of this function
--         matchesOperand (doesMatch, actualOps) (OpnList _spec) = (doesMatch && True, actualOps)
--         matchesOp actualOp specOp =
--           case (specOp, actualOp) of
--             (Nothing, Nothing) -> True
--             (Nothing, Just _) -> True
--             (Just Op {name = PolyName {unicode}}, Just op') -> op' == unicode
--             (Just _, Nothing) -> False

data FormulaMatchResult
  = MissingInformation
  | NoMatch String
  | Matches
  deriving stock (Eq)

instance Semigroup FormulaMatchResult where
  MissingInformation <> _ = MissingInformation
  _ <> MissingInformation = MissingInformation
  NoMatch err <> _ = NoMatch err
  _ <> NoMatch err = NoMatch err
  Matches <> Matches = Matches

instance Monoid FormulaMatchResult where
  mempty = Matches

formulaMatchesSpec :: FunctionMap -> Map Text Formula -> FormulaSpec -> Formula -> (Map Text Formula, FormulaMatchResult)
formulaMatchesSpec funcMap = go
  where
    noMatch xs = NoMatch (List.unwords xs)
    --
    genericNoMatch env spec f = (env, noMatch ["Can't match specification", Text.unpack (showFormulaSpec spec), "with formula", show f])
    --
    go env (Named n spec) formula =
      let matchesOrNot theEnv = go theEnv spec formula in
      case Map.lookup n env of
        Just prevFormula ->
          if prevFormula == formula
          then matchesOrNot env
          else (env, noMatch ["Can't match formula", Text.unpack $ infixFormulaText prevFormula, "with", Text.unpack $ infixFormulaText formula])
        Nothing -> matchesOrNot (Map.insert n formula env)
    go env Atomic Atom {} = (env, Matches)
    go env Whatever _ = (env, Matches)
    go env s@(ToEval fname args) formula =
      let argFormulas = mapMaybe (\argSpec -> maybeFormulaSpecName argSpec >>= \argName -> argName `Map.lookup` env) args
      in
        case checkResult funcMap fname argFormulas formula of
          _res:__ -> (env, Matches)
          _doesntcheckout ->
            case buildFormulaFromSpec funcMap env s of
              Just f ->
                if f == formula
                then (env, Matches)
                else genericNoMatch env s formula
              Nothing -> (env, MissingInformation)
    go env (SubstituteForInAs substitute substituendum oldF) formula =
      let substituteName = fromMaybe (error "Substitute in formula substitution must be named") $ maybeFormulaSpecName substitute
       in case (Map.lookup substituteName env, Map.lookup substituendum env, Map.lookup oldF env) of
            (Just substituteForm, Just substituendumForm, Just oldForm) ->
              case go env substitute substituteForm of
                (env', Matches) ->
                  case mSubstituteForIn substituteForm substituendumForm oldForm of
                    Right f ->
                      if f == formula
                      then (env', Matches)
                      else (env', noMatch ["Can't match substitution result", show f, "with expected formula", show formula])
                    Left err -> (env', NoMatch err)
                (env', nomatch) -> (env', nomatch)
            (Just substituteForm, Nothing, Just oldFormula) ->
              -- (∀ y y) matches forall "var" (substituteForInAs "var" "term" "f" "_")
              case Set.toList $ findSubstitutionWithSubstituendum substituteForm formula oldFormula of
                [substituendumForm] ->
                  go (Map.insert substituendum substituendumForm env) substitute substituteForm
                [] -> (env, noMatch ["No unification possible"])
                (_ : _ : _) -> (env, noMatch ["Too many unification solutions"])
            (Nothing, Just substituendumForm, Just oldFormula) ->
              case Set.toList $ findSubstitutionWithSubstituendum substituendumForm oldFormula formula of
                [substituteForm] ->
                   case go env substitute substituteForm of
                    (env', Matches) -> (Map.insert substituteName substituteForm env', Matches)
                    (env', nomatch) -> (env', nomatch)
                [] -> (env, noMatch ["No unification possible"])
                (_ : _ : _) -> (env, noMatch ["Too many unification solutions"])
            (Just substituteForm, Just substituendumForm, Nothing) ->
              case go env substitute substituteForm of
                (env', Matches) ->
                  case mSubstituteForIn substituendumForm substituteForm formula of
                    Right oldFormula -> (Map.insert oldF oldFormula env', Matches)
                    Left err -> (env', NoMatch err)
                (env', nomatch) -> (env', nomatch)
            (Nothing, Nothing, Just oldFormula) ->
              case Set.toList $ findSubstitution oldFormula formula of
                [(substituteForm, substituendumForm)] ->
                  ( Map.insert substituteName substituteForm $ Map.insert substituendum substituendumForm env,
                   Matches )
                [] -> if oldFormula == formula -- no actual substitution
                      then (env, Matches)
                      else (env, noMatch ["No unification possible"])
                _morethanone -> (env, noMatch ["Too many unification solutions"])
            __ ->
              (env, MissingInformation)
    go env (F OpSpec {mOp = Nothing, operands = []}) _f =
      -- matches anything (technically shouldn't match atomic formulas)
      (env, Matches)
    go env (F OpSpec {mOp = Just specOp, operands}) f@(Formula theOp _)
      | specOp == theOp =
        -- check operands now
        go env (F OpSpec {mOp = Nothing, operands}) f
    go parentEnv parentSpec@(F OpSpec {mOp = Nothing, operands}) parentF@(Formula _ formulaOperands) =
      checkOperands parentEnv (sortOperandSpecs operands) formulaOperands
      where
        sortOperandSpecs remaining =
          case span isIndexedOpn remaining of
            ([], []) -> []
            ([], x : remaining') -> x : sortOperandSpecs remaining'
            (indexed, remaining') ->
              List.sortBy (\a b -> case (a, b) of (IndexedOpn ix _, IndexedOpn j _) -> compare ix j; _ -> GT) indexed
                ++ sortOperandSpecs remaining'
        checkOperands env [] = const (env, Matches)
        checkOperands env (IndexedOpn _ spec : specs) =
          \case
            [] -> genericNoMatch env parentSpec parentF
            opnd : opnds ->
              let (env', res) = go env spec opnd
                  (env'', res') = checkOperands env' specs opnds
               in (env'', res <> res')
        checkOperands env [OpnList spec] =
          \case
            [] -> (env, Matches)
            opnd : opnds -> (env, List.foldr (\a acc -> let (_, res) = go env spec a in res <> acc) (snd $ go env spec opnd) opnds)
        checkOperands env (OpnList spec : nextOperandSpec : specs) =
          \case
            [] ->
              if any isIndexedOpn specs
                then genericNoMatch env parentSpec parentF
                else (env, Matches)
            opnds ->
              let (_listOk, remainder) = List.break (\f -> snd (go env spec f) == Matches) opnds
               in checkOperands env (nextOperandSpec : specs) remainder
    go env spec f =
      genericNoMatch env spec f

buildFormulaFromSpec :: FunctionMap -> Map Text Formula -> FormulaSpec -> Maybe Formula
buildFormulaFromSpec funcMap env = go
  where
    go =
      \case
        Named n spec ->
          case Map.lookup n env of
            Just formula ->
              case formulaMatchesSpec funcMap env spec formula of
                (_, Matches) -> Just formula
                _nomatch -> Nothing
            Nothing -> go spec
        Whatever -> Nothing
        Atomic -> Nothing
        ToEval fname args ->
          -- TODO: do we need args to be formula specs? check the
          -- systems we have (if not we can just make them
          -- names/refs)
          case fmap (applyForward funcMap fname) (mapM go args) of
            Just (f : _other) -> Just f
            _noresult -> Nothing
        SubstituteForInAs substitute substituendum inf ->
          (do
              substituteFormula <- go substitute
              substituendumFormula <- Map.lookup substituendum env
              inFormula <- Map.lookup inf env
              rightToMaybe $ mSubstituteForIn substituteFormula substituendumFormula inFormula)
        F opspec ->
          case opspec of
            OpSpec Nothing _ -> Nothing
            OpSpec (Just opr) operands ->
              if length operands < arityNumber (operatorArity opr) || not (all isIndexedOpn operands)
              then Nothing
              else -- assume order is correct
                let operandFormulas = mapM (go . operandSpec) operands
                in fmap (Formula opr) operandFormulas
