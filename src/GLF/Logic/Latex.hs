module GLF.Logic.Latex
  ( Latex,
    (<.>),
    command0,
    command,
    command2,
    environment,
    comment,
    braces,
    brackets,
    chevrons,
    fromText,
    toText,
  )
where

import Data.List.NonEmpty (NonEmpty((:|)))
import Data.Semigroup (sconcat)
import Data.String (IsString (..))
import Data.Text (Text)
import qualified Data.Text as Text

newtype Latex = Latex (Text -> Text)

instance Show Latex where
  show (Latex l) = Text.unpack (l "")

instance Semigroup Latex where
  (Latex l) <> (Latex l') = Latex (l . l')

instance Monoid Latex where
  mempty = Latex id

instance IsString Latex where
  fromString s = Latex $ \t -> Text.pack s <> t

(<.>) :: Latex -> Latex -> Latex
l <.> l' = sconcat (l :| ["\n", l'])

environment :: Text -> Latex -> Latex
environment label (Latex l) =
  Latex $ \t ->
    "\\begin{" <> label <> "}\n"
      <> l "\n\\end{"
      <> label
      <> "}"
      <> t

command0 :: Text -> Latex
command0 label = Latex $ \t -> "\\" <> label <> "{}" <> t

command :: Text -> Text -> Latex -> Latex
command label option (Latex l) =
  Latex $ \t ->
    "\\"
      <> label
      <> ( if Text.null option
             then "{"
             else "[" <> option <> "]{"
         )
      <> l "}"
      <> t

command2 :: Text -> Text -> Latex -> Latex -> Latex
command2 label option l l' =
  command label option l <> braces l'

comment :: Text -> Latex
comment c = Latex $ \t -> "% " <> c <> "\n" <> t

braces :: Latex -> Latex
braces = between ("{", "}")

chevrons :: Latex -> Latex
chevrons = between ("<", ">")

brackets :: Latex -> Latex
brackets = between ("[", "]")

between :: (Text, Text) -> Latex -> Latex
between (s, e) (Latex l) = Latex $ \t -> s <> l e <> t

fromText :: Text -> Latex
fromText t = Latex $ \t' -> t <> t'

toText :: Latex -> Text
toText (Latex l) = l ""
