{-# LANGUAGE QuasiQuotes #-}

-- |
-- Module      : GLF.Logic.Backwards
-- Description : Definitions for Backwards-style deduction systems
-- Copyright   : (c) bruno cuconato, 2022-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Backwards
  ( --hypothesisDiscard,
  )
where

-- -- | Discard a hypothesis and close the goal, if possible.
-- --
-- -- Backwards Natural Deduction-style deduction systems only.
-- hypothesisDiscard :: CompiledRule -- special rule
-- hypothesisDiscard =
--   CompiledRule
--     { ruleName =
--         (PolyName {ascii = "hypothesisDiscard", unicode = "hypothesis ⌁", latex = "mathrm{hypothesisDiscard}"}),
--       provisos = [],
--       action =
--         NoAction,
--       arguments =
--         Query
--           []
--           [i|MATCH (#{goalNodeName}:Goal)-[:Derives]->(ded)
-- WHERE id(#{goalNodeName}) = $#{goalNodeName} AND id(ded) IN #{goalNodeName}.hyps
-- RETURN id(ded) AS _|],
--       query =
--         ([], [i|MATCH (leaf:Goal)-[:Derives]->(ded)
-- WHERE id(leaf) = $#{goalNodeName}
-- WITH leaf, id(ded) AS dedId, size(leaf.hyps) - 1 AS len
-- UNWIND range(0, len) AS ix
-- MATCH (hypIntroNode:Rule)
-- WHERE leaf.hyps[ix] = dedId AND id(hypIntroNode) = leaf.discards[ix]
-- CREATE (hypIntroNode)<-[h:Closes {role: "hypothesis"}]-(leaf)
-- WITH collect(h) AS _, leaf, dedId
-- REMOVE leaf:Goal
-- SET leaf:Leaf
-- SET leaf.hyps = null
-- SET leaf.discards = null
-- RETURN id(leaf) AS ruleId, dedId AS derives, [] AS goals|]),
--       description =
--         RuleDescription
--           { help = "Discard hypothesis and turn goal into leaf",
--             results = [],
--             premises = []
--           }
--     }
