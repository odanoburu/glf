{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- |
-- Module      : GLF.Logic.Parser
-- Description : Common parts of logic language parsers
-- Copyright   : (c) bruno cuconato, 2020
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
--
-- Declares common functions and types for logic language parsers.
module GLF.Logic.Parser
  ( LogicParser,
    Operator (..),
    Parser,
    Text,
    LanguageId(..),
    atomLabel,
    int,
    parens,
    parseLogic,
    nonLogicalDefinition,
  )
where

import Control.Monad
import qualified Control.Monad.Combinators.Expr as Expr
import Data.Bifunctor (first)
import Data.Char
import qualified Data.List as List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import qualified Data.Text as Text
import Data.Void (Void)
import GLF.Logic.Base
import Text.Megaparsec hiding (label, some, token)
import Text.Megaparsec.Char (space, string, char)
--import Text.Megaparsec.Debug
import qualified Text.Megaparsec.Char.Lexer as L

---
-- Parsing
type LogicParser = Text -> Fallible Formula

type Parser = Parsec Void Text

spaceConsumer :: ParsecT Void Text m ()
spaceConsumer = L.space spaces empty empty
  where
    spaces = void $ takeWhile1P Nothing isSpace

lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceConsumer

symbol :: Text -> Parser Text
symbol = L.symbol spaceConsumer

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

makeOperatorTable :: [Operator] -> OperatorTable'
-- for prefix formula parsing, megaparsec takes care of infix parsing
makeOperatorTable opss =
  let kvs = concatMap (\op@Op {name = PolyName{ascii, unicode}} -> fmap (,op) [unicode, ascii]) opss
   in List.foldl' go mempty kvs
  where
    go :: OperatorTable' -> (Text, Operator) -> OperatorTable'
    go (OpTable opTable) (k, v) =
      OpTable $
        Map.insertWith
          ( \a b ->
              if a /= b
                then error ("repeat operator " ++ Text.unpack k)
                else a
          )
          k
          v
          opTable

-- | Data type for an operator table. See 'makeOperatorTable'.
newtype OperatorTable' = OpTable (Map Text Operator)
  deriving newtype (Semigroup, Monoid)

atomLabel :: Parser Text
-- ^ An atom label is a nonempty string without whitespace nor
-- parentheses
atomLabel = takeWhile1P Nothing isAlphaNum

int :: Parser Int
int = lexeme L.decimal

operatorForbiddenChars :: [Char]
-- ^ Symbolic and punctuation characters which may not be part of an
-- operator string name.
operatorForbiddenChars = ['(', ')', ',', '.']

parseFormula :: Parser Text -> [Operator] -> Parser Formula
-- atomLabelParser parses atom label and nothing more (specially not
-- whitespace)
parseFormula atomLabelParser opss =
  space
    *> ( -- try prefixFormula
         --   <|>
         infixFormula

       )
      <* eof
  where
    atomParser = Atom <$> atomLabelParser
    opParser = do
      opStr <-
        lexeme
        (Text.cons <$> satisfy (\c -> isSymbol c || isPunctuation c)
         <*> takeWhileP
          (Just "operator character")
          (\c ->
              not (isSpace c || c `elem` operatorForbiddenChars))
        <?> "operator")
      let mOp = Map.lookup opStr simplifiedOpTable
      case mOp of
        Just op -> return op
        Nothing -> fail $ "Unrecognized operator ‘" ++ Text.unpack opStr ++ "’"
      where
        (OpTable simplifiedOpTable) = makeOperatorTable opss
    infixFormula = Expr.makeExprParser atomOrOtherOp infixOperatorTable
      where
        infixOperatorTable = fmap snd
          . Map.toDescList
          . Map.fromListWith (<>) $ mapMaybe (\op -> infixMaybe op >>= \exprOp -> fmap (\(Precedence prec) -> (prec, [exprOp])) (opPrecedence op)) opss
        atomOrOtherOp =
          parens infixFormula <|> lexeme (atomParser <* notFollowedBy (char '(')) <|> go
        infixMaybe op@Op {name = PolyName{ascii, unicode}, associativity} =
          case associativity of
            Prefix{} -> Just (Expr.Prefix unary)
            Postfix{} -> Just (Expr.Postfix unary)
            InfixLeft{} -> Just (Expr.InfixL binary)
            InfixRight{} -> Just (Expr.InfixR binary)
            InfixNone{} -> Just (Expr.InfixN binary)
            None _ -> Nothing
          where
            thisOpParser = lexeme . choice $ fmap string [unicode, ascii]
            unary = thisOpParser >> pure (\e -> Formula op [e])
            binary = thisOpParser >> pure (\e1 e2 -> Formula op [e1, e2])
        go = do
          op@Op {associativity} <- opParser
          case associativity of
            None arity -> operands op (case arity of Fixed n -> (n, n); AtLeast n -> (n, maxBound))
            _infix -> fail (Text.unpack (operatorUnicodeName op) ++ " operator uses infix notation")
          where
            operands op (_, 0) = return (Formula op [])
            operands op (l, u) = parens $
              do
                opnd <- infixFormula
                opnds <- count' (l - 1) (u - 1) (symbol "," *> infixFormula)
                return $ Formula op (opnd:opnds)
    prefixFormula =
      lexeme atomParser
        <|> parens go
      where
        go = do
          op@Op {associativity} <- opParser
          let (l, u) =
                case associativity of
                  Prefix{} -> (1, 1)
                  Postfix{} -> (1, 1)
                  InfixLeft{} -> (2, 2)
                  InfixRight{} -> (2, 2)
                  InfixNone{} -> (2, 2)
                  None (Fixed n) -> (n, n)
                  None (AtLeast n) -> (n, maxBound)
          operands <- count' l u prefixFormula
          return $ Formula op operands

parseLogic :: Parser Text -> [Operator] -> (Text -> Fallible Formula)
-- ^ Defined a parser for formulas of a given logic. @parseLogic
-- atom ops@ is a multifix parser; it may parse restricted
-- S-expressions, infix expressions, or a mix of both. In the case
-- of S-expressions, allowed forms are either atoms parseable by
-- @atom@, or lists whose head elements and number of elements are
-- specified by @ops@. I.e.,
--
-- [(-> a (| b c)), a -> |(b c), a -> b | c]
--
-- are all valid ways of writing the same formula.
parseLogic atomParser opTable input =
  fromEitherStr . first errorBundlePretty $
    parse go "" input
  where
    go = parseFormula atomParser opTable

parseAs :: Parser a -> Text -> Either String a
parseAs p input = first errorBundlePretty $ parse p "" input


nonLogicalDefinition :: Text -> Either String (Text, Arity)
nonLogicalDefinition = parseAs go
  where
    go = do
      n <- space *> optional (char '.') *> lexeme atomLabel
      arityN <- int
      isVariableArity <- isJust <$> optional (symbol "+")
      _ <- eof
      return (Text.cons '.' n, if isVariableArity then AtLeast arityN else Fixed arityN)
