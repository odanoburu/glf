{-# LANGUAGE QuasiQuotes #-}

-- |
-- Module      : GLF.Logic.Tableaux
-- Description : Definitions for Tableaux-style deduction systems
-- Copyright   : (c) bruno cuconato, 2022-…
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
module GLF.Logic.Tableaux
  ( -- * Tableaux meta-rules
    -- closeTableaux,
    -- closeSignedTableaux,

    -- * Common Tableaux operators and formula specifiers

    -- negation,
    -- negationOf,
    -- true,
    -- false,
    -- isTrue,
    -- isFalse,
  )
where

-- -- | Close Tableaux branch, if possible.
-- closeTableaux :: CompiledRule
-- closeTableaux =
--   CompiledRule
--     { ruleName = name,
--       action = EndsSubProof,
--       arguments =
--         Query
--           []
--           [i|#{findOpposite}
-- RETURN id(oppositeFormula) AS ok|],
--       provisos = [],
--       query =
--       ([], [i|#{findOpposite}
-- REMOVE #{goalNodeName}:Goal
-- REMOVE derivesOpposite:Goal
-- SET #{goalNodeName}:Leaf, #{goalNodeName}.name = "#{unicode name}", #{goalNodeName}.argNames = #{goalNodeName}.argNames + "#{closesArgName}", #{goalNodeName}.args = #{goalNodeName}.args + id(derivesOpposite)
-- SET derivesOpposite.name = "#{unicode name}"
-- CREATE (derivesOpposite)<-[:Closes]-(#{goalNodeName})
-- WITH #{goalNodeName},derivesOpposite,oppositeFormula
-- OPTIONAL MATCH (#{goalNodeName})-[:DerivedBy*]->(useless)
-- DETACH DELETE useless
-- WITH collect(#{goalNodeName}) AS _,derivesOpposite,#{goalNodeName},oppositeFormula
-- MATCH (#{crownNodeName})-[:Meta]->(r:Root)
-- WHERE id(r) = $#{rootNodeName}
-- MERGE (#{goalNodeName})-[:Meta]->(#{crownNodeName})
-- RETURN id(derivesOpposite) AS ruleId, id(oppositeFormula) AS derives, [] AS goals|]),
--       description =
--         RuleDescription
--           { help = "Close Tableaux branch if possible.",
--             results = [],
--             premises = [(("positive",atomicFormula), []), (("negative", negationOf atomicFormula), [])]
--           }
--     }
--   where
--     findOpposite :: Text
--     findOpposite =
--       [i|MATCH (#{goalNodeName}:Goal)-[:Derives]->(#{goalDerivesNodeName})
-- WHERE id(#{goalNodeName}) = $#{goalNodeName}
-- WITH #{goalNodeName}, #{goalDerivesNodeName}
-- OPTIONAL MATCH (#{goalDerivesNodeName})<-[:Builds]-(negatedAtom)
-- WHERE #{goalDerivesNodeName}.op = "¬" AND negatedAtom.op IS NULL
-- WITH #{goalNodeName}, #{goalDerivesNodeName}, negatedAtom
-- OPTIONAL MATCH (#{goalDerivesNodeName})-[:Builds]->(atomNegation)
-- WHERE #{goalDerivesNodeName}.op IS NULL AND atomNegation.op = "¬"
-- WITH #{goalNodeName}, #{goalDerivesNodeName}, negatedAtom, atomNegation
-- MATCH (oppositeFormula)
-- WHERE id(oppositeFormula) = id(atomNegation) OR id(oppositeFormula) = id(negatedAtom)
-- WITH #{goalNodeName}, #{goalDerivesNodeName}, oppositeFormula
-- MATCH (derivesOpposite)-[:DerivedBy*]->(#{goalNodeName})
-- WHERE (derivesOpposite)-[:Derives]->(oppositeFormula)|]
--     name = PolyName {ascii = "closeTableaux", unicode = "closeTableaux", latex = "mathrm{close}"}

-- -- | Close signed Tableaux branch, if possible.
-- closeSignedTableaux :: CompiledRule
-- closeSignedTableaux =
--   CompiledRule
--     { ruleName = name,
--       action = EndsSubProof,
--       provisos = [],
--       arguments =
--         Query
--           []
--           [i|#{findOpposite}
-- RETURN id(_oppositeFormula) AS ok|],
--       query =
--               ([], [i|#{findOpposite}
-- REMOVE #{goalNodeName}:Goal
-- REMOVE derivesOpposite:Goal
-- SET #{goalNodeName}:Leaf, #{goalNodeName}.name = "#{unicode name}", #{goalNodeName}.argNames = #{goalNodeName}.argNames + "#{closesArgName}", #{goalNodeName}.args = #{goalNodeName}.args + id(derivesOpposite)
-- SET derivesOpposite.name = "#{unicode name}"
-- CREATE (derivesOpposite)<-[:Closes]-(#{goalNodeName})
-- WITH #{goalNodeName},derivesOpposite,_oppositeFormula
-- OPTIONAL MATCH (#{goalNodeName})-[:DerivedBy*]->(useless)
-- DETACH DELETE useless
-- WITH collect(#{goalNodeName}) AS _,derivesOpposite,#{goalNodeName},_oppositeFormula
-- MATCH (#{crownNodeName})-[:Meta]->(r:Root)
-- WHERE id(r) = $#{rootNodeName}
-- MERGE (#{goalNodeName})-[:Meta]->(#{crownNodeName})
-- RETURN id(derivesOpposite) AS ruleId, id(_oppositeFormula) AS derives, [] AS goals|]),
--       description =
--         RuleDescription
--           { help = "Close signed Tableaux branch if possible.",
--             results = [],
--             premises = [(("true", isTrue atomicFormula), []), (("false", isFalse atomicFormula), [])]
--           }
--     }
--   where
--     findOpposite :: Text
--     findOpposite =
--       [i|MATCH (#{goalNodeName}:Goal)-[:Derives]->(#{goalDerivesNodeName})
-- WHERE id(#{goalNodeName}) = $#{goalNodeName}
-- WITH #{goalNodeName}, #{goalDerivesNodeName}
-- MATCH (#{goalDerivesNodeName})<-[:Builds{opn:1}]-(_atom)
-- WHERE #{goalDerivesNodeName}.op = "⊤" OR #{goalDerivesNodeName}.op = "⊥" AND _atom.op IS NULL
-- WITH #{goalNodeName}, #{goalDerivesNodeName}, _atom
-- MATCH (_atom)-[:Builds]->(_oppositeFormula)
-- WHERE (_oppositeFormula.op = "⊤" OR _oppositeFormula.op = "⊥") AND _oppositeFormula.op <> #{goalDerivesNodeName}.op
-- WITH #{goalNodeName}, #{goalDerivesNodeName}, _oppositeFormula
-- MATCH (derivesOpposite)-[:DerivedBy*]->(#{goalNodeName})
-- WHERE (derivesOpposite)-[:Derives]->(_oppositeFormula)|]
--     name = PolyName {ascii = "closeSignedTableaux", unicode = "closeSignedTableaux", latex = "mathrm{close}"}

-- negation :: Operator
-- negation = prefixOp "¬" "~" "neg"

-- negationOf :: FormulaSpec -> FormulaSpec
-- negationOf f = op negation <> opn 1 f

-- true :: Operator
-- true = prefixOp "⊤" "+" "top"

-- false :: Operator
-- false = prefixOp "⊥" "-" "bot"

-- isTrue :: FormulaSpec -> FormulaSpec
-- isTrue f = op true <> opn 1 f

-- isFalse :: FormulaSpec -> FormulaSpec
-- isFalse f = op false <> opn 1 f
