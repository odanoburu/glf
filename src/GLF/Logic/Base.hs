{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleInstances #-}

-- |
-- Module      : GLF.Logic.Base
-- Description : Library of logic functions
-- Copyright   : (c) bruno cuconato, 2020
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
--
-- Library module for logic functions. Mostly export useful functions
-- from other modules.
module GLF.Logic.Base
  ( Arity (..),
    arityNumber,
    operatorArity,
    Associativity (..),
    Precedence(..),
    opPrecedence,
    mFixedArity,
    getArity,
    formulaFreeVariables,
    formulaBoundVariables,
    freeInFormula,
    notFreeInFormula,
    Fallible(..),
    failed,
    fromEitherStr,
    LanguageId(..),
    LanguageDefinitions(..),
    Index,
    Formula (..),
    Operator (..),
    nonLogicalOperator,
    operatorBinds,
    operatorUnicodeName,
    operatorLatexName,
    label,
    appendNames,
    PolyName (..),
    polyname,
    nameAscii,
    samePolyName,
    monoName,
    asciiName,
    newLevelName,
    operator,
    nullaryOp,
    prefixOp,
    postfixOp,
    rightAssociativeOp,
    leftAssociativeOp,
    binaryOp,
    naryOp,
    variadicOp,
    binaryBinds,
    formulaText,
    infixFormulaText,
    formulaTextWith,
    infixFormulaTextWith,
    showFormula,
    subformulas,
    OperatorRule (..),
    OperatorQualification (..),
    operatorRuleName,
    introductionRuleName,
    eliminationRuleName,
    version1, version2, version3, version4,
    mSubstituteForIn,
    findSubstitution,
    findSubstitutionWithSubstituendum,
    rightToMaybe,
    Text,
  )
where

import Control.Monad (ap)
import Data.Aeson (ToJSON,FromJSON)
import qualified Data.Aeson as JSON
import Data.Foldable
import qualified Data.List as List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
--import Debug.Trace (trace)
import GHC.Generics (Generic)
import qualified Database.PostgreSQL.Simple.ToField as DBToField
import qualified Database.PostgreSQL.Simple.FromField as DBFromField
import qualified Database.PostgreSQL.Simple.TypeInfo.Static as TI

data PolyName = PolyName {ascii :: Text, unicode :: Text, latex :: Text}
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (ToJSON)

instance Show PolyName where
  show PolyName {unicode} = Text.unpack unicode

polyname :: Text -> Text -> Text -> PolyName
polyname unicode ascii latex = PolyName{ascii, unicode, latex}

nameAscii :: PolyName -> Text
nameAscii PolyName{ascii} = ascii

monoName :: Text -> PolyName
monoName t = PolyName t t t

appendNames :: PolyName -> PolyName -> PolyName
appendNames PolyName{ascii, unicode, latex} PolyName{ascii=ascii2, unicode=unicode2, latex=latex2}
  = PolyName {ascii = ascii <> ascii2, unicode = unicode <> unicode2, latex = latex <> latex2}

newLevelName :: Text -> PolyName
-- name function for rules that introduce a subproof at a new level
-- in Fitch systems
newLevelName name = PolyName {ascii = name <> "(+1)", unicode = name <> "(+1)", latex = Text.concat ["mathrm{", name, "}"]}

asciiName :: Text -> PolyName
asciiName name = PolyName {ascii = name, unicode = name, latex = Text.concat ["operatorname{", name, "}"]}

samePolyName :: Text -> PolyName -> Bool
samePolyName name PolyName {ascii, unicode, latex} = name `elem` [ascii, unicode, latex]

data Fallible a = Err Text | Ok a
  deriving stock (Show, Generic)

instance Semigroup a => Semigroup (Fallible a) where
  (Ok a) <> (Ok b) = Ok (a <> b)
  (Ok _) <> (Err err) = Err err
  (Err err) <> (Ok _) = Err err
  (Err err1) <> (Err err2) = Err (Text.concat [err1, "\n", err2])

instance Monoid a => Monoid (Fallible a) where
  mempty  = Ok mempty
  mappend = (<>)

instance Functor Fallible where
    fmap f (Ok a) = Ok (f a)
    fmap _ (Err err) = Err err

instance Applicative Fallible where
    pure  = Ok
    (<*>) = ap

instance Monad Fallible where
    return = pure
    Ok a >>= k = k a
    Err err >>= _ = Err err

failed :: [Text] -> Fallible a
failed errComponents = Err (Text.concat errComponents)

fromEitherStr :: Either String a -> Fallible a
fromEitherStr (Left err) = Err (Text.pack err)
fromEitherStr (Right ok) = Ok ok

newtype LanguageId = LanguageId Int
  deriving (Eq, Ord, Show, Num, DBToField.ToField, DBFromField.FromField, ToJSON, FromJSON) via Int

newtype LanguageDefinitions = LangDefs [(Text, Arity)]
  deriving (Eq, Ord, Show, ToJSON, FromJSON) via [(Text, Arity)]

instance DBFromField.FromField LanguageDefinitions where
    fromField f bs
      | DBFromField.typeOid f /= TI.textOid  = DBFromField.returnError DBFromField.Incompatible f ""
      | otherwise =
        case fmap JSON.decodeStrict' bs of
          Just (Just defs) -> pure defs
          Just Nothing -> DBFromField.returnError DBFromField.ConversionFailed f ""
          Nothing -> DBFromField.returnError DBFromField.UnexpectedNull f ""

data Arity = Fixed Int | AtLeast Int
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

newtype Precedence = Precedence Double
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (ToJSON)

data Associativity
  = -- for unary operators
    Prefix Precedence
  | Postfix Precedence
  | -- for binary operators
    InfixRight Precedence
  | InfixLeft Precedence
  | InfixNone Precedence
  | -- for binary and above
    None Arity
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (ToJSON)

type Index = Int

data Operator = Op
  { -- | Operator name
    name :: PolyName,
    -- | Associativity of operator
    associativity :: Associativity,
    -- | Bind the operands with indices in the list
    binds :: [Int]
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (ToJSON)

opPrecedence :: Operator -> Maybe Precedence
opPrecedence Op{associativity} =
  case associativity of
    Prefix prec -> Just prec
    Postfix prec -> Just prec
    InfixLeft prec -> Just prec
    InfixRight prec -> Just prec
    InfixNone prec -> Just prec
    None _ -> Nothing

nonLogicalOperator :: Text -> Arity -> Operator
nonLogicalOperator n arity = Op {name = asciiName n, associativity = None arity, binds = []}

operatorArity :: Operator -> Arity
operatorArity Op{associativity} = getArity associativity

arityNumber :: Arity -> Int
arityNumber = \case
  Fixed n -> n
  AtLeast n -> n

operatorBinds :: Formula -> [Text]
operatorBinds = \case
  Atom _ -> []
  Formula Op {binds} fs -> concatMap go $ zip [1 ..] fs
    where
      go (opndIx, Atom varname) = [varname | opndIx `elem` binds]
      go _ = []

-- | Define a zero arity operator (a constant)
nullaryOp :: Text -> Text -> Text -> Operator
nullaryOp unicode ascii latex = Op (PolyName {unicode, ascii, latex}) (None $ Fixed 0) []

prefixOp, postfixOp :: Text -> Text -> Text -> Double -> Operator

-- | Define a unary prefix operator
prefixOp unicode ascii latex prec = Op (PolyName {unicode, ascii, latex}) (Prefix (Precedence prec)) []

-- | Define a unary postfix operator
postfixOp unicode ascii latex prec = Op (PolyName {unicode, ascii, latex}) (Postfix (Precedence prec)) []

rightAssociativeOp,
  leftAssociativeOp,
  binaryOp ::
    Text -> Text -> Text -> Double -> Operator

-- | Define a binary operator that associates to the right
rightAssociativeOp unicode ascii latex prec = Op (PolyName {unicode, ascii, latex}) (InfixRight (Precedence prec)) []

-- | Define a binary operator that associates to the left
leftAssociativeOp unicode ascii latex prec = Op (PolyName {unicode, ascii, latex}) (InfixLeft (Precedence prec)) []

-- | Define a binary operator that does not associate
binaryOp unicode ascii latex prec = Op (PolyName {unicode, ascii, latex}) (InfixNone (Precedence prec)) []

naryOp, variadicOp :: Text -> Text -> Text -> Int -> Operator

-- | Define an operator with fixed arity
naryOp unicode ascii latex n = Op (PolyName {unicode, ascii, latex}) (None (Fixed $ fromEnum n)) []

-- | @varyOp name altNames n@ Define an operator with variable arity
-- of at least @n@
variadicOp unicode ascii latex n = Op (PolyName {unicode, ascii, latex}) (None (AtLeast $ fromEnum n)) []

binaryBinds :: Text -> Text -> Text -> Operator
binaryBinds unicode ascii latex = Op (PolyName {unicode, ascii, latex}) (None (Fixed 2)) [1]

getArity :: Associativity -> Arity
getArity = \case
  Prefix{} -> Fixed 1
  Postfix{} -> Fixed 1
  InfixLeft{} -> Fixed 2
  InfixRight{} -> Fixed 2
  InfixNone{} -> Fixed 2
  None a -> a

mFixedArity :: Associativity -> Maybe Int
mFixedArity = \case
  Prefix{} -> Just 1
  Postfix{} -> Just 1
  InfixLeft{} -> Just 2
  InfixRight{} -> Just 2
  InfixNone{} -> Just 2
  None (Fixed n) -> Just n
  None AtLeast {} -> Nothing

label :: Operator
label = binaryOp ":" ":" "mathbin{:}" 500

data Formula = Atom Text | Formula Operator [Formula]
  deriving stock (Eq, Ord)

subformulas :: Formula -> Set Formula
subformulas n@Atom {} = Set.singleton n
subformulas n@(Formula _ fs) = Set.unions (Set.singleton n : fmap subformulas fs)

subformulaOf :: Formula -> Formula -> Bool
f `subformulaOf` g = f `Set.member` subformulas g

operator :: Formula -> Maybe Operator
operator Atom {} = Nothing
operator (Formula op _) = Just op

showFormula :: (Operator -> String) -> Formula -> String
showFormula f form =
  case form of
    Atom x -> Text.unpack x
    Formula op [] -> concat ["(", f op, ")"]
    Formula op fs -> concat ["(", f op, " ", unwords (fmap (showFormula f) fs), ")"]

formulaTextWith :: (Operator -> Text) -> Formula -> Text
formulaTextWith f form =
  case form of
    Atom x -> x
    Formula op [] -> Text.concat ["(", f op, ")"]
    Formula op fs -> Text.concat ["(", f op, " ", Text.unwords (fmap (formulaTextWith f) fs), ")"]

data Side = L | R | N deriving stock (Eq)

infixFormulaTextWith :: (Operator -> Text) -> Formula -> Text
infixFormulaTextWith f = go
  where
    go = \case
      Atom a -> a
      Formula op@Op{associativity} fs ->
        case (associativity, fs) of
          (Prefix prec, [child]) ->
            f op <> " " <> parensIf R (prec, L) (getOp child) (go child)
          (Prefix _, _) -> error "Prefix formula must have one operand"
          (Postfix prec, [child]) ->
            parensIf L (prec, R) (getOp child) (go child) <> " " <> f op
          (Postfix _, _) -> error "Postfix formula must have one operand"
          (InfixRight prec, [lchild, rchild]) -> goBinary (prec, R, op) lchild rchild
          (InfixRight _, _) -> error "InfixRight formula must have two operands"
          (InfixNone prec, [lchild, rchild]) -> goBinary (prec, N, op) lchild rchild
          (InfixNone _, _) -> error "InfixNone formula must have two operands"
          (InfixLeft prec, [lchild, rchild]) -> goBinary (prec, L, op) lchild rchild
          (InfixLeft _, _) -> error "InfixLeft formula must have two operands"
          (None (Fixed 0), []) ->
            f op
          (None _arity, children) ->
            f op <> parens (Text.intercalate ", " $ fmap go children)
    goBinary (prec, side, op) lchild rchild =
      parensIf L (prec, side) (getOp lchild) (go lchild)
      <> " " <> f op <> " "
      <> parensIf R (prec, side) (getOp rchild) (go rchild)
    parens t = Text.concat ["(", t, ")"]
    parensIf side (outerPrec, outerSide) (Just (innerPrec, innerSide)) =
      if innerPrec < outerPrec
         || innerPrec == outerPrec
            && (innerSide /= outerSide || side /= outerSide)
      then parens
      else id
    parensIf _ _ _ = id
    getOp Atom{} = Nothing
    getOp (Formula Op{associativity} _) =
      case associativity of
        Prefix prec -> Just (prec, L)
        Postfix prec -> Just (prec, R)
        InfixLeft prec -> Just (prec, L)
        InfixRight prec -> Just (prec, R)
        InfixNone prec -> Just (prec, N)
        None _ -> Nothing

formulaText :: Formula -> Text
formulaText = formulaTextWith operatorUnicodeName

infixFormulaText :: Formula -> Text
infixFormulaText = infixFormulaTextWith operatorUnicodeName

operatorUnicodeName :: Operator -> Text
operatorUnicodeName Op {name = PolyName {unicode}} = unicode

operatorLatexName :: Operator -> Text
operatorLatexName Op {name = PolyName {latex}} = latex

instance Show Formula where
  show = showFormula (show . name)

introductionRuleName :: Operator -> PolyName
introductionRuleName = flip operatorRuleName (Introduction NoS)

eliminationRuleName :: Operator -> PolyName
eliminationRuleName = flip operatorRuleName (Elimination NoS)

version1, version2, version3, version4 :: PolyName -> PolyName
version1 name = name `appendNames` polyname "₁" "1" "_{1}"
version2 name = name `appendNames` polyname "₂" "2" "_{2}"
version3 name = name `appendNames` polyname "₃" "3" "_{3}"
version4 name = name `appendNames` polyname "₄" "4" "_{4}"

data OperatorQualification
  = LeftS
  | RightS
  | NoS
  | TrueV
  | FalseV

data OperatorRule
  = Introduction OperatorQualification
  | Elimination OperatorQualification

operatorRuleName :: Operator -> OperatorRule -> PolyName
operatorRuleName Op {name = PolyName {ascii, unicode, latex}} r =
  case r of
    Introduction d -> go "I" d
    Elimination d -> go "E" d
  where
    go typ side =
      PolyName {ascii = ascii <> suffix, unicode = unicode <> suffix, latex = Text.concat [latex, "\\mathrm{", suffix, "}"]}
      where
        suffix =
          typ
            <> case side of
              NoS -> ""
              RightS -> "-r"
              LeftS -> "-l"
              TrueV -> "-T"
              FalseV -> "-F"


freeInFormula :: Text -> Formula -> Bool
freeInFormula varname f =
  -- case f of
  --   Formula op [formula, _label]
  --     | op == label -> freeInFormula varname formula
  --   _nonlabelled ->
      not $ varname `Set.member` formulaBoundVariables f

notFreeInFormula :: Text -> Formula -> Bool
notFreeInFormula varname f =
  not $ varname `Set.member` formulaFreeVariables f

mSubstituteForIn :: Formula -> Formula -> Formula -> Either String Formula
mSubstituteForIn substitute substituendum inF =
  case subst Set.empty inF of
    Just f -> Right f
    Nothing ->
      let substituteStr = showFormula (Text.unpack . operatorUnicodeName) substitute
          substituendumStr = showFormula (Text.unpack . operatorUnicodeName) substituendum
          formulaStr = showFormula (Text.unpack . operatorUnicodeName) inF
       in Left
            ( unwords
                [ "Term",
                  substituteStr,
                  "not free for variable",
                  substituendumStr,
                  "in formula",
                  formulaStr
                ]
            )
  where
    substituteFreeVars = collect Set.empty Set.empty substitute
      where
        collect frees bounds =
          \case
            Atom x -> if x `Set.member` bounds then frees else Set.insert x frees
            f@(Formula _ fs) -> foldMap (collect frees (List.foldl' (flip Set.insert) bounds binds)) fs
              where
                binds = operatorBinds f
    subst bounds f =
      case f of
        Atom x ->
          if f == substituendum
            then
              if Set.null (bounds `Set.intersection` substituteFreeVars) && not (x `Set.member` bounds)
                then Just substitute
                else Nothing
            else Just f
        Formula theOp fs ->
          if substituendum `Set.member` Set.map Atom boundHere
            then Just f
            else case foldr goOperand (Just []) fs of
              Just fs' -> Just (Formula theOp fs')
              Nothing -> Nothing
          where
            boundHere = Set.fromList (operatorBinds f)
            bounds' = bounds `Set.union` boundHere
            goOperand opnd = \case
              Nothing -> Nothing
              Just opnds' ->
                case subst bounds' opnd of
                  Just opnd' -> Just (opnd' : opnds')
                  Nothing -> Nothing

findSubstitutionWithSubstituendum :: Formula -> Formula -> Formula -> Set Formula
findSubstitutionWithSubstituendum substituendum old new =
  if substituendum `subformulaOf` old
  then go old new
  else Set.singleton substituendum
  where
    go f' f | f' == substituendum = Set.fromList [f]
    go (Atom _) _ = Set.empty
    go (Formula theOp _) (Formula theOp' _)
      | theOp /= theOp' -- no unification possible
        =
        Set.empty
    go (Formula _ _) (Atom _) = Set.empty
    go (Formula Op {binds} fs) (Formula _ fs') =
      -- same operator given previous cases
      if null binds
        || not
          ( any
              (\(ix, f) -> ix `elem` binds && f == substituendum)
              (zip [1 ..] fs)
          )
        then
          foldMap' (uncurry go) (zip fs fs')
        else -- substituendum is bound here, so it won't unify
          Set.empty


findSubstitution :: Formula -> Formula -> Set (Formula, Formula)
findSubstitution = go
  where
    go (Atom x) f =
      case f of
        Atom y | x == y -> Set.empty
        _ -> Set.fromList [(f, Atom x)]
    go (Formula theOp _) (Formula theOp' _)
      | theOp /= theOp' -- no unification possible
        =
        Set.empty
    go (Formula _ _) (Atom _) = Set.empty
    go (Formula _ fs) (Formula _ fs') =
      foldMap' (uncurry go) (zip fs fs')

rightToMaybe :: Either a b -> Maybe b
rightToMaybe (Right x) = Just x
rightToMaybe (Left _) = Nothing

formulaFreeVariables :: Formula -> Set Text
formulaFreeVariables = collect Set.empty
  where
    collect boundVars =
      \case
        Atom x ->
          if x `Set.member` boundVars
          then Set.empty
          else Set.singleton x
        f@(Formula _ fs) ->
          foldMap (collect (List.foldl' (flip Set.insert) boundVars binds)) fs
          where
            binds = operatorBinds f

formulaBoundVariables :: Formula -> Set Text
formulaBoundVariables = collect Set.empty
  where
    collect boundVars =
      \case
        Atom _ -> boundVars
        f@(Formula _ fs) ->
          foldMap (collect (List.foldl' (flip Set.insert) boundVars binds)) fs
          where
            binds = operatorBinds f
