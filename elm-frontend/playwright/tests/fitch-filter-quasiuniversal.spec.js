const { test, expect } = require('./glf');

test('test', async ({ userPage, page }) => {

  await page.getByText('Filter', { exact: true }).click();
  await page.locator('textarea[name="languageInput"]').click();
  await page.locator('textarea[name="languageInput"]').fill('.P 2');
  await page.locator('textarea[name="assumptionsInput"]').click();
  await page.locator('textarea[name="assumptionsInput"]').fill('∇(x, .P(x, y)) → ⊥ : _');
  await page.locator('textarea[name="goalInput"]').click();
  await page.locator('textarea[name="goalInput"]').fill('∇(x, .P(x, y) → ⊥) : _');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.locator('#ruleSelection').selectOption('Assumption(+1)');
  await page.locator('textarea').fill('.P(x, y) : _ ; x *');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∇I₁');
  await page.getByPlaceholder('Proof lines').fill('2');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→E');
  await page.getByPlaceholder('Proof lines').fill('1,3');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→I');
  await page.getByPlaceholder('Proof lines').fill('2-4');
  await page.locator('textarea').fill('.P(x, y) → ⊥ : _ ; x *');
  await page.getByRole('button', { name: 'GO' }).click();
  await expect(page.locator('#errors')).not.toBeEmpty(); // checks if there's an error
  await page.getByRole('link', { name: 'Delete' }).click();
});
