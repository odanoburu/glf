// glf.js
const base = require('@playwright/test');

async function slideRange (page) {
  await page.waitForSelector("id=rangeCheck", {state: "visible"});
  // apparently that's the only way to drag a range input
  await page.$eval('id=rangeCheck', (e, value) => {
    e.value = value;
    e.dispatchEvent(new Event('input', { 'bubbles': true }));
    e.dispatchEvent(new Event('change', { 'bubbles': true }));
  }, 100);
}

class GLFPage {

  constructor(page) {
    this.page = page;
  }

  async goto() {
    await this.page.goto('/');
  }

  async signUpAsTemporary() {

    await this.goto();

    await this.page.locator('text=Try it!').click();

    await this.page.locator('text=Are you a human?');

    await slideRange(this.page);

    await this.page.locator('id=doneButton').click();

  }

  async signIn() {
    await this.goto();

    await this.page.locator('text=Login').click();

    await this.page.locator('[placeholder="Username"]').fill('abacaxi');

    await this.page.locator('[placeholder="Username"]').press('Tab');

    await this.page.locator('[placeholder="Password"]').fill('abacaxi');

    await this.page.locator('[placeholder="Password"]').press('Tab');

    await this.page.locator('text=Login').press('Enter');

    await this.page.locator('id=glfMenu');
  }

  async checkProof() {
    const exportBtn = this.page.getByRole('button', { name: 'Export' });
    await base.expect(exportBtn).toBeDisabled();
    await this.page.getByRole('button', { name: 'Check proof' }).click();
    await base.expect(exportBtn).toBeEnabled();
  }

  async exportDownloadProof() {
    // Start waiting for download before clicking. Note no await.
    const downloadPromise = this.page.waitForEvent('download');
    const exportBtn = this.page.getByRole('button', { name: 'Export' });
    await exportBtn.click();
    const download = await downloadPromise;

    // Wait for the download process to complete and save the downloaded file somewhere.
    await download.saveAs('/tmp/' + download.suggestedFilename());
  }

  async signOut() {
    await this.page.hover('id=logo');

    await this.page.locator('text=Logout').click();

    await this.page.locator('id=home-glf-description');

  }

  async deleteAccount() {
    await this.page.hover('id=logo');

    await this.page.locator('text=Delete Account').click();

    await this.page.locator('id=home-glf-description');
  }
};


// Extend base test with new fixtures
/// see https://playwright.dev/docs/test-fixtures
exports.test = base.test.extend({
  glfPage: async ({ page }, use) => {
    // only navigate to page
    const glfPage = new GLFPage(page);
    await glfPage.goto();
    await use(glfPage);
  },
  userPage: async ({ page }, use) => {
    // navigate and sign in
    const userPage = new GLFPage(page);
    await userPage.signUpAsTemporary();
    await use(userPage);
  },
});
exports.expect = base.expect;
exports.slideRange = slideRange;
