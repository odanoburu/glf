const { test, expect } = require('./glf');

test('test', async ({ userPage, page }) => {

  await page.getByText('iALC').click();
  await page.locator('textarea[name="languageInput"]').click();
  await page.locator('textarea[name="languageInput"]').fill('.E 2, .LFirm 0, .PracLaw 0, .S 2');
  await page.locator('textarea[name="assumptionsInput"]').click();
  await page.locator('textarea[name="assumptionsInput"]').fill('.E(a, f), f ^ .LFirm : ∃(E, a); ∃(S, j) ; _, .S(j, a), j ^ (∃(S, ∃(E, .LFirm)) : _) -] (.PracLaw : _)');
  await page.locator('textarea[name="goalInput"]').click();
  await page.locator('textarea[name="goalInput"]').fill('j ^ .PracLaw : _');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.locator('#ruleSelection').selectOption('∃I');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('1, 3');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∃I');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('2, 5');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('⟞E');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('4,6');
  await page.getByRole('button', { name: 'GO' }).click();
  await userPage.checkProof();
  await page.getByRole('link', { name: 'Delete' }).click();
});
