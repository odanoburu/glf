const { test, expect } = require('./glf');

test('test', async ({ userPage, page }) => {

  await page.getByText('Ultrafilter').click();
  await page.locator('textarea[name="languageInput"]').click();
  await page.locator('textarea[name="languageInput"]').fill('.P 1');
  await page.locator('textarea[name="assumptionsInput"]').click();
  await page.locator('textarea[name="assumptionsInput"]').fill('∀(x, .P(x)) : _');
  await page.locator('textarea[name="goalInput"]').click();
  await page.locator('textarea[name="goalInput"]').fill('∇(y, .P(y)) : _');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.locator('#ruleSelection').selectOption('∀E₄');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('1');
  await page.locator('textarea').fill('.P(y) : _ ; y *');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∇I₁');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('2');
  await page.getByRole('button', { name: 'GO' }).click();
  await userPage.checkProof();
  await page.getByRole('link', { name: 'Delete' }).click();
});
