const { test, expect } = require('./glf');

test('test', async ({ userPage, page }) => {

  await page.getByText('FOL', {exact: true}).click();
  await page.locator('textarea[name="languageInput"]').click();
  await page.locator('textarea[name="languageInput"]').fill('.P 1, .Q 2');
  await page.locator('textarea[name="assumptionsInput"]').click();
  await page.locator('textarea[name="assumptionsInput"]').fill('∀(x, .P(x))');
  await page.locator('textarea[name="goalInput"]').click();
  await page.locator('textarea[name="goalInput"]').fill('∀(y, .P(y))');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.locator('#ruleSelection').selectOption('∀E');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('1');
  await page.locator('textarea').fill('.P(z)');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∀I');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('2');
  await page.locator('textarea').click();
  await page.locator('textarea').fill('∀(y, .P(y))');
  await page.getByRole('button', { name: 'GO' }).click();
  await userPage.checkProof();
  await page.getByRole('link', { name: 'Delete' }).click();
});
