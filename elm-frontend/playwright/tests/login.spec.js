const { test, expect } = require('./glf');

test('log in and out', async ({ glfPage, page }) => {

  await glfPage.signIn();

  // Check deductive systems exist
  await page.locator('css=.option.system-name');

  await glfPage.signOut();

});
