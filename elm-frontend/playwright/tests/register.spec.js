const { test, expect, slideRange } = require('./glf');

test('registration of new user', async ({ glfPage, page }) => {

  await page.locator('text=Register').click();

  await page.locator('[placeholder="Username"]').fill('oioioi7687');

  await page.locator('[placeholder="Password"]').fill('oioioi7687');

  await page.locator('[placeholder="Confirm password"]').fill('oioioi7687');

  await slideRange(page);

  await page.locator('text=Register').click();

  // Check user
  await page.locator('css=#username:has-text(oioioi7687)');

  // Check deductive systems exist
  await page.locator('css=.option.system-name');

  await glfPage.deleteAccount();

});
