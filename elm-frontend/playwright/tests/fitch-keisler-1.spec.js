const { test, expect } = require('./glf');

test('test', async ({ userPage, page }) => {

  await page.getByText('Keisler').click();
  await page.locator('textarea[name="languageInput"]').click();
  await page.locator('textarea[name="languageInput"]').fill('.P 2, .U 2');
  await page.locator('textarea[name="assumptionsInput"]').click();
  await page.locator('textarea[name="assumptionsInput"]').fill('∃(x,  (.P(x, y) → .U(y, x)) → ⊥) → ⊥ : _');
  await page.locator('textarea[name="goalInput"]').click();
  await page.locator('textarea[name="goalInput"]').fill('&(x, .P(x, y)) → &(x, .U(y,x)) : _');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.locator('#ruleSelection').selectOption('Assumption(+1)');
  await page.getByRole('textbox').click();
  await page.getByRole('textbox').fill('&(x, .P(x, y)) : _');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('&E');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('2');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('Assumption(+1)');
  await page.getByRole('textbox').click();
  await page.getByRole('textbox').fill('(.P(x, y) → .U(y, x)) → ⊥ : _ ; x');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∃I');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('4');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→E');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('1,5');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('RAA');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('4-6');
  await page.locator('textarea').fill('.P(x, y) → .U(y, x) : _');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→E');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('7,3');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('&I');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('8');
  await page.locator('textarea').fill(' &(x, .U(y, x)) : _');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→I');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('2-9');
  await page.getByRole('button', { name: 'GO' }).click();
  await userPage.checkProof();
  //await userPage.exportDownloadProof();
  await page.getByRole('link', { name: 'Delete' }).click();
});
