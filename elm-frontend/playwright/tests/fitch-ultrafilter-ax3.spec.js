const { test, expect } = require('./glf');

test('test', async ({ userPage, page }) => {

  await page.getByText('Ultrafilter').click();
  await page.locator('textarea[name="languageInput"]').click();
  await page.locator('textarea[name="languageInput"]').fill('.P 2, .Q 2');
  await page.locator('textarea[name="assumptionsInput"]').click();
  await page.locator('textarea[name="assumptionsInput"]').fill('∇(x, .P(x, y)) ∧ ∇(x, .Q(y, x)) : _');
  await page.locator('textarea[name="goalInput"]').click();
  await page.locator('textarea[name="goalInput"]').fill('∇(z, .P(z, y) ∧ .Q(y, z)) : _');
  await page.getByRole('button', { name: 'Submit' }).click();

  await page.locator('#ruleSelection').selectOption('∧E-l');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('1');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∧E-r');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('1');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∇E₁');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('2');
  await page.locator('textarea').fill('.P(z, y) : _ ; z *');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∇E₁');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('3');
  await page.locator('textarea').fill('.Q(y, z) : _ ; z *');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∧I');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('4,5');
  await page.locator('textarea').fill('.P(z, y) ∧ .Q(y, z) : _ ; z*');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.pause();
  await page.locator('#ruleSelection').selectOption('∇I₁');
  await page.getByPlaceholder('Proof lines').click();
  await page.getByPlaceholder('Proof lines').fill('6');
  await page.getByRole('button', { name: 'GO' }).click();
  await userPage.checkProof();
  await page.getByRole('link', { name: 'Delete' }).click();
});
