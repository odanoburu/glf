const { test, expect } = require('./glf');

test('test', async ({ userPage, page }) => {

  await page.getByText('Ultrafilter').click();
  await page.locator('textarea[name="languageInput"]').click();
  await page.locator('textarea[name="languageInput"]').fill('.P 3, .Q 3');
  await page.locator('textarea[name="goalInput"]').click();
  await page.locator('textarea[name="goalInput"]').fill('∀(a, ∀(b, ∀(x, .P(a, b, x) → .Q(b, a, x)) → (∀(x, .P(a, b, x)) → ∀(x, .Q(b, a, x))))) : _');
  await page.getByRole('button', { name: 'Submit' }).click();
  // start
  await page.locator('#ruleSelection').selectOption('Assumption(+1)');
  await page.locator('textarea').fill('∀(x, .P(a, b, x) → .Q(b, a, x)) : _ ; a; b');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∀E₃');
  await page.getByPlaceholder('Proof lines').fill('1');
  await page.locator('textarea').fill('.P(a, b, x) → .Q(b, a, x) : _ ; a; b; x');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('Assumption(+1)');
  await page.getByRole('textbox').click();
  await page.getByRole('textbox').fill('∀(x, .P(a, b, x)) : _ ; a; b');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∀E₃');
  await page.getByPlaceholder('Proof lines').fill('3');
  await page.locator('textarea').fill('.P(a, b, x) : _ ; a; b; x');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→E');
  await page.getByPlaceholder('Proof lines').fill('2, 4');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∀I₁');
  await page.getByPlaceholder('Proof lines').fill('5');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→I');
  await page.getByPlaceholder('Proof lines').fill('3-6');
  await page.locator('textarea').fill('∀(x, .P(a, b, x)) → ∀(x, .Q(b, a, x)) : _ ; a ; b');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('→I');
  await page.getByPlaceholder('Proof lines').fill('1-7');
  await page.locator('textarea').fill('∀(x, .P(a, b, x) → .Q(b, a, x)) → (∀(x, .P(a, b, x)) → ∀(x, .Q(b, a, x))) : _ ; a ; b');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∀I₁');
  await page.getByPlaceholder('Proof lines').fill('8');
  await page.getByRole('button', { name: 'GO' }).click();
  await page.locator('#ruleSelection').selectOption('∀I₁');
  await page.getByPlaceholder('Proof lines').fill('9');
  await page.getByRole('button', { name: 'GO' }).click();
  await userPage.checkProof();
  await page.getByRole('link', { name: 'Delete' }).click();
});
