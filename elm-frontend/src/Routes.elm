module Routes exposing ( Route(..), RegisterAs(..), DeductionInputInfo
                       , parseUrl, pathFor, loginPath, registerPath, userPath, buttonLink
                       )

import Api exposing (RootId)
import Url exposing (Url)
import Url.Parser exposing (..)
import Url.Parser.Query as Query
import Url.Builder exposing (..)
import Html exposing (Attribute, Html, a, text)
import Html.Attributes exposing (href, class)
import Dict


type RegisterAs = AsGuest | AsRegisteredUser

type alias DeductionInputInfo =
     { system : String
     , languageInput: String
     , assumptionsInput : String
     , goalsInput : String
     }

type Route
    = HomeRoute
    | LoginRoute
    | RegisterRoute RegisterAs
    | UserRoute (Maybe DeductionInputInfo) (Maybe RootId) {- to delete this proof -}
    | BackwardsDeductionRoute
    | TableauxDeductionRoute
    | ProofRoute String RootId
    | NotFoundRoute


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map HomeRoute top
        , map LoginRoute (s "login")
        , map RegisterRoute (s "register"
                                 <?> Query.map (Maybe.withDefault AsRegisteredUser)
                                    (Query.enum "as" (Dict.fromList [("user", AsRegisteredUser), ("guest", AsGuest)])))
        , map UserRoute
              (s "systems"
               <?> Query.map4 (\s l a g ->
                              Maybe.map (\sys ->
                                        {system = sys
                                        , languageInput = Maybe.withDefault "" l
                                        , assumptionsInput = Maybe.withDefault "" a
                                        , goalsInput = Maybe.withDefault "" g
                                        })
                                        s)
                              (Query.string "system")
                              (Query.string "language")
                              (Query.string "assumptions")
                              (Query.string "goals")
               <?> Query.int "delete-proof")
        , map ProofRoute
              (s "proof" </> Url.Parser.string </> Url.Parser.int)
        ]


parseUrl : Url -> Route
parseUrl url =
    case parse matchers url of
        Just route ->
            route

        Nothing ->
            NotFoundRoute


pathFor : Route -> String
pathFor route =
    let proofUrlString kind proofId = absolute [ "proof", String.fromInt proofId ] [ Url.Builder.string "by" kind ]
    in
    case route of
        HomeRoute -> absolute [] []

        LoginRoute -> absolute ["login"] []

        RegisterRoute AsRegisteredUser -> absolute ["register"] []

        RegisterRoute AsGuest -> absolute ["register"] [Url.Builder.string "as" "guest"]

        UserRoute _ mProofToDelete ->
            absolute
              ["systems"]
              <| case mProofToDelete of
                   Just proofId -> [Url.Builder.int "delete-proof" proofId]
                   Nothing -> []

        BackwardsDeductionRoute -> proofUrlString "nd" 0

        TableauxDeductionRoute -> proofUrlString "tableaux" 0

        ProofRoute systemName proofId -> absolute ["proof", systemName, String.fromInt proofId] []

        NotFoundRoute -> absolute ["404"] []


loginPath : String
loginPath = pathFor LoginRoute

registerPath : String
registerPath = pathFor (RegisterRoute AsRegisteredUser)

userPath : String
userPath = pathFor (UserRoute Nothing Nothing)

buttonLink : Route -> String -> List (Attribute msg) -> Html msg
buttonLink route buttonText attrs =
  a ([href (pathFor route), class "btn"] ++ attrs) [text buttonText]
