module Main exposing (init, main, subscriptions)

import Pages.Home as Home
import Pages.Login as Login
import Pages.Register as Register
import Pages.User as User
-- import Pages.Backwards as Backwards
import Pages.Fitch as Fitch
-- import Pages.Tableaux as Tableaux
import Api exposing (..)
import Base exposing (..)
import Routes exposing (Route(..), RegisterAs(..), parseUrl, pathFor, buttonLink)

import Browser exposing (UrlRequest)
import Browser.Navigation as Nav exposing (Key)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Url exposing (Url)

type AppState = NotLogged
              | Logged SharedState

type alias Model =
    { state : AppState
    , payload : CrossPagePayload
    , navKey : Key
    , route : Route
    , page : Page
    }

type CrossPagePayload
     = NoPayload
     | HaveSystem SystemInfo
     | HaveProofInfo RawProofInfo
     | HaveSystemProof SystemInfo RawProofInfo

type Page
    = PageHome Home.Model
    | PageLogin Login.Model
    | PageRegister Register.Model
    | PageUser User.Model
    -- | PageBackwards Backwards.Model
    | PageFitch Fitch.Model
    -- | PageTableaux Tableaux.Model
    | PageLoading
    | PageNone

type Msg
    = OnUrlChange Url
    | OnUrlRequest UrlRequest
    | HomeMsg Home.Msg
    | LoginMsg Login.Msg
    | RegisterMsg Register.Msg
    | UserMsg User.Msg
    | GotProofInfo RawProofInfo
    | GotSystemInfo SystemInfo
    -- | BackwardsMsg Backwards.Msg
    | FitchMsg Fitch.Msg
    ---- | TableauxMsg Tableaux.Msg
    -- NOTE: we include username here because we have deferred
    -- logout for trial/guest users, but these may have stopped the
    -- trial and became registered users, at which point we don't
    -- want to log them out anymore
    | LogOut String
    | DeleteAccount String
    | NoOp

main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = OnUrlRequest
        , onUrlChange = OnUrlChange
        }



init : Flags -> Url -> Key -> ( Model, Cmd Msg )
init flags url navKey =
    let
        loginStatus = maybe NotLogged (\authInfo -> Logged {auth = authInfo}) flags.auth
        model =
            { navKey = navKey
            , payload = NoPayload
            , state = loginStatus
            , route = parseUrl url
            , page = PageNone
            }
    in
        ( model, Cmd.none )
            |> loadCurrentPage

loadCurrentPage : ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
loadCurrentPage ( model, cmd ) =
    let
        nonePage = ( (PageNone, Routes.NotFoundRoute), Cmd.none )
        loginPage =
            let
                ( pageModel, pageCmd ) =
                    Login.init ()
            in
                ( (PageLogin pageModel, Routes.LoginRoute)
                ,  Cmd.map LoginMsg pageCmd
                )
        userPage auth mDeductionInfo mProofToDelete =
            let
                ( pageModel, pageCmd ) =
                    User.init auth mDeductionInfo mProofToDelete
            in
                ( (PageUser pageModel, Routes.UserRoute Nothing Nothing)
                ,  Cmd.map UserMsg pageCmd


                )
        ( (page, route), newCmd ) =
            case model.route of
                Routes.HomeRoute ->
                    let
                        ( pageModel, pageCmd ) =
                            Home.init ()
                    in
                        ( (PageHome pageModel, Routes.HomeRoute), Cmd.map HomeMsg pageCmd )

                Routes.LoginRoute ->
                    case model.state of
                        NotLogged -> loginPage

                        Logged state -> userPage state.auth Nothing Nothing

                Routes.RegisterRoute registerAs ->
                    case model.state of
                        NotLogged ->
                            let ( pageModel, pageCmd ) = Register.init registerAs
                            in ( (PageRegister pageModel, Routes.RegisterRoute registerAs)
                               , Cmd.map RegisterMsg pageCmd
                               )
                        Logged state -> userPage state.auth Nothing Nothing

                Routes.UserRoute mDeductionInput mProofToDelete ->
                    case model.state of
                        NotLogged ->
                            loginPage
                        Logged state -> userPage state.auth mDeductionInput mProofToDelete

                Routes.BackwardsDeductionRoute ->
                    case model.state of
                        NotLogged ->
                            loginPage
                        Logged state ->
                            case model.payload of
                                -- GotDeduction deduction proof ->
                                --     let
                                --         ( pageModel, pageCmd ) =
                                --             Backwards.init state deduction proof
                                --     in
                                --         ((PageBackwards pageModel, Routes.BackwardsDeductionRoute), Cmd.map BackwardsMsg pageCmd)

                                _ -> nonePage

                Routes.ProofRoute systemName proofId ->
                    case model.state of
                        NotLogged ->
                            loginPage
                        Logged state ->
                            case model.payload of
                              HaveSystemProof systemInfo deduction ->
                                    let
                                        ( pageModel, pageCmd ) =
                                            Fitch.init systemInfo deduction
                                    in
                                        ((PageFitch pageModel, Routes.ProofRoute systemName proofId), Cmd.map FitchMsg pageCmd)

                              _ -> ( (PageLoading, Routes.ProofRoute systemName proofId)
                                     , case model.payload of
                                         NoPayload ->
                                           Cmd.batch [ Cmd.map getProofInfo <| Api.getGlfLoadDeductionByProofId (basicAuthString state.auth) proofId
                                                     , Cmd.map getSystemInfo <| Api.getGlfSystemdata (basicAuthString state.auth) (Just systemName)
                                                     ]
                                         _ -> Cmd.none
                                   )

                Routes.TableauxDeductionRoute ->
                    case model.state of
                        NotLogged -> loginPage
                        Logged state ->
                            case model.payload of
                            --     GotDeduction deductionInfo deduction ->
                            --         let ( pageModel, pageCmd ) =
                            --                 Tableaux.init state deductionInfo deduction
                            --         in ( (PageTableaux pageModel, Routes.TableauxDeductionRoute), Cmd.map TableauxMsg pageCmd )
                                _ -> nonePage

                Routes.NotFoundRoute ->
                    nonePage
    in
    ( { model | page = page
      , route = route
      , payload = case model.payload of
                    HaveSystemProof _ _ -> NoPayload
                    _ -> model.payload
      }
    , Cmd.batch [ newCmd
                , cmd
                , if page /= model.page then resetViewport NoOp else Cmd.none
                , if model.route /= route then Nav.pushUrl model.navKey (pathFor route) else Cmd.none
                ]
    )

receive = receiveFunction (\_ -> NoOp)

getProofInfo = receive GotProofInfo

getSystemInfo = receive (\syss -> case syss of
                                   [s] -> GotSystemInfo s
                                   _ -> NoOp)

subscriptions : Model -> Sub Msg
subscriptions model =
    case model.page of
        PageHome pageModel ->
            Sub.map HomeMsg (Home.subscriptions pageModel)

        PageLogin pageModel ->
            Sub.map LoginMsg (Login.subscriptions pageModel)

        PageRegister pageModel ->
            Sub.map RegisterMsg (Register.subscriptions pageModel)

        PageUser pageModel ->
            Sub.map UserMsg (User.subscriptions pageModel)

        -- PageBackwards pageModel ->
        --     Sub.map BackwardsMsg (Backwards.subscriptions pageModel)

        PageFitch pageModel ->
            Sub.map FitchMsg (Fitch.subscriptions pageModel)

        -- PageTableaux pageModel ->
        --     Sub.map TableauxMsg (Tableaux.subscriptions pageModel)

        PageLoading -> Sub.none

        PageNone -> Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case (model.state, msg, model.page) of
        ( _, OnUrlRequest urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model
                    , Nav.pushUrl model.navKey (Url.toString url)
                    )

                Browser.External url ->
                    ( model
                    , Nav.load url
                    )

        ( _, OnUrlChange url, _ ) ->
            let
                newRoute =
                    Routes.parseUrl url
            in
            ( { model | route = newRoute }, Cmd.none )
                |> loadCurrentPage

        ( _, HomeMsg subMsg, PageHome pageModel ) ->
             let ( newPageModel, newCmd ) =
                     Home.update subMsg pageModel
             in
                 ( { model | page = PageHome newPageModel }
                 , Cmd.map HomeMsg newCmd
                 )

        ( NotLogged, LoginMsg subMsg, PageLogin pageModel ) ->
             let ( newPageModel, newCmd ) =
                     Login.update subMsg pageModel
             in
                 ( { model | page = PageLogin newPageModel }
                 , Cmd.map LoginMsg newCmd
                 )

        (NotLogged, RegisterMsg subMsg, PageRegister pageModel) ->
            let
                ( newPageModel, newCmd ) =
                    Register.update model.navKey subMsg pageModel
            in
                ( { model | page = PageRegister newPageModel }
                , Cmd.map RegisterMsg newCmd
                )


        (Logged state, UserMsg subMsg, PageUser userModel) ->
            let
                ( newPageModel, newCmd ) =
                    User.update state model.navKey subMsg userModel
            in
                ( { model | page = PageUser newPageModel }
                , Cmd.map UserMsg newCmd
                )

        (_, GotSystemInfo system, _) ->
            ( { model | payload
                = case model.payload of
                       HaveProofInfo proof -> HaveSystemProof system proof
                       _ -> HaveSystem system
                }
            , Cmd.none
            ) |> loadCurrentPage

        (_, GotProofInfo proofInfo, _) ->
            ( { model | payload
                = case model.payload of
                       HaveSystem system -> HaveSystemProof system proofInfo
                       _ -> HaveProofInfo proofInfo
                       }
            , Cmd.none
            ) |> loadCurrentPage

        -- ( Logged state (Just dedInfo), BackwardsMsg subMsg, PageBackwards pageModel ) ->
        --     case subMsg of
        --         -- PAGE CHANGE: delete proof
        --         Backwards.ExitDeduction ->
        --             ( { model | route = Routes.UserRoute
        --               , state = Logged state Nothing
        --               , payload = NoPayload
        --               }
        --             , Cmd.none
        --             ) |> loadCurrentPage
        --         _ ->
        --             let
        --                 (newPageModel, newCmd ) =
        --                     Backwards.update state dedInfo subMsg pageModel
        --             in
        --                 ( {model | page = PageBackwards newPageModel }
        --                 , Cmd.map BackwardsMsg newCmd
        --                 )

        ( Logged state, FitchMsg subMsg, PageFitch pageModel ) ->
            let ( newPageModel, newCmd ) =
                    Fitch.update state model.navKey subMsg pageModel
            in ( { model | page = PageFitch newPageModel }, Cmd.map FitchMsg newCmd )

        -- ( Logged state (Just dedInfo), TableauxMsg subMsg, PageTableaux pageModel ) ->
        --     case subMsg of
        --         -- PAGE CHANGE: delete proof
        --         Tableaux.ExitDeduction ->
        --             ( { model | route = Routes.UserRoute
        --               , state = Logged state Nothing
        --               , payload = NoPayload
        --               }
        --             , Cmd.none
        --             ) |> loadCurrentPage

                -- _ ->
                --     let ( newPageModel, newCmd ) =
                --             Tableaux.update state dedInfo subMsg pageModel
                --     in ( { model | page = PageTableaux newPageModel }, Cmd.map TableauxMsg newCmd )

        ( _, HomeMsg _, _ ) ->
            ( model, Cmd.none )

        ( _, LoginMsg _, _ ) ->
            ( model, Cmd.none )

        ( _, RegisterMsg _, _ ) ->
            ( model, Cmd.none )

        ( _, UserMsg _, _ ) ->
            ( model, Cmd.none )

        -- ( _, BackwardsMsg _, _ ) ->
        --     ( model, Cmd.none )

        ( _, FitchMsg _, _ ) ->
            ( model, Cmd.none )

        -- ( _, TableauxMsg _, _ ) ->
        --     ( model, Cmd.none )

        ( Logged state, LogOut userId, _ ) ->
            if state.auth.id == userId
            then
                ( { model | state = NotLogged
                  , route = Routes.HomeRoute
                  }
                , Cmd.batch [ setAuthInfo Nothing
                            , Nav.pushUrl model.navKey (pathFor Routes.HomeRoute)
                            ]
                ) |> loadCurrentPage
            else
                ( model, Cmd.none )

        ( NotLogged, LogOut _, _ ) ->
            ( model, Cmd.none )

        ( Logged state, DeleteAccount userId, _ ) ->
            if state.auth.id == userId
            then
                ( { model | state = NotLogged
                  , route = Routes.HomeRoute
                  }
                , Cmd.batch [ Cmd.map receiveAccountDeletion
                                  <| Api.getGlfDeleteAccount (basicAuthString state.auth)
                            , setAuthInfo Nothing
                            , Nav.pushUrl model.navKey (pathFor Routes.HomeRoute)
                            ]
                ) |> loadCurrentPage
            else
                ( model, Cmd.none )

        ( NotLogged, DeleteAccount _, _ ) ->
            ( model, Cmd.none )

        ( _, NoOp, _ ) ->
            ( model, Cmd.none )

receiveAccountDeletion succeeded = NoOp

--- VIEWS

view : Model -> Browser.Document Msg
view model =
    { title = "labeled graph logical framework"
    , body = [ currentPage model ]
    }


currentPage : Model -> Html Msg
currentPage model =
    let
        page =
            case (model.page, model.state) of
                (PageHome pageModel, _) ->
                    Home.view pageModel
                        |> Html.map HomeMsg

                (PageLogin pageModel, NotLogged) ->
                    Login.view pageModel
                        |> Html.map LoginMsg

                (PageRegister pageModel, NotLogged) ->
                    Register.view pageModel
                        |> Html.map RegisterMsg

                (PageUser pageModel, Logged _) ->
                    User.view pageModel
                        |> Html.map UserMsg

                -- (PageBackwards pageModel, Logged state (Just dedInfo)) ->
                --     Backwards.view dedInfo pageModel
                --         |> Html.map BackwardsMsg

                (PageFitch pageModel, Logged _) ->
                    Fitch.view pageModel
                        |> Html.map FitchMsg

                -- (PageTableaux pageModel, Logged state (Just dedInfo)) ->
                --     Tableaux.view dedInfo pageModel
                --         |> Html.map TableauxMsg

                (PageLoading, _) -> loadingView

                (PageNone, _) -> notFoundView

                -- make sure exhaustiveness checker complains when
                -- we add new page, so enumerate all pages
                ( PageLogin _, _ )     -> inconsistencyView
                ( PageRegister _, _ )  -> inconsistencyView
                ( PageUser _, _)       -> inconsistencyView
                -- ( PageBackwards _, _ ) -> inconsistencyView
                ( PageFitch _, _ )     -> inconsistencyView
                -- ( PageTableaux _, _ )  -> inconsistencyView
    in
    section []
        [ nav model
        , page
        , glfFooter
        ]


glfMenu : AuthInfo -> Html Msg
glfMenu user =
    let logout =
            button [ type_ "button"
                   , class "interaction"
                   , id "logout-button"
                   , onClick (LogOut user.id)
                   ]
            [ text "Logout" ]
        home =
            buttonLink HomeRoute "Home" [ class "interaction", id "home-button" ]
        delete =
            button [ type_ "button"
                   , class "interaction"
                   , id "deleteAccount-button"
                   , onClick (DeleteAccount user.id)
                   ]
                [ text "Delete Account" ]
    in
    div [ id "glfMenu"]
        [ glfLogo []
        , ul [ class "nesting" ]
            [ li [] [span [id "username"] [text user.id]]
            , li [] [ home ]
            , li [ class "flex-push" ] [ delete ]
            , li [] [ logout ]
            ]
        ]

nav : Model -> Html Msg
nav model =
    let
        links =
            case (model.page, model.state) of
                (_, NotLogged) -> [ ]
                (PageHome _, _) -> [ ]
                (_, Logged state) ->
                    [ glfMenu state.auth
                    ]
    in
    div
        [ class "nav" ]
        links


loadingView : Html msg
loadingView =
    div [] [ span [class "spin"] [text "⊢"] ]

notFoundView : Html msg
notFoundView =
    div []
        [ text "Not found"
        ]

inconsistencyView : Html msg
inconsistencyView = div [] [ text "Inconsistent app state" ]

glfFooter : Html msg
glfFooter =
    footer [class "subpage"]
        [ ul [class "horizontal-list"]
              [ li []
                    [ details []
                          [ summary [] [ text "Warranty" ]
                          , text "This is freely-available research software. There are no warranties."
                          ]
                    ]
              , li []
                  [ details []
                        [ summary [] [ text "Privacy" ]
                        , paragraph
                              [ "The only personal information GLF may store is the user's email address (if provided)."
                              , "It is only used for password recovery and nothing else."
                              , "User emails are stored unencrypted, and should not be accessible to anyone else unless the server is compromised."
                              , "Users may delete their account and all their proofs by logging in and pressing the appropriate button in the menu."
                              ]
                        , paragraph
                              [ "The HTTP server we use may log IP adresses for debugging purposes."
                              , " These are also stored unencrypted, and should not be accessible to anyone else unless the server is compromised."
                              ]
                        , paragraph [ "We will not share personal information with anyone, unless ordered so by a legitimate court of law." ]
                        , paragraph [ "This website does not use cookies." ]
                        ]
                  ]
              , li []
                  [ details []
                        [ summary [] [ text "Copyright" ]
                        , p []  [ a [href "http://www.tecmf.inf.puc-rio.br/", target "_blank", rel "noopener"] [ text "TecMF" ], text " ©2019-…" ]
                        -- TODO: include email here, privately
                        ---, address [] []
                        ]
                  ]
              ]
        ]
