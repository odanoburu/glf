module Pages.Login exposing (Model, Msg(..)
                            , init, subscriptions, update, view, viewLink, viewInput, Errors, addError, viewErrors, clearError, clearErrorCmd
                            , scrollToElement, scrollToElementIfNotInView, scrollToErrorCmd, glfUsernameInputId, glfPasswordInputId, noErrors
                            )

import Routes
import Base exposing (..)
import Pages.Home exposing (..)
import Api as Api

import Html exposing (..)
import Html.Attributes as Attr exposing (..)
import Html.Events exposing (..)
import Http
import Browser.Dom as DOM
import Browser.Navigation as Nav
import Task
import Process
import Set exposing (Set)
import Json.Decode as Decode

type alias Errors = Set String

addError : String -> Errors -> Errors
addError err errs = Set.insert err errs

clearError : String -> Errors -> Errors
clearError err errs = Set.remove err errs

noErrors : Errors
noErrors = Set.empty

type alias Model = { auth : AuthInfo, errors : Errors }

type Msg
    = Username String
    | Password String
    | LoginAttempt
    | LoginFailed String
    | LoginSucceeded AuthInfo
    | ClearError String
    | NoOp

init : () -> ( Model, Cmd Msg )
init _ =
    ( { auth = { id = "", password = "" }, errors = Set.empty }
    , focusOn NoOp glfUsernameInputId
    )

glfUsernameInputId : String
glfUsernameInputId = "glfUsernameInput"

glfPasswordInputId : String
glfPasswordInputId = "glfPasswordInput"

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

--- UPDATE

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Username id ->
            ( { model | auth = {id = id, password = model.auth.password} }
            , Cmd.none
            )

        Password password ->
            ( { model | auth = {id = model.auth.id, password = password} }
            , Cmd.none)

        LoginAttempt ->
            let basicAuth = basicAuthString model.auth
                loginAttemptCmd = Api.getGlfCheckAuth basicAuth
            in (model, Cmd.map (checkAuth model.auth) loginAttemptCmd)

        LoginFailed err -> ( {model | errors = addError err model.errors}
                           , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError err)]
                           )

        LoginSucceeded authInfo ->
            ( model
            , Cmd.batch [ setAuthInfo (Just authInfo)
                        , Nav.reload
                        ]
            )

        ClearError err ->
            ( { model | errors = clearError err model.errors }
            , Cmd.none
            )

        NoOp -> ( model, Cmd.none )

-- checkAuth : Result _ Bool -> Msg
-- TODO: report http error and other errors differently
checkAuth authInfo r =
    case r of
        Err (Http.BadStatus 401, _) -> LoginFailed "Wrong username or password"
        Err (httpError, mInfo) -> LoginFailed <| httpErrorString "Authorization failed" httpError mInfo
        Ok False -> LoginFailed "Authentication unsuccessful"
        Ok True -> LoginSucceeded authInfo

--- VIEW

view : Model -> Html Msg
view model =
    let formId = "loginForm"
    in
        div []
            [ div [class "subpage"]
                  [ glfLogo []
                  , h3 [] [Html.text "Identification"]
                  , Html.form [ id formId ] []
                  , viewInput "text" "Username" model.auth.id Username [Attr.form formId, id glfUsernameInputId]
                  , viewInput "password" "Password" model.auth.password Password [Attr.form formId, id glfPasswordInputId]
                  , loginButton LoginAttempt [id "login-button", Attr.form formId]
                  , div [ id "no-user"]
                      [ text "No user? "
                      , viewLink Routes.registerPath
                      ]
                  ]
            , viewErrors model.errors
            ]

viewInput : String -> String -> String -> (String -> msg)
          -> List (Attribute msg) -> Html msg
viewInput t p v toMsg attrs =
  input ([ type_ t, placeholder p, value v, onInput toMsg ] ++ attrs) []

viewLink : String -> Html msg
viewLink path = a [ href path ] [ text path ]

viewErrors : Errors -> Html msg
viewErrors errors =
    div [id "errors"]
        ( errors
        |> Set.toList
        |> List.concatMap (\err -> [text err, br [] []])
        )

clearErrorCmd : msg -> Cmd msg
clearErrorCmd msg =
    cmdAfter (5 * 1000) (\_ -> msg)

scrollToElement : msg -> String -> Cmd msg
-- set viewport to the start of element
scrollToElement msg elemId =
    DOM.getElement elemId
        |> Task.andThen (\{element, viewport} -> DOM.setViewport viewport.x element.y)
        |> Task.attempt (\_ -> msg)

scrollToElementIfNotInView : msg -> String -> Cmd msg
-- set viewport to bottom of element if not in view
scrollToElementIfNotInView msg elemId =
    DOM.getElement elemId
        |> Task.andThen
           (\{element, viewport} ->
                if element.x >= viewport.x
                && viewport.x + viewport.width >= element.x + element.width
                && element.y >= viewport.y
                && viewport.y + viewport.height >= element.y + element.height
                then -- in view, no need to do anything
                    Task.succeed ()
                else -- not in view
                    let elementBottom = element.y + element.height
                    in
                        -- set viewport to show the bottom of the element with as little scrolling as possible
                        DOM.setViewport viewport.x (elementBottom - viewport.height))
               |> Task.attempt (\_ -> msg)

scrollToErrorCmd : msg -> Cmd msg
scrollToErrorCmd msg = scrollToElementIfNotInView msg "errors"

loginButton : msg -> List (Attribute msg) -> Html msg
loginButton msg attrs = makeButton msg "Login" attrs
