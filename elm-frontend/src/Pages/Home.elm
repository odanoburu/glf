module Pages.Home
    exposing ( Model, Msg(..)
             , init, subscriptions, update, view
             , makeButton
             )

import Routes exposing (pathFor, Route(..), RegisterAs(..), buttonLink)
import Base exposing (..)

import Html exposing (..)
import Html.Attributes as Attr exposing (..)
import Html.Events exposing (..)


type alias Model = {}

type Msg = NoOp

init : () -> ( Model, Cmd Msg )
init _ =
    ( {}, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

--- UPDATE

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp -> ( model, Cmd.none )

--- VIEW

view : Model -> Html Msg
view _ =
    div []
        [ div [class "subpage"]
              [ glfLogo [style "max-height" "3.1em"]
              , div [id "home-glf-description"]
                  [ strong [] [text "GLF"]
                  , text " is a labelled Natural Deduction logical framework. "
                  , text "In this page you find implementations of several deductive systems, "
                  , text "in Fitch-style and (forthcoming) Gentzen-style Natural Deduction. "
                  , text "GLF's focus is on providing a simple meta-language for implementing deductive systems, "
                  , text "not on proving their correctness properties. The interfaces you find here should be "
                  , text "as close as possible to pen-and-paper proofs (e.g., there is no coding involved for the end-user)."
                  ]
              , ul [class "horizontal-list"]
                  [ homeMenuItem <| buttonLink (RegisterRoute AsGuest) "Try it!" []
                  , homeMenuItem <| buttonLink LoginRoute "Login" []
                  , homeMenuItem <| buttonLink (RegisterRoute AsRegisteredUser) "Register" []
                  , homeMenuItem (a [class "btn", href "https://gitlab.com/odanoburu/glf/", target "_blank", rel "noopener"] [ text "Source"])
                  ]
              ]
        ]

homeMenuItem : Html msg -> Html msg
homeMenuItem item =
    li [] [item]

makeButton : msg -> String -> List (Attribute msg) -> Html msg
makeButton msg description attrs =
    button ([ type_ "button", onClick msg] ++ attrs)
        [ text description ]
