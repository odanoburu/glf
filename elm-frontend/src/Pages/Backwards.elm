module Pages.Backwards exposing (Model, Msg(..), init, subscriptions, update, view, parentOfToChildrenOf, removeNodeAndDescendants)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Set exposing (Set)
import Tuple exposing (..)
import Json.Decode as Decode
import Dict as Dict
import Array as Array exposing (Array)
import Routes
import Base exposing (..)
import Api exposing (..)
import Dict exposing (..)
import Pages.Login exposing (..)
import Pages.User as User exposing (..)

type ArgumentChoice = Enumerated (List FormulaId) | Manual (Array String)

type StrategyStatus
    = NoneChosen
    --| Chosen String -- TODO: add option to choose strategies
    | Running String

type alias GoalState =
    { rule : String
    , argumentNames : List String
    , possibleArguments : List (List (String, FormulaId))
    , argumentChoice : ArgumentChoice
    , strategyStatus : StrategyStatus
    }

type alias GoalsState =
    Dict GoalId GoalState

emptyGoalState : RuleInfo -> GoalState
emptyGoalState rule =
    { rule = rule.ruleName
    , possibleArguments = []
    , argumentNames = ruleArgumentNames rule
    , argumentChoice = Manual (Array.initialize (List.length rule.argumentNames) (always ""))
    , strategyStatus = NoneChosen
    }

type alias Model =
  { source : NodeId
  , formulas : Dict FormulaId String
  , nodes : Dict NodeId NodeInfo
  , childrenOf : Dict NodeId (List NodeId)
  , disabledInput : Bool
  , goalsState : GoalsState
  , helpView : HelpView
  , errors : Errors
  }

type Msg
    = NoOp
    | PickRule GoalId String
    | GotPossibleRuleArguments GoalId RuleInfo PossibleArguments
    | PickEnumeratedArgument GoalId String {- option key -}
    | ChangeManualArgument GoalId Int {- argument index -} String
    | GoRule (Maybe NodeId) GoalId String (List String) {- arg names -} ArgumentChoice
    | GotRuleResult (Maybe NodeId) GoalId RuleApp
    | GotFormulaIdsGoRule (List (FormulaId, Formula)) (Maybe NodeId) GoalId String (List String) {- arg names -}
    | GotFormulas (List (FormulaId, Formula))
    | PickStrategy GoalId String
    | RunStrategy String NodeId GoalId
    | FailedStrategy String GoalId String
    | GetSubDeduction RootId (GoalId, String)
    | GotSubdeduction NodeId GoalId SubProof
    | RevertProof (Maybe NodeId) NodeId
    | RevertedProofGoal (Maybe NodeId) NodeId GoalId
    | DeleteDeduction
    | ExitDeduction
    | GetDeductionExport
    | GotDeductionExport String
    | PickHelp HelpView
    | GotError String
    | ClearError String

initGoalsState : RuleInfo -> List NodeInfo -> Dict NodeId GoalState
initGoalsState defaultRule nodeInfos =
    List.foldl
        (\n gss ->
             case n.kind of
                 GoalNode -> Dict.insert n.nodeId (emptyGoalState defaultRule) gss
                 _ -> gss
        )
        Dict.empty
        nodeInfos

init : SharedState -> WorkingDeductionInfo -> ProofInfo -> ( Model, Cmd Msg )
init state deduction proof =
    let formulasMap = Dict.fromList proof.proof.formulas
        (source, errors) =
            case deduction.sources of
                [] -> (-1, Set.singleton "No source node")
                [s] -> (s, Set.empty)
                _ -> (-1, Set.singleton "Too many source nodes for backwards deduction system")
        initialGoalsState = initGoalsState deduction.defaultRule proof.proof.nodeInfos
    in
    ( { source = source
      , formulas = formulasMap
      , nodes = Dict.fromList
                <| List.map (\n -> ( n.nodeId, n ))
                    proof.proof.nodeInfos
      , childrenOf = parentOfToChildrenOf proof.proof.parentOf
      , goalsState = initialGoalsState
      , disabledInput = False
      , helpView = NoHelp
      , errors = errors
      }
    , Cmd.batch
          <| List.map (\g -> getPossibleArgs state deduction g deduction.defaultRule) (Dict.keys initialGoalsState)
    )

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

parentOfToChildrenOf : List (NodeId, NodeId) -> Dict NodeId (List NodeId)
parentOfToChildrenOf parentOf =
    List.foldl
        (\(parent, child) childrenOf ->
             Dict.update parent (maybe (Just [child]) (\children -> Just (child::children))) childrenOf
        )
        Dict.empty
        parentOf

--getPossibleArgs : SharedState -> WorkingDeductionInfo -> GoalId -> String ->
getPossibleArgs state dedInfo goal rule =
    Cmd.map (receiveRulePossibleArgs goal rule)
        <| postGlfListArgsBySystemByRule (basicAuthString state.auth) dedInfo.system.systemName rule.ruleName
            [(goalArgName, goal), (rootArgName, dedInfo.root)]

update : SharedState -> WorkingDeductionInfo -> Msg -> Model -> ( Model, Cmd Msg )
update state dedInfo msg model =
    let getFormulas formulaIds = User.makeGetFormulas GotError GotFormulas state.auth model.formulas formulaIds
        modelWithFormulas formulas = { model | formulas = insertMany formulas model.formulas}
        applyRule mParentId goalId ruleName otherArguments =
            Cmd.map (receiveRuleResult mParentId goalId ruleName)
                <| postGlfRuleBySystemByRootIdByRule
                    (basicAuthString state.auth)
                    dedInfo.system.systemName
                    dedInfo.root
                    ruleName
                    dedInfo.mLanguageId
                    ((goalArgName, goalId)::(rootArgName, dedInfo.root)::otherArguments)
        emptyGS = emptyGoalState dedInfo.defaultRule
    in
    case msg of
        NoOp -> ( model, Cmd.none )

        PickRule goal ruleName ->
            case List.filter (\r -> r.ruleName == ruleName) dedInfo.system.rulesInfo of
                [r] ->
                    let filteredArgNames =
                            caseArguments (ruleArgumentNames r) [] (\_ -> ruleArgumentNames r)
                    in
                        ( { model | goalsState =
                                Dict.insert
                                    goal
                                    (initializeGoalState r [])
                                    model.goalsState
                          }
                        , getPossibleArgs state dedInfo goal r
                        )
                _ ->
                    ( { model | errors = addError ("Could not find rule named " ++ ruleName) model.errors }
                    , Cmd.none
                    )

        GotPossibleRuleArguments goal rule possibleArgs ->
            case possibleArgs of
                PossibleArguments (_, possibleArguments) ->
                    ( { model
                          | goalsState =
                            Dict.update
                                goal
                                (\mGs ->
                                     case mGs of
                                         Just gs ->
                                             if rule.ruleName == gs.rule
                                             then
                                                 Just { gs | possibleArguments = possibleArguments
                                                      }
                                             else
                                                 Just gs
                                         Nothing ->
                                             Just <| initializeGoalState rule possibleArguments
                                )
                                model.goalsState
                      }
                    , Cmd.none
                    )

        PickEnumeratedArgument goal ix ->
            ( { model
                  | goalsState = Dict.update goal
                                             (Maybe.map
                                                  (\gs ->
                                                       let argIx = Maybe.withDefault 0 <| String.toInt ix
                                                       in { gs
                                                              | argumentChoice
                                                                  = maybe (Manual <| Array.initialize
                                                                               (List.length gs.argumentNames)
                                                                               (\_ -> ""))
                                                                          (\args -> Enumerated <| List.map second args)
                                                                    <| safeIndex gs.possibleArguments argIx
                                                          })
                                             )
                    model.goalsState
              }
            , Cmd.none )

        ChangeManualArgument goal ix text ->
            ( { model
                  | goalsState =
                      Dict.update
                          goal
                          (Maybe.map (\gs -> { gs
                                                 | argumentChoice =
                                                     Manual (case gs.argumentChoice of
                                                                 Manual argChoice -> Array.set ix text argChoice
                                                                 Enumerated _ ->
                                                                     Array.initialize
                                                                         (List.length gs.argumentNames)
                                                                         (\argIx -> if argIx == ix then text else ""))
                                             }))
                          model.goalsState
              }
            , Cmd.none
            )

        GoRule mParentId goalId ruleName argNames argChoice ->
            ( { model | disabledInput = True }
            , case argChoice of
                  Enumerated formulaIdArgs ->
                      applyRule mParentId goalId ruleName (zip argNames formulaIdArgs)
                  Manual formulaTexts ->
                      let formulaTextsList = Array.toList formulaTexts
                      in Cmd.map (receiveFormulaIdsAndApplyRule formulaTextsList mParentId goalId ruleName argNames)
                          (postGlfAddFormulasBySystemByRootId (basicAuthString state.auth) dedInfo.system.systemName dedInfo.root dedInfo.mLanguageId formulaTextsList)
            )

        GotFormulaIdsGoRule formulas mParentId goalId ruleName argNames ->
            let newModel = modelWithFormulas formulas
            in
                ( newModel
                , applyRule mParentId goalId ruleName (List.map2 (\argName (fId, _) -> (argName, fId)) argNames formulas)
                )

        GotRuleResult mParentId goalId ruleApp ->
            ( let mParent = Maybe.andThen (\parentId -> Dict.get parentId model.nodes) mParentId
                  mGoal = Dict.get goalId model.nodes
                  ruleName = ruleApp.ruleApplied.unicode
              in case mGoal of
                     Just goal ->
                         let node = { goal | nodeId = ruleApp.node
                                    , kind = case ruleApp.goals of
                                                 [] -> LeafNode
                                                 _ -> RuleNode ruleName
                                    , ruleArguments =
                                        case Dict.get goalId model.goalsState of
                                            Just goalState ->
                                                List.filter (\arg ->
                                                            -- filter virtual arguments (start with _ not followed by _)
                                                            case String.toList <| String.left 2 (first arg) of
                                                                ['_','_'] -> True
                                                                ['_', _] -> False
                                                                _ -> True
                                                                )
                                                       (zip goalState.argumentNames
                                                            (case goalState.argumentChoice of
                                                                 Enumerated argIds -> argIds
                                                                 Manual _ -> []))
                                            Nothing -> []
                                    }
                             goals = List.map (\g -> (g.node, { nodeId = g.node
                                                              , derives = g.derives
                                                              , discards = g.introducedHyps
                                                              , kind = GoalNode
                                                              , ruleArguments = []
                                                              , role = g.role
                                                              })) ruleApp.goals
                             (nodesWithNewNode, newChildrenOf) = nodeNewId goalId mParentId node model.nodes model.childrenOf
                             newNodes = insertMany goals nodesWithNewNode
                         in { model | nodes = newNodes
                            , source =
                                case mParentId of
                                    -- first goal, empty proof
                                    Nothing -> ruleApp.node
                                    -- not first goal, keep source node
                                    Just _ -> model.source
                            , goalsState =
                                insertMany (List.map (\g -> (g.node, emptyGS)) ruleApp.goals)
                                    <| Dict.remove goalId model.goalsState
                            , childrenOf = Dict.insert ruleApp.node (List.map (\g -> g.node) ruleApp.goals) newChildrenOf
                            , disabledInput = False
                            }
                     Nothing ->
                         { model | errors = addError "Can not find goal" model.errors
                         , disabledInput = False
                         }
            , Cmd.batch (getFormulas (ruleAppFormulas ruleApp) -- get any missing formulas
                        :: List.map (\g -> getPossibleArgs state dedInfo g.node dedInfo.defaultRule) ruleApp.goals
                        )
            )

        PickStrategy goalId strategyName ->
            ( model
            , Cmd.none
            )

        RunStrategy strategyName parentId goalId ->
            let mRole = Maybe.map (.role) <| Dict.get goalId model.nodes
            in
            ( { model |
                    goalsState =
                        Dict.update
                            goalId
                            (\mGS ->
                                 case mGS of
                                     Nothing -> Nothing
                                     Just gs -> Just { gs | strategyStatus = Running strategyName }
                            )
                            model.goalsState
              , disabledInput = True
              }
            , Cmd.map (receiveStrategyResult strategyName parentId (goalId, mRole))
                <| postGlfRunStrategyBySystemByStrategy (basicAuthString state.auth) dedInfo.system.systemName strategyName dedInfo.mLanguageId [goalId]
            )

        FailedStrategy name goal err ->
            let (newModel, cmd) = update state dedInfo (GotError err) model
            in
            ( { newModel |
                goalsState =
                    Dict.update
                        goal
                        (Maybe.map (\gs -> if gs.strategyStatus == Running name
                                           then { gs | strategyStatus = NoneChosen }
                                           else gs -- TODO: change to Chosen when added
                                   ))
                        model.goalsState
              , disabledInput = False
              }
            , cmd
            )

        GetSubDeduction rootParent (oldGoalId, role) ->
            ( model
            , Cmd.map (receiveSubDeduction rootParent oldGoalId)
                <| postGlfLoadSubDeductionBySystemByRootIdByMaxLevel (basicAuthString state.auth) dedInfo.system.systemName rootParent 25 (Just role)
            )

        GotSubdeduction subrootParent oldGoalId subproof ->
            let nodes = Dict.union
                            (Dict.fromList <| List.map (\n -> (n.nodeId, n)) subproof.nodeInfos)
                            (Dict.remove oldGoalId model.nodes)
                newGoalsState = initGoalsState dedInfo.defaultRule subproof.nodeInfos
            in
                ( { formulas = Dict.union model.formulas (Dict.fromList subproof.formulas)
                  , nodes = nodes
                  -- order matters
                  , source = if oldGoalId == model.source
                             -- have to update source, since goal is
                             -- not the source anymore
                             then case Dict.get subproof.subroot nodes of
                                      Just subRootNode -> subRootNode.nodeId
                                      Nothing -> model.source -- shouldn't happen
                             else model.source
                  , childrenOf =
                      Dict.union
                          -- NOTE: this parent information does not
                          -- include its siblings
                          (parentOfToChildrenOf
                               <| List.filter (\(p,c) -> c /= subproof.subroot) subproof.parentOf)
                          -- NOTE: so we update the old version
                          -- instead instead
                          (Dict.update
                               subrootParent
                               (Maybe.map (List.map (\c -> if c == oldGoalId then subproof.subroot else c)))
                               model.childrenOf)
                  , disabledInput = False
                  , goalsState = Dict.remove oldGoalId model.goalsState
                  , helpView = model.helpView
                  , errors = model.errors
                  }
                , Cmd.batch
                      <| List.map (\g -> getPossibleArgs state dedInfo g dedInfo.defaultRule) (Dict.keys newGoalsState)
                )

        GotFormulas formulas ->
            ( modelWithFormulas formulas
            , Cmd.none
            )

        RevertProof mParentId nodeId ->
            ( { model | disabledInput = True }
            , Cmd.map (receiveProofReversal mParentId nodeId)
                <| postGlfRevertBySystemByNodeId (basicAuthString state.auth) dedInfo.system.systemName nodeId []
            )


        RevertedProofGoal mParentId nodeId goalId ->
            ( case Dict.get nodeId model.nodes of
                  Just nodeToDelete ->
                      let goal = { nodeId = goalId
                                 , derives = nodeToDelete.derives
                                 , discards = nodeToDelete.discards
                                 , kind = GoalNode
                                 , ruleArguments = []
                                 , role = nodeToDelete.role
                                 }
                          (goalsToDelete, nodesWithRemovals, childrenOfWithRemovals) = removeNodeAndDescendants nodeId ([], model.nodes, model.childrenOf)
                          (updatedNodes, updatedChildrenOf) = nodeNewId nodeId mParentId goal nodesWithRemovals childrenOfWithRemovals
                      in { model | nodes = updatedNodes
                         , source = if nodeId == model.source
                                    -- reverted the whole proof
                                    then goalId
                                    else model.source
                         , goalsState = Dict.insert goalId emptyGS
                                        <| List.foldl Dict.remove model.goalsState goalsToDelete
                         , disabledInput = False
                         , childrenOf = updatedChildrenOf
                         }
                  Nothing -> { model | errors = addError "Could not find deleted node" model.errors
                             , disabledInput = False
                             }
            , Cmd.none)

        DeleteDeduction ->
            ( model
            , Cmd.map receiveProofDeletion
                <| getGlfDeleteBySystemByRootId (basicAuthString state.auth) dedInfo.system.systemName dedInfo.root
            )

        -- NOTE: handled at top-level
        ExitDeduction -> ( model, Cmd.none )

        GetDeductionExport ->
            ( model
            , Cmd.map receiveExport
                <| getGlfExportLatexBySystemByRootId (basicAuthString state.auth) dedInfo.system.systemName dedInfo.root dedInfo.mLanguageId
            )

        GotDeductionExport exportedDeduction ->
            ( model
            , downloadExportedDeduction exportedDeduction
            )

        PickHelp hv ->
            ( { model | helpView = hv }
            , Cmd.none
            )

        GotError err ->
            ( { model | errors = addError err model.errors
              , disabledInput = False
              }
            , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError err)]
            )

        ClearError err ->
            ( { model | errors = clearError err model.errors }
            , Cmd.none
            )


removeNodeAndDescendants : NodeId -> (List NodeId, Dict NodeId NodeInfo, Dict NodeId (List NodeId))
                          -> (List NodeId, Dict NodeId NodeInfo, Dict NodeId (List NodeId))
removeNodeAndDescendants id (deletedGoals, nodes, childrenOf) =
    case Dict.get id nodes of
        Just node ->
            let moreDeletedGoals =
                    case node.kind of
                        GoalNode -> id::deletedGoals
                        _ -> deletedGoals
            in
                List.foldl
                    removeNodeAndDescendants
                    ( moreDeletedGoals
                    , Dict.remove id nodes
                    , Dict.remove id childrenOf
                    )
                    (Maybe.withDefault [] <| Dict.get id childrenOf)
        Nothing -> (deletedGoals, nodes, childrenOf)


receive = receiveFunction GotError

receiveExport =
    receive (\r ->
                 case r of
                     Ok e -> GotDeductionExport e
                     Err error -> GotError error
            )

receiveFormulaIdsAndApplyRule formulaTexts mParentId goal ruleName argNames =
    receive (\r ->
                 case r of
                     Ok fIds -> GotFormulaIdsGoRule (zip fIds formulaTexts) mParentId goal ruleName argNames
                     Err msg -> GotError msg)


receiveSubDeduction subrootParent oldGoalId =
    receive (\r ->
                 case r of
                     Err msg -> GotError msg
                     Ok subproof -> GotSubdeduction subrootParent oldGoalId subproof)

receiveStrategyResult strategyName parentId (goalId, mRole) r =
    receive
        (\oksOrError ->
             case oksOrError of
                 Ok [True] ->
                     case mRole of
                         Just role -> GetSubDeduction parentId (goalId, role)
                         Nothing -> GotError "Did not find node role"
                 Ok _ -> GotError "Could not apply strategy to goal"
                 Err msg -> GotError msg
        )
        r
    |> (\msg -> case msg of
                    GotError err -> FailedStrategy strategyName goalId err
                    _ -> msg
       )

receiveProofReversal mParentId nodeId = receive (\goalId -> RevertedProofGoal mParentId nodeId goalId)

receiveProofDeletion =
    receive (\deleted -> if deleted
                         then ExitDeduction
                         else GotError "Could not delete proof")

receiveRulePossibleArgs goal rule r =
    case r of
        Err (_, Just response) -> GotError response.body
        Err _ -> GotError "Error fetching possible rules"
        Ok (Err err) -> GotError err
        Ok (Ok possibleArguments) -> GotPossibleRuleArguments goal rule possibleArguments

receiveRuleResult mParentId goal rule r =
    case r of
        Err (_, Just response) -> GotError response.body
        Err _ -> GotError "Error fetching possible rules"
        Ok (Err err) -> GotError err
        Ok (Ok ruleApp) -> GotRuleResult mParentId goal ruleApp

view : WorkingDeductionInfo -> Model -> Html Msg
view dedInfo model =
    let deductionOver = Dict.isEmpty model.goalsState
        help =
            viewHelp
                NoOp
                PickHelp
                (ul []
                     [ li [] [ text "Goals are shown in "
                             , span [style "color" "red"] [text "red"]
                             , text ", and proof leaves in "
                             , span [style "color" "green"] [text "green"]
                             ]
                     , li [] [text "Hypotheses introduced at a proof step are shown in ", span [style "color" "orange"] [text "orange"]]
                     , li [] [text "After a rule is selected, the system checks whether it can be applied, and asks for its arguments"]
                     , li [] [text "Arguments may be inputted manually, or selected from a non-exhaustive list provided by the system"]
                     , li [] [text "Arguments may be inputted directly, or selected among the options offered automatically"]
                     , li [] [text "The arguments employed by previous proof steps are shown in ", span [style "color" "purple"] [text "purple"]]
                     , li [] [text "If the system supports it, you may also choose to attempt an automatic proof for a goal from a range of available strategies"]
                     ])
                [ dt [] [revertProofButton []]
                , dd []
                    [text "Reverse a proof step (and its subproof)"]
                ]
                dedInfo.system
                model.helpView
    in
    Html.div []
        [ Html.ul [id "deduction-meta", class "horizontal-list"]
              [ li [] [helpButton PickHelp model.helpView]
              , li [] [backToSystemsButton ExitDeduction]
              , li [] [deleteDeductionButton DeleteDeduction]
              , li [] [exportButton GetDeductionExport (not deductionOver)]
              ]
        , help
        , viewErrors model.errors
        , Html.div [class "deduction", attribute "data-over" (if deductionOver then "true" else "false")]
            [ viewProof
                  model.disabledInput
                  dedInfo.defaultRule
                  dedInfo.system
                  model.formulas
                  model.nodes
                  model.childrenOf
                  model.goalsState
                  model.source
                  dedInfo.root
            ]
        ]

caseArguments : List String -> a -> (() -> a) -> a
caseArguments argNames def f =
    case argNames of
        [] -> def
        -- special case: arguments are virtual/don't matter
        ["_"] -> def
        _ -> f ()

selectArguments : GoalId -> List String -> List (List (String, Int)) -> Bool -> Html Msg
selectArguments goal argNames possibleArgs disabledInput =
    let freeFormKey = "FreeForm"
        manualArgumentsOption =
            Html.option [value freeFormKey, selected True]
                [ Html.text "Free-form input" ]
        noArgumentsOption =
            Html.option []
                [ Html.text "No Arguments" ]
    in
        Html.select
            [ onInput
                  (\opt ->
                       if opt == freeFormKey
                       then -- chooses free form argument input
                           ChangeManualArgument goal 0 ""
                       else
                           PickEnumeratedArgument goal opt
                  )
            , disabled (caseArguments argNames True (\_ -> False) || disabledInput)
            , class "ruleArgumentSelection"
            ]
        <| caseArguments
            argNames
            [ noArgumentsOption ]
            (\_ ->
                 ( manualArgumentsOption ::
                       List.indexedMap
                           (\ix args ->
                                Html.option
                                [value (String.fromInt <| ix)]
                                [ Html.text
                                      <| showDict
                                      <| List.map2 (\argName (arg, argId) -> (argName, arg))
                                      argNames args
                                ]
                           )
                       possibleArgs
                 )
            )

viewProof : Bool -> RuleInfo -> SystemInfo -> Dict FormulaId Formula -> Dict NodeId NodeInfo -> Dict NodeId (List NodeId) -> GoalsState -> NodeId -> RootId
          -> Html Msg
viewProof disabledInput defR system formulas nodes childrenOf goalsState rootNode rootId =
    let getNode = \nId -> Dict.get nId nodes
        goNode parentId child =
            case Dict.get child nodes of
                Just node ->
                    case node.kind of
                        GoalNode ->
                            viewNode
                                formulas
                                (Just parentId)
                                node
                                [makeGoal (Just parentId) node]
                        _ -> makeNode (Just parentId) node
                Nothing -> Html.text "Missing node or goal"
        makeGoal mParentId goal =
            let goalId = goal.nodeId
                gs = Maybe.withDefault (emptyGoalState defR) (Dict.get goalId goalsState)
                strategyButton =
                    case system.strategiesInfo of
                        [] -> span [] []
                        StrategyInfo s::_ ->
                            -- TODO: add choice of strategy
                            case gs.strategyStatus of
                                NoneChosen ->
                                    button [ type_ "button"
                                           , class "applyStrategyButton"
                                           , disabled disabledInput
                                           , onClick (RunStrategy
                                                          s
                                                          (Maybe.withDefault rootId mParentId)
                                                          goalId
                                                     )
                                           ]
                                           [ Html.text "Strategy" ]
                                Running _ -> span [class "applyingStrategy", class "loader"] [Html.text "⊢"]
                maybeSelectManualArguments =
                    case gs.argumentChoice of
                        Manual args ->
                            caseArguments
                                gs.argumentNames
                                (span [] [])
                                (\_ -> selectManualArguments (ChangeManualArgument goalId) disabledInput gs.argumentNames args)
                        Enumerated _ -> span [] []
            in
                div [class "interaction"]
                    [ selectRules disabledInput Nothing (PickRule goalId) (Just gs.rule) system.rulesInfo
                    , selectArguments goalId gs.argumentNames gs.possibleArguments disabledInput
                    , applyRuleButton
                          (GoRule
                               mParentId
                               goalId
                               gs.rule
                               gs.argumentNames
                               gs.argumentChoice
                          )
                          (caseArguments gs.argumentNames
                               -- when a rule has no arguments we
                               -- use the argument collection
                               -- request to instead check if the
                               -- rule is applicable or not; a
                               -- single useless result if so, and
                               -- no results if not
                               (List.length gs.possibleArguments == 0)
                               (\_ -> False) || disabledInput
                          )
                    , strategyButton
                    , maybeSelectManualArguments
                    ]
        makeNode mParentId node =
            viewNode
                formulas
                mParentId
                node
                (List.map (goNode node.nodeId) (Maybe.withDefault [] (Dict.get node.nodeId childrenOf)))
    in case Dict.get rootNode nodes of
           Just node ->
               case node.kind of
                   RuleNode _ -> makeNode Nothing node
                   -- should only ever happen with empty proofs
                   GoalNode -> viewNode formulas
                                        Nothing
                                        node
                                        [makeGoal Nothing node]
                   _ -> Html.text "Unexpected root node kind"
           Nothing -> Html.text "Missing root"

viewNode : Dict FormulaId Formula -> Maybe NodeId -> NodeInfo -> List (Html Msg) -> Html Msg
viewNode formulas mParentId node childrenHTML =
    let getFormula = User.viewFormula formulas
    in Html.ol []
        [ Html.li [class "nobullet"]
              (Html.div [attribute "data-kind" (case node.kind of
                                                    RuleNode _ -> "Node"
                                                    GoalNode -> "Goal"
                                                    LeafNode -> "Leaf"
                                                    _ -> "_unexpected"
                                               )]
                   [ Html.span [class "role"] [Html.text node.role]
                   , Html.span [class "formula"] [getFormula node.derives]
                   , Html.span [class "ruleApplicationName"]
                       [ Html.text
                            <| case node.kind of
                                   RuleNode rule -> rule
                                   _ -> ""
                       ]
                   , Html.span [class "objectNotation ruleApplicationArguments"]
                       <| List.map (\arg -> Html.span [class "pairNotation", attribute "data-pair-key" (first arg)]
                                        [ getFormula (second arg) ])
                           node.ruleArguments
                   , Html.ul [class "listNotation introducedHypotheses"]
                       (List.map ((\el -> Html.li [] [el]) << getFormula) node.discards)
                   , case node.kind of
                         RuleNode _ ->
                             revertProofButton [ type_ "button"
                                               , class "interaction revertProofButton"
                                               , onClick (RevertProof mParentId node.nodeId)
                                               ]
                         _ -> Html.span [class "ignore"] []

                   ]
              :: childrenHTML)
        ]

showDict : List (String, String) -> String
showDict pairs = String.concat
                 [ "{"
                 , String.join ", " (List.map
                                         (\pair -> String.concat [first pair, ": ", second pair])
                                         pairs)
                 , "}"]

nodeNewId : NodeId -> Maybe NodeId -> NodeInfo -> Dict NodeId NodeInfo -> Dict NodeId (List NodeId)
          -> (Dict NodeId NodeInfo, Dict NodeId (List NodeId))
-- remove old node, substitute for new one, and update parent node
-- children if there is one
nodeNewId oldId mParentId new nodes childrenOf =
    let nodesWithoutOld = Dict.remove oldId nodes
        nodesWithNew = Dict.insert new.nodeId new nodesWithoutOld
    in
        ( nodesWithNew
        , case mParentId of
              Nothing ->  childrenOf
              Just parentId ->
                  Dict.update
                      parentId (Maybe.map (\children ->
                                               List.map (\childId -> if childId == oldId then new.nodeId else childId) children))
                          childrenOf
        )

initializeGoalState : RuleInfo -> List (List (String, FormulaId)) -> GoalState
initializeGoalState r possibleArgs =
    let argumentNames = ruleArgumentNames r
    in
        { rule = r.ruleName
        , argumentNames = argumentNames
        , possibleArguments = possibleArgs
        , argumentChoice = Manual (Array.initialize (List.length argumentNames) (\_ -> ""))
        , strategyStatus = NoneChosen
        }
