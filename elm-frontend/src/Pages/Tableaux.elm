module Pages.Tableaux exposing (Model, Msg(..), init, subscriptions, update, view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Dict exposing (Dict)
import Browser.Dom as DOM
import Task
import Set
import Json.Decode as JSON
import Tuple

import Api exposing (..)
import Base exposing (..)
import Pages.Login exposing (..)
import Pages.User as User exposing (..)
import Pages.Backwards as Backwards

type alias GoalState =
    { rule : String }

type alias Model =
    { errors : Errors
    , source : NodeId
    , formulas : Dict FormulaId String
    , nodes : Dict NodeId NodeInfo
    , childrenOf : Dict NodeId (List NodeId)
    , goalsState : Dict GoalId GoalState
    , disabledInput : Bool
    , highlighted : List NodeId
    , helpView : HelpView
    , undoCache : List RuleApp
    , openBranches : Int
    }

type Msg = NoOp
    | GotGoalRule GoalId String
    | ApplyRule GoalId String
    | GotRuleApplicationResults GoalId RuleApp
    | GotFormulas (List (FormulaId, Formula))
    | PickHelp HelpView
    | RevertApplication RuleApp
    | RevertedApplication NodeId (List NodeId) (List NodeId)
    | DeleteDeduction
    | ExitDeduction
    | GetDeductionExport
    | GotDeductionExport String
    | HighlightNode (Bool, NodeId)
    | GotError String
    | ClearError String

init : SharedState -> WorkingDeductionInfo -> ProofInfo -> ( Model, Cmd Msg )
init state dedInfo deduction =
    let formulasMap = Dict.fromList deduction.proof.formulas
        (source, errors) =
            case deduction.sources of
                [] -> (-1, Set.singleton "No source node")
                [s] -> (s, Set.empty)
                _ -> (-1, Set.singleton "Too many source nodes for Tableaux system")
        goalIds = List.filterMap (\n -> if n.kind == GoalNode then Just n.nodeId else Nothing) deduction.proof.nodeInfos
        childrenOf = Backwards.parentOfToChildrenOf deduction.proof.parentOf
        openBranches =
            List.foldl
              (\g n ->
                   maybe
                     (n + 1)
                     (\children ->
                          case children of
                              [] -> n + 1
                              _ -> n)
                   <| Dict.get g childrenOf)
              0
              goalIds
    in
    ( { errors = errors
      , source = source
      , formulas = formulasMap
      , nodes = Dict.fromList
                <| List.map (\n -> ( n.nodeId, n ))
                    deduction.proof.nodeInfos
      , childrenOf = childrenOf
      , goalsState = Dict.empty
      , disabledInput = openBranches == 0
      , highlighted = []
      , helpView = NoHelp
      , undoCache = []
      , openBranches = openBranches
      }
    , if openBranches == 0 then Cmd.none else checkRulesProvisos state.auth dedInfo.system dedInfo.root goalIds
    )


tableauxUndoCacheSize : Int
tableauxUndoCacheSize = 5

checkRulesProvisos : AuthInfo -> SystemInfo -> RootId -> List GoalId -> Cmd Msg
checkRulesProvisos auth system rootId goals =
    let cmd goal =
            Cmd.map (receiveRuleProvisoResult goal)
                <| getGlfFindMatchingRulesBySystemByGoalId
                    (basicAuthString auth)
                    system.systemName
                    goal
    in Cmd.batch
        <| List.map cmd goals

receiveRuleProvisoResult goalId =
    receive (\r ->
                 case r of
                     [] -> NoOp
                     [rule] -> GotGoalRule goalId rule
                     _ -> GotError "Too many arguments for rule"
            )

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

update : SharedState -> WorkingDeductionInfo -> Msg -> Model -> ( Model, Cmd Msg )
update state dedInfo msg model =
    let dedOver = model.openBranches == 0 in
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotGoalRule goal rule ->
            ( { model | goalsState = Dict.insert goal {rule = rule} model.goalsState }
            , Cmd.none
            )

        ApplyRule goal rule ->
            ( { model | disabledInput = True}
            , Cmd.map (receiveRuleApplicationResults goal)
                  <| postGlfRuleBySystemByRootIdByRule
                      (basicAuthString state.auth)
                      dedInfo.system.systemName
                      dedInfo.root
                      rule
                      dedInfo.mLanguageId
                      [(rootArgName, dedInfo.root), (goalArgName, goal)] )

        GotRuleApplicationResults goal ruleApp ->
            let getFormulas = makeGetFormulas GotError GotFormulas state.auth model.formulas
                goalIds = List.map (.node) ruleApp.goals
                makeGoal g =
                    ( g.node
                    , { nodeId = g.node
                      , derives = g.derives
                      , discards = []
                      , kind = GoalNode
                      , ruleArguments = [(parentArgName, ruleApp.node)]
                      , role = g.role
                      }
                    )
                goalAndParent g =
                    case g.introducedHyps of
                        [p] -> (g.node, p)
                        _ -> (g.node, -1)
                parentsAndGoals = groupBy (\(_, p) -> p) (List.map goalAndParent ruleApp.goals)
                newNodes = List.sortBy (\(_, g) -> g.derives) <| List.map makeGoal ruleApp.goals
                nodesWithUpdatedApplication = Dict.update ruleApp.node (Maybe.map (\n -> { n | kind = RuleNode ruleApp.ruleApplied.unicode })) model.nodes
                (goalsToDelete, updatedNodes, updatedChildrenOf) =
                    case ruleApp.goals of
                        [] -> -- is leaf
                             List.foldl
                                 Backwards.removeNodeAndDescendants
                                 ( [ruleApp.node, goal]
                                 , Dict.update
                                     goal
                                     (Maybe.map (\n -> { n | kind = LeafNode, ruleArguments = (closesArgName, ruleApp.node) :: n.ruleArguments }))
                                     nodesWithUpdatedApplication
                                 , Dict.remove goal model.childrenOf
                                 )
                                 (Maybe.withDefault [] <| Dict.get goal model.childrenOf)
                        _ -> -- not a leaf
                            ( [goal]
                            , insertMany newNodes nodesWithUpdatedApplication
                            , List.foldl
                                 (\(p, gps) childrenOf ->
                                      let childrenGoals = List.map Tuple.first gps
                                      in Dict.update p (Just << maybe childrenGoals ((++) childrenGoals)) childrenOf
                                 )
                                model.childrenOf
                                parentsAndGoals
                            )
                openBranches = case parentsAndGoals of
                      -- leaf, closed a branch
                      [] -> model.openBranches - 1
                      _ -> model.openBranches + (List.length goalIds - List.length parentsAndGoals)
            in
            ( { model | goalsState =
                    if openBranches == 0
                    then Dict.empty
                    else removeMany goalsToDelete model.goalsState
              , nodes = updatedNodes
              , childrenOf = updatedChildrenOf
              , disabledInput = False
              , undoCache =
                  case ruleApp.goals of
                      [] ->
                          -- can't undo after branch closure, so
                          -- remove from cache all applications
                          -- leading to it
                          Tuple.first
                              <| List.foldl
                                  (\app (newCache, touched) ->
                                       if Set.member app.node touched || List.any (\g -> Set.member g.node touched) app.goals
                                       then (newCache, Set.union touched (Set.fromList <| app.node :: List.map .node app.goals))
                                       else (newCache ++ [app], touched)
                                  )
                                  ([], Set.fromList [goal, ruleApp.node])
                                  model.undoCache
                      _ -> ruleApp :: List.take (tableauxUndoCacheSize - 1) model.undoCache
              , openBranches = openBranches
              }
            , Cmd.batch
                  [ getFormulas (ruleAppFormulas ruleApp) -- get any missing formulas
                  , if openBranches == 0 then Cmd.none else checkRulesProvisos state.auth dedInfo.system dedInfo.root goalIds
                  ]
            )

        GotFormulas formulas ->
            ( { model | formulas = insertMany formulas model.formulas }
            , Cmd.none
            )

        PickHelp helpView ->
            ( { model | helpView = helpView}
            , Cmd.none
            )

        RevertApplication lastApp ->
            let nodeId = lastApp.node
                childrenGoals = List.map (\{node} -> node) lastApp.goals
                dataParents = List.concatMap (\{introducedHyps} -> introducedHyps) lastApp.goals
            in
                ( { model | disabledInput = True }
                , Cmd.map (receiveApplicationReversal nodeId childrenGoals dataParents)
                    <| postGlfRevertBySystemByNodeId
                        (basicAuthString state.auth)
                        dedInfo.system.systemName
                        lastApp.node
                        [ ListP tableauxRevertChildrenGoalsParamName childrenGoals
                        , ListP tableauxRevertDataParentsParamName dataParents
                        ]
                )

        RevertedApplication nodeId deletedGoals parents ->
            let parentsAndGoals = groupBy (\(p, _) -> p) (zip parents deletedGoals)
            in
            ( { model | nodes = Dict.update nodeId (Maybe.map (\node -> {node | kind = GoalNode})) <| removeMany deletedGoals model.nodes
              , childrenOf = removeMany parents model.childrenOf
              , goalsState = removeMany deletedGoals model.goalsState
              , disabledInput = False
              , undoCache = List.filter (\{node} -> node /= nodeId) model.undoCache
              , openBranches = model.openBranches - (List.length deletedGoals - List.length parentsAndGoals)
              }
            , checkRulesProvisos state.auth dedInfo.system dedInfo.root [nodeId]
            )

        -- NOTE: handled at top-level
        ExitDeduction -> ( model, Cmd.none )

        DeleteDeduction ->
            ( model
            , Cmd.map receiveProofDeletion
                <| getGlfDeleteBySystemByRootId (basicAuthString state.auth) dedInfo.system.systemName dedInfo.root
            )

        GetDeductionExport ->
            ( model,
                  Cmd.map receiveExport
                  <| getGlfExportLatexBySystemByRootId (basicAuthString state.auth) dedInfo.system.systemName dedInfo.root dedInfo.mLanguageId
            )

        GotDeductionExport exportedDeduction ->
            ( model
            , downloadExportedDeduction exportedDeduction
            )

        HighlightNode (True, nodeId) ->
            ( { model | highlighted = nodeId :: model.highlighted }
            , cmdAfter (1.2*1000) (\_ -> HighlightNode (False, nodeId)) -- maybe scroll into view if not in view
            )
        HighlightNode (False, nodeId) ->
            ( { model | highlighted = removeFirst nodeId model.highlighted }
            , Cmd.none
            )

        GotError error ->
            ( { model | errors = addError error model.errors
              , disabledInput = False
              }
            , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError error)]
            )

        ClearError error ->
            ( { model | errors = clearError error model.errors }
            , Cmd.none
            )

receiveApplicationReversal nodeId deletedGoals dataParents =
    receive (\_ -> RevertedApplication nodeId deletedGoals dataParents)

receiveProofDeletion =
    receive (\deleted -> if deleted
                         then ExitDeduction
                         else GotError "Could not delete proof")

receiveExport =
    receive (\r ->
                 case r of
                     Ok e -> GotDeductionExport e
                     Err error -> GotError error
            )


view : WorkingDeductionInfo -> Model -> Html Msg
view dedInfo model =
    let deductionOver = model.openBranches == 0
        help =
            viewHelp
                NoOp
                PickHelp
                (ul []
                     [ li [] [ text "Goals (formulas that have not been expanded yet) are shown in "
                             , span [style "color" "red"] [text "red"]
                             , text ", and proof leaves (formulas that close a branch) are shown in "
                             , span [style "color" "green"] [text "green"]
                             , text ", while regular expanded formulas are shown in black"
                             ]
                     , li [] [text "Click on a goal to apply the rule applicable to it, or attempt to close a branch"]
                     , li [] [text "Click on a rule name to highlight the formula which originated it"]
                     , li [] [text "Click on a leaf formula to highlight the formula which closes the branch."]
                     ])
                    [ dt [] [revertApplicationButton (\_ -> NoOp) (Just ()) False]
                    , dd []
                        [text "Undo latest successful rule application; after a branch is closed, it can not be undone"]
                    ]
                    dedInfo.system
                    model.helpView
    in
    Html.div []
        [ Html.ul [id "deduction-meta", class "horizontal-list"]
              [ li [] [revertApplicationButton RevertApplication (safeIndex model.undoCache 0) deductionOver]
              , li [] [helpButton PickHelp model.helpView]
              , li [] [backToSystemsButton ExitDeduction]
              , li [] [deleteDeductionButton DeleteDeduction]
              , li [] [exportButton GetDeductionExport (not deductionOver)]
              ]
        , help
        , viewErrors model.errors
        , span [ class "tableauxOpenBranches"
               , style "visibility" <| if deductionOver then "hidden" else "visible"
               ]
            [ text <| "Open branches: "
            , span [class "tableauxOpenBranchesNumber", style "color" "red"] [text <| String.fromInt model.openBranches]]
        , Html.div [class "deduction", attribute "data-over" (if deductionOver then "true" else "false")]
            [ viewTableaux
                  dedInfo.system
                  model.formulas
                  model.nodes
                  model.childrenOf
                  model.goalsState
                  model.source
                  dedInfo.root
                  model.highlighted
                  model.disabledInput
            ]
        ]

viewTableaux : SystemInfo -> Dict FormulaId String -> Dict NodeId NodeInfo -> Dict NodeId (List NodeId) -> Dict GoalId GoalState -> NodeId -> RootId
             -> List NodeId -> Bool
             -> Html Msg
viewTableaux system formulas nodes childrenOf goalsState sourceNode rootId highlighted disabledInput =
    let getNode = \nId -> Dict.get nId nodes
        goNode parentId child =
            case Dict.get child nodes of
                Just node ->
                    let mGoalState =
                            case node.kind of
                                GoalNode -> Dict.get node.nodeId goalsState
                                _ -> Nothing
                    in makeNode (Just parentId) mGoalState node
                Nothing -> Html.text "Missing node or goal"
        makeNode mParentId mGoalState node =
            viewNode
                nodes
                formulas
                highlighted
                mParentId
                (if disabledInput then Nothing else mGoalState)
                node
                (List.map (goNode node.nodeId) (Maybe.withDefault [] (Dict.get node.nodeId childrenOf)))
    in case Dict.get sourceNode nodes of
           Just node -> makeNode Nothing (Dict.get node.nodeId goalsState) node
           Nothing -> Html.text "Missing root"

viewNode : Dict NodeId NodeInfo -> Dict FormulaId Formula -> List NodeId -> Maybe NodeId -> Maybe GoalState -> NodeInfo -> List (Html Msg) -> Html Msg
viewNode nodes formulas highlighted mParentId mGoalState node childrenHTML =
    let getFormula = User.viewFormula formulas
        nodeId = node.nodeId
    in Html.ol []
        [ Html.li [class "nobullet"]
              (Html.div ([ id (String.fromInt nodeId)
                         , classList [("blink", List.member nodeId highlighted)]
                         , attribute "data-kind" (case node.kind of
                                                      RuleNode _ -> "Node"
                                                      GoalNode -> "Goal"
                                                      LeafNode -> "Leaf"
                                                      _ -> "_unexpected"
                                                 )
                         ])
                   [ button
                         -- OPTIMIZE: maybe have Msg like user
                         -- clicked and then check if there's a rule
                         -- to apply or not
                         ((case node.kind of
                              GoalNode -> maybe [class "wait", disabled True] (\gs -> [onClick (ApplyRule node.nodeId gs.rule)]) mGoalState
                              LeafNode -> case assoc closesArgName node.ruleArguments of
                                              Just closesId -> [onClick (HighlightNode (True, closesId))]
                                              _ -> []
                              _ -> [])
                         ++ [ class "as-text", class "formula" ])
                         [ getFormula node.derives ]
                   , case assoc parentArgName node.ruleArguments of
                         Just parentId ->
                             case Dict.get parentId nodes of
                                 Just {kind} ->
                                     case kind of
                                         RuleNode ruleName ->
                                             button [ class "parentRuleApplicationName"
                                                    , class "as-text"
                                                    , onClick (HighlightNode (True, parentId))
                                                    ]
                                                 [text ruleName]
                                         _ -> span [] []
                                 Nothing -> span [] []
                         _ -> span [] []
                   ]
              :: childrenHTML)
        ]

receive = receiveFunction GotError

receiveRuleApplicationResults goal =
    receive (\r ->
                 case r of
                     Err err -> GotError err
                     Ok ruleApp -> GotRuleApplicationResults goal ruleApp)

revertApplicationButton : (a -> msg) -> Maybe a -> Bool -> Html msg
revertApplicationButton toMsg sentinel isDisabled  =
    button ([ type_ "button"
           , id "revertApplicationButton"
           ] ++
                case sentinel of
                    Just thing -> [onClick (toMsg thing), disabled isDisabled]
                    Nothing -> [disabled True])
      [ Html.text "Undo" ]
