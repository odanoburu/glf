module Pages.User exposing ( Model, Msg(..), Formula, WorkingDeductionInfo, HelpView(..), init, subscriptions, update, view
                           , makeGetFormulas, ruleAppFormulas, exportButton, downloadExportedDeduction, helpButton, viewHelp, deleteDeductionLink, ruleArgumentNames
                           , viewProposition, applyRuleButton, selectRules, selectManualArguments, viewFormula, viewFormulaList
                           , backToSystemsLink, viewRecord, splitListString, revertProofButton, ruleSelectionId, receive, ruleStartsSubproofNewLevel
                           )

import Html exposing (..)
import Html.Attributes as Attr exposing (..)
import Html.Events exposing (..)
import Http
import Set exposing (Set)
import Dict exposing (Dict)
import Json.Decode as Decode
import File.Download as Download
import Array as Array exposing (Array)
import Browser.Navigation as Nav
import Maybe as Maybe
import Task
import Routes exposing (..)
import Base exposing (..)
import Api exposing (..)
import Pages.Login exposing (..)

type alias SystemName = String

type alias Model =
  { errors : Errors
  , systemsByKind : Maybe (List (String, List SystemInfo))
  , system : Maybe SystemInfo
  , assumptionsInput : String
  , goalInput : String
  , languageInput : String
  }

type Msg
    = GetSystems (Maybe DeductionInputInfo)
    | GotSystemsInfo (Maybe String) {- optional pre-selected system -} (List SystemInfo)
    | NewDeduction SystemInfo DeductionInput
    | PickSystem (Maybe String)
    | LanguageInput String
    | AssumptionsInput String
    | GoalInput String
    | GotDeductionId RootId
    | GotError String
    | ClearError String
    | NoOp

init : AuthInfo -> Maybe DeductionInputInfo -> Maybe RootId-> ( Model, Cmd Msg )
init authInfo mDeductionInput mProofToDelete =
    ( { systemsByKind = Nothing
      , errors = Set.empty
      , system = Nothing
      , assumptionsInput = maybe "" .assumptionsInput mDeductionInput
      , goalInput = maybe "" .goalsInput mDeductionInput
      , languageInput = maybe "" .languageInput mDeductionInput
      }
    , case mProofToDelete of
           Just proofId -> Cmd.map (receiveDeductionDeletion mDeductionInput)
                                   <| getGlfDeleteByProofId (basicAuthString authInfo) proofId
           Nothing -> getSystemInfo authInfo mDeductionInput

    )

getSystemInfo : AuthInfo -> Maybe DeductionInputInfo -> Cmd Msg
getSystemInfo authInfo mDeductionInput  = Cmd.map (receiveSystemInfo <| Maybe.map .system mDeductionInput)
             <| Api.getGlfSystemdata (basicAuthString authInfo) Nothing

receiveDeductionDeletion mDeductionInputInfo =
    receive (\deleted -> if deleted then GetSystems mDeductionInputInfo else GotError "Could not delete proof")

receiveSystemInfo mSystem =
    receive (GotSystemsInfo mSystem)

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

type alias Formula = String

type alias WorkingDeductionInfo =
    { goalsInput : List Formula
    , assumptionsInput : List Formula
    , system : SystemInfo
    , mLanguageId : Maybe Int
    , sources : List NodeId
    , root : RootId
    , defaultRule : RuleInfo
    }

update : SharedState -> Nav.Key -> Msg -> Model -> ( Model, Cmd Msg )
update state navKey msg model =
    case msg of
        GetSystems mDeductionInput ->
            ( model
            , getSystemInfo state.auth mDeductionInput
            )
        GotSystemsInfo mSystem systems ->
            ( { model | systemsByKind = Just (groupBy (\s -> systemKindName s.kind) systems) }
            , Task.perform PickSystem (Task.succeed mSystem)
            )

        GotError error ->
            ( { model | errors = addError error model.errors }
            , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError error)]
            )

        NewDeduction system deductionInput ->
            let basicAuth = basicAuthString state.auth
                cmd = postGlfNewDeductionBySystem
                        basicAuth
                        system.systemName
                        deductionInput
            in ( model
               , Cmd.map newDeductionInfo cmd
               )

        PickSystem Nothing -> ( {model | system = Nothing}, Cmd.none )
        PickSystem (Just systemName) ->
            case List.concatMap (\(_, ss) -> List.filter (\sys -> sys.systemName == systemName) ss) <| Maybe.withDefault [] model.systemsByKind of
                [] -> ( model, Cmd.none)
                system :: _ ->
                    ( { model | system = Just system }
                    , scrollToElement NoOp chosenSystemId
                    )

        LanguageInput languageInput ->
            ( { model | languageInput = languageInput }
            , Cmd.none
            )

        AssumptionsInput assumptionsInput ->
            ( { model | assumptionsInput = assumptionsInput }
            , Cmd.none
            )

        GoalInput goalInput ->
            ( { model | goalInput = goalInput }
            , Cmd.none
            )

        GotDeductionId proofId ->
            ( model
            , case model.system of
                Nothing -> Cmd.none
                Just system -> Nav.pushUrl navKey (pathFor <| ProofRoute system.systemName proofId)
            )

        ClearError err ->
            ( { model | errors = clearError err model.errors }
            , Cmd.none
            )

        NoOp -> ( model, Cmd.none )

makeGetNode : List ProofNodeInfo -> (NodeId -> Maybe ProofNodeInfo)
makeGetNode nodeInfos =
    let nodesMap = Dict.fromList
                   <| List.map (\node -> (node.proofNodeId, node)) nodeInfos
    in \nodeId -> Dict.get nodeId nodesMap

proofInfoToWorkingDeductionInfo : SystemInfo -> RawProofInfo -> Maybe WorkingDeductionInfo
proofInfoToWorkingDeductionInfo systemInfo proofInfo =
    let formulasMap = Dict.fromList proofInfo.formulas
        getNode = makeGetNode proofInfo.nodesInfo
        getFormula nodeId = Dict.get nodeId formulasMap
    in case (proofInfo.goals, systemInfo.rulesInfo) of
           ([], _) -> Nothing
           (_, []) -> Nothing
           -- TODO: handle forward systems correctly
           (r :: _, defR::_) -> getFormula (Tuple.second r)
                  |> Maybe.andThen (\goalFormula ->
                                        Just { goalsInput = [goalFormula]
                                             -- FIXME: will silently erase assumptions
                                             , assumptionsInput = List.filterMap (\ass -> getFormula ass.formula) proofInfo.initialAssumptions
                                             , system = systemInfo
                                             , mLanguageId = proofInfo.mLanguage
                                             , sources = List.map Tuple.first proofInfo.goals
                                             , root = proofInfo.id
                                             , defaultRule = defR
                                             })

deductionRootInfoToWorkingDeductionInfo : RuleInfo -> SystemInfo -> DeductionRootRawInfo -> RawProofInfo -> WorkingDeductionInfo
deductionRootInfoToWorkingDeductionInfo defR system dedInfo proofInfo =
    { goalsInput = [dedInfo.goalInput]
    , assumptionsInput = Array.toList dedInfo.assumptionsInput
    , system = system
    , mLanguageId = proofInfo.mLanguage
    , sources = List.map Tuple.first proofInfo.goals
    , root = proofInfo.id
    , defaultRule = defR
    }

receive = receiveFunction GotError

newDeductionInfo =
    receive
      (\r -> case r of
           Err err -> GotError err
           Ok newDeduction -> GotDeductionId newDeduction.root)

chosenSystemId : String
chosenSystemId = "chosenSystem"

view : Model -> Html Msg
view model =
    let viewSystemsAvailable systemsByKind =
            let go (kind, systems) =
                    div [class "systems-by-kind"]
                        [ h4 [] [ text kind ]
                        , ul [ class "horizontal-list" ]
                            <| List.map
                                (\s ->
                                     li ((if maybe False (\ss -> s.systemName == ss.systemName) model.system
                                          then [class "selected", onClick (PickSystem Nothing)]
                                          else [onClick (PickSystem (Just s.systemName))])
                                             ++ [class "option", class "system-name"])
                                     [text s.systemName]
                                )
                                systems
                        ]
            in div [id "systems-available"]
            <| List.map go systemsByKind
        selectText isHidden =
            span (if isHidden then [class "hidden"] else []) [text "Select a system:"]
    in
    case model.system of
        Nothing ->
            case model.systemsByKind of
                Nothing -> div [] [ text "Loading systems… "
                                  , span [class "loader"] [ text "⊢" ]
                                  ]
                Just [] -> div [] [ text "No systems available" ]
                Just systemsByKind ->
                    div []
                        [ selectText False
                        , viewSystemsAvailable systemsByKind
                        ]
        Just chosenSystem ->
            div []
                [ selectText True
                , viewSystemsAvailable (Maybe.withDefault [] <| model.systemsByKind)
                , viewSystemInfo chosenSystem
                , viewNewDeductionInput chosenSystem
                    model.languageInput
                    model.assumptionsInput
                    model.goalInput
                , viewErrors model.errors
                ]

viewSystemInfo : SystemInfo -> Html Msg
viewSystemInfo system =
    div [ id chosenSystemId ]
        [ span [class "system-name", class "math"] [ h3 [] [ text system.systemName ] ]
        , div []
            [ p [] [text system.description]
            , details []
                  [ summary [] [ text "Syntax Help" ]
                  , viewSystemSyntax system
                  ]
            , if List.isEmpty system.deductions
              then span [] []
              else div [ class "system-deductions" ]
                  [ h4 [] [ text "Existing deductions" ]
                  , ul [class "system-deductions-list"]
                      <| List.map (viewDeduction system) system.deductions
                  ]
            ]
        ]

viewDeduction : SystemInfo -> DeductionRootRawInfo -> Html Msg
viewDeduction system deduction =
    a [ class "deduction", class "no-link-styling", class "sm-hz-padding"
      , href (pathFor <| ProofRoute system.systemName deduction.root)
      ]
      [ span [class "hover-red"] [viewProposition (Array.toList deduction.assumptionsInput) [deduction.goalInput]]
      ]

viewProposition : List String -> List String -> Html a
viewProposition assumptions goals =
    span [class "proposition"]
        [ span []
              [viewFormulaList assumptions]
        , span [class "bg-font", class "vsm-hz-padding"] [text "⊢"]
        , span []
            [viewFormulaList goals]]

viewNewDeductionInput : SystemInfo -> String -> String -> String -> Html Msg
viewNewDeductionInput system language assumptions goals  =
    let formId = "new-deduction-form"
    in
    div [ class "new-deduction" ]
        [ h4 [] [ text "New deduction"]
        , Html.form [ id formId ] []
        , if system.extensible
          then -- non-logical language input
              div [ id "language-form" ]
                  [ label [for "languageInput"] [ text "Non-logical Language (comma-separated list of constructs and their arities)" ]
                  , viewMathInput language [ name "languageInput", onInput LanguageInput, Attr.form formId, placeholder ".IsAlive 1, .IsParent 2" ]
                  ]
          else span [] []
        , div [id "assumptions-form", style "display" <| if system.kind == TableauxSys then "none" else "inherit" ]
            [ label [for "assumptionsInput"] [text "Assumptions"]
            , viewMathInput assumptions
                [ name "assumptionsInput", onInput AssumptionsInput, Attr.form formId, placeholder "A → B, A"]
            ]
        , label [for "goalInput"] [text "Goals"]
        , viewMathInput goals [ name "goalInput", onInput GoalInput, Attr.form formId, placeholder "B" ]
        , button [ type_ "button"
                 , onClick
                       <| case (Result.map3 DeductionInput
                                (splitListString language)
                                (splitListString assumptions)
                                (splitListString goals)) of
                              Ok dedInput -> NewDeduction system dedInput
                              Err error -> GotError error
                 , Attr.form formId
                 ]
            [ text "Submit" ]
        ]

viewFormulaList : List String -> Html a
viewFormulaList formulas = span [class "math"] [Html.text <| String.join ", " formulas]

viewFormulaText : String -> Html a
viewFormulaText formula = span [class "math"] [Html.text formula]

splitListString : String -> Result String (List String)
splitListString string =
    let go nopen ix acc cs =
            case cs of
                [] ->
                    if nopen == 0
                    then Ok (List.reverse <| ix :: acc)
                    else Err "There are unclosed parentheses"
                '('::cc -> go (nopen + 1) (ix+1) acc cc
                ')'::cc ->
                    if nopen > 0
                    then go (nopen - 1) (ix+1) acc cc
                    else Err "Trying to close parenthesis when there are no open ones"
                ','::cc ->
                    if nopen == 0
                    then go nopen (ix+1) (ix :: acc) cc
                    else go nopen (ix+1) acc cc
                _::cc -> go nopen (ix+1) acc cc
    in
        case go 0 0 [] (String.toList string) of
            Err err -> Err err
            Ok [] -> Ok []
            Ok endIndices ->
                let splits =
                        List.map2
                            (\lastCommaIx nextCommaIx -> String.slice (lastCommaIx+1) nextCommaIx string)
                            (-1::endIndices)
                            endIndices
                in Ok <|
                    List.filterMap
                        (\f ->
                             let trimmedF = String.trim f
                             in
                                 if String.isEmpty trimmedF
                                 then Nothing
                                 else Just trimmedF
                        )
                        splits



viewMathInput : String -> List (Attribute Msg) -> Html Msg
viewMathInput mathText attrs =
    textarea ([ rows 2, cols 80, spellcheck False, class "math"] ++ attrs) [ text mathText ]

ruleSelectionId : String
ruleSelectionId = "ruleSelection"

ruleStartsSubproofNewLevel : RuleInfo -> Bool
ruleStartsSubproofNewLevel r =
  case r.ruleAction of
    StartsSubProof True -> True
    StartsSubProof _ -> False
    NoAction -> False
    EndsSubProof -> False
    Axiom -> False

selectRules : Bool -> Maybe String -> (String -> msg) -> Maybe String -> List RuleInfo -> Html msg
selectRules isDisabled mId msg mRuleChosen rules =
    Html.select
        (maybe [] (\i -> [id i]) mId ++ [onInput msg, class ruleSelectionId, disabled isDisabled])
        (List.map (\rule ->
                       Html.option (maybe [] (\r -> if r == rule.ruleName then [ selected True ] else []) mRuleChosen)
                       [Html.text rule.ruleName]
                  )
             rules)

selectManualArguments : (Int -> String -> msg) -> Bool -> List String -> Array String -> Html msg
-- Int is index into list/array, which should have the same size (we
-- should probably harmonize this into Array (String, String) at
-- some point)
selectManualArguments msg isDisabled argNames selectedArguments =
    Html.div []
        (List.indexedMap
             (\ix argName ->
                  Html.span [class "formulaInput", attribute "data-argumentName" argName]
                      [ Html.textarea
                            [ value (Maybe.withDefault "" <| Array.get ix selectedArguments)
                            , rows 3
                            , cols 82
                            , wrap "soft"
                            , spellcheck False
                            , onInput (msg ix)
                            , disabled isDisabled
                            ]
                            []
                      ]
             )
             argNames)

applyRuleButton : msg -> Bool -> Html msg
applyRuleButton msg isDisabled =
    button [ type_ "button"
           , class "applyRuleButton"
           , onClick msg
           , disabled isDisabled
           ]
    [Html.text "GO"]


viewFormula : Dict FormulaId Formula -> FormulaId -> Html msg
viewFormula formulas formulaId =
    maybe (Html.span [class "missingFormula"] [])
        viewFormulaText
        <| Dict.get formulaId formulas

makeGetFormulas : (String -> msg) -> (List (FormulaId, Formula) -> msg) -> AuthInfo -> Dict FormulaId String -> List FormulaId -> Cmd msg
makeGetFormulas errMsg okMsg auth formulas formulaIds =
    let missingFormulaIds = List.filter (\fId -> not
                                             <| Dict.member fId formulas) formulaIds
        receiveFormulas = receiveFunction errMsg okMsg
    in case missingFormulaIds of
           [] -> Cmd.none
           _ ->
               Cmd.map
                   receiveFormulas
                   (postGlfGetFormulas (basicAuthString auth) missingFormulaIds)

ruleAppFormulas : RuleApp -> List FormulaId
ruleAppFormulas ruleApp = ruleApp.derives :: List.concatMap (\g -> g.derives :: g.introducedHyps) ruleApp.goals

exportButton : msg -> Bool -> Html msg
exportButton toggleMsg isDisabled =
    button [ type_ "button"
           , class "exportProofButton"
           , onClick toggleMsg
           , disabled isDisabled
           ]
    [ Html.text "Export" ]

downloadExportedDeduction : String -> Cmd msg
downloadExportedDeduction exportedDeduction =
    Download.string "proof.tex" "application/x-tex" exportedDeduction


type HelpView = NoHelp | SystemHelp | DeductionHelp | SyntaxHelp | RuleHelp

helpButton : (HelpView -> msg) -> HelpView -> Html msg
helpButton toMsg helpView =
    let toggleMsg =
            toMsg <| case helpView of
                         NoHelp -> SystemHelp
                         _ -> NoHelp
    in
    button [type_ "button", onClick toggleMsg, id "helpButton", class (if helpView == NoHelp then "notToggled" else "toggled")]
        [Html.text "Help"]

viewHelp : msg -> (HelpView -> msg) -> Html msg -> List (Html msg) -> SystemInfo -> HelpView -> Html msg
viewHelp noOpMsg helpMsg kindSpecificHelp otherButtons system helpView =
    let
        item = case system.kind of
                   ForwardsSys ->
                       \classNames mRole f ->
                           li (class "math" :: attribute "data-role" (Maybe.withDefault "" mRole) :: List.map class classNames) [text f]
                   BackwardsSys -> \classNames _ f -> li (class "math" :: List.map class classNames) [text f]
                   TableauxSys -> \classNames _ f -> li (class "math" :: List.map class classNames) [text f]
        viewRuleHelp rule =
            [ div [class "ruleHelp"]
                  [ dt []
                        [ button [type_ "button", onClick noOpMsg]
                              [ Html.text rule.ruleName ]
                        ]
                  , dd []
                      -- NOTE: can't use List.all because lists are different types, sigh
                      [ if List.isEmpty rule.rulePremises && List.isEmpty rule.ruleResults && List.isEmpty rule.ruleProvisos
                        then
                            text rule.help
                        else
                            div [class "rule-diagram", class "vertical-list"]
                                [ ul [class "horizontal-list", class "rule-premises"]
                                      (List.map (\((premiseRole, premiseRoot), hypotheses) ->
                                                     case hypotheses of
                                                         [] -> item ["rule-premise", "sole-premise"] (Just premiseRole) premiseRoot
                                                         _ ->
                                                             ul [class "vertical-list"]
                                                                 [ ul [class "horizontal-list"]
                                                                       (List.map (\(role, h) -> item ["rule-hypothesis"] (Just role) h) hypotheses)
                                                                 , item ["rule-ellipsis"] Nothing ""
                                                                 , item ["rule-premise"] (Just premiseRole) premiseRoot
                                                                 ]
                                                )
                                           rule.rulePremises)
                                , span [class "rule-line"] []
                                , ul [class "horizontal-list", class "rule-results"]
                                    <| List.map (\rs ->
                                                     case rs of
                                                         [r] -> item ["rule-result"] Nothing r
                                                         _ -> ul [class "vertical-list", class "rule-results-branch"]
                                                              <| List.map (item ["rule-result"] Nothing) rs
                                                )
                                                rule.ruleResults
                                , ul [class "rule-provisos"]
                                    <| List.map (\prov -> li [class "rule-proviso"] [Html.text prov]) rule.ruleProvisos
                                ]
                      ]
                  ]
            ]
        pickHelpMenu =
            ul [class "horizontal-list"]
                <| List.map
                    (\(hv, name) ->
                         li [ classList [("selected", hv == helpView)]
                            , class "option"
                            , onClick (helpMsg hv)
                            ]
                            [text name])
                    [ (SystemHelp, "Interface")
                    , (DeductionHelp, "Deduction")
                    , (SyntaxHelp, "Syntax")
                    , (RuleHelp, "Rules")
                    ]
        displayIf hv = if hv == helpView then style "display" "inherit" else style "display" "none"
    in
        div [ id "appHelp", style "display" (if helpView == NoHelp then "none" else "inherit")]
            [ pickHelpMenu
            , div [id "kindHelp", displayIf SystemHelp]
                [ kindSpecificHelp
                ]
            , div [id "syntaxHelp", displayIf SyntaxHelp]
                [ viewSystemSyntax system
                ]
            , div [id "deductionHelp", displayIf DeductionHelp]
                [ dl []
                      <| otherButtons ++
                          [ dt [] [helpButton (\_ -> noOpMsg) NoHelp]
                          , dd []
                              [text "Toggle view of this help text"]
                          , dt [] [backToSystemsLink]
                          , dd []
                              [text "Return to system+deduction selection (all progress is saved)"]
                          , dt [] [deleteDeductionLink (-1)]
                          , dd []
                              [text "Delete this deduction and return to system+deduction selection"]
                          , dt [] [exportButton noOpMsg False]
                          , dd []
                              [text "To export a finished proof to Latex, just press the Export button."]
                          ]
                ]
            , div [id "rulesHelp", displayIf RuleHelp]
                [ dl [class "horizontal-list"] (List.concatMap viewRuleHelp system.rulesInfo)
                ]
            ]

deleteDeductionLink : RootId -> Html msg
deleteDeductionLink proofId =
    buttonLink (UserRoute Nothing (Just proofId)) "Delete" [ id "deleteDeductionButton" ]

ruleArgumentNames : RuleInfo -> List String
ruleArgumentNames {argumentNames} =
    List.concatMap (\a ->
                        case a of
                            SoleArg n -> [n]
                            SubProofArg hn n -> [hn, n]
                   )
        argumentNames

backToSystemsLink : Html msg
backToSystemsLink =
    buttonLink (UserRoute Nothing Nothing) "New Deduction" []

viewRecord : List (Attribute msg) -> List (String, Html msg) -> Html msg
viewRecord attrs pairs =
    Html.span (class "objectNotation" :: attrs)
        <| List.map (\(k, v) ->
                         Html.span [class "pairNotation", attribute "data-pair-key" k]
                             [ v ]
                    )
            pairs

systemKindName : SystemKindInfo -> String
systemKindName sk =
    case sk of
        ForwardsSys -> "Fitch-style"
        BackwardsSys -> "Gentzen-style"
        TableauxSys -> "Tableaux"

operatorPrecedence : Operator -> Float
operatorPrecedence op =
  case op.associativity of
    Prefix (Precedence prec) -> prec
    Postfix (Precedence prec) -> prec
    InfixRight (Precedence prec) -> prec
    InfixLeft (Precedence prec) -> prec
    InfixNone (Precedence prec) -> prec
    None _ -> 0/0 -- NaN

viewSystemSyntax : SystemInfo -> Html msg
viewSystemSyntax system =
    let operators = system.operatorsInfo
        nArity associativity =
            case associativity of
                Prefix _ -> 1
                Postfix _ -> 1
                InfixRight _ -> 2
                InfixLeft _ -> 2
                InfixNone _ -> 2
                None (Fixed n) -> n
                None (AtLeast n) -> n + 1
        sortedOperators = List.sortBy (\{associativity} -> nArity associativity) operators
        exampleFormulas n = List.map (Char.fromCode >> String.fromChar) <| List.range 65 (64 + n)
        operatorInfixExample op associativity =
            text
              <| case associativity of
                     Prefix _ -> op ++ " A"
                     Postfix _ -> "A " ++ op
                     InfixRight _ -> String.join " " ["A", op, "B", op, "C", "≍", "A", op, "(B", op, "C)"]
                     InfixLeft _ -> String.join " " ["A", op, "B", op, "C", "≍", "(A", op, "B)", op, "C"]
                     InfixNone _ -> String.join " " ["A", op, "B", op, "C", "≭", "(A", op, "B)", op, "C", "≭", "A", op, "(B", op, "C)"]
                     None (Fixed 0) -> op
                     None arity -> op ++ "(" ++ String.join ", " (exampleFormulas <| nArity (None arity)) ++ ")"

        operatorPrefixExample op arity =
            text
              <| String.concat
                  [ "("
                  , String.join " " (op :: exampleFormulas (nArity arity))
                  , ")"
                  ]
        viewOperator op =
            div [class "system-operator"]
                [ h6 [] [text "Unicode"]
                , ul [class "horizontal-list"]
                    [ li [] [operatorPrefixExample op.name.unicode op.associativity, text ","]
                    , li [] [operatorInfixExample op.name.unicode op.associativity]
                    ]
                , h6 [] [text "Ascii"]
                , ul [class "horizontal-list"]
                    [ li [] [operatorPrefixExample op.name.ascii op.associativity, text ","]
                    , li [] [operatorInfixExample op.name.ascii op.associativity]
                    ]
                ]
    in div [class "system-syntax"]
        [ div [class "logical-language"]
              [ h4 [] [text "Logical"]
              , div [class "system-operators"]
                  (List.map viewOperator sortedOperators)
              , text "Operator precedence for infix notation in increasing order of precedence (further to the left = lower precedence): "
              , ol [class "horizontal-list", class "system-operators-precedence"]
                  (List.filterMap
                       (\(prec, opsSamePrecedence) ->
                            if isNaN prec
                            then Nothing
                            else
                              case opsSamePrecedence of
                                  [] -> Nothing
                                  [op] -> Just (li [] [text op.name.unicode])
                                  _::_ ->
                                      Just <|
                                          li []
                                              [ ul [class "vertical-list", class "operators-same-precedence"]
                                                    (List.map (\op -> li [] [text op.name.unicode]) opsSamePrecedence)
                                              ]

                       )
                       <| groupBy operatorPrecedence <| List.sortBy operatorPrecedence system.operatorsInfo)
              , text "(Operators in the same column have the same precedence.)"
              ]
        , if system.extensible
            then div [class "nonlogical-language"]
                [ h4 [] [text "Non-logical"]
                , p []
                    [ text "Non-logical language constructs like predicates must be preceded by a period ("
                    , code [] [text "."]
                    , text "), e.g.: "
                    , code [] [text ".IsEven(n)"]
                    , text " in infix notation or "
                    , code [] [ text "(.IsEven n)"]
                    , text " in prefix."]
                ]
          else span [] []
        ]

revertProofButton : List (Attribute msg) -> Html msg
revertProofButton attrs = button attrs [Html.text "⤶"]
