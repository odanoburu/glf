module Pages.Fitch exposing (Model, Msg(..), init, subscriptions, update, view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Set exposing (Set)
import Tuple exposing (..)
import Json.Decode as Decode
import Dict as Dict
import Array as Array exposing (Array)
import Routes
import Base exposing (..)
import Api exposing (..)
import Browser.Dom as Dom
import Browser.Navigation as Nav
import Task
import Dict exposing (..)
import Pages.Login exposing (..)
import Pages.User as User exposing (..)

type RoseTree a = Leaf a | Branch a (List (RoseTree a))

-- (start, end): start is assumption line, end is line just before
-- closing subproof
type alias SubProofs = List (RoseTree (Int, Int))

type alias Node =
    { derives : FormulaId
    , node : NodeId
    , rule : RuleInfo
    , arguments : List (String, NodeId)
    }

type alias Model =
  { formulas : Dict FormulaId String
  , nodes : Array Node
  , nodeIndex : Dict NodeId Int
  , subproofs : SubProofs
  , openSubproofs : List Int
  , rule : RuleInfo
  , argumentInput : String
  , formulaArgumentsInput : Array Formula
  , disabledInput : Bool
  , done : Bool
  , helpView : HelpView
  , errors : Errors
  , proofId : RootId
  , mLanguageId : Maybe Int
  , initialAssumptions : List Formula
  , goal : (Formula, FormulaId)
  , system : SystemInfo
  }

type Msg = PickRule String
         | ChangeFormulaArgument Int String
         | ChangeArguments String
         | GotFormulas (List (FormulaId, Formula))
         | GoRule RuleInfo String (Array Formula) -- do we need List String (argnames) if it's already at RuleInfo?
         | GoRuleAfterAddedArgumentFormulas RuleInfo (List (String, NodeId)) (List (FormulaId, Formula))
         | GotRuleResult RuleInfo (List (String, NodeId)) RuleApp
         | GetDeductionExport
         | GotDeductionExport String
         | CheckProof
         | ProofChecksOut
         | DeleteDeduction
         | RevertSubProof Int NodeId
         | RevertedSubProof Int NodeId
         | PickHelp HelpView
         | NoOp
         | GotError String
         | ClearError String

unknownRule : RuleInfo
unknownRule =
    { ruleName = "UnknownRule"
    , ruleAction = NoAction
    , argumentNames = []
    , formulaArgumentNames = []
    , help = ""
    , rulePremises = []
    , ruleProvisos = []
    , ruleResults = []
    }

noSubproofToCloseError : String
noSubproofToCloseError = "Rule should close subproof, but there is no subproof to close"

subproofBounds : RoseTree (Int, Int) -> (Int, Int)
subproofBounds sub =
    case sub of
        Leaf bs -> bs
        Branch bs _ -> bs

addSubproof : (Int, Int) -> SubProofs -> SubProofs
addSubproof bounds subs =
    let inBounds bs =
            first bounds < first bs && second bounds > second bs
    in
    case listSpan (subproofBounds >> inBounds) subs of
        (children, notChildren) ->
            (case children of
                [] -> Leaf bounds
                _ :: _ -> Branch bounds children)
            :: notChildren


ruleStartsSubproof : RuleInfo -> Bool
ruleStartsSubproof r =
    case r.ruleAction of
        StartsSubProof _ -> True
        _ -> False

ruleStartsSubproofNewLevel : RuleInfo -> Bool
ruleStartsSubproofNewLevel r =
  case r.ruleAction of
    StartsSubProof True -> True
    StartsSubProof _ -> False
    NoAction -> False
    EndsSubProof -> False
    Axiom -> False

init : SystemInfo -> RawProofInfo -> ( Model, Cmd Msg )
init system proof =
    let formulasMap = Dict.fromList proof.formulas
        getFormula fId = Dict.get fId formulasMap |> Maybe.withDefault "..."
        rules = system.rulesInfo
        defaultRule = -- default first rule: first one that starts a new subproof
            let filteredRules =
                    if Array.isEmpty nodesArray
                        && List.any ruleStartsSubproofNewLevel rules
                    then
                        List.filter
                            ruleStartsSubproof
                            rules
                    else rules
            in
                case filteredRules of
                    [] -> unknownRule
                    r::_ -> r
        defaultRuleArgumentNames = defaultRule.argumentNames
        nodes = proof.nodesInfo
        nodesMap = Dict.fromList (List.map (\n -> (n.proofNodeId, n)) nodes)
        sorted = List.sortWith (\n m -> compare n.proofNodeId m.proofNodeId) nodes
        rulesMap = Dict.fromList (List.map (\r -> (r.ruleName, r)) rules)
        getRule r = dictGet unknownRule r rulesMap
        nodesArray =
          let ruleApps = sorted
                    |> List.map
                          (\n ->
                           { derives = n.derives
                           , node = n.proofNodeId
                           , rule = getRule n.ruleName
                           , arguments = []
                           }
                           )
                    |> Array.fromList
              -- assumptions = List.map (\ass -> {derives = ass.formula, node = ass.id, rule = getRule "Assumption", arguments = []}) proof.initialAssumptions
              --             |> Array.fromList
          in ruleApps
        collectSubproofs currentIx open sps =
            case Array.get currentIx nodesArray of
                Just node ->
                    case node.rule.ruleAction of
                        StartsSubProof True -> collectSubproofs (currentIx + 1) (currentIx :: open) sps
                        StartsSubProof False ->
                            collectSubproofs (currentIx + 1) open sps
                            -- case open of
                            --     [] -> collectSubproofs (currentIx + 1) [currentIx] sps
                            --     startIx :: others ->
                            --         collectSubproofs (currentIx + 1) (currentIx :: others)
                            --             <| addSubproof (startIx, currentIx - 1) sps
                        Axiom -> collectSubproofs (currentIx + 1) open sps
                        NoAction ->
                            collectSubproofs (currentIx + 1) open sps
                        EndsSubProof ->
                            case open of
                                [] -> Nothing
                                stIx :: others ->
                                    collectSubproofs (currentIx + 1) others
                                        <| addSubproof (stIx, currentIx - 1) sps
                Nothing -> Just (open, sps)
        afterPremisesIndex ix =
            case Array.get ix nodesArray of
                Just node ->
                    case List.filter (\ass -> ass.id == node.node) proof.initialAssumptions of
                        [] -> ix
                        _ -> afterPremisesIndex (ix + 1)
                Nothing -> ix
        (openSubproofs, subproofs, mSubproofsError) =
            case collectSubproofs (afterPremisesIndex 0) [] [] of
                Just (a, b) -> (a, b, Nothing)
                Nothing -> ([], [], Just noSubproofToCloseError)
        errors = Set.empty
    in
    ( { formulas = formulasMap
      , nodes = nodesArray
      , nodeIndex = Dict.fromList <| List.indexedMap (\ix n -> (n.node, ix)) <| Array.toList nodesArray
      , subproofs = subproofs
      , openSubproofs = openSubproofs
      , rule = defaultRule
      , argumentInput = ""
      , formulaArgumentsInput = initializeFormulaArgumentArray defaultRule
      , disabledInput = False
      -- Whether proof is already complete. We assume it's not, but
      -- if it is then the user can still press the check proof
      -- button to confirm
      , done = False
      , helpView = NoHelp
      , errors = maybe errors (\e -> addError e errors) mSubproofsError
      , proofId = proof.id
      , mLanguageId = proof.mLanguage
      , initialAssumptions = List.map (\ass -> getFormula ass.formula) proof.initialAssumptions
      , goal =
        case proof.goals of
          (_, goalFormulaId) :: _ -> (getFormula goalFormulaId, goalFormulaId)
          _ -> ("...", -1)
      , system = system
      }
    , Cmd.none
    )

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

initializeFormulaArgumentArray : RuleInfo -> Array Formula
initializeFormulaArgumentArray rule =
    Array.repeat (List.length rule.formulaArgumentNames) ""

getNode : Int -> Array Node -> SubProofs -> Maybe Node
getNode nodeIx nodes subproofs =
    let inSubproof sub =
            let (sttIx, endIx) = subproofBounds sub
            in nodeIx > sttIx && nodeIx <= endIx
    in
        if List.any inSubproof subproofs
        then Nothing
        else Array.get nodeIx nodes

update : SharedState -> Nav.Key -> Msg -> Model -> ( Model, Cmd Msg )
update state navKey msg model =
    let getFormulas formulaIds = User.makeGetFormulas GotError GotFormulas state.auth model.formulas formulaIds
        nodes = model.nodes
    in
    case msg of
        PickRule ruleName ->
            let rule = Maybe.withDefault unknownRule
                       <| find (\r -> if r.ruleName == ruleName then Just r else Nothing) model.system.rulesInfo
            in
                ( { model | rule = rule
                  , argumentInput = ""
                  , formulaArgumentsInput = initializeFormulaArgumentArray rule
                  }
                , Cmd.none
                )

        ChangeArguments argumentInput ->
            ( { model | argumentInput = argumentInput }
            , Cmd.none
            )

        ChangeFormulaArgument ix formulaArgument ->
            ( { model | formulaArgumentsInput = Array.set ix formulaArgument model.formulaArgumentsInput }
            , Cmd.none
            )

        GotFormulas formulas ->
            ( { model | formulas = insertMany formulas model.formulas }
            , Cmd.none
            )

        GoRule rule args formulaArgs ->
            let argIndex strArg = strArg |> String.toInt |> Maybe.map (\n -> n - 1)
                indexArg numArg = String.fromInt (numArg + 1)
                rNodeArgs =
                    let checkArg arg strIndex =
                            case arg of
                                SoleArg argName ->
                                    Result.fromMaybe
                                        (String.concat
                                             [ "Provided value for argument ‘"
                                             , argName
                                             , "’ is ‘"
                                             , strIndex
                                             , "’, but should be an integer indicating a proof line to be used as argument.\nRemember to separate proof line arguments by a comma"])
                                        (argIndex strIndex)
                                            |> Result.andThen
                                               (\ix ->
                                                    case getNode ix nodes model.subproofs of
                                                        Nothing ->
                                                            if isJust (Array.get ix nodes)
                                                            then -- node exists
                                                                Err ("Can't refer to proof line " ++ strIndex
                                                                         ++  ", since its subproof has been closed already")
                                                            else
                                                                Err ("No proof line referable by " ++ strIndex)
                                                        Just node -> Ok [(argName, node)])
                                SubProofArg hypothesisArg rootArg ->
                                    case List.map argIndex <| String.split "-" strIndex of
                                        [Just begIx, Just endIx] ->
                                            let allSubproofs =
                                                  case model.openSubproofs of
                                                    lastSubBegIx :: _ -> addSubproof (lastSubBegIx, Array.length model.nodes - 1) model.subproofs
                                                    _ -> model.subproofs
                                            in
                                            if List.any (\sub -> subproofBounds sub == (begIx, endIx)) allSubproofs
                                            then
                                                case (Array.get begIx nodes, Array.get endIx nodes) of
                                                   (Just begNode, Just endNode) -> Ok [(hypothesisArg, begNode), (rootArg, endNode)]
                                                   (Nothing, _) -> Err ("No node referable by " ++ indexArg begIx)
                                                   (_, Nothing) -> Err ("No node referable by " ++ indexArg endIx)
                                            else
                                              Err <| String.concat ["No subproof range referable by ", strIndex]
                                        _ ->
                                            Err
                                            <| String.concat [ "Expected subproof range argument (e.g., 2-4) for argument ‘"
                                                             , hypothesisArg, "-", rootArg
                                                             , "’, but got ‘", strIndex, "’"
                                                             ]
                        nodeArgsOrErr =
                            List.foldr
                                (\(arg, strIndex) acc ->
                                     case acc of
                                         Err err -> Err err
                                         Ok argNodes ->
                                             case checkArg arg strIndex of
                                                 Ok moreArgs -> Ok (moreArgs ++ argNodes)
                                                 Err err -> Err err
                                )
                                (Ok [])
                                (List.map2 (\arg strIx -> (arg, String.trim strIx)) rule.argumentNames <| String.split "," args)
                    in nodeArgsOrErr
                        |> Result.andThen
                           (\nodeArgs ->
                                let numberNeededArgs = List.length (ruleArgumentNames rule)
                                    numberProvidedArgs = List.length nodeArgs
                                in
                                if numberNeededArgs == numberProvidedArgs
                                then Ok <| List.map (\(n, node) -> (n, node.node)) nodeArgs
                                else Err (String.join " "
                                             [ "Supplied"
                                             , String.fromInt <| numberProvidedArgs
                                             , "arguments, but should supply"
                                             , String.fromInt <| numberNeededArgs
                                             , "instead"]))
                formulaArgumentsNumber = List.length rule.formulaArgumentNames
                rFormulaArguments =
                    let (_, emptyFormulaArgs) =
                            List.foldr (\fName (ix, empty) ->
                                            (ix + 1, if String.isEmpty <| String.trim (Maybe.withDefault ""
                                                                                           <| Array.get ix formulaArgs)
                                                     then fName :: empty
                                                     else empty))
                                        (0, [])
                                        rule.formulaArgumentNames
                    in if List.isEmpty emptyFormulaArgs
                       then
                           Ok ()
                       else
                           let errFn fName = "‘" ++ fName ++ "’ formula argument is empty"
                           in
                               Err (String.join "\n" (List.map errFn emptyFormulaArgs))
                applyRuleGetResult parsedNodeArgs =
                    Cmd.map (receiveRuleResult rule parsedNodeArgs)
                        <| postGlfRuleBySystemByProofIdByRule
                             (basicAuthString state.auth)
                             model.system.systemName
                             model.proofId
                             rule.ruleName
                             model.mLanguageId
                             parsedNodeArgs
            in
                case rFormulaArguments |> Result.andThen (\_ -> rNodeArgs) of
                    Err error ->
                        ( { model | errors = addError error model.errors }
                        , clearErrorCmd (ClearError error)
                        )
                    Ok nodeArgs ->
                        ( { model | disabledInput = True }
                        , if formulaArgumentsNumber == 0
                          then -- no formula arguments to add to graph
                              applyRuleGetResult nodeArgs
                          else -- must add formulas before applying rule
                              let formulaArgsList = Array.toList formulaArgs
                              in
                              Cmd.map
                                  (receiveFormulaArgumentIdsAndApplyRule formulaArgsList rule nodeArgs)
                                  (postGlfAddFormulasBySystemByProofId
                                    (basicAuthString state.auth)
                                    model.system.systemName
                                    model.proofId
                                    model.mLanguageId
                                    formulaArgsList)
                        )

        GoRuleAfterAddedArgumentFormulas rule nodeArgs formulaArgsWithIds ->
            ( { model | formulas = insertMany formulaArgsWithIds model.formulas }
            ,  Cmd.map (receiveRuleResult rule nodeArgs)
                <| postGlfRuleBySystemByProofIdByRule
                    (basicAuthString state.auth)
                    model.system.systemName
                    model.proofId
                    rule.ruleName
                    model.mLanguageId
                    ( List.map2 (\n (fId, _) -> (n, fId)) rule.formulaArgumentNames formulaArgsWithIds
                     ++ nodeArgs)
            )

        GotRuleResult rule args ruleApp ->
                let baseModel =
                        { model | nodes =
                              Array.push
                              { derives = ruleApp.derives
                              , node = ruleApp.node
                              , rule = rule
                              , arguments = args
                              }
                              nodes
                        , argumentInput = ""
                        , formulaArgumentsInput = initializeFormulaArgumentArray model.rule
                        , disabledInput = False
                        , nodeIndex = Dict.insert ruleApp.node (Array.length nodes) model.nodeIndex
                        , errors = noErrors
                        }
                    (updatedSubProofs, updatedOpenSubproofs) =
                       case rule.ruleAction of
                         NoAction -> (baseModel.subproofs, baseModel.openSubproofs)
                         Axiom -> (baseModel.subproofs, baseModel.openSubproofs)
                         StartsSubProof startsNewLevel ->
                           let newSubproofIndex = Array.length nodes
                           in
                           if startsNewLevel
                           then -- starts new open subproof
                             ( baseModel.subproofs, newSubproofIndex :: baseModel.openSubproofs )
                           else -- close open subproof, if any (needed for a rule like disjunction elimination)
                             -- case baseModel.openSubproofs of
                             --   sttIx :: others ->
                             --       ( addSubproof (sttIx, newSubproofIndex - 1) baseModel.subproofs, newSubproofIndex :: others )
                             --   [] ->
                                   ( baseModel.subproofs, baseModel.openSubproofs )
                         EndsSubProof ->
                             case baseModel.openSubproofs of
                                 [] ->
                                     ( addSubproof (0, Array.length nodes - 1) baseModel.subproofs
                                     , []
                                     )
                                 stIx :: others ->
                                     ( addSubproof (stIx, Array.length nodes - 1) baseModel.subproofs
                                     , others
                                     )

                in
                ( { baseModel | subproofs = updatedSubProofs, openSubproofs = updatedOpenSubproofs}
                , Cmd.batch [focusOn NoOp ruleSelectionId, getFormulas (ruleAppFormulas ruleApp)]
                )

        CheckProof ->
            ( model
            , Cmd.map receiveCheck
              <| getGlfCheckByProofId (basicAuthString state.auth) model.proofId
            )

        ProofChecksOut ->
            ( { model | done = True }
            , Cmd.none)

        GetDeductionExport ->
            ( model
            , Cmd.map receiveExport
                <| getGlfExportLatexBySystemByProofId (basicAuthString state.auth) model.system.systemName model.proofId model.mLanguageId
            )

        GotDeductionExport exportedDeduction ->
            ( model
            , downloadExportedDeduction exportedDeduction
            )

        DeleteDeduction ->
            ( model
            , Nav.replaceUrl navKey (Routes.pathFor <| Routes.UserRoute Nothing (Just model.proofId))
            )

        RevertSubProof ix nodeId ->
            ( model
            , Cmd.map (receiveRevertAttempt ix nodeId)
               <| postGlfRevertBySystemByNodeId (basicAuthString state.auth) model.system.systemName nodeId
            )

        RevertedSubProof ix nodeId ->
            (case Array.get ix model.nodes of
                Nothing -> { model | errors = addError "No node to revert" model.errors }
                Just removedNode ->
                    let (subproofs, openSubproofs) =
                            case removedNode.rule.ruleAction of
                                StartsSubProof inNewLevel ->
                                    case model.openSubproofs of
                                        [] -> -- INVARIANT: shouldn't ever happen
                                            (model.subproofs, model.openSubproofs)
                                        sttIx :: otherStartIndices ->
                                            if inNewLevel
                                            then -- just remove the open subproof
                                                (model.subproofs, otherStartIndices)
                                            else -- also remove closed subproof, if any
                                                case model.subproofs of
                                                    [] -> ([], otherStartIndices)
                                                    sub :: otherSubproofs ->
                                                        case sub of
                                                            Leaf bounds ->
                                                                if second bounds == ix - 1
                                                                then -- closed by the removed node
                                                                    (otherSubproofs, first bounds :: otherStartIndices)
                                                                else (sub :: otherSubproofs, otherStartIndices)
                                                            Branch bounds children ->
                                                                if second bounds == ix - 1
                                                                then -- closed by the removed node
                                                                    (children ++ otherSubproofs, first bounds :: otherStartIndices)
                                                                else
                                                                    (sub :: otherSubproofs, otherStartIndices)
                                NoAction -> (model.subproofs, model.openSubproofs)
                                Axiom -> (model.subproofs, model.openSubproofs)
                                EndsSubProof ->
                                    case model.subproofs of
                                        [] -> -- INVARIANT: shouldn't ever happen
                                            (model.subproofs, model.openSubproofs)
                                        sub :: otherSubproofs ->
                                            case sub of
                                                Leaf bounds -> (otherSubproofs, first bounds :: model.openSubproofs)
                                                Branch bounds children -> (children ++ otherSubproofs, first bounds :: model.openSubproofs)
                    in
                    { model | nodes = Array.slice 0 (Array.length model.nodes - 1) model.nodes
                    , nodeIndex = Dict.remove nodeId model.nodeIndex
                    , subproofs = subproofs
                    , openSubproofs = openSubproofs
                    }
            , Cmd.none
            )

        PickHelp helpView ->
            ( { model | helpView = helpView }
            , Cmd.none
            )

        NoOp -> ( model, Cmd.none )

        GotError error ->
            ( { model | errors = addError error model.errors
              , disabledInput = False
              }
            , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError error)]
            )

        ClearError error ->
            ( { model | errors = clearError error model.errors }
            , Cmd.none
            )

receive = receiveFunction GotError

receiveRevertAttempt ix nodeId = receive (\_ -> RevertedSubProof ix nodeId)

receiveCheck =
    receive (\r ->
              case r of
                Ok _ -> ProofChecksOut
                Err err -> GotError err
            )

receiveExport =
    receive (\r ->
                 case r of
                     Ok e -> GotDeductionExport e
                     Err error -> GotError error
            )


receiveRuleResult rule nodeArgs =
    receive (\r ->
                 case r of
                     Err err -> GotError err
                     Ok ruleApp -> GotRuleResult rule nodeArgs ruleApp)

receiveFormulaArgumentIdsAndApplyRule formulaArguments rule args =
    receive (\r ->
                 case r of
                     Err err -> GotError err
                     Ok addedFormulaIds -> GoRuleAfterAddedArgumentFormulas rule args (zip addedFormulaIds formulaArguments)
            )

checkProofButton : msg -> Bool -> Html msg
checkProofButton msg isDisabled =
    button [ type_ "button"
           , class "checkProofButton"
           , onClick msg
           , disabled isDisabled
           ]
    [Html.text "Check proof"]

view : Model -> Html Msg
view model =
    let nodes = model.nodes
        viewRuleApplication ix node =
            li [class "ruleApplication", class "nobullet", classList [("startsSubProof", ruleStartsSubproof node.rule)]]
                [ span [class "ruleApplicationIndex"] [ text (String.fromInt <| ix + 1) ]
                , span [class "ruleApplicationDerives"] [ viewFormula model.formulas node.derives ]
                , span [class "ruleApplicationName"]
                    [ text node.rule.ruleName
                    , case List.filterMap (\(k, n) ->
                                               maybe Nothing
                                                   (\l -> Just (k, String.fromInt (l + 1)))
                                               <| Dict.get n model.nodeIndex)
                        node.arguments of
                          [] -> span [class "ignore"] []
                          args -> viewRecord [class "ruleApplicationArguments"] (List.map (mapSecond text) args)
                    ]
                , if ix == Array.length nodes - 1
                    && not model.done
                    -- very crude way to disallow reversal of initial assumptions, which should not be modified.
                    -- (user can always delete the deduction and start a new one if they made the wrong assumption.)
                    && ix > List.length model.initialAssumptions
                  then -- last node which is not an initial assumption may be reverted
                      revertProofButton [ type_ "button"
                                        , class "interaction revertProofButton"
                                        , onClick (RevertSubProof ix node.node)
                                        ]
                  else span [] []
                ]
        viewNodes node r =
            case node.rule.ruleAction of
                NoAction ->
                    { r | ix = r.ix + 1
                    , curr = Array.push (viewRuleApplication r.ix node) r.curr
                    }
                Axiom ->
                    { r | ix = r.ix + 1
                    , curr = Array.push (viewRuleApplication r.ix node) r.curr
                    }
                StartsSubProof inNewLevel ->
                    if inNewLevel
                    then
                        { r | ix = r.ix + 1
                        , curr = Array.fromList [ viewRuleApplication r.ix node ]
                        , stack = r.curr :: r.stack
                        }
                    else
                        { r | ix = r.ix + 1
                        , curr = Array.push (viewRuleApplication r.ix node) r.curr
                        }
                EndsSubProof ->
                    case r.stack of
                        [] ->
                            { r | ix = r.ix + 1
                            , curr = Array.push (viewRuleApplication r.ix node) r.curr
                            }
                        x::xs ->
                            { r | ix = r.ix + 1
                            , curr = Array.push (viewRuleApplication r.ix node)
                                     <| Array.push (ul [class "subproof"] (Array.toList r.curr)) x
                            , stack = xs
                            }
        proofView = Array.foldl viewNodes {ix = 0, curr = Array.empty, stack = []} nodes
        -- barely does anything when the proof is complete
        flattenedProofView = List.foldl (\x elems -> (Array.toList x) ++ [ul [] elems]) (Array.toList proofView.curr) proofView.stack
    in
        div []
            [ Html.ul [id "deduction-meta", class "horizontal-list"]
                  [ li [] [helpButton PickHelp model.helpView]
                  , li [] [backToSystemsLink]
                  , li [] [deleteDeductionLink model.proofId]
                  , li [] [exportButton GetDeductionExport (not model.done)]
                  ]
            , viewHelp
                  NoOp
                  PickHelp
                  (ul []
                       [ li [] [text "Proofs start with special rules that require no arguments and introduce either assumptions or axioms or tautologies"]
                       , li [] [text "Be careful: assumptions, axioms, and tautologies are unchecked; GLF accepts them unconditionally"]
                       , li [] [text "Assumptions must be discarded to complete the proof, which is done with the special Q.E.D. rule"]
                       , li [] [text "Most rules take arguments as proof lines, which are specified either by their index or by a range of indices (separated by a hyphen)"]
                       , li [] [text "Argument names are shown between curly braces in the Rules section of the Help menu"]
                       , li [] [text "Depending on the rule, the user may need to specify formula arguments"]
                       ])
                  []
                  model.system
                  model.helpView
            , div [class "deduction", attribute "data-over" (if model.done then "true" else "false")]
                [ ul []
                      [ li [class "nobullet", class "flex", class "bold"]
                           [ viewProposition model.initialAssumptions [Tuple.first model.goal] ]
                      , li [class "nobullet"]
                            [ul [class "proof"] flattenedProofView]
                      , li [class "nobullet"]
                          [ div [class "interaction"]
                                [ Html.text "Rule:"
                                , selectRules
                                    model.disabledInput
                                    (Just ruleSelectionId)
                                    PickRule
                                    (Just model.rule.ruleName)
                                    model.system.rulesInfo
                                , case model.rule.argumentNames of
                                      a::argNames ->
                                          let viewArg arg =
                                                  case arg of
                                                      SoleArg argN -> argN
                                                      SubProofArg h argN -> h ++ "-" ++ argN
                                          in
                                          inputText model.disabledInput
                                              (String.concat ["Arguments (", String.join ", " (List.map viewArg <| a::argNames), "):"])
                                              "ruleNodeArguments"
                                              "Proof lines"
                                              ChangeArguments
                                              model.argumentInput
                                      _ -> span [] []
                                , selectManualArguments ChangeFormulaArgument model.disabledInput model.rule.formulaArgumentNames model.formulaArgumentsInput
                                , applyRuleButton (GoRule model.rule model.argumentInput model.formulaArgumentsInput) model.disabledInput
                                , let openProofs = not <| List.isEmpty model.openSubproofs
                                      derivesGoal = Array.get (Array.length nodes - 1) nodes
                                        |> (\n -> Maybe.map .derives n == Just (Tuple.second model.goal))
                                  in checkProofButton CheckProof (openProofs || not derivesGoal)
                                ]
                          ]
                      , li [class "nobullet"]
                          [ viewErrors model.errors ]
                      ]
                ]
            ]


inputText : Bool -> String -> String -> String -> (String -> msg) -> String -> Html msg
inputText isDisabled labelText iden placeholderStr msg text =
    div [class "inputText"]
        [ label [for iden]
             [Html.text labelText]
        , input [type_ "text", id iden, onInput msg, value text, placeholder placeholderStr, disabled isDisabled] []
        ]

viewFormula = User.viewFormula
