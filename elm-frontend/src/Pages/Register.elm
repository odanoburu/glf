module Pages.Register exposing (Model, Msg(..), init, subscriptions, update, view)

import Api exposing (..)
import Base exposing (..)
import Pages.Home exposing (..)
import Pages.Login exposing (..)
import Routes exposing (RegisterAs(..), pathFor, Route(..))

import Random as Random
import Browser.Dom as DOM
import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes as Attr
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Decode
import Set exposing (Set)
import Task


type alias Model =
  { isGuest : Bool
  , username : String
  , password : String
  , passwordAgain : String
  , email : String
  , humanCheck1 : String
  , humanCheck2 : String
  , errors : Errors
  }

type Msg
    = NoOp
    | Username String
    | Password String
    | PasswordAgain String
    | Email String
    | HumanCheck1 String
    | HumanCheck2 String
    | RegisterAttempt
    | RegisterFailed String
    | GeneratedRandomAuthInfo AuthInfo
    | RegistrationSuccessful AuthInfo
    | RegistrationFailed String
    | SetRegistrationAs Bool
    | ClearError String

init : RegisterAs -> ( Model, Cmd Msg )
init registerAs =
    let isGuest =
            case registerAs of
                AsGuest -> True
                AsRegisteredUser -> False
    in
    ( { isGuest = isGuest
      , username = ""
      , password = ""
      , passwordAgain = ""
      , email = ""
      , humanCheck1 = "0"
      , humanCheck2 = ""
      , errors = Set.empty
      }
    , focusOn NoOp (if isGuest then rangeCheckId else glfUsernameInputId)
    )

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

--- UPDATE

mismatchedPasswordsError : String
mismatchedPasswordsError = "Passwords must be the same"

update : Nav.Key -> Msg -> Model -> ( Model, Cmd Msg )
update navKey msg model =
    case msg of
        SetRegistrationAs isGuest ->
            ( { model | isGuest = isGuest }
            , focusOn NoOp (if isGuest then rangeCheckId else glfUsernameInputId)
            )

        Username username ->
            ( { model | username = username }, Cmd.none )

        Password password ->
            let errors =
                    if password == model.passwordAgain
                    then
                        clearError mismatchedPasswordsError model.errors
                    else
                        addError mismatchedPasswordsError model.errors
            in
                ({ model
                     | password = password
                     , errors = errors
                 }
                , Cmd.none
                )

        PasswordAgain passwordAgain ->
            let errors =
                    if model.password == passwordAgain
                    then clearError mismatchedPasswordsError model.errors
                    else addError mismatchedPasswordsError model.errors
            in
                ( { model
                      | passwordAgain = passwordAgain
                      , errors = errors
                  }
                , Cmd.none
                )

        Email email ->  ({ model | email = email }, Cmd.none)

        HumanCheck1 i -> ({ model | humanCheck1 = i }, Cmd.none)

        HumanCheck2 s -> ({ model | humanCheck2 = s }, Cmd.none)

        RegisterAttempt -> checkAndRegister model

        RegisterFailed error ->
            ( { model | errors = addError error model.errors}
            , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError error)]
            )

        GeneratedRandomAuthInfo authInfo ->
            ( model
            , attemptRegistration
                  ( True
                  , { user = authInfo.id
                    , pass = authInfo.password
                    , salt = ""
                    , email = "" -- optional
                    }
                  )
            )

        RegistrationFailed error ->
            ( { model | errors = addError error model.errors}
            , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError error)]
            )

        RegistrationSuccessful authInfo ->
            ( model
            , Cmd.batch [ setAuthInfo <| Just authInfo
                        , Nav.reload
                        ]

            )

        ClearError error ->
            ( { model | errors = clearError error model.errors }
            , Cmd.none
            )

        NoOp -> ( model, Cmd.none )


checkAndRegister : Model -> ( Model, Cmd Msg )
checkAndRegister model =
    if model.humanCheck1 == "100" && model.humanCheck2 == "" && model.password == model.passwordAgain
    -- NOTE: humans will slide as asked, and bots will fill up the
    -- invisible form item, which humans will ignore
    then
        ( { model | errors = Set.empty}
        , if model.isGuest
          then
              Cmd.map GeneratedRandomAuthInfo generateRandomAuthInfo
          else
              let user =
                      { user = model.username
                      , email = model.email
                      , pass = model.password
                      , salt = ""
                      }
              in
                  attemptRegistration (False, user)
        )
    -- probably a bot, do nothing
    else
        let slideError = "Remember to slide all the way to the right"
        in
            ( { model
                    -- no need to report password error because it's already there
                  | errors = addError slideError model.errors }
            , Cmd.batch [scrollToErrorCmd NoOp, clearErrorCmd (ClearError slideError)]
            )

attemptRegistration : (Bool, User) -> Cmd Msg
attemptRegistration (isGuest, user) =
    let
        authInfo = { id = user.user, password = user.pass }
        registrationAttemptCmd = Api.postSignUpRequest (isGuest, user)
    in Cmd.map (registrationResult authInfo) registrationAttemptCmd

registrationResult authInfo r =
    case r of
        Err (httpError, mInfo) -> RegistrationFailed <| httpErrorString "Registration failed" httpError mInfo
        Ok (Err msg) -> RegistrationFailed msg
        Ok _ -> RegistrationSuccessful authInfo

{-| Generate a random character within a certain keyCode range
    lowerCaseLetter =
        char 65 90
-}
char : Int -> Int -> Random.Generator Char
char start end =
    Random.map Char.fromCode (Random.int start end)

{-| Generate a random string of a given length with a given character generator
    fiveLetterEnglishWord =
        string 5 Random.Char.english
-}
string : Int -> Random.Generator Char -> Random.Generator String
string stringLength charGenerator =
    Random.map String.fromList (Random.list stringLength charGenerator)

generateRandomAuthInfo : Cmd AuthInfo
generateRandomAuthInfo =
    let asciiGenerator = char 65 122 -- from A to z
        stringSize = 50 -- arbitrary choice
        usernameSize = 25 -- arbitrary choice
        makeAuthInfo randomStr =
            let id = String.append "temporaryUser=" <| String.right usernameSize randomStr
                password = String.left (stringSize - usernameSize) randomStr
            in
                { id = id
                , password = password
                }
    in Random.generate makeAuthInfo (string stringSize asciiGenerator)

rangeCheckId : String
rangeCheckId = "rangeCheck"

--- VIEW

view : Model -> Html Msg
view model =
    let formId = "registrationForm"
        hiddenIfGuest = classList [("noDisplay", model.isGuest)]
    in
    div []
        [ div [class "subpage"]
          [ glfLogo []
          , h3 []
              [ Html.text
                    (if model.isGuest
                         -- just check if it's a bot (to the best of
                         -- our ability and budget)
                     then "Are you a human?"
                     else "Registration")
              ]
          , Html.form [ id formId ] []
          , viewInput "text" "Username" model.username Username [Attr.form formId, id glfUsernameInputId, hiddenIfGuest]
          , viewInput "email" "(optional) Email" model.email Email [Attr.form formId, title "An email is the only way to reset a lost password.", hiddenIfGuest]
          , viewInput "password" "Password" model.password Password [Attr.form formId, hiddenIfGuest, id glfPasswordInputId]
          , viewInput "password" "Confirm password" model.passwordAgain PasswordAgain [Attr.form formId, hiddenIfGuest, id "glfConfirmPassword"]
          , span []
              [ label [for rangeCheckId]
                    [text "Please slide until the end:"]
              ]
          , input [ id rangeCheckId
                  , type_ "range"
                  , value model.humanCheck1
                  , Attr.min "0"
                  , Attr.max "100"
                  , step "0.01"
                  , onInput HumanCheck1
                  , Attr.form formId
                  ] []
          , viewInput "text" "Leave this blank if you are a human" model.humanCheck2 HumanCheck2 [attribute "class" "humanCheck2", Attr.form formId]
          , if model.isGuest
            then div [ class "subpage" ]
                [ strong [] [ text "Notice: " ]
                , p []
                    [ text "As a non-registered user your work is liable to deletion after 30 minutes. "
                    , text "If you would like to save your progress, "
                    , a (onClickAnchor (SetRegistrationAs (not model.isGuest)) Routes.registerPath)
                        [ text "register as an user" ]
                    , text " (no personal information is required)."
                    ]
                ]
            else span [] []
          , if model.isGuest
            then
                makeButton RegisterAttempt "Done" [Attr.form formId, id "doneButton"]
            else
                registerButton RegisterAttempt [Attr.form formId]
          ]
        , viewErrors model.errors
        ]

onClickAnchor : msg -> String -> List (Attribute msg)
onClickAnchor msg url =
    [ Html.Events.custom "click" <| Decode.succeed { message = msg, stopPropagation = True, preventDefault = True }
    , href url
    ]

registerButton : msg -> List (Attribute msg) -> Html msg
registerButton msg attrs = makeButton msg "Register" attrs
