port module Base exposing (AuthInfo, Flags, SharedState
                     , setAuthInfo
                     , httpErrorString, receiveFunction, glfLogo
                     -- utilities
                     , basicAuthString, find, insertMany, removeMany, zip, maybe, isJust, safeIndex, groupBy
                     , assoc, removeFirst, cmdAfter, paragraph, resetViewport, focusOn, listSpan, arrayMapCombine, arrayGet, dictGet
                     )

import Base64
import Html exposing (..)
import Html.Attributes exposing (..)
import Browser.Dom as Dom
import Http
import Task
import Process
import Array exposing (Array)
import Dict as Dict exposing (Dict)

type alias SharedState = { auth : AuthInfo }

type alias AuthInfo = { id : String, password : String }

type alias Flags = { auth : Maybe AuthInfo }

-- PORTS
port setAuthInfo : Maybe AuthInfo -> Cmd msg
---

glfLogo : List (Attribute msg) -> Html msg
glfLogo extraAttrs =
    object ([type_ "image/svg+xml", attribute "data" "/assets/icon.svg", id "logo", attribute "aria-label" "GLF logo"] ++ extraAttrs) [ ]

basicAuthString : AuthInfo -> String
basicAuthString user = "Basic "
                 ++ Base64.encode (user.id ++ ":" ++ user.password)

httpErrorString : String
                -> Http.Error
                -> Maybe { metadata : Http.Metadata, body : String }
                -> String
httpErrorString prefix err mInfo =
    (\msg -> String.join "\n" [prefix, msg])
            <| String.join " "
                <| case err of
                       Http.BadUrl url -> ["Bad url", url]
                       Http.Timeout -> ["Request timeout"]
                       Http.NetworkError -> ["Network error\n\nAre you connected to the internet?\nCould the server be offline?"]
                       Http.BadStatus status -> ["Bad request status:", String.fromInt status, maybe "" (\{metadata} -> metadata.statusText) mInfo]
                       Http.BadBody bodyErr -> ["Bad request body:", bodyErr]

receiveFunction : (String -> msg) -> (a -> msg) -> Result (Http.Error, Maybe {body : String, metadata : Http.Metadata}) a -> msg
receiveFunction err f r =
    case r of
        Err (httpErr, mInfo) -> err <| httpErrorString "Request failed" httpErr mInfo
        Ok a -> f a


--- UTILITIES
maybe : b -> (a -> b) -> Maybe a -> b
maybe def f m =
    case m of
        Nothing -> def
        Just x -> f x

insertMany : List (comparable, a) -> Dict comparable a -> Dict comparable a
insertMany xs d = Dict.union d (Dict.fromList xs)

removeMany : List comparable -> Dict comparable a -> Dict comparable a
removeMany ks d = List.foldl Dict.remove d ks

dictGet : v -> comparable -> Dict comparable v -> v
dictGet def k dict = Dict.get k dict |> Maybe.withDefault def

isJust : Maybe a -> Bool
isJust m =
    case m of
        Just _ -> True
        Nothing -> False

safeIndex : List a -> Int -> Maybe a
safeIndex xs ix =
    case (xs, ix) of
        ([], 0) -> Nothing
        (x::_, 0) -> Just x
        (_::ys, _) -> safeIndex ys (ix - 1)
        ([], _) -> Nothing

find : (a -> Maybe b) -> List a -> Maybe b
find f xs =
    case xs of
        [] -> Nothing
        y :: ys ->
            case f y of
                Just x -> Just x
                Nothing -> find f ys

zip : List a -> List b -> List (a, b)
zip = List.map2 Tuple.pair

groupBy : (a -> comparable) -> List a -> List (comparable, List a)
groupBy f xs =
    let go last cur acc ys =
            case ys of
                [] ->
                    (last, List.reverse cur) :: acc
                y::yss ->
                    let fy = f y
                    in
                        if fy == last
                        then
                            go last (y::cur) acc yss
                        else
                            go fy [y] ((last, List.reverse cur) :: acc) yss
    in
        case List.sortBy f xs of
            [] -> []
            [x] -> [(f x, [x])]
            x::y::zs ->
                let fx = f x
                in List.reverse <| go fx [x] [] (y::zs)

removeFirst : a -> List a -> List a
removeFirst y xs =
    case xs of
        [] -> []
        x :: xss -> if x == y then xss else x :: removeFirst y xss

assoc : a -> List (a, b) -> Maybe b
assoc needle assocs =
    case assocs of
        (hay, val) :: rest ->
            if needle == hay
            then Just val
            else assoc needle rest
        _ -> Nothing

cmdAfter : Float -> (Result x () -> msg) -> Cmd msg
cmdAfter t toMsg =
    Task.attempt toMsg (Process.sleep t)

unlines : List String -> String
unlines = String.join "\n"

paragraph : List String -> Html msg
paragraph paraLines = p [] [text (unlines paraLines)]

resetViewport : msg -> Cmd msg
resetViewport msg =
    Task.perform (\_ -> msg) (Dom.setViewport 0 0)

focusOn : msg -> String -> Cmd msg
focusOn msg id =
    Task.attempt (\_ -> msg)
        <| Dom.focus id

listSpan : (a -> Bool) -> List a -> (List a, List a)
-- like in Haskell's Data.List:
--- 'span' @p xs@ is equivalent to @('takeWhile' p xs, 'dropWhile' p xs)@
listSpan p elems =
    let go xs =
            case xs of
                [] -> (xs, xs)
                y :: ys ->
                    if p y
                    then
                        case go ys of
                            (yess, nos) -> (y :: yess, nos)
                    else
                        ([], xs)
    in
        go elems

arrayMapCombine : (a -> Maybe b) -> Array a -> Maybe (Array b)
arrayMapCombine f xs =
     Array.foldl
         (\x mys ->
            Maybe.andThen
              (\ys ->
                   case f x of
                     Nothing -> Nothing
                     Just y -> Just <| Array.push y ys)
              mys)
        (Just Array.empty)
        xs

arrayGet : v -> Int -> Array v -> v
arrayGet def ix array = Array.get ix array |> Maybe.withDefault def
