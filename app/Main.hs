{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where


import Options.Declarative

--import GLF
import GLF.Base
import GLF.Server
import GLF.GraphBackend (initializeEnv)
import GLF.Logic (defaultSystems)


main :: IO ()
main = run "glf" Nothing $
    Group "Labeled Logical Framework"
    [ subCmd "generate-client-api" generateClientApi
    , subCmd "serve" serve
    ]


serve :: Flag ""  '["config"] "DIRECTORY" "Path to directory where configuration files are" (Maybe FilePath)
      -> Flag "p" '["port"] "PORT" "Bind to this port" (Maybe Int)
      -> Flag ""  '["log-level"] "n" "Level of logging details (1-3, from less detailed to more detailed)" (Maybe Int)
      -> Cmd " Start server listening on PORT (make a request for docs/ to see documentation)" ()
serve configDir cliPort cliLogLevel = do
  cfg <- liftIO $ readConfig (get configDir)
  let cfg'' = maybe cfg (\port -> cfg{port = port}) $ get cliPort
      cfg'  = maybe cfg'' (\logLevel -> cfg{logLevel = if logLevel < 1 then None else if logLevel > 2 then Dev else Prod}) $ get cliLogLevel
  env <- liftIO $ initializeEnv cfg defaultSystems
  liftIO $ glfServe env (initializeState env) (logLevel cfg') (port cfg')


generateClientApi :: Flag "o" '["output"] "DIRECTORY" "Directory where files are placed " FilePath -> Cmd " Generate client code to access HTTP API" ()
generateClientApi outDir = liftIO $ generateElm (get outDir)

--   subcommands
--     parseSubCommand
--       ("strategy", Database <$> strategyP, "Attempt proof strategy on input goals"),
--       ("load", Database <$> loadP, "Parse and load deductions. Multiple comma-separated formulas may be provided, the last one is the goal formula, while the others are the assumptions"),
--       ("script", Database <$> scriptP, "Generate or run proof script"),
--       ("latex", Database <$> latexP, "Generate Latex code to typeset proof"),
--       ("debug", Standalone <$> debugP, "Debug GLF")
--     ]
--   where
--     serveP = Serve <$> portP <*> logLevelP
--       where
--         portP =
--           option
--             auto
--             ( metavar "PORT" <> short 'p' <> long "port"
--                 <> help "Port to use"
--                 <> value 2487
--                 <> showDefault
--             )
--         logLevelP =
--           flag None Dev (long "log-devel" <> help "Log requests at development level" <> showDefault)
--             <|> flag None Prod (long "log-prod" <> help "Log requests at production level" <> showDefault)
--     strategyP =
--       StrategyC
--         <$> systemP
--         <*> mLanguageIdP
--         <*> strArgument (metavar "STRATEGY" <> help "Proof strategy to use")
--         <*> some (argument auto (metavar "GOAL" <> help "Goals to attempt proof strategy on"))
--       where
--         mLanguageIdP =
--           option
--           (Just <$> auto)
--           ( metavar "LANGUAGEID" <> long "language"
--             <> help "Non-logical language identifier"
--             <> value Nothing
--           )
--     systemP =
--       options
--         (Map.elems $ unSystems defaultSystems)
--         (Text.unpack . GLF.System.systemName)
--         "Logical system to use"
--         (metavar "SYSTEM" <> short 's' <> long "system")
--     loadP = Load <$> systemP <*> labelP <*> many assumptionP <*> some goalP <*> many definitionP
--       where
--         labelP = strOption (metavar "LABEL" <> long "label" <> help "Deduction label" <> value "")
--         assumptionP =
--           strOption
--             ( metavar "ASSUMPTION" <> short 'a' <> long "assumption"
--                 <> help "Deduction assumption"
--             )
--         goalP = strArgument (metavar "GOALS" <> help "Deduction goal")
--         definitionP = strArgument (metavar "DEFINITIONS" <> help "Language definition (operator name and its arity separated by spaces)")
--     clientP =
--       GenerateClient
--         <$> strOption
--           ( metavar "OUTPUT-DIR" <> short 'o' <> long "output"
--               <> help "Path to output directory"
--           )
--     scriptP =
--       Script
--         <$> ( (flag True True (short 'e' <> long "execute" <> help "Execute proof script") *> (Execute <$> strOption (metavar "FILE" <> short 'f' <> long "file")))
--                 <|> ( flag False False (long "generate" <> short 'g' <> help "Generate proof script fom complete proof" <> showDefault)
--                         *> ( Generate <$> rootP
--                                <*> switch (long "haskell" <> help "Generate proof script in Haskell syntax instead of the default JSON")
--                            )
--                     )
--             )
--     rootP = option auto (long "root" <> short 'r' <> metavar "ROOT" <> help "Integer ID of proof root")
--     latexP = Latex <$> rootP
--     debugP = pure Debug
--  config@Config {glf = GLFConfig {backend}} <- readConfig (mConfigPath args)
      -- runAppWithConfig config' defaultSystems $ do
      --   env <- Reader.ask
      --   st <- State.get
      --   case selectedBackend of
      --     BoltB -> lift (goDB @Bolt env st databaseCmd)
      --     RedisB -> lift (goDB @RedisGraph env st databaseCmd)
  -- where
  --   go :: Config -> StandaloneCmd -> IO ()
  --   go _cfg = \case
  --     GenerateClient outDir -> liftIO $ generateElm outDir
  --     Debug -> liftIO printSystems
  --       where
  --         printSystems = mapM_ printSystem (Map.elems $ unSystems defaultSystems)
  --           where
  --             printSystem System {systemName, rules} =
  --               putStrLn ("(System " ++ Text.unpack systemName ++ "\n  (rules:\n")
  --                 *> mapM_ printRule rules
  --                 <* putStrLn "))"
  --             printRule CompiledRule {ruleName = PolyName {unicode}, arguments, query = (formulaArgNames, queryText)} =
  --               putStrLn ("    (Rule " ++ Text.unpack unicode)
  --                 *> (putStrLn "      (Args\n" *> putArguments arguments <* putStrLn ")")
  --                 *> (putStrLn "      (FormulaArgs\n" *> putStrLn (show formulaArgNames) <* putStrLn ")")
  --                 *> Text.IO.putStrLn queryText
  --                 <* putStrLn ")"
  --               where
  --                 putArgCheckQueries argsInfos =
  --                   putStrLn "        (ArgChecking"
  --                     <* mapM (\ArgumentInfo{argumentName, specification} -> Text.IO.putStrLn
  --                               $ Text.concat [argumentName, ": ", showFormulaSpec specification]) argsInfos
  --                     <* putStrLn ")"
  --                 putArguments (Query args argQuery) =
  --                   putStrLn "        (ArgCollectingQuery"
  --                     *> Text.IO.putStrLn argQuery
  --                     <* putStrLn ")"
  --                     <* putArgCheckQueries args
  --                 putArguments (UserInput formulaArgs args) =
  --                   putStrLn "(Interactive-arguments"
  --                     *> Text.IO.putStrLn (Text.intercalate ", " $ fmap argumentName (concatMap nodeArgumentInfos args ++ formulaArgs))
  --                     <* putStrLn ")"
  --                     <* putArgCheckQueries (concatMap nodeArgumentInfos args ++ formulaArgs)
  --                 putArguments NoArguments = return ()
  --   goDB :: forall c. CypherBackend c => Env -> AppState -> DatabaseCmd -> c ()
  --   goDB env@Env {systems} AppState{parsers} databaseCmd =
  --     case databaseCmd of
  --       StrategyC sys@System {systemName, strategies} mLangId stgName goals -> do
  --         case lookupParser systemName mLangId parsers of
  --           Nothing -> liftIO . putStrLn $ "Specified language not found."
  --           Just parser ->
  --             case lookup stgName strategies of
  --               Nothing -> liftIO . putStrLn $ "Specified proof strategy not found."
  --               Just st -> do
  --                 -- TODO: check that proof is in the same system as
  --                 -- the strategy specified
  --                 r <- runStrategyGoals (SystemWithParser (sys, parser)) "" st Nothing goals
  --                 liftIO $ print r
  --       Serve port logLevel -> liftIO $ glfServe @c env (initializeState env) logLevel port
  --       Load sys@System {systemName} label assumptionsInput goalsInput languageInput -> do
  --         parserAndMLangIdOr <- initializeLanguageParser' "" sys languageInput
  --         case parserAndMLangIdOr of
  --           Left err -> fail err
  --           Right (parser, mLangId) -> do
  --             r <-
  --               loadRootFormula'
  --                 (SystemWithParser (sys, parser))
  --                 mLangId
  --                 ""
  --                 deductionInfoStub
  --                 { systemName = systemName,
  --                   label = Text.pack label,
  --                   assumptionsInput = assumptionsInput,
  --                   languageInput = languageInput,
  --                   goalsInput = goalsInput
  --                 }
  --                 mempty
  --             case r of
  --               Left loadError -> fail loadError -- report error
  --               Right (goalId, _, DeductionRootRawInfo {root}) ->
  --                 liftIO printLoad
  --                 where
  --                   printLoad = logMsg $ unwords ["Root node has ID:", show root, "\nGoal node has ID:", show goalId]
  --       Script (Execute fp) -> do
  --         proofscriptOrErr <- liftIO $ JSON.eitherDecodeFileStrict' fp
  --         case proofscriptOrErr of
  --           Left err -> fail err
  --           Right ps@ProofScript {system} ->
  --             case getSystem systems system of
  --               Nothing -> fail $ "Could not find system " ++ Text.unpack system
  --               Just sys ->
  --                 runProofScript sys ps ""
  --                   >>= \case
  --                     Left err -> fail err
  --                     Right root ->
  --                       liftIO . putStrLn $
  --                         "Successfully applied proof script, resulting proof is root at node with ID " ++ show root
  --       Script (Generate root haskellFormat) ->
  --         proofScriptFromProof "" root
  --           >>= \ps ->
  --             liftIO $
  --               if not haskellFormat
  --                 then ByteString.putStr (JSON.encode ps)
  --                 else print ps
  --       Latex root -> do
  --         ps@ProofScript{language, system = systemName} <- proofScriptFromProof "" root
  --         case systems `getSystem` systemName of
  --           Nothing -> fail ("No system named " ++ Text.unpack systemName)
  --           Just sys -> do
  --             languageParserInfoOr <- initializeLanguageParser' "" sys language
  --             case languageParserInfoOr of
  --               Left err -> fail err
  --               Right (logicParser, _mLangId) -> do
  --                 liftIO . Text.IO.putStrLn $ proofScriptToLatex (SystemWithParser (sys, logicParser)) ps
