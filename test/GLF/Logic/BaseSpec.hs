module GLF.Logic.BaseSpec (spec) where

import Data.Either
import GLF.Logic.Base
import GLF.Logic.FOL
import qualified Data.Set as Set
import Test.Hspec

spec :: Spec
spec = do
  describe "* Logic Base" $ do
    describe "** Unification" $ do
      specify "Finds proper substitution, when simple variable change" $
        findSubstitutionWithSubstituendum (Atom "x") (Formula implication [Atom "x", Atom "y"]) (Formula implication [Atom "z", Atom "y"])
        `shouldBe` Set.fromList [Atom "z"]

      specify "Finds proper substitution, when simple variable change, avoiding capture" $
        findSubstitutionWithSubstituendum (Atom "x") (Formula implication [Atom "x", Formula universal [Atom "x", Atom "y"]]) (Formula implication [Atom "z", Formula universal [Atom "x", Atom "y"]])
        `shouldBe` Set.fromList [Atom "z"]

      specify "Finds proper substitution, even without substitutendum information" $
        findSubstitution (Formula implication [Atom "x", Atom "y"]) (Formula implication [Atom "y", Atom "y"])
        `shouldBe` Set.fromList [(Atom "y", Atom "x")]

    describe "** Substitution" $ do
      specify "Substitute free var for fresh var" $
        mSubstituteForIn (Atom "y") (Atom "z") (Formula universal [Atom "x", Formula implication [Atom "x", Atom "z"]]) `shouldSatisfy` isRight

      specify "Trivial substitution (none)" $
        mSubstituteForIn (Atom "w") (Atom "a") (Formula universal [Atom "x", Formula implication [Atom "x", Atom "z"]]) `shouldSatisfy` isRight

      specify "Can't substitute " $
        mSubstituteForIn
          (Atom "z")
          (Atom "y")
          (Formula implication
            [Atom "y",
             Formula universal [Atom "y", Formula implication [Atom "y", Atom "z"]]]) `shouldSatisfy` isRight

      specify "No capture of unbound variable: z->y in (→ y (∀ y (→ y z)))" $
        mSubstituteForIn
          (Atom "y")
          (Atom "z")
          (Formula implication
            [Atom "y",
             Formula universal [Atom "y", Formula implication [Atom "y", Atom "z"]]]) `shouldSatisfy` isLeft

      specify "No capture of unbound variable in more complex term as substitute" $
        mSubstituteForIn
          (Formula implication [Atom "x", Atom "y"])
          (Atom "x")
          (Formula implication
            [Formula implication [Atom "x", Atom "y"],
             Formula universal [Atom "y", Formula implication [Atom "y", Atom "x"]]]) `shouldSatisfy` isLeft
