module GLF.Logic.ParserSpec (spec) where

import Data.Either
import Prelude hiding (pred)
import Test.Hspec

import GLF.System
import GLF.Logic
import GLF.Logic.Base
import GLF.Logic.Parser

testParses :: LogicParser -> [Text] -> Formula -> Expectation
testParses p inputs f = fmap p inputs `shouldSatisfy` all (== Right f)

testNoParse :: LogicParser -> Text -> Expectation
testNoParse p input = p input `shouldSatisfy` isLeft

minimalP, folP :: LogicParser
minimalP = systemWithLanguageParser minimalND []
folP = systemWithLanguageParser folBND [unaryPred, binaryPred, ternaryPred]

ternaryPred, binaryPred, unaryPred :: (Text, Arity)
ternaryPred =  (".T", Fixed 3)
binaryPred = (".B", Fixed 2)
unaryPred = (".U", Fixed 1)

spec :: Spec
spec = describe "* Parser tests" $ do
  it "parses atoms" $
    testParses minimalP ["A"] (atom "A")
  it "parser nullary operators" $
    testParses minimalP ["(⊥)", "⊥"] bottom
  it "parses composite formulas" $
    testParses minimalP ["a -> b | c & ⊥", "a -> (b | (c & ⊥))", "  a ->(b  |(c & ⊥) )  ", "(-> a (| b (& c (⊥))))", " (-> a(| b(&\tc(⊥)))) \t"] connectiveEx0
  it "fails to parse logical operator as prefix" $
    testNoParse minimalP "->(a, b)"
  it "fails to parse nullary operator with parenthesis (infix)" $
    testNoParse minimalP "⊥()"
  it "parses predicates" $
    testParses folP ["(.T x y z)", ".T(x, y, z)"] (pred ternaryPred [var "x", var "y", var "z"])
  it "parses quantifiers 0" $
    testParses folP ["∀(y, .U(y)) "] quantifierEx0
  it "parses quantifiers 1" $
    testParses folP ["∀(y, .U(y)) "] quantifierEx0
  it "parses quantifiers 2" $
    testParses folP ["∀(x, b & .B(x, y))"] $ f universal [var "x", f conjunction [atom "b", pred binaryPred [var "x", var "y"]]]
  it "parses nested quantifiers" $
    testParses folP ["a -> ∀(y, .U(y))"] $ f implication [atom "a", quantifierEx0]
  where
    bottom = f bottomOp []
    connectiveEx0 = f implication [atom "a", f disjunction [atom "b", f conjunction [atom "c", bottom]]]
    quantifierEx0 = f universal [var "y", pred unaryPred [var "y"]]
    var = Atom
    atom = Atom
    f = Formula
    pred predicateInfo = Formula (uncurry nonLogicalOperator predicateInfo)
