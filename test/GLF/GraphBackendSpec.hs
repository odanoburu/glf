{-# LANGUAGE TypeApplications #-}
module GLF.GraphBackendSpec (spec) where

import Data.Either
import Data.Text (Text)
import qualified Data.Map.Strict as Map
import Test.Hspec

import GLF.Base
import GLF.GraphBackend
import GLF.ProofScript
import GLF.Logic
import GLF.System

-- {-# LANGUAGE RankNTypes #-}
-- runTest :: (a -> Expectation) -> (forall c. CypherBackend c => c a) -> Env -> Expectation
-- runTest ex ac env =
--   runApp'
--     env
--     (AppState Map.empty)
--     ( case connPool env of
--         BoltPipes {} -> lift (ac @Bolt )
--         RedisConns {} -> lift (ac @RedisGraph)
--     ) >>= ex

strategy :: System -> Text -> Text -> Env -> Expectation
strategy system stName formulaInput env =
  runApp'
    env
    (AppState Map.empty)
    ( case connPool env of
        BoltPipes {} -> lift (go @Bolt)
        RedisConns {} -> lift (go @RedisGraph)
    )
    >>= ( `shouldSatisfy`
            all
              ( \case
                  Right (Actual _ gs) -> null gs
                  _ -> False
              )
        )
  where
    systemWith = SystemWithParser (system, systemWithLanguageParser system [])
    go :: CypherBackend cypher => cypher [Either TacticFail TacticSuccess]
    go =
      do
        let dedInfo =
              DeductionRootRawInfo
              { root = 0,
                goalsInput = [formulaInput],
                assumptionsInput = [],
                languageInput = [],
                label = mempty,
                systemName = GLF.System.systemName system
              }
        lr <- loadRootFormula' systemWith Nothing "" dedInfo mempty
        case (lr, lookup stName (strategies system)) of
          (Right (goals, _, _), Just st) -> runStrategyGoals systemWith "" st Nothing goals
          _ -> return [Left Total]

scriptRuns :: System -> ProofScript -> Env -> Expectation
scriptRuns system ps env@Env {connPool} =
  runApp'
    env
    (AppState Map.empty)
    ( case connPool of
        BoltPipes {} -> lift (runProofScript @Bolt system ps graph)
        RedisConns {} -> lift (runProofScript @RedisGraph system ps graph)
    )
    >>= (`shouldSatisfy` isRight)
  where
    graph :: GraphName
    graph = mempty

spec :: Spec
spec =
  beforeAll
    ( do
        cfg <- readConfig Nothing
        connPool <- initConnectionPool cfg
        return Env {connPool, systems = defaultSystems}
    )
    ( describe "* Backend tests" $
        do
          describe "** Logical Operators" $ do
            it "backwards implication" $ scriptRuns minimalImplicationalND backwardsImplication

            it "forwards implication" $ scriptRuns minimalImplicationalFitch forwardsImplication

            it "backwards disjunction" $ scriptRuns minimalND backwardsDisjunction

            it "forwards disjunction" $ scriptRuns minimalFitch forwardsDisjunction

            it "backwards conjunction" $ scriptRuns minimalND backwardsConjunction

            it "forwards conjunction" $ scriptRuns minimalFitch forwardsConjunction

            xit "(: x (⊑ (^ A (:: C)) (^ B (:: D)))) ⊢ (: x (⊑ (^ A (:: C)) (^ B (:: D))))" $
              scriptRuns ialcBND inclusionNominal

            xit "(: x (^ (⊓ A B) (:: (∀^ r)))) ⊢ (: x (^ (⊓ A B) (:: (∀^ r))))" $
              scriptRuns ialcBND intersectionNominal

            xit "(: x (^ (⊔ A B) (:: (∃^ r) (∃^ s)))) ⊢ (: x (^ (⊔ B A) (:: (∃^ r) (∃^ s))))" $
              scriptRuns ialcBND unionNominal

            xit "(≼ y x) (: x (^ A (:: B C))) ⊢ (: y (^ A (:: B C)))" $
              scriptRuns ialcBND precedes

            xit "iALC forall> (: y (^ A (:: (∀^ R) (∃^ S) (∃^ Q)))) ⊢ (: y (^ A (:: (∀^ R) (∃^ S) (∃^ Q))))" $
              scriptRuns ialcBND forall

            xit "(: x (^ (∃ R A) (:: (∃^ S) (∃^ Q)))) ⊢ (: x (^ (∃ R A) (:: (∃^ S) (∃^ Q))))" $
              scriptRuns ialcBND exists

            xit "iALC gen> (^ A (:: (∃^ S) (∃^ Q) (∀^ R))), (^ A (:: (∃^ S) (∃^ Q))) ⊢ (^ A (:: (∃^ S) (∃^ Q)))" $
              scriptRuns ialcBND gen

            xit "iALC gen-n> (: y (^ A (:: (∃^ S) (∃^ Q) (∀^ R)))), (: x (^ A (:: ))) ⊢ (: x (^ A (:: )))" $
              scriptRuns ialcBND genNominal

            xit "∀-⊑-mix-i rule test" $
              scriptRuns ialcBND forallInclusion

            it "FOL:Fw: (∀ x (.P x)) ⊢ (∀ u (.P u))" $ scriptRuns folFitch universalTest

            it "FOL:Bw: (∀ x (.P x)) ⊢ (∀ y (.P y))" $ scriptRuns folBND universalTestBND

            it "FOL:Fw: (∃ x (.P x)) ⊢ (∃ y (.P y))" $ scriptRuns folFitch existentialTest

            it "FOL:Bw: (∃ x (.P x)) ⊢ (∃ y (.P y))" $ scriptRuns folBND existentialTestBND

          -- describe "** Supporting functions" $ do
          --   -- the order of tests matters:
          --   --- - the proofs that exist at this time depend on it
          --   -- - the more proofs we have the longer the tests will take
          --   it "returns the proper root information" . runTest (`shouldSatisfy` not . null) $ do
          --     showRoots "" Nothing

          describe "** Scripted Proofs" $ do
            it "Fibonacci M-> formula with n = 3" $ scriptRuns minimalImplicationalND fibo3

            it "(→ (→ p (→ q r)) (→ (→ p q) (→ p r)))" $ scriptRuns minimalImplicationalND lukasiewicz

            it "(→ (→ A (→ B C)) (→ (→ A B) (→ A C)))" $ scriptRuns minimalImplicationalFitch implSimpleFw

            it "(& (→ p q) (→ r s)),  (| (~ q) (~ s)) ⊢ (| (~ p) (~ r))" $ scriptRuns minimalND destructiveDilemma

            it "(→ p q), (→ r s), (| p (~ s)) ⊢ (| q (~ r))" $ scriptRuns minimalND bidirectionalDilemma

            it "(→ p q), (→ p r) ⊢ (→ p (& q r))" $ scriptRuns minimalND composition

            it "(& (→ p q) p) ⊢ q" $ scriptRuns minimalND modusPonens

            it "(& (→ p q) (~ q)) ⊢ (~ p)" $ scriptRuns minimalND modusTolens

            it "(& (→ p q) (→ q r)) ⊢ (→ p r)" $ scriptRuns minimalND hypotheticalSyllogism

            it "(& (| p q) (~ p)) ⊢ q" $ scriptRuns intuitionisticND disjunctiveSyllogism

            it "(~ (& p q)) ⊢ (| (~ p) (~ q))" $ scriptRuns classicalND deMorgan1

            it "(→ (~ (& p q)) (| (~ p) (~ q)))" $ scriptRuns classicalFitch deMorgan1Fw

            it "(→ (→ (□ p) (⊥)) (⋄ (→ p (⊥))))" $ scriptRuns modalKFitch modalTaut

            it "(◻ A) ⊢ (◻ (-> B A))" $ scriptRuns modalKFitch modalTaut2

            xit "iALC xaila 2020 first example" $
              scriptRuns ialcBND xaila1

            xit "iALC xaila 2020 second example" $
              scriptRuns ialcBND xaila2

            it "FOL: halting problem" $ scriptRuns folFitch lessSimpleFol

          -- tests that take longer should go last, so we get faster
          -- feedback if errors exist
          describe "** Proof Strategies" $ do
            it "proves basic tautologies" $ do
              strategy minimalImplicationalND "M->" "(→ A A)"

            it "proves fibonacci M-> formula with n=3 automatically" $
              strategy minimalImplicationalND "M->" "(→ (→ A (→ B C)) (→ (→ A B) (→ A C)))"

            it "proves lukasiewicz" $ do
              strategy minimalImplicationalND "M->" "(→ (→ p (→ q r)) (→ (→ p q) (→ p r)))"
    )

fibo3, lukasiewicz, implSimpleFw, destructiveDilemma, bidirectionalDilemma, composition, hypotheticalSyllogism, disjunctiveSyllogism, modusPonens, modusTolens, deMorgan1, deMorgan1Fw, modalTaut, modalTaut2, gen, genNominal, xaila1, xaila2, forallInclusion :: ProofScript
fibo3 = ProofScript {language = [], system = "M->", initialAssumptions = [(40,"(→ A1 A2)"), (34,"(→ A1 (→ A2 A3))")], steps = Step {formula = "(→ A1 A3)", rule = "->I", _id = 229, discards = [], args = [("antecedent", (1, "A1")), ("consequent", (3, "A3"))], nextSteps = [("predicate", Step {formula = "A3", rule = "->E", _id = 230, discards = [], args = [("antecedent", (2, "A2")), ("implication", (5, "(→ A2 A3)"))], nextSteps = [("minor", Step {formula = "A2", rule = "->E", _id = 232, discards = [], args = [("antecedent", (1, "A1")), ("implication", (6, "(→ A1 A2)"))], nextSteps = [("major", Step {formula = "(→ A1 A2)", rule = "hypothesis \8961", _id = 231, discards = [], args = [], nextSteps = []}), ("minor", Step {formula = "A1", rule = "hypothesis \8961", _id = 233, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(→ A2 A3)", rule = "->E", _id = 234, discards = [], args = [("antecedent", (1, "A1")), ("implication", (8, "(→ A1 (→ A2 A3))"))], nextSteps = [("minor", Step {formula = "A1", rule = "hypothesis \8961", _id = 235, discards = [], args = [], nextSteps = []}), ("major", Step {formula = "(→ A1 (→ A2 A3))", rule = "hypothesis \8961", _id = 44, discards = [], args = [], nextSteps = []})]})]})]}}
lukasiewicz = ProofScript {language = [], system = "M->", initialAssumptions = [], steps = Step {formula = "(→ (→ p (→ q r)) (→ (→ p q) (→ p r)))", rule = "->I", _id = 61, discards = [], args = [("antecedent", (1, "(→ p (→ q r))")), ("consequent", (2, "(→ (→ p q) (→ p r))"))], nextSteps = [("predicate", Step {formula = "(→ (→ p q) (→ p r))", rule = "->I", _id = 62, discards = [], args = [("antecedent", (3, "(→ p q)")), ("consequent", (4, "(→ p r)"))], nextSteps = [("predicate", Step {formula = "(→ p r)", rule = "->I", _id = 63, discards = [], args = [("antecedent", (5, "p")), ("consequent", (6, "r"))], nextSteps = [("predicate", Step {formula = "r", rule = "->E", _id = 64, discards = [], args = [("antecedent", (7, "q")), ("implication", (8, "(→ q r)"))], nextSteps = [("major", Step {formula = "(→ q r)", rule = "->E", _id = 66, discards = [], args = [("antecedent", (5, "p")), ("implication", (1, "(→ p (→ q r))"))], nextSteps = [("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 67, discards = [], args = [], nextSteps = []}), ("major", Step {formula = "(→ p (→ q r))", rule = "hypothesis \8961", _id = 51, discards = [], args = [], nextSteps = []})]}), ("minor", Step {formula = "q", rule = "->E", _id = 68, discards = [], args = [("antecedent", (5, "p")), ("implication", (3, "(→ p q)"))], nextSteps = [("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 69, discards = [], args = [], nextSteps = []}), ("major", Step {formula = "(→ p q)", rule = "hypothesis \8961", _id = 65, discards = [], args = [], nextSteps = []})]})]})]})]})]}}
implSimpleFw = ProofScript {language = [], system = "M->Fw", initialAssumptions = [], steps = Step {formula = "(→ (→ A (→ B C)) (→ (→ A B) (→ A C)))", rule = "->I", _id = 280, discards = [("antecedentHypothesis", 272)], args = [("predicate", (279, "(→ (→ A B) (→ A C))")), ("antecedentHypothesis", (272, "(→ A (→ B C))"))], nextSteps = [("predicate", Step {formula = "(→ (→ A B) (→ A C))", rule = "->I", _id = 279, discards = [("antecedentHypothesis", 273)], args = [("predicate", (278, "(→ A C)")), ("antecedentHypothesis", (273, "(→ A B)"))], nextSteps = [("predicate", Step {formula = "(→ A C)", rule = "->I", _id = 278, discards = [("antecedentHypothesis", 274)], args = [("predicate",(277, "C")), ("antecedentHypothesis", (274, "A"))], nextSteps = [("predicate", Step {formula = "C", rule = "->E", _id = 277, discards = [], args = [("major", (275,"(→ B C)")), ("minor", (276,"B"))], nextSteps = [("minor", Step {formula = "B", rule = "->E", _id = 276, discards = [], args = [("major", (273, "(→ A B)")), ("minor", (274, "A"))], nextSteps = [("major", Step {formula = "(→ A B)", rule = "Assumption(+1)", _id = 273, discards = [], args = [], nextSteps = []}), ("minor", Step {formula = "A", rule = "Assumption(+1)", _id = 274, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(→ B C)", rule = "->E", _id = 275, discards = [], args = [("major", (272, "(→ A (→ B C))")), ("minor", (274, "A"))], nextSteps = [("major", Step {formula = "(→ A (→ B C))", rule = "Assumption(+1)", _id = 272, discards = [], args = [], nextSteps = []}), ("minor", Step {formula = "A", rule = "Assumption(+1)", _id = 274, discards = [], args = [], nextSteps = []})]})]})]})]})]}}
destructiveDilemma = ProofScript {language = [], system = "Min", initialAssumptions = [(3958,"(& (-> p q) (-> r s))"), (394835, "(| (-> q (\8869 )) (-> s (\8869 )))")], steps = Step {formula = "(| (-> p (\8869 )) (-> r (\8869 )))", rule = "|E", _id = 95, discards = [("leftHypothesis", 97), ("rightHypothesis", 96)], args = [("disjunction", (1,"(| (-> q (\8869 )) (-> s (\8869 )))"))], nextSteps = [("resultLeft", Step {formula = "(| (-> p (\8869 )) (-> r (\8869 )))", rule = "|I-l", _id = 196, discards = [], args = [], nextSteps = [("left", Step {formula = "(-> p (\8869 ))", rule = "->I", _id = 197, discards = [("hypothesis", 180)], args = [], nextSteps = [("predicate", Step {formula = "(\8869 )", rule = "->E", _id = 199, discards = [], args = [("antecedent", (2, "q"))], nextSteps = [("minor", Step {formula = "q", rule = "->E", _id = 179, discards = [], args = [("antecedent", (3, "p"))], nextSteps = [("major", Step {formula = "(-> p q)", rule = "&E-l", _id = 181, discards = [], args = [("conjunction", (7, "(& (-> p q) (-> r s))"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) (-> r s))", rule = "hypothesis \8961", _id = 201, discards = [], args = [], nextSteps = []})]}), ("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 180, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> q (\8869 ))", rule = "hypothesis \8961", _id = 97, discards = [], args = [], nextSteps = []})]})]})]}), ("resultRight", Step {formula = "(| (-> p (\8869 )) (-> r (\8869 )))", rule = "|I-r", _id = 175, discards = [], args = [], nextSteps = [("right", Step {formula = "(-> r (\8869 ))", rule = "->I", _id = 412, discards = [("hypothesis", 177)], args = [], nextSteps = [("predicate", Step {formula = "(\8869 )", rule = "->E", _id = 52, discards = [], args = [("antecedent", (4, "s"))], nextSteps = [("minor", Step {formula = "s", rule = "->E", _id = 176, discards = [], args = [("antecedent", (6, "r"))], nextSteps = [("major", Step {formula = "(-> r s)", rule = "&E-r", _id = 178, discards = [], args = [("conjunction", (7,"(& (-> p q) (-> r s))"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) (-> r s))", rule = "hypothesis \8961", _id = 200, discards = [], args = [], nextSteps = []})]}), ("minor", Step {formula = "r", rule = "hypothesis \8961", _id = 177, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> s (\8869 ))", rule = "hypothesis \8961", _id = 96, discards = [], args = [], nextSteps = []})]})]})]}), ("disjunction", Step {formula = "(| (-> q (\8869 )) (-> s (\8869 )))", rule = "hypothesis \8961", _id = 299, discards = [], args = [], nextSteps = []})]}}
bidirectionalDilemma = ProofScript {language = [], system = "Min", initialAssumptions = [(39485,"(-> p q)"), (87645876,"(-> r s)"), (48574,"(| p (-> s (\8869 )))")], steps = Step {formula = "(| q (-> r (\8869 )))", rule = "|E", _id = 231, discards = [("leftHypothesis", 284), ("rightHypothesis", 232)], args = [("disjunction", (1,"(| p (-> s (\8869 )))"))], nextSteps = [("resultLeft", Step {formula = "(| q (-> r (\8869 )))", rule = "|I-l", _id = 264, discards = [], args = [], nextSteps = [("left", Step {formula = "q", rule = "->E", _id = 265, discards = [], args = [("antecedent", (3, "p"))], nextSteps = [("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 284, discards = [], args = [], nextSteps = []}), ("major", Step {formula = "(-> p q)", rule = "hypothesis \8961", _id = 259, discards = [], args = [], nextSteps = []})]})]}), ("resultRight", Step {formula = "(| q (-> r (\8869 )))", rule = "|I-r", _id = 260, discards = [], args = [], nextSteps = [("right", Step {formula = "(-> r (\8869 ))", rule = "->I", _id = 261, discards = [("hypothesis", 283)], args = [], nextSteps = [("predicate", Step {formula = "(\8869 )", rule = "->E", _id = 262, discards = [], args = [("antecedent", (5,"s"))], nextSteps = [("minor", Step {formula = "s", rule = "->E", _id = 282, discards = [], args = [("antecedent", (7,"r"))], nextSteps = [("major", Step {formula = "(-> r s)", rule = "hypothesis \8961", _id = 263, discards = [], args = [], nextSteps = []}), ("minor", Step {formula = "r", rule = "hypothesis \8961", _id = 283, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> s (\8869 ))", rule = "hypothesis \8961", _id = 232, discards = [], args = [], nextSteps = []})]})]})]}), ("disjunction", Step {formula = "(| p (-> s (\8869 )))", rule = "hypothesis \8961", _id = 51, discards = [], args = [], nextSteps = []})]}}
composition = ProofScript {language = [], system = "Min", initialAssumptions = [(7485674,"(-> p q)"), (3785,"(-> p r)")], steps = Step {formula = "(-> p (& q r))", rule = "->I", _id = 406, discards = [("hypothesis", 255), ("hypothesis", 408)], args = [], nextSteps = [("predicate", Step {formula = "(& q r)", rule = "&I", _id = 412, discards = [], args = [], nextSteps = [("left", Step {formula = "q", rule = "->E", _id = 254, discards = [], args = [("antecedent", (1,"p"))], nextSteps = [("major", Step {formula = "(-> p q)", rule = "hypothesis \8961", _id = 396, discards = [], args = [], nextSteps = []}), ("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 255, discards = [], args = [], nextSteps = []})]}), ("right", Step {formula = "r", rule = "->E", _id = 407, discards = [], args = [("antecedent", (1,"p"))], nextSteps = [("major", Step {formula = "(-> p r)", rule = "hypothesis \8961", _id = 233, discards = [], args = [], nextSteps = []}), ("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 408, discards = [], args = [], nextSteps = []})]})]})]}}
modusPonens = ProofScript {language = [], system = "Min", initialAssumptions = [(3755,"(& (-> p q) p)")], steps = Step {formula = "q", rule = "->E", _id = 242, discards = [], args = [("antecedent", (1,"p"))], nextSteps = [("major", Step {formula = "(-> p q)", rule = "&E-l", _id = 411, discards = [], args = [("conjunction", (2,"(& (-> p q) p)"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) p)", rule = "hypothesis \8961", _id = 236, discards = [], args = [], nextSteps = []})]}), ("minor", Step {formula = "p", rule = "&E-r", _id = 410, discards = [], args = [("conjunction", (2,"(& (-> p q) p)"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) p)", rule = "hypothesis \8961", _id = 243, discards = [], args = [], nextSteps = []})]})]}}
modusTolens = ProofScript {language = [], system = "Min", initialAssumptions = [(3857,"(& (-> p q) (-> q (\8869 )))")], steps = Step {formula = "(-> p (\8869 ))", rule = "->I", _id = 245, discards = [("hypothesis", 107)], args = [], nextSteps = [("predicate", Step {formula = "(\8869 )", rule = "->E", _id = 246, discards = [], args = [("antecedent", (1,"q"))], nextSteps = [("minor", Step {formula = "q", rule = "->E", _id = 69, discards = [], args = [("antecedent", (2,"p"))], nextSteps = [("major", Step {formula = "(-> p q)", rule = "&E-l", _id = 41, discards = [], args = [("conjunction", (3,"(& (-> p q) (-> q (\8869 )))"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) (-> q (\8869 )))", rule = "hypothesis \8961", _id = 247, discards = [], args = [], nextSteps = []})]}), ("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 107, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> q (\8869 ))", rule = "&E-r", _id = 409, discards = [], args = [("conjunction", (3,"(& (-> p q) (-> q (\8869 )))"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) (-> q (\8869 )))", rule = "hypothesis \8961", _id = 236, discards = [], args = [], nextSteps = []})]})]})]}}
hypotheticalSyllogism = ProofScript {language = [], system = "Min", initialAssumptions = [(3573,"(& (-> p q) (-> q r))")], steps = Step {formula = "(-> p r)", rule = "->I", _id = 305, discards = [("hypothesis", 184)], args = [], nextSteps = [("predicate", Step {formula = "r", rule = "->E", _id = 195, discards = [], args = [("antecedent", (1,"q"))], nextSteps = [("minor", Step {formula = "q", rule = "->E", _id = 183, discards = [], args = [("antecedent", (2,"p"))], nextSteps = [("major", Step {formula = "(-> p q)", rule = "&E-l", _id = 185, discards = [], args = [("conjunction", (3, "(& (-> p q) (-> q r))"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) (-> q r))", rule = "hypothesis \8961", _id = 196, discards = [], args = [], nextSteps = []})]}), ("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 184, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> q r)", rule = "&E-r", _id = 182, discards = [], args = [("conjunction", (3,"(& (-> p q) (-> q r))"))], nextSteps = [("conjunction", Step {formula = "(& (-> p q) (-> q r))", rule = "hypothesis \8961", _id = 288, discards = [], args = [], nextSteps = []})]})]})]}}
disjunctiveSyllogism = ProofScript {language = [], system = "Int", initialAssumptions = [(83754,"(& (| p q) (-> p (\8869 )))")], steps = Step {formula = "q", rule = "|E", _id = 186, discards = [("leftHypothesis", 347), ("rightHypothesis", 187)], args = [("disjunction", (1,"(| p q)"))], nextSteps = [("disjunction", Step {formula = "(| p q)", rule = "&E-l", _id = 349, discards = [], args = [("conjunction", (2, "(& (| p q) (-> p (\8869 )))"))], nextSteps = [("conjunction", Step {formula = "(& (| p q) (-> p (\8869 )))", rule = "hypothesis \8961", _id = 338, discards = [], args = [], nextSteps = []})]}), ("resultLeft", Step {formula = "q", rule = "\8869-\128165", _id = 189, discards = [], args = [("bottom", (4, "(\8869 )"))], nextSteps = [("bottom", Step {formula = "(\8869 )", rule = "->E", _id = 346, discards = [], args = [("antecedent", (5,"p"))], nextSteps = [("major", Step {formula = "(-> p (\8869 ))", rule = "&E-r", _id = 348, discards = [], args = [("conjunction", (2,"(& (| p q) (-> p (\8869 )))"))], nextSteps = [("conjunction", Step {formula = "(& (| p q) (-> p (\8869 )))", rule = "hypothesis \8961", _id = 188, discards = [], args = [], nextSteps = []})]}), ("minor", Step {formula = "p", rule = "hypothesis \8961", _id = 347, discards = [], args = [], nextSteps = []})]})]}), ("resultRight", Step {formula = "q", rule = "hypothesis \8961", _id = 187, discards = [], args = [], nextSteps = []})]}}
deMorgan1 = ProofScript {language = [], system = "Cla", initialAssumptions = [(5784,"(-> (& p q) (\8869 ))")], steps = Step {formula = "(| (-> p (\8869 )) (-> q (\8869 )))", rule = "⊥↯", _id = 41, discards = [("hypothesis", 46), ("hypothesis", 44)], args = [("bottom", (1,"(\8869)"))], nextSteps = [("absurd", Step {formula = "(\8869)", rule = "->E", _id = 43, discards = [], args = [("antecedent", (2,"(& p q)"))], nextSteps = [("minor", Step {formula = "(& p q)", rule = "&I", _id = 45, discards = [], args = [], nextSteps = [("right", Step {formula = "q", rule = "⊥↯", _id = 65, discards = [("hypothesis", 67)], args = [("bottom", (1, "(\8869)"))], nextSteps = [("absurd", Step {formula = "(\8869)", rule = "->E", _id = 66, discards = [], args = [("antecedent", (3,"(| (-> p (\8869 )) (-> q (\8869 )))"))], nextSteps = [("minor", Step {formula = "(| (-> p (\8869 )) (-> q (\8869 )))", rule = "|I-r", _id = 68, discards = [], args = [], nextSteps = [("right", Step {formula = "(-> q (\8869))", rule = "hypothesis \8961", _id = 67, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> (| (-> p (\8869 )) (-> q (\8869 ))) (\8869))", rule = "hypothesis \8961", _id = 46, discards = [], args = [], nextSteps = []})]})]}), ("left", Step {formula = "p", rule = "⊥↯", _id = 61, discards = [("hypothesis", 63)], args = [("bottom", (1, "(\8869)"))], nextSteps = [("absurd", Step {formula = "(\8869)", rule = "->E", _id = 62, discards = [], args = [("antecedent", (3,"(| (-> p (\8869 )) (-> q (\8869 )))"))], nextSteps = [("minor", Step {formula = "(| (-> p (\8869 )) (-> q (\8869 )))", rule = "|I-l", _id = 64, discards = [], args = [], nextSteps = [("left", Step {formula = "(-> p (\8869))", rule = "hypothesis \8961", _id = 63, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> (| (-> p (\8869 )) (-> q (\8869 ))) (\8869))", rule = "hypothesis \8961", _id = 44, discards = [], args = [], nextSteps = []})]})]})]}), ("major", Step {formula = "(-> (& p q) (\8869 ))", rule = "hypothesis \8961", _id = 344, discards = [], args = [], nextSteps = []})]})]}}
deMorgan1Fw = ProofScript {language = [], system = "ClaFw", initialAssumptions = [], steps = Step {formula = "(-> (-> (& p q) (_|_ )) (| (-> p (_|_ )) (-> q (_|_ ))))", rule = "->I", _id = 197, discards = [("antecedentHypothesis", 201)], args = [("predicate", (196, "(| (-> p (_|_ )) (-> q (_|_ )))")), ("antecedentHypothesis", (201,"(-> (& p q) (_|_ ))"))], nextSteps = [("predicate", Step {formula = "(| (-> p (_|_ )) (-> q (_|_ )))", rule = "⊥↯", _id = 196, discards = [("negativeHypothesis", 191)], args = [("absurd", ((195,"(_|_ )"))), ("negativeHypothesis", (191,"(-> (| (-> p (_|_ )) (-> q (_|_ ))) (_|_ ))"))], nextSteps = [("absurd", Step {formula = "(_|_ )", rule = "->E", _id = 195, discards = [], args = [("major", (201,"(-> (& p q) (_|_ ))")), ("minor", (194,"(& p q)"))], nextSteps = [("minor", Step {formula = "(& p q)", rule = "&I", _id = 194, discards = [], args = [("left", (207,"p")), ("right", (204,"q"))],
 nextSteps = [("left", Step {formula = "p", rule = "⊥↯", _id = 207, discards = [("negativeHypothesis", 205)], args = [("absurd", (206,"(_|_ )")), ("negativeHypothesis", (205,"(-> p (_|_ ))"))], nextSteps = [("absurd", Step {formula = "(_|_ )", rule = "->E", _id = 206, discards = [], args = [("major", (191,"(-> (| (-> p (_|_ )) (-> q (_|_ ))) (_|_ ))")), ("minor", (1,"(| (-> p (_|_ )) (-> q (_|_ )))"))], nextSteps = [("minor", Step {formula = "(| (-> p (_|_ )) (-> q (_|_ )))", rule = "|I-l", _id = 193, discards = [], args = [("right", (1, "(-> q (_|_ ))")),("left", (205,"(-> p (_|_ ))"))], nextSteps = [("left", Step {formula = "(-> p (_|_ ))", rule = "Assumption(+1)", _id = 205, discards = [], args = [], nextSteps = []})]}), ("major", Step {formula = "(-> (| (-> p (_|_ )) (-> q (_|_ ))) (_|_ ))", rule = "Assumption", _id = 191, discards = [], args = [], nextSteps = []})]})]}), ("right", Step {formula = "q", rule = "⊥↯", _id = 204, discards = [("negativeHypothesis", 202)], args = [("absurd", (192,"(_|_ )")), ("negativeHypothesis", (202, "(-> q (_|_ ))"))], nextSteps = [("absurd", Step {formula = "(_|_ )", rule = "->E", _id = 192, discards = [], args = [("major", (191,"(-> (| (-> p (_|_ )) (-> q (_|_ ))) (_|_ ))")), ("minor", (203,"(| (-> p (_|_ )) (-> q (_|_ )))"))], nextSteps = [("major", Step {formula = "(-> (| (-> p (_|_ )) (-> q (_|_ ))) (_|_ ))", rule = "FreeAssumption", _id = 191, discards = [], args = [], nextSteps = []}), ("minor", Step {formula = "(| (-> p (_|_ )) (-> q (_|_ )))", rule = "|I-r", _id = 203, discards = [], args = [("right", (202, "(-> q (_|_ ))")), ("disjunction", (1, "(| (-> p (_|_ )) (-> q (_|_ )))"))], nextSteps = [("right", Step {formula = "(-> q (_|_ ))", rule = "Assumption(+1)", _id = 202, discards = [], args = [], nextSteps = []})]})]})]})]}), ("major", Step {formula = "(-> (& p q) (_|_ ))", rule = "Assumption(+1)", _id = 201, discards = [], args = [], nextSteps = []})]})]})]}}
modalTaut = ProofScript {language = [], system = "Modal K", initialAssumptions = [], steps = Step {formula = "(-> (-> (◻ p) (⊥ )) (◇ (-> p (⊥ ))))", rule = "->E", _id = 621, discards = [], args = [], nextSteps = [("minor", Step {formula = "(-> (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (⊥ )) (◇ (-> p (⊥ ))))", rule = "DUAL-l", _id = 49, discards = [], args = [("p", (654, "(→ p (⊥))"))], nextSteps = []}), ("major", Step {formula = "(-> (-> (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (⊥ )) (◇ (-> p (⊥ )))) (-> (-> (◻ p) (⊥ )) (◇ (-> p (⊥ )))))", rule = "->E", _id = 48, discards = [], args = [], nextSteps = [("major", Step {formula = "(-> (-> (-> (◻ p) (⊥ )) (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (⊥ ))) (-> (-> (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (⊥ )) (◇ (-> p (⊥ )))) (-> (-> (◻ p) (⊥ )) (◇ (-> p (⊥ ))))))", rule = "TAUT", _id = 624, discards = [("tautology", 624)], args = [], nextSteps = []}), ("minor", Step {formula = "(-> (-> (◻ p) (⊥ )) (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (⊥ )))", rule = "->E", _id = 47, discards = [], args = [], nextSteps = [("minor", Step {formula = "(-> (◻ (-> (-> p (⊥ )) (⊥ ))) (◻ p))", rule = "->E", _id = 46, discards = [], args = [], nextSteps = [("major", Step {formula = "(-> (◻ (-> (-> (-> p (⊥ )) (⊥ )) p)) (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (◻ p)))", rule = "K", _id = 641, discards = [], args = [("p", (384, "(-> (-> p (⊥ )) (⊥ ))")),("q", (654, "p"))], nextSteps = []}), ("minor", Step {formula = "(◻ (-> (-> (-> p (⊥ )) (⊥ )) p))", rule = "◻I", _id = 628, discards = [], args = [("premise", (639,"(-> (-> (-> p (⊥ )) (⊥ )) p)"))], nextSteps = [("premise", Step {formula = "(-> (-> (-> p (⊥ )) (⊥ )) p)", rule = "TAUT", _id = 639, discards = [("tautology", 639)], args = [], nextSteps = []})]})]}), ("major", Step {formula = "(-> (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (◻ p)) (-> (-> (◻ p) (⊥ )) (-> (◻ (-> (-> p (⊥ )) (⊥ ))) (⊥ ))))", rule = "TAUT", _id = 637, discards = [("tautology", 637)], args = [], nextSteps = []})]})]})]}}
modalTaut2 = ProofScript {language = [], system = "Modal K", initialAssumptions = [(13027, "(\9723 A)")], steps = Step {formula = "(\9723 (\8594 B A))", rule = "\8594E", _id = 13605, discards = [], args = [("major",(12017,"(\8594 (\9723 A) (\9723 (\8594 B A)))")),("minor",(13027,"(\9723 A)"))], nextSteps = [("major",Step {formula = "(\8594 (\9723 A) (\9723 (\8594 B A)))", rule = "\8594E", _id = 12017, discards = [], args = [("major",(13032,"(\8594 (\9723 (\8594 A (\8594 B A))) (\8594 (\9723 A) (\9723 (\8594 B A))))")),("minor",(12016,"(\9723 (\8594 A (\8594 B A)))"))], nextSteps = [("minor",Step {formula = "(\9723 (\8594 A (\8594 B A)))", rule = "\9723I", _id = 12016, discards = [], args = [("premise",(12015,"(\8594 A (\8594 B A))"))], nextSteps = [("premise",Step {formula = "(\8594 A (\8594 B A))", rule = "TAUT", _id = 12015, discards = [("tautology",12015)], args = [], nextSteps = []})]}),("major",Step {formula = "(\8594 (\9723 (\8594 A (\8594 B A))) (\8594 (\9723 A) (\9723 (\8594 B A))))", rule = "K", _id = 13032, discards = [], args = [("p", (23, "A")), ("q", (32,"(\8594 B A)"))], nextSteps = []})]}),("minor",Step {formula = "(\9723 A)", rule = "TAUT", _id = 13027, discards = [("",13027)], args = [], nextSteps = []})]}}

inclusionNominal, intersectionNominal, unionNominal, precedes, forall, exists :: ProofScript
inclusionNominal = ProofScript {language = [], system = "iALC", initialAssumptions = [(8347,"(: x (\8849 (^ A (:: C)) (^ B (:: D))))")], steps = Step {formula = "(: x (\8849 (^ A (:: C)) (^ B (:: D))))", rule = "\8849I-n", _id = 9929, discards = [("hypothesis", 9933)], args = [], nextSteps = [("pred", Step {formula = "(: x (^ B (:: D)))", rule = "\8849E-n", _id = 9932, discards = [], args = [("included", (1,"(^ A (:: C))"))], nextSteps = [("included", Step {formula = "(: x (^ A (:: C)))", rule = "hypothesis \8961", _id = 9933, discards = [], args = [], nextSteps = []}), ("super", Step {formula = "(: x (\8849 (^ A (:: C)) (^ B (:: D))))", rule = "hypothesis \8961", _id = 9916, discards = [], args = [], nextSteps = []})]})]}}
intersectionNominal =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions = [(3475,"(: x (^ (\8851 A B) (:: (\8704^ r))))")],
      steps =
        Step
          { formula = "(: x (^ (\8851 A B) (:: (\8704^ r))))",
            rule = "\8851I-n",
            _id = 9995,
            discards = [],
            args = [],
            nextSteps =
              [ ( "left",
                  Step
                    { formula = "(: x (^ A (:: (\8704^ r))))",
                      rule = "\8851E-l-n",
                      _id = 10014,
                      discards = [],
                      args = [("right", (1,"B"))],
                      nextSteps =
                        [ ( "intersection",
                            Step
                              { formula = "(: x (^ (\8851 A B) (:: (\8704^ r))))",
                                rule = "hypothesis \8961",
                                _id = 9983,
                                discards = [],
                                args = [],
                                nextSteps = []
                              }
                          )
                        ]
                    }
                ),
                ( "right",
                  Step
                    { formula = "(: x (^ B (:: (\8704^ r))))",
                      rule = "\8851E-r-n",
                      _id = 10001,
                      discards = [],
                      args = [("left", (2, "A"))],
                      nextSteps =
                        [ ( "intersection",
                            Step
                              { formula = "(: x (^ (\8851 A B) (:: (\8704^ r))))",
                                rule = "hypothesis \8961",
                                _id = 10000,
                                discards = [],
                                args = [],
                                nextSteps = []
                              }
                          )
                        ]
                    }
                )
              ]
          }
    }
unionNominal =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions = [(3875,"(: x (^ (\8852 A B) (:: (\8707^ r) (\8707^ s))))")],
      steps =
        Step
          { formula = "(: x (^ (\8852 B A) (:: (\8707^ r) (\8707^ s))))",
            rule = "\8852E-n",
            _id = 10208,
            discards = [("hypothesis", 10212), ("hypothesis", 10209)],
            args = [("union", (1,"(^ (⊔ A B) (:: (\8707^ r) (\8707^ s)))")), ("x", (2,"x"))],
            nextSteps =
              [ ( "left",
                  Step
                    { formula = "(: x (^ (\8852 B A) (:: (\8707^ r) (\8707^ s))))",
                      rule = "\8852I-r-n",
                      _id = 10157,
                      discards = [],
                      args = [],
                      nextSteps =
                        [ ( "right",
                            Step
                              { formula = "(: x (^ A (:: (\8707^ r) (\8707^ s))))",
                                rule = "hypothesis \8961",
                                _id = 10212,
                                discards = [],
                                args = [],
                                nextSteps = []
                              }
                          )
                        ]
                    }
                ),
                ( "right",
                  Step
                    { formula = "(: x (^ (\8852 B A) (:: (\8707^ r) (\8707^ s))))",
                      rule = "\8852I-l-n",
                      _id = 10009,
                      discards = [],
                      args = [],
                      nextSteps =
                        [ ( "left",
                            Step
                              { formula = "(: x (^ B (:: (\8707^ r) (\8707^ s))))",
                                rule = "hypothesis \8961",
                                _id = 10209,
                                discards = [],
                                args = [],
                                nextSteps = []
                              }
                          )
                        ]
                    }
                ),
                ( "union",
                  Step
                    { formula = "(: x (^ (\8852 A B) (:: (\8707^ r) (\8707^ s))))",
                      rule = "hypothesis \8961",
                      _id = 10190,
                      discards = [],
                      args = [],
                      nextSteps = []
                    }
                )
              ]
          }
    }
precedes = ProofScript {language = [], system = "iALC", initialAssumptions = [(875,"(\8828 y x)"), (4857,"(: x (^ A (:: B C)))")], steps = Step {formula = "(: y (^ A (:: B C)))", rule = "\8828E", _id = 9994, discards = [], args = [("x", (1,"x"))], nextSteps = [("nominal", Step {formula = "(: x (^ A (:: B C)))", rule = "hypothesis \8961", _id = 9981, discards = [], args = [], nextSteps = []}), ("precedence", Step {formula = "(\8828 y x)", rule = "hypothesis \8961", _id = 9995, discards = [], args = [], nextSteps = []})]}}
forall =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions = [(8475465,"(: y (^ A (:: (∀^ R ) (∃^ S))))")],
      steps =
        Step
          { formula = "(: y (^ A (:: (∀^ R ) (∃^ S))))",
            rule = "∀E",
            _id = 1,
            discards = [],
            args = [("x", (1,"x"))],
            nextSteps =
              [ ( "forall",
                  Step
                    { formula = "(: x (^ (∀^ R) (:: (∃^ S) (∃^ Q))))",
                      rule = "∀I",
                      _id = 2,
                      discards = [],
                      args = [("y", (2,"y"))],
                      nextSteps =
                        [ ( "forall",
                            Step {formula = "(: y (^ A (:: (∀^ R) (∃^ S) (∃^ Q))))", rule = "hypothesis \8961", _id = 3, discards = [], args = [], nextSteps = []}
                          )
                        ]
                    }
                )
              ]
          }
    }
exists =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions = [(38573,"(: x (^ (∃ R A) (:: (∃^ S) (∃^ Q))))")],
      steps =
        Step
          { formula = "(: x (^ (∃ R A) (:: (∃^ S) (∃^ Q))))",
            rule = "∃I",
            _id = 1,
            discards = [],
            args = [("y", (1, "y"))],
            nextSteps =
              [ ( "exists",
                  Step
                    { formula = "(: x (^ (∃ R A) (:: (∃^ S) (∃^ Q))))",
                      rule = "∃E",
                      _id = 2,
                      discards = [],
                      args = [("x", (2,"x"))],
                      nextSteps = [("exists", Step {formula = "(: x (^ (∃ R A) (:: (∃^ S) (∃^ Q))))", rule = "hypothesis \8961", _id = 3, discards = [], args = [], nextSteps = []})]
                    }
                )
              ]
          }
    }

gen =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions =
        [(3587,"(^ A (:: (∃^ S) (∃^ Q)))")],
      steps =
        Step
          { formula = "(^ A (:: (∃^ S) (∃^ Q) (∀^ R)))",
            rule = "Gen",
            _id = 1,
            discards = [],
            args = [],
            nextSteps =
              [ ( "gen",
                  Step
                    { formula = "(^ A (:: (∃^ S) (∃^ Q)))",
                      rule = "hypothesis \8961",
                      _id = 2,
                      discards = [],
                      args = [],
                      nextSteps =
                        []
                    }
                )
              ]
          }
    }

forallInclusion =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions =
        [(9856,"(: x (^ B (:: (∀^ R) (∀^ T))))")],
      steps =
        Step
          { formula = "(: x (^ (⊑ A B) (:: (∀^ R) (∀^ T) (∃^ Y) (∃^ U))))",
            rule = "∀-⊑-mix-i",
            _id = 1,
            discards = [],
            args = [],
            nextSteps =
              []
          }
    }

genNominal =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions =
        [(38575,"(: x (^ A (::)))")],
      steps =
        Step
          { formula = "(: y (^ A (:: (∀^ R))))",
            rule = "Gen-n",
            _id = 1,
            discards = [],
            args = [("x", (1,"x"))],
            nextSteps =
              [ ( "gen",
                  Step
                    { formula = "(: x (^ A (::)))",
                      rule = "hypothesis \8961",
                      _id = 2,
                      discards = [],
                      args = [],
                      nextSteps =
                        []
                    }
                )
              ]
          }
    }

xaila1 =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions =
        [ (387453,"(: org (⊑ (^ (∃ h (∃ a (∃ c O))) (::)) (^ NP (::))))"),
          (375,"(: cert (⊑ (^ CA (::)) (^ O (:: (∃^ c) (∃^ a) (∃^ h)))))"),
          (37587435,"(: cert (^ Acc (::)))"),
          (348957,"(: cert (⊑ (^ Acc (::)) (^ CA (::))))")
        ],
      steps =
        Step
          { formula = "(: org (^ NP (::)))",
            rule = "⊑E-n",
            _id = 1,
            discards = [],
            args = [("included", (1,"(^ (∃ h (∃ a (∃ c O))) (::))"))],
            nextSteps =
              [ ( "included",
                  Step
                    { formula = "(: org (^ (∃ h (∃ a (∃ c O))) (::)))",
                      rule = "∃I",
                      _id = 2,
                      discards = [],
                      args = [("y", (2, "fin"))],
                      nextSteps =
                        [ ( "exists",
                            Step
                              { formula = "(: fin (^ (∃ a (∃ c O)) (:: (∃^ h))))",
                                rule = "∃I",
                                _id = 3,
                                discards = [],
                                args = [("y", (3,"aud"))],
                                nextSteps =
                                  [ ( "exists",
                                      Step
                                        { formula = "(: aud (^ (∃ c O) (:: (∃^ a) (∃^ h))))",
                                          rule = "∃I",
                                          _id = 3,
                                          discards = [],
                                          args = [("y", (4,"cert"))],
                                          nextSteps =
                                            [ ( "exists",
                                                Step
                                                  { formula = "(: cert (^ O (:: (∃^ c) (∃^ a) (∃^ h))))",
                                                    rule = "⊑E-n",
                                                    _id = 4,
                                                    discards = [],
                                                    args = [("included", (5,"(^ CA (::))"))],
                                                    nextSteps =
                                                      [ ( "included",
                                                          Step
                                                            { formula = "(: cert (^ CA (::)))",
                                                              rule = "⊑E-n",
                                                              _id = 6,
                                                              discards = [],
                                                              args = [("included", (6, "(^ Acc (::))"))],
                                                              nextSteps =
                                                                [ ( "included",
                                                                    Step
                                                                      { formula = "(: cert (^ Acc (::)))",
                                                                        rule = "hypothesis \8961",
                                                                        _id = 8,
                                                                        discards = [],
                                                                        args = [],
                                                                        nextSteps = []
                                                                      }
                                                                  ),
                                                                  ( "super",
                                                                    Step
                                                                      { formula = "(: cert (⊑ (^ Acc (::)) (^ CA (::))))",
                                                                        rule = "hypothesis \8961",
                                                                        _id = 9,
                                                                        discards = [],
                                                                        args = [],
                                                                        nextSteps = []
                                                                      }
                                                                  )
                                                                ]
                                                            }
                                                        ),
                                                        ( "super",
                                                          Step
                                                            { formula = "(: cert (⊑ (^ CA (::)) (^ O (:: (∃^ c) (∃^ a) (∃^ h)))))",
                                                              rule = "hypothesis \8961",
                                                              _id = 5,
                                                              discards = [],
                                                              args = [],
                                                              nextSteps = []
                                                            }
                                                        )
                                                      ]
                                                  }
                                              )
                                            ]
                                        }
                                    )
                                  ]
                              }
                          )
                        ]
                    }
                ),
                ( "super",
                  Step
                    { formula = "(: org (⊑ (^ (∃ h (∃ a (∃ c O))) (::)) (^ NP (::))))",
                      rule = "hypothesis \8961",
                      _id = 7,
                      discards = [],
                      args = [],
                      nextSteps = []
                    }
                )
              ]
          }
    }

xaila2 =
  ProofScript
    { system = "iALC",
      language = [],
      initialAssumptions =
        [ (3847,"(⊑ (: ed (^ (⊓ (∃ i (∃ d NP)) (∃ nT (∃ o O))) (∷))) (: ed (^ (∃ i D) (∷))))"),
          -- DOUBT: is this the correct hypothesis? "(: ed (⊑ (^ (⊓ (∃ i (∃ d NP)) (∃ nT (∃ o O))) (::)) (^ (∃ i D) (:: L))))",
          (38575,"(: ed (∃ nT (∃ o O)))"),
          (3985,"(: org (^ NP (:: (∃^ i) (∃^ d))))")
        ],
      steps =
        Step
          { formula = "(: act (^ D (:: (∃^ i))))",
            rule = "∃E",
            _id = 1,
            discards = [],
            args = [("x", (2, "ed"))],
            nextSteps =
              [ ( "exists",
                  Step
                    { formula = "(: ed (^ (∃ i D) (::)))",
                      rule = "inclusionElimination",
                      _id = 2,
                      discards = [],
                      args = [("included", (1,"(: ed (^ (⊓ (∃ i (∃ d NP)) (∃ nT (∃ o O))) (::)))"))],
                      nextSteps =
                        [ ( "super",
                            Step
                              { formula = "(⊑ (: ed (^ (⊓ (∃ i (∃ d NP)) (∃ nT (∃ o O))) (∷))) (: ed (^ (∃ i D) (∷))))",
                                rule = "hypothesisDiscard",
                                _id = 3,
                                discards = [],
                                args = [],
                                nextSteps =
                                  []
                              }
                          ) -- ,
                          -- ( "included",
                          --   Step
                          --     { formula = "(: ed (^ (⊓ (∃ i (∃ d NP)) (∃ nT (∃ o O))) (∷)))",
                          --       rule = "⊓I-n",
                          --       _id = 4,
                          --       discards = [],
                          --       args = [],
                          --       nextSteps =
                          --         []
                          --     }
                          -- )
                        ]
                    }
                )
              ]
          }
    }
universalTestBND, existentialTestBND :: ProofScript
universalTestBND = ProofScript {system = "FOL,Bw", initialAssumptions = [(13184,"(\8704 x (.P x))")], language = [".P 1"], steps = Step {formula = "(\8704 y (.P y))", rule = "\8704I", _id = 13153, discards = [], args = [("term",(13186,"z"))], nextSteps = [("predicate",Step {formula = "(.P z)", rule = "\8704E", _id = 13188, discards = [], args = [("substitutionBody",(13186,".P(x)"))], nextSteps = [("predicate",Step {formula = "(\8704 x (.P x))", rule = "Leaf", _id = 13244, discards = [], args = [], nextSteps = []})]})]}}
existentialTestBND = ProofScript {system = "FOL,Bw", initialAssumptions = [(13198,"(\8707 x (.P x))")], language = [".P 1"], steps = Step {formula = "(\8707 y (.P y))", rule = "\8707E", _id = 13263, discards = [("hypothesis",13264)], args = [("constant",(13261,"z")),("existential",(13197,"(∃ x (.P x))"))], nextSteps = [("predicate",Step {formula = "(\8707 y (.P y))", rule = "\8707I", _id = 13157, discards = [], args = [("term",(13261,"z"))], nextSteps = [("predicate",Step {formula = "(.P z)", rule = "Leaf", _id = 13264, discards = [], args = [], nextSteps = []})]}),("existential",Step {formula = "(\8707 x (.P x))", rule = "Leaf", _id = 13191, discards = [], args = [], nextSteps = []})]}}
universalTest,existentialTest :: ProofScript
universalTest = ProofScript {system = "FOL", initialAssumptions = [(13347,"(\8704 x (.P x))")], language = [".P 1"], steps = Step {formula = "(\8704 y (.P y))", rule = "\8704I", _id = 13328, discards = [], args = [("predicate",(13327,"(.P z)"))], nextSteps = [("predicate",Step {formula = "(.P z)", rule = "\8704E", _id = 13327, discards = [], args = [("predicate",(13347,"(\8704 x (.P x))"))], nextSteps = [("predicate",Step {formula = "(\8704 x (.P x))", rule = "Assumption", _id = 13347, discards = [("",13347)], args = [], nextSteps = []})]})]}}
existentialTest = ProofScript {system = "FOL", initialAssumptions = [(13421,"(\8707 x (.P x))")], language = [".P 1"], steps = Step {formula = "(\8707 y (.P y))", rule = "\8707E", _id = 13406, discards = [("hypothesis",13424)], args = [("existential",(13421,"(\8707 x (.P x))")),("predicate",(13425,"(\8707 y (.P y))")),("hypothesis",(13424,"(.P z)"))], nextSteps = [("predicate",Step {formula = "(\8707 y (.P y))", rule = "\8707I", _id = 13425, discards = [], args = [("predicate",(13424,"(.P z)"))], nextSteps = [("predicate",Step {formula = "(.P z)", rule = "Assumption(+1)", _id = 13424, discards = [], args = [], nextSteps = []})]}),("existential",Step {formula = "(\8707 x (.P x))", rule = "Assumption", _id = 13421, discards = [("",13421)], args = [], nextSteps = []})]}}
lessSimpleFol :: ProofScript
lessSimpleFol = ProofScript {system = "FOL", initialAssumptions = [(13170,"(\8704 p (\8594 (\8743 (.IsProgram p) (\172 (.Stops p p))) (.Stops (.m) p)))"),(14128,"(\8704 p (\8594 (\8743 (.IsProgram p) (.Stops (.m) p)) (\172 (.Stops p p))))")], language = [".IsProgram 1",".Stops 2",".m 0"], steps = Step {formula = "(\172 (.IsProgram (.m)))", rule = "\172I", _id = 13242, discards = [("assumption",13171)], args = [("assumption",(13171,"(.IsProgram (.m))")),("absurd",(13174,"(\8869)"))], nextSteps = [("absurd",Step {formula = "(\8869)", rule = "\172E", _id = 13174, discards = [], args = [("negation",(13199,"(\172 (.Stops (.m) (.m)))")),("affirmation",(13211,"(.Stops (.m) (.m))"))], nextSteps = [("affirmation",Step {formula = "(.Stops (.m) (.m))", rule = "\8594E", _id = 13211, discards = [], args = [("major",(13209,"(\8594 (\8743 (.IsProgram (.m)) (\172 (.Stops (.m) (.m)))) (.Stops (.m) (.m)))")),("minor",(13206,"(\8743 (.IsProgram (.m)) (\172 (.Stops (.m) (.m))))"))], nextSteps = [("major",Step {formula = "(\8594 (\8743 (.IsProgram (.m)) (\172 (.Stops (.m) (.m)))) (.Stops (.m) (.m)))", rule = "\8704E", _id = 13209, discards = [], args = [("predicate",(13170,"(\8704 p (\8594 (\8743 (.IsProgram p) (\172 (.Stops p p))) (.Stops (.m) p)))"))], nextSteps = [("predicate",Step {formula = "(\8704 p (\8594 (\8743 (.IsProgram p) (\172 (.Stops p p))) (.Stops (.m) p)))", rule = "Assumption", _id = 13170, discards = [("",13170)], args = [], nextSteps = []})]}),("minor",Step {formula = "(\8743 (.IsProgram (.m)) (\172 (.Stops (.m) (.m))))", rule = "\8743I", _id = 13206, discards = [], args = [("left",(13171,"(.IsProgram (.m))")),("right",(13199,"(\172 (.Stops (.m) (.m)))"))], nextSteps = [("left",Step {formula = "(.IsProgram (.m))", rule = "Assumption(+1)", _id = 13171, discards = [], args = [], nextSteps = []}),("right",Step {formula = "(\172 (.Stops (.m) (.m)))", rule = "\172I", _id = 13199, discards = [("assumption",13173)], args = [("assumption",(13173,"(.Stops (.m) (.m))")),("absurd",(13197,"(\8869)"))], nextSteps = [("absurd",Step {formula = "(\8869)", rule = "\172E", _id = 13197, discards = [], args = [("negation",(13196,"(\172 (.Stops (.m) (.m)))")),("affirmation",(13173,"(.Stops (.m) (.m))"))], nextSteps = [("affirmation",Step {formula = "(.Stops (.m) (.m))", rule = "Assumption(+1)", _id = 13173, discards = [], args = [], nextSteps = []}),("negation",Step {formula = "(\172 (.Stops (.m) (.m)))", rule = "\8594E", _id = 13196, discards = [], args = [("major",(13195,"(\8594 (\8743 (.IsProgram (.m)) (.Stops (.m) (.m))) (\172 (.Stops (.m) (.m))))")),("minor",(13192,"(\8743 (.IsProgram (.m)) (.Stops (.m) (.m)))"))], nextSteps = [("minor",Step {formula = "(\8743 (.IsProgram (.m)) (.Stops (.m) (.m)))", rule = "\8743I", _id = 13192, discards = [], args = [("left",(13171,"(.IsProgram (.m))")),("right",(13173,"(.Stops (.m) (.m))"))], nextSteps = [("left",Step {formula = "(.IsProgram (.m))", rule = "Assumption(+1)", _id = 13171, discards = [], args = [], nextSteps = []}),("right",Step {formula = "(.Stops (.m) (.m))", rule = "Assumption(+1)", _id = 13173, discards = [], args = [], nextSteps = []})]}),("major",Step {formula = "(\8594 (\8743 (.IsProgram (.m)) (.Stops (.m) (.m))) (\172 (.Stops (.m) (.m))))", rule = "\8704E", _id = 13195, discards = [], args = [("predicate",(14128,"(\8704 p (\8594 (\8743 (.IsProgram p) (.Stops (.m) p)) (\172 (.Stops p p))))"))], nextSteps = [("predicate",Step {formula = "(\8704 p (\8594 (\8743 (.IsProgram p) (.Stops (.m) p)) (\172 (.Stops p p))))", rule = "Assumption", _id = 14128, discards = [("",14128)], args = [], nextSteps = []})]})]})]})]})]})]}),
("negation",Step {formula = "(\172 (.Stops (.m) (.m)))", rule = "\172I", _id = 13199, discards = [("assumption",13173)], args = [("assumption",(13173,"(.Stops (.m) (.m))")),("absurd",(13197,"(\8869)"))], nextSteps = [("absurd",Step {formula = "(\8869)", rule = "\172E", _id = 13197, discards = [], args = [("negation",(13196,"(\172 (.Stops (.m) (.m)))")),("affirmation",(13173,"(.Stops (.m) (.m))"))], nextSteps = [("affirmation",Step {formula = "(.Stops (.m) (.m))", rule = "Assumption(+1)", _id = 13173, discards = [], args = [], nextSteps = []}),("negation",Step {formula = "(\172 (.Stops (.m) (.m)))", rule = "\8594E", _id = 13196, discards = [], args = [("major",(13195,"(\8594 (\8743 (.IsProgram (.m)) (.Stops (.m) (.m))) (\172 (.Stops (.m) (.m))))")),("minor",(13192,"(\8743 (.IsProgram (.m)) (.Stops (.m) (.m)))"))], nextSteps = [("minor",Step {formula = "(\8743 (.IsProgram (.m)) (.Stops (.m) (.m)))", rule = "\8743I", _id = 13192, discards = [], args = [("left",(13171,"(.IsProgram (.m))")),("right",(13173,"(.Stops (.m) (.m))"))], nextSteps = [("left",Step {formula = "(.IsProgram (.m))", rule = "Assumption(+1)", _id = 13171, discards = [], args = [], nextSteps = []}),("right",Step {formula = "(.Stops (.m) (.m))", rule = "Assumption(+1)", _id = 13173, discards = [], args = [], nextSteps = []})]}),("major",Step {formula = "(\8594 (\8743 (.IsProgram (.m)) (.Stops (.m) (.m))) (\172 (.Stops (.m) (.m))))", rule = "\8704E", _id = 13195, discards = [], args = [("predicate",(14128,"(\8704 p (\8594 (\8743 (.IsProgram p) (.Stops (.m) p)) (\172 (.Stops p p))))"))], nextSteps = [("predicate",Step {formula = "(\8704 p (\8594 (\8743 (.IsProgram p) (.Stops (.m) p)) (\172 (.Stops p p))))", rule = "Assumption", _id = 14128, discards = [("",14128)], args = [], nextSteps = []})]})]})]})]})]})]}}

backwardsImplication, forwardsImplication, backwardsDisjunction, forwardsDisjunction, backwardsConjunction, forwardsConjunction :: ProofScript
backwardsImplication = ProofScript {language = [], system = "M->", initialAssumptions = [(12960,"A")], steps = Step {formula = "(\8594 A A)", rule = "\8594I", _id = 13161, discards = [("hypothesis",13163),("hypothesis",12947)], args = [], nextSteps = [("predicate",Step {formula = "A", rule = "\8594E", _id = 13162, discards = [], args = [("antecedent",(12957,"A"))], nextSteps = [("minor",Step {formula = "A", rule = "Leaf", _id = 13163, discards = [], args = [], nextSteps = []}),("major",Step {formula = "(\8594 A A)", rule = "\8594I", _id = 13164, discards = [("hypothesis",12947)], args = [], nextSteps = [("predicate",Step {formula = "A", rule = "Leaf", _id = 12947, discards = [], args = [], nextSteps = []})]})]})]}}
forwardsImplication = ProofScript {language = [], system = "M->Fw", initialAssumptions = [(14575,"A")], steps = Step {formula = "(\8594 A A)", rule = "\8594I", _id = 14579, discards = [("antecedentHypothesis",14575)], args = [("predicate",(14577,"A")),("antecedentHypothesis",(14575,"A"))], nextSteps = [("predicate",Step {formula = "A", rule = "\8594E", _id = 14577, discards = [], args = [("major",(14576,"(\8594 A A)")),("minor",(14575,"A"))], nextSteps = [("major",Step {formula = "(\8594 A A)", rule = "\8594I", _id = 14576, discards = [("antecedentHypothesis",14575)], args = [("predicate",(14575,"A")),("antecedentHypothesis",(14575,"A"))], nextSteps = [("predicate",Step {formula = "A", rule = "Assumption", _id = 14575, discards = [("",14575)], args = [], nextSteps = []})]}),("minor",Step {formula = "A", rule = "Assumption", _id = 14575, discards = [("",14575)], args = [], nextSteps = []})]})]}}
backwardsDisjunction = ProofScript {language = [], system = "Min", initialAssumptions = [(13211,"(\8744 A B)")], steps = Step {formula = "(\8744 B A)", rule = "\8744E", _id = 13380, discards = [("hypothesis",13381),("hypothesis",13382)], args = [("disjunction",(13209,"(\8744 A B)"))], nextSteps = [("resultLeft",Step {formula = "(\8744 B A)", rule = "\8744I-r", _id = 13182, discards = [], args = [], nextSteps = [("right",Step {formula = "A", rule = "Leaf", _id = 13382, discards = [], args = [], nextSteps = []})]}),("resultRight",Step {formula = "(\8744 B A)", rule = "\8744I-l", _id = 13183, discards = [], args = [], nextSteps = [("left",Step {formula = "B", rule = "Leaf", _id = 13381, discards = [], args = [], nextSteps = []})]}),("disjunction",Step {formula = "(\8744 A B)", rule = "Leaf", _id = 13205, discards = [], args = [], nextSteps = []})]}}
forwardsDisjunction = ProofScript {language = [], system = "MinFw", initialAssumptions = [(14139,"(\8744 A B)")], steps = Step {formula = "(\8744 B A)", rule = "\8744E", _id = 14576, discards = [("leftHypothesis",14206),("rightHypothesis",14140)], args = [("disjunction",(14139,"(\8744 A B)")),("resultLeft",(14141,"(\8744 B A)")),("leftHypothesis",(14206,"A")),("resultRight",(14205,"(\8744 B A)")),("rightHypothesis",(14140,"B"))], nextSteps = [("resultLeft",Step {formula = "(\8744 B A)", rule = "\8744I-r", _id = 14141, discards = [], args = [("right",(14206,"A"))], nextSteps = [("right",Step {formula = "A", rule = "Assumption(+1)", _id = 14206, discards = [], args = [], nextSteps = []})]}),("resultRight",Step {formula = "(\8744 B A)", rule = "\8744I-l", _id = 14205, discards = [], args = [("left",(14140,"B"))], nextSteps = [("left",Step {formula = "B", rule = "Assumption", _id = 14140, discards = [], args = [], nextSteps = []})]}),("disjunction",Step {formula = "(\8744 A B)", rule = "Assumption", _id = 14139, discards = [("",14139)], args = [], nextSteps = []})]}}
backwardsConjunction = ProofScript {language = [], system = "Min", initialAssumptions = [(13833,"(\8743 A B)")], steps = Step {formula = "(\8743 B A)", rule = "\8743I", _id = 13895, discards = [], args = [], nextSteps = [("left",Step {formula = "B", rule = "\8743E-r", _id = 13324, discards = [], args = [("conjunction",(13832,"(\8743 A B)"))], nextSteps = [("conjunction",Step {formula = "(\8743 A B)", rule = "Leaf", _id = 13214, discards = [], args = [], nextSteps = []})]}),("right",Step {formula = "A", rule = "\8743E-l", _id = 13323, discards = [], args = [("conjunction",(13832,"(\8743 A B)"))], nextSteps = [("conjunction",Step {formula = "(\8743 A B)", rule = "Leaf", _id = 13896, discards = [], args = [], nextSteps = []})]})]}}
forwardsConjunction = ProofScript {language = [], system = "MinFw", initialAssumptions = [(12912,"(\8743 A B)")], steps = Step {formula = "(\8743 B A)", rule = "\8743I", _id = 13328, discards = [], args = [("left",(13310,"B")),("right",(13309,"A"))], nextSteps = [("left",Step {formula = "B", rule = "\8743E-r", _id = 13310, discards = [], args = [("conjunction",(12912,"(\8743 A B)"))], nextSteps = [("conjunction",Step {formula = "(\8743 A B)", rule = "Assumption", _id = 12912, discards = [("",12912)], args = [], nextSteps = []})]}),("right",Step {formula = "A", rule = "\8743E-l", _id = 13309, discards = [], args = [("conjunction",(12912,"(\8743 A B)"))], nextSteps = [("conjunction",Step {formula = "(\8743 A B)", rule = "Assumption", _id = 12912, discards = [("",12912)], args = [], nextSteps = []})]})]}}
