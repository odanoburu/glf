{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

-- |
-- Module      : GLF.Server
-- Description : Serve a REST API for deduction graphs
-- Copyright   : (c) bruno cuconato, 2020
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
--
-- Declare a REST API for interacting with deduction graphs through an HTTP
-- server.
--
-- When the server is running, issuing a GET request to @docs/@ will
-- return the documentation of the API.
module GLF.Server
  ( GlfAPI,
    LogLevel (..),
    Port,
    glfServe,
    generateElm,
  )
where

import qualified Bound
import Control.Applicative
import Control.Monad.RWS hiding (lift, pass)
import Crypto.Random
import Data.Aeson (FromJSON, ToJSON)
import qualified Data.Aeson as JSON
import qualified Data.Aeson.Types as Json.Types
import qualified Data.Vector as Vector
import Data.Bifunctor
import qualified Data.ByteString as ByteString
import Data.ByteString.Lazy (ByteString)
import Data.Char as Char
import qualified Database.PostgreSQL.Simple as DB
import Data.Foldable hiding (toList)
import qualified Data.Map as Map
import Data.String.Interpolate (i)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as TextEncoding
import Data.Text.Lazy (pack)
import Data.Text.Lazy.Encoding (encodeUtf8)
--import Debug.Trace (trace)
import GHC.Exts (toList)
import GHC.Generics (Generic)
import GLF.Base
import GLF.GraphBackend
import GLF.ProofScript
import GLF.Logic.Base
import GLF.Rule
import GLF.Server.Auth
import GLF.Server.ElmType
import GLF.System
import qualified Generics.SOP as SOP
import qualified Language.Elm.Definition as Definition
import qualified Language.Elm.Expression as Expression
import qualified Language.Elm.Pattern as Pattern
import qualified Language.Elm.Pretty as Pretty
import qualified Language.Elm.Simplification as Simplification
import qualified Language.Elm.Type as Type
import Language.Haskell.To.Elm
import Network.HTTP.Types (ok200)
import Network.Wai (responseLBS)
import Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.Cors (cors, corsRequestHeaders, simpleCorsResourcePolicy)
import Network.Wai.Middleware.RequestLogger (logStdout, logStdoutDev)
import Prettyprinter.Render.Text (hPutDoc)
import Servant hiding (Param)
import Servant.Docs
import Servant.To.Elm
import System.Directory
import System.FilePath ((<.>), (</>))
import qualified System.FilePath as FilePath
import System.IO
import Prelude hiding (init)


type UserAPI =
  "system-data"
      :> QueryParam "system" Text
      :> Get '[JSON] [SystemInfo]
  --   :<|> "listArgs" :> Capture "system" Text
  --     :> Capture "rule" Text
  --     :> ReqBody '[JSON] [(Text, NodeId)]
  --     :> Post '[JSON] (Fallible PossibleArguments)
  :<|> "getFormulas"
    :> ReqBody '[JSON] [NodeId]
    :> Post '[JSON] [(FormulaId, Text)]
--   :<|> "findMatchingRules" :> Capture "system" Text
--     :> Capture "goalId" GoalId
--     :> Get '[JSON] [Text]
  :<|> "rule" :> Capture "system" Text
    :> Capture "proofId" RootId
    :> QueryParam "languageId" LanguageId
    :> Capture "rule" Text
    :> ReqBody '[JSON] [(Text, NodeId)]
    :> Post '[JSON] (Fallible RuleApp)
  :<|> "newDeduction"
    :> Capture "system" Text
    :> ReqBody '[JSON] DeductionInput
    :> Post '[JSON] (Fallible DeductionRootRawInfo)
  :<|> "loadDeduction"
    :> Capture "proofId" RootId
    :> Get '[JSON] RawProofInfo
--   :<|> "loadSubDeduction"
--     :> Capture "system" Text
--     :> Capture "rootId" RootId
--     :> Capture "maxLevel" Int
--     -- if present, actually load the subdeduction rooted on the
--     -- node just below the provided root with this role
--     :> ReqBody '[JSON] (Maybe Text)
--     -- OPTIMIZE: maybe we don't return the formulas, let the
--     -- client ask for them (the ones it needs)
--     :> Post '[JSON] (Fallible SubProof)
  :<|> "addFormulas" :> Capture "system" Text
    :> Capture "proofId" RootId
    :> QueryParam "languageId" LanguageId
    :> ReqBody '[JSON] [Text]
    :> Post '[JSON] (Fallible [FormulaId])
  :<|> "delete"
    :> Capture "proofId" RootId
    :> Get '[JSON] Bool
  :<|> "check"
    :> Capture "proofId" RootId
    :> Get '[JSON] (Fallible RootId)
  :<|> "revert" :> Capture "system" Text
    :> Capture "nodeId" NodeId
--      :> ReqBody '[JSON] [Param]
    :> Post '[JSON] GoalId
--   :<|> "runStrategy" :> Capture "system" Text
--     :> QueryParam "languageId" Int
--     :> Capture "strategy" Text
--     :> ReqBody '[JSON] [GoalId]
--     :> Post '[JSON] (Fallible [Bool])
  :<|> "exportLatex"
    :> Capture "system" Text
    :> Capture "proofId" RootId
    :> QueryParam "languageId" LanguageId
    :> Get '[JSON] (Fallible Text)
  :<|> "deleteAccount" :> Get '[JSON] Bool
  -- does nothing, just to check authentication
  :<|> "checkAuth" :> Get '[JSON] Bool


data Arg = SoleArg Text | SubProofArg Text Text
  deriving stock (Generic)
  deriving anyclass (ToJSON, SOP.Generic, SOP.HasDatatypeInfo)
  deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.Arg" Arg

data RuleInfo = RuleInfo
  { ruleName :: Text,
    ruleAction :: RuleAction,
    argumentNames :: [Arg],
    formulaArgumentNames :: [Text],
    help :: Text,
    rulePremises :: [((Text, Text), [(Text, Text)])],
    ruleProvisos :: [Text],
    ruleResults :: [[Text]]
  }
  deriving stock (Generic)
  deriving anyclass (ToJSON, SOP.Generic, SOP.HasDatatypeInfo)
  deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.RuleInfo" RuleInfo

deriving anyclass instance SOP.Generic RuleAction

deriving anyclass instance SOP.HasDatatypeInfo RuleAction

deriving via (ElmType "Api.RuleAction" RuleAction) instance (HasElmDecoder JSON.Value RuleAction)

deriving via (ElmType "Api.RuleAction" RuleAction) instance (HasElmEncoder JSON.Value RuleAction)

deriving via (ElmType "Api.RuleAction" RuleAction) instance (HasElmType RuleAction)

-- deriving anyclass instance SOP.Generic Param

-- deriving anyclass instance SOP.HasDatatypeInfo Param

-- deriving via (ElmType "Api.Param" Param) instance (HasElmDecoder JSON.Value Param)

-- deriving via (ElmType "Api.Param" Param) instance (HasElmEncoder JSON.Value Param)

-- deriving via (ElmType "Api.Param" Param) instance (HasElmType Param)

-- newtype StrategyInfo = StrategyInfo Text
--   deriving stock (Generic)
--   deriving anyclass (SOP.Generic, SOP.HasDatatypeInfo, ToJSON)
--   deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.StrategyInfo" StrategyInfo

deriving stock instance Generic DeductionInput

deriving anyclass instance ToJSON DeductionInput

deriving anyclass instance FromJSON DeductionInput

deriving anyclass instance SOP.Generic DeductionInput

deriving anyclass instance SOP.HasDatatypeInfo DeductionInput

deriving via (ElmType "Api.DeductionInput" DeductionInput) instance (HasElmDecoder JSON.Value DeductionInput)

deriving via (ElmType "Api.DeductionInput" DeductionInput) instance (HasElmEncoder JSON.Value DeductionInput)

deriving via (ElmType "Api.DeductionInput" DeductionInput) instance (HasElmType DeductionInput)

deriving anyclass instance SOP.Generic PolyName

deriving anyclass instance SOP.HasDatatypeInfo PolyName

deriving via (ElmType "Api.PolyName" PolyName) instance (HasElmDecoder JSON.Value PolyName)

deriving via (ElmType "Api.PolyName" PolyName) instance (HasElmEncoder JSON.Value PolyName)

deriving via (ElmType "Api.PolyName" PolyName) instance (HasElmType PolyName)

deriving anyclass instance SOP.Generic Goal

deriving anyclass instance SOP.HasDatatypeInfo Goal

deriving via (ElmType "Api.Goal" Goal) instance (HasElmDecoder JSON.Value Goal)

deriving via (ElmType "Api.Goal" Goal) instance (HasElmEncoder JSON.Value Goal)

deriving via (ElmType "Api.Goal" Goal) instance (HasElmType Goal)

deriving stock instance Generic RuleApp

deriving anyclass instance ToJSON RuleApp

deriving anyclass instance SOP.Generic RuleApp

deriving anyclass instance SOP.HasDatatypeInfo RuleApp

deriving via (ElmType "Api.RuleApp" RuleApp) instance (HasElmDecoder JSON.Value RuleApp)

deriving via (ElmType "Api.RuleApp" RuleApp) instance (HasElmEncoder JSON.Value RuleApp)

deriving via (ElmType "Api.RuleApp" RuleApp) instance (HasElmType RuleApp)

deriving anyclass instance SOP.Generic DeductionRootRawInfo
deriving anyclass instance SOP.HasDatatypeInfo DeductionRootRawInfo
deriving via (ElmType "Api.DeductionRootRawInfo" DeductionRootRawInfo) instance ToJSON DeductionRootRawInfo
deriving via (ElmType "Api.DeductionRootRawInfo" DeductionRootRawInfo) instance (HasElmDecoder JSON.Value DeductionRootRawInfo)
deriving via (ElmType "Api.DeductionRootRawInfo" DeductionRootRawInfo) instance (HasElmEncoder JSON.Value DeductionRootRawInfo)
deriving via (ElmType "Api.DeductionRootRawInfo" DeductionRootRawInfo) instance (HasElmType DeductionRootRawInfo)

data SystemKindInfo = ForwardsSys | BackwardsSys | TableauxSys
  deriving stock (Generic)
  deriving anyclass (ToJSON, SOP.Generic, SOP.HasDatatypeInfo)
  deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.SystemKindInfo" SystemKindInfo

data SystemInfo = SystemInfo
  { systemName :: Text,
    operatorsInfo :: [Operator],
    extensible :: Bool,
    rulesInfo :: [RuleInfo],
--    strategiesInfo :: [StrategyInfo],
    kind :: SystemKindInfo,
    deductions :: [DeductionRootRawInfo],
    description :: Text
  }
  deriving stock (Generic)
  deriving anyclass (SOP.Generic, SOP.HasDatatypeInfo)
  deriving (ToJSON, HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.SystemInfo" SystemInfo

deriving anyclass instance SOP.Generic Precedence

deriving anyclass instance SOP.HasDatatypeInfo Precedence

deriving via (ElmType "Api.Precedence" Precedence) instance (HasElmDecoder JSON.Value Precedence)

deriving via (ElmType "Api.Precedence" Precedence) instance (HasElmEncoder JSON.Value Precedence)

deriving via (ElmType "Api.Precedence" Precedence) instance (HasElmType Precedence)


deriving anyclass instance SOP.Generic Associativity

deriving anyclass instance SOP.HasDatatypeInfo Associativity

deriving via (ElmType "Api.Associativity" Associativity) instance (HasElmDecoder JSON.Value Associativity)

deriving via (ElmType "Api.Associativity" Associativity) instance (HasElmEncoder JSON.Value Associativity)

deriving via (ElmType "Api.Associativity" Associativity) instance (HasElmType Associativity)

deriving anyclass instance SOP.Generic Arity

deriving anyclass instance SOP.HasDatatypeInfo Arity

deriving via (ElmType "Api.Arity" Arity) instance (HasElmDecoder JSON.Value Arity)

deriving via (ElmType "Api.Arity" Arity) instance (HasElmEncoder JSON.Value Arity)

deriving via (ElmType "Api.Arity" Arity) instance (HasElmType Arity)

deriving anyclass instance SOP.Generic Operator

deriving anyclass instance SOP.HasDatatypeInfo Operator

deriving via (ElmType "Api.Operator" Operator) instance (HasElmDecoder JSON.Value Operator)

deriving via (ElmType "Api.Operator" Operator) instance (HasElmEncoder JSON.Value Operator)

deriving via (ElmType "Api.Operator" Operator) instance (HasElmType Operator)

-- newtype PossibleArguments = PossibleArguments ([Text {-argnames-}], [[(Text {-label-}, NodeId {-arg-})]])
--   deriving stock (Generic)
--   deriving anyclass (ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)
--   deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.PossibleArguments" PossibleArguments

deriving stock instance Generic NodeKind

deriving anyclass instance ToJSON NodeKind

deriving anyclass instance FromJSON NodeKind

deriving anyclass instance SOP.Generic NodeKind

deriving anyclass instance SOP.HasDatatypeInfo NodeKind

deriving via (ElmType "Api.NodeKind" NodeKind) instance (HasElmDecoder JSON.Value NodeKind)

deriving via (ElmType "Api.NodeKind" NodeKind) instance (HasElmEncoder JSON.Value NodeKind)

deriving via (ElmType "Api.NodeKind" NodeKind) instance (HasElmType NodeKind)

data NodeInfo = NodeInfo
  { nodeId :: NodeId,
    derives :: FormulaId,
    discards :: [FormulaId],
    kind :: NodeKind,
    ruleArguments :: [(Text, NodeId)], -- TODO: remove argnames as this info should be available elsewhere and makes it more verbose
    role :: Text
  }
  deriving stock (Generic)
  deriving anyclass (ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)
  deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.NodeInfo" NodeInfo

data SubProof = SubProof
  { subroot :: RootId,
    parentOf :: [(NodeId, NodeId)],
    nodeInfos :: [NodeInfo],
    formulas :: [(FormulaId, Text)]
  }
  deriving stock (Generic)
  deriving anyclass (ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)
  deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.SubProof" SubProof

deriving via Int instance ToHttpApiData LanguageId
deriving via Int instance FromHttpApiData LanguageId
deriving via Int instance HasElmType LanguageId
deriving via Int instance HasElmEncoder Text LanguageId
deriving via Int instance HasElmEncoder JSON.Value LanguageId
deriving via Int instance HasElmDecoder JSON.Value LanguageId

-- root is the absolute root of the proof (labeled :Root on the
-- graph); sources are the :Rule (or :Goal nodes from which
-- subproofs follow)
data ProofInfo = ProofInfo
  { proof :: SubProof,
    mLanguageId :: Maybe LanguageId,
    sources :: [NodeId],
    assumptions :: [(NodeId, FormulaId)]
  }
  deriving stock (Generic)
  deriving anyclass (ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)
  deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.ProofInfo" ProofInfo

deriving anyclass instance SOP.Generic ProofNodeInfo
deriving anyclass instance SOP.HasDatatypeInfo ProofNodeInfo
deriving via (ElmType "Api.ProofNodeInfo" ProofNodeInfo) instance ToJSON ProofNodeInfo
deriving via (ElmType "Api.ProofNodeInfo" ProofNodeInfo) instance HasElmType ProofNodeInfo
deriving via (ElmType "Api.ProofNodeInfo" ProofNodeInfo) instance (HasElmEncoder JSON.Value ProofNodeInfo)
deriving via (ElmType "Api.ProofNodeInfo" ProofNodeInfo) instance (HasElmDecoder JSON.Value ProofNodeInfo)

deriving anyclass instance SOP.Generic AssumptionInfo
deriving anyclass instance SOP.HasDatatypeInfo AssumptionInfo
deriving via (ElmType "Api.AssumptionInfo" AssumptionInfo) instance ToJSON AssumptionInfo
deriving via (ElmType "Api.AssumptionInfo" AssumptionInfo) instance HasElmType AssumptionInfo
deriving via (ElmType "Api.AssumptionInfo" AssumptionInfo) instance (HasElmEncoder JSON.Value AssumptionInfo)
deriving via (ElmType "Api.AssumptionInfo" AssumptionInfo) instance (HasElmDecoder JSON.Value AssumptionInfo)


deriving anyclass instance SOP.Generic RawProofInfo
deriving anyclass instance SOP.HasDatatypeInfo RawProofInfo
deriving via (ElmType "Api.RawProofInfo" RawProofInfo) instance ToJSON RawProofInfo
deriving via (ElmType "Api.RawProofInfo" RawProofInfo) instance HasElmType RawProofInfo
deriving via (ElmType "Api.RawProofInfo" RawProofInfo) instance (HasElmEncoder JSON.Value RawProofInfo)
deriving via (ElmType "Api.RawProofInfo" RawProofInfo) instance (HasElmDecoder JSON.Value RawProofInfo)

deriving anyclass instance SOP.Generic (Fallible a)
deriving anyclass instance SOP.HasDatatypeInfo (Fallible a)

instance ToJSON a => ToJSON (Fallible a) where
  toJSON (Err err) = JSON.object ["err" JSON..= JSON.toJSON err]
  toJSON (Ok a) = JSON.object ["ok" JSON..= JSON.toJSON a]

instance FromJSON a => FromJSON (Fallible a) where
  parseJSON (JSON.Object obj) = (Err <$> obj JSON..: "err") <|> (Ok <$> (obj JSON..: "ok"))
  parseJSON other =
    Json.Types.prependFailure
      "parsing Fallible failed, "
      (Json.Types.typeMismatch "Object" other)

instance HasElmType a => HasElmType (Fallible a) where
  elmType =
    Type.App (Type.App "Result.Result" (Type.Global "String.String")) (elmType @a)

instance HasElmDecoder JSON.Value a => HasElmDecoder JSON.Value (Fallible a) where
  elmDecoder =
    Expression.App
      "Json.Decode.oneOf"
      $ Expression.List
        [ Expression.App
            ( Expression.App
                "Json.Decode.map"
                (Expression.Global "Result.Err")
            )
            ( Expression.App
                ( Expression.App
                    "Json.Decode.field"
                    (Expression.String "err")
                )
                "Json.Decode.string"
            ),
          Expression.App
            ( Expression.App
                "Json.Decode.map"
                (Expression.Global "Result.Ok")
            )
            ( Expression.App
                ( Expression.App
                    "Json.Decode.field"
                    (Expression.String "ok")
                )
                (elmDecoder @JSON.Value @a)
            )
        ]

instance HasElmEncoder JSON.Value a => HasElmEncoder JSON.Value (Fallible a) where
  elmEncoder =
    Expression.Lam $
      Bound.toScope $
        Expression.Case
          (pure $ Bound.B ())
          [ ( Pattern.Con "Result.Err" [Pattern.Var 0],
              Bound.toScope $
                Expression.App
                  "Json.Encode.object"
                  $ Expression.List
                    [Expression.tuple (Expression.String "err") (Expression.App "Json.Encode.string" $ pure $ Bound.B 0)]
            ),
            ( Pattern.Con "Result.Ok" [Pattern.Var 0],
              Bound.toScope $
                Expression.App
                  "Json.Encode.object"
                  $ Expression.List
                    [Expression.tuple (Expression.String "ok") $ Expression.App (elmEncoder @JSON.Value @a) $ pure $ Bound.B 0]
            )
          ]

-- fromEither :: Either String a -> Fallible a
-- fromEither = \case
--   Left err -> Err (Text.pack err)
--   Right a -> Ok a

type ProtectedUserAPI =
  BasicAuth "Deduction information and manipulation" User
    :> "glf"
    :> UserAPI

instance HasElmEndpoints api => HasElmEndpoints (BasicAuth "Deduction information and manipulation" User :> api) where
  elmEndpoints' = elmEndpoints' @(Header' '[Required, Servant.Strict] "Authorization" Text :> api)

type SignUpAPI =
  "signUp"
    :> ( "request"
           :> ReqBody '[JSON] (Bool, User) -- True if user is temporary (should be deleted after some time)
           :> Post '[JSON] (Fallible Text)
           --- NOTE: for now there is little protection against
           --- robots signing up, but I have this proof-of-work
           --- anti-spam system in mind…
           -- :<|> "followUp" :> ReqBody '[JSON] (Text, Int)
           --     :> Post '[JSON] Bool
       )

type GlfAPI = ProtectedUserAPI :<|> SignUpAPI

signUpServer :: ServerT SignUpAPI (App IO)
signUpServer = requestS
  where
    requestS :: (Bool, User) -> App IO (Fallible Text)
    requestS (temporary, User {user, email, pass}) = do
      case validation of
        _:_ -> return $ Err (Text.unlines validation)
        [] -> do
          saltBS :: ByteString.ByteString <- liftIO $ getRandomBytes 128
          let salt = encodeBase64 saltBS
              saltText = TextEncoding.decodeUtf8 salt
              passDigest = hashPassword pass saltText
              hashedpass = TextEncoding.decodeUtf8 $ encodeBase64 passDigest
              createUserQuery = [i|INSERT INTO users (user_name, user_email, salt, hashpass, is_temporary, created_on)
VALUES (?, ?, ?, ?, ?, now() AT TIME ZONE 'UTC')
ON CONFLICT (user_name) DO NOTHING;|]
          withConnection $ \conn -> do
            nModifiedRows <- liftIO $ DB.execute conn createUserQuery (user :: Text, email :: Text, saltText :: Text, hashedpass :: Text, temporary)
            case nModifiedRows of
              0 -> return (Err "User already exists")
              1 -> do
                -- TODO: whenever a user registers we check if there
                -- are stale users and delete them
                _ <- liftIO $ deleteStaleTemporaryUsersAndTheirWork conn
                return $ Ok user
              _biggerThanOne -> return $ Err "Internal error: created more than one user"
      where
        validation = concat
          [ [ "Username length must be between 6 and 1024 characters"
            | Text.compareLength user 5 == LT ||
              Text.compareLength user 1025 == GT ],
            [ "Username must not contain a colon (“:”)" | Text.any (== ':') user ],
            [ "Password length must be between 6 and 1024 characters"
            | Text.compareLength pass 5 == LT ||
              Text.compareLength pass 1025 == GT ],
            [ "Username characters must all be printable and in the ASCII range"
            | Text.any (\c -> not $ Char.isAscii c && Char.isPrint c) user ]
          ]

instance ToCapture (Capture "Args" JSON.Value) where
  toCapture _ =
    DocCapture
      "rule arguments"
      "Arguments to the rule (can be any valid JSON value)"

instance ToCapture (Capture "strategy" Text) where
  toCapture _ =
    DocCapture
      "strategy name"
      "Name of strategy to use"

instance ToCapture (Capture "eliminatorId" FormulaId) where
  toCapture _ =
    DocCapture
      "eliminatorId"
      "(integer) ID of the formula to be used as the eliminator predicate in the implication elimination rule"

instance ToCapture (Capture "nodeId" NodeId) where
  toCapture _ =
    DocCapture
      "nodeId"
      "(integer) ID of the node"

instance ToCapture (Capture "goalId" GoalId) where
  toCapture _ =
    DocCapture
      "goalId"
      "(integer) ID of the goal"

instance ToCapture (Capture "proofId" RootId) where
  toCapture _ =
    DocCapture
      "proofId"
      "(integer) ID of the proof"

instance ToParam (QueryParam "languageId" LanguageId) where
  toParam _ =
    DocQueryParam "languageId"
                  ["324", "18", "42"]
                  "(integer) Language identifier, if any."
                  Normal

instance ToParam (QueryParam "system" Text) where
  toParam _ =
    DocQueryParam "system"
                  ["M->", "FOL"]
                  "(text) System name identifier, if any."
                  Normal

instance ToCapture (Capture "maxLevel" Int) where
  toCapture _ =
    DocCapture
      "maxLevel"
      "(integer) maximum number of levels of the deduction to return"

instance ToCapture (Capture "system" Text) where
  toCapture _ =
    DocCapture
      "system"
      "(string) name of deductive system to use"

instance ToCapture (Capture "rule" Text) where
  toCapture _ =
    DocCapture
      "rule"
      "(string) name of deduction rule"

instance ToSample Int where
  toSamples _ = samples [42, 27]

instance ToSample (Fallible a) where
  toSamples _ = samples [] -- TODO:

instance ToSample SystemInfo where
  toSamples _ = samples []

-- instance ToSample PossibleArguments where
--   toSamples _ = samples []

instance ToSample Char where
  toSamples _ = samples ['a', 'b']

instance ToSample JSON.Value where
  toSamples _ = samples []

-- instance ToSample NodeInfo where
--   toSamples _ = samples [] --[sampleNode14, sampleNode16]

-- instance ToSample ProofInfo where
--   toSamples _ = samples []

-- instance ToSample SubProof where
--   toSamples _ = samples []

instance ToSample Text where
  toSamples _ = samples ["(A1 -> (A2 -> A1))", "A4"]

-- instance ToSample System where
--   toSamples _ = samples []

-- instance ToSample RuleApp where
--   toSamples _ = samples []

instance ToSample RawProofInfo where
  toSamples _ = samples []

-- instance ToSample (Map Text NodeId) where
--   toSamples _ = samples [mempty]

instance ToSample DeductionInput where
  toSamples _ =
    samples [ noLanguageDeductionInput ["A -> B", "A"] ["B"],
              DeductionInput ["HasChild 1", "ParentOf 2"] ["IsParent(a, b)"] ["HasChild(a)"]
            ]

instance ToSample () where
  toSamples _ = singleSample ()

-- instance ToSample Param where
--   toSamples _ = samples [NodeP "premise" 384, ListP "children" [345, 2837, 2]]

userServer :: ServerT ProtectedUserAPI (App IO)
userServer theUser@User{user = userName} =
  systemData
  --   :<|> listArgsS
  :<|> getFormulasS
  --   :<|> findMatchingRuleS
  :<|> ruleS
  :<|> newDeductionS
  :<|> loadDeductionS
  --   :<|> loadSubDeductionS
  :<|> addFormulasS
  :<|> deleteS
  :<|> checkS
  :<|> revertS
  --   :<|> runStrategyS
  :<|> exportLatexS
    -- :<|>
  :<|> deleteAccountS
  :<|> checkAuthS
  where
    systemData mSysName = do
      roots <- withConnection $ showRoots (user theUser)
      let rootsMap =
            Map.fromListWith
              (<>)
              ( ( \dedInfo@DeductionRootRawInfo {systemName} ->
                    (systemName, [dedInfo])
                )
                  <$> roots
              )
      syss <- asks (Map.elems . unSystems . GLF.Base.systems)
      let systemInfos = systemInfo rootsMap <$> syss
      return $ maybe systemInfos (\sysName -> filter (\SystemInfo{systemName} -> systemName == sysName) systemInfos) mSysName
      where
        systemInfo
          sysDedsMap
          System {systemName, operators, extensible, kind, rules, functions, description} =
            SystemInfo
              { systemName,
                operatorsInfo = operators,
                extensible,
                rulesInfo = ruleInfo (Map.fromList functions) <$> rules,
                -- strategiesInfo = fmap (StrategyInfo . fst) strategies,
                kind = systemKindToKindInfo kind,
                deductions = Map.findWithDefault [] systemName sysDedsMap,
                description = description

              }
          where
            systemKindToKindInfo =
              \case
                FitchSys -> ForwardsSys
                BackNDSys -> BackwardsSys
                GLF.System.TableauxSys -> GLF.Server.TableauxSys

        ruleInfo functions theRule@DeductiveRule {name = PolyName {unicode}, premises, provisos, conclusion, action, helpText} =
          RuleInfo
            { ruleName = unicode,
              ruleAction = action,
              argumentNames = fmap premiseArg premises,
              formulaArgumentNames =
                case formulaSpecAnchorable functions (premiseNames theRule) conclusion of
                  Unanchorable _ -> [resultFormulaArgName]
                  Anchorable _ anchors | Map.null anchors -> []
                  _ -> [resultFormulaArgName],
              help = helpText,
              rulePremises = fmap (\Premise{root, hypotheses, role} -> ((role, showFormulaSpec root), fmap (second showFormulaSpec) hypotheses)) premises,
              ruleProvisos = provisoTexts,
              ruleResults = fmap (fmap showFormulaSpec) [[conclusion]]
            }
          where
            provisoTexts =
              fmap showProviso provisos
            -- interactiveArgsOnly = filter (not . usesInteractiveNamingConvention)
            --   where
            --     usesInteractiveNamingConvention n =
            --       case Text.unpack $ Text.take 2 n of
            --         ['_', c] | c /= '_' -> True
            --         _nonreservedName -> False
            premiseArg Premise{role, hypotheses} =
              case hypotheses of
                [] -> SoleArg role
                (hypRole, _) : _  -> SubProofArg hypRole role
    getFormulasS fs = withConnection (getFormulaTexts fs)
    -- doRule :: Text -> Text -> (System -> CompiledRule -> c a) -> App IO a
    -- doRule sysName ruleName f = do
    --   syss <- asks systems
    --   let mSysRule = getRuleSystem syss sysName ruleName
    --   case mSysRule of
    --     Just (sys, rule) -> liftCypher' (f sys rule)
    --     Nothing ->
    --       fail [i|Could not find rule ‘#{ruleName}’ in system ‘#{sysName}’|]
    doRuleSystem :: Text -> Maybe LanguageId -> Text -> (SystemWithParser -> DeductiveRule -> DBio a) -> App IO a
    doRuleSystem sysName mLangId ruleName f = do
      withSystemParser sysName mLangId (fail . Text.unpack) $
        \sysWith@(SystemWithParser (sys, _parser)) ->
          case systemGetRule sys ruleName of
            Just theRule ->
              withConnection $ f sysWith theRule
            Nothing -> fail "Could not find this rule in this system"
    -- listArgsS sysName ruleName args =
    --   doRule sysName ruleName
    --     ( \_ rule -> case arguments rule of
    --         Query _ q -> Ok . PossibleArguments <$> interactiveRuleArgs "" q (Map.fromList args)
    --         _ -> return $ Err "Internal error: rule has no way of listing its possible arguments"
    --     )
    -- findMatchingRuleS sysName goal =
    --   withSystem sysName (fail . Text.unpack) $
    --     \ sys -> liftCypher' $ matchingRulesBySystem "" sys goal
    ruleS sysName proofId mLangId ruleName args =
      doRuleSystem sysName mLangId ruleName
        ( \systemWith theRule conn -> do
            mApplyRuleForwards systemWith proofId theRule ruleArguments conn
        )
      where
        ruleArguments = Map.fromList args
    newDeductionS sysName DeductionInput {languageInput, assumptionsInput, goalsInput} = do
      systemWithParserMaybeLangIdOr <- initializeLanguageParser sysName languageInput
      case systemWithParserMaybeLangIdOr of
        Left err -> return (Err $ Text.pack err)
        Right (sysWith, mLanguageId) -> do
          let dedInfo = deductionInfoStub { systemName = sysName,
                                            languageInput = Vector.fromList languageInput,
                                            goalInput = head goalsInput,
                                            assumptionsInput = Vector.fromList assumptionsInput
                                          }
          withConnection $ \conn -> do
            loadDeduction sysWith userName mLanguageId dedInfo conn
      --     case res of
      --       Left err -> return $ Err (Text.pack err)
      --       Right dedInfo@DeductionRootRawInfo {root} ->
      --         Ok . (dedInfo,) <$> getProofInfo root
    -- makeProofNodes rootId nodesInfo otherFormulas = do
    --   let (formulaIds, nodeInfos, parentOf, sources) = (\(a, b, c, d) -> (Set.toList a, b, c, d)) $ foldl' getInfos mempty nodesInfo
    --       formulaIds' = otherFormulas ++ formulaIds
    --   formulas <- liftCypher' $ getNodeFormulas "" formulaIds'
    --   return (nodeInfos, zip formulaIds' formulas, sources, parentOf)
    --   where
    --     getInfos
    --       (formulas, infos, parentOf, sources)
    --       ProofNodeInfo
    --         { proofNodeId = nodeId,
    --           kind,
    --           derives = derives,
    --           ruleArgNames,
    --           ruleNodeArgs,
    --           discards,
    --           parent = mParent,
    --           role,
    --           hypotheses
    --         } =
    --         ( List.foldl' (flip Set.insert) formulas (derives : concat [ruleNodeArgs, fmap snd discards, hypotheses]),
    --           NodeInfo
    --             { nodeId,
    --               derives = derives,
    --               role,
    --               discards = hypotheses,
    --               kind = kind,
    --               ruleArguments = zip ruleArgNames ruleNodeArgs
    --             } :
    --           infos,
    --           maybe parentOf (\p -> (p, nodeId) : parentOf) mParent,
    --           maybe sources (\p -> if p == rootId then nodeId : sources else sources) mParent
    --         )
    getProofInfo rootId = getRawProofInfo rootId
    loadDeductionS proofId = withConnection $ getProofInfo proofId
    -- loadSubDeductionS _system rootId _maxLevel mRole = do
    --   nodesInfo <- liftCypher' $ getRawSubproofNodes "" (SubProofRoot mRole rootId)
    --   (nodeInfos, formulaMap, rootsIds, parentOf) <- makeProofNodes rootId nodesInfo []
    --   return $ case rootsIds of
    --     [subRoot] -> Ok (SubProof {subroot = subRoot, nodeInfos, formulas = formulaMap, parentOf})
    --     [] -> Err "No subroot"
    --     _ -> Err "Expect single subroot"
    addFormulasS sysName proofId mLangId input =
      withSystemParser sysName mLangId (return . Err) $
        \(SystemWithParser (_, parser)) ->
          withConnection $ addFormulas parser proofId input
    deleteS proofId = withConnection $ deleteDeduction proofId
    withSystem :: Text -> (Text -> App IO a) -> (System -> App IO a) -> App IO a
    withSystem systemName mkErr f = do
      syss <- asks systems
      case getSystem syss systemName of
        Just sys -> f sys
        Nothing -> mkErr [i|Could not find system ‘#{systemName}’|]
    checkS proofId = withConnection (checkProof proofId)
      >>= \case
            Ok _ -> return $ Ok proofId
            Err err -> return $ Err err
    withSystemParser :: Text -> Maybe LanguageId -> (Text -> App IO a) -> (SystemWithParser -> App IO a) -> App IO a
    withSystemParser systemName mLangId mkErr f = do
      getLoadSystemLanguageParser systemName mLangId >>= \case
        Left err -> mkErr (Text.pack err)
        Right sysWith -> f sysWith
    revertS systemName nodeId =
      withSystem systemName (fail . Text.unpack) (\sys -> withConnection $ revertDeduction sys nodeId)
    -- runStrategyS sysName mLangId strategyName goals =
    --   withSystemParser
    --     sysName
    --     mLangId
    --     (return . Err)
    --     ( \sysWith@(SystemWithParser (System{strategies}, _)) ->
    --         case lookup strategyName strategies of
    --           Just strategy -> do
    --             r <- liftCypher' $ runStrategyGoals sysWith "" strategy Nothing goals
    --             return . Ok $ (isRight <$> r)
    --           Nothing ->
    --             return $ Err [i|No strategy named #{strategyName} for system #{sysName}|]
    --     )
    exportLatexS sysName proofId mLangId =
      withSystemParser sysName mLangId (return . Err) $
        \sysWith ->
          withConnection $ \conn -> do
            proofInfo <- getProofInfo proofId conn
            return $ Ok (proofToLatex sysWith proofInfo)
    deleteAccountS = withConnection $ deleteUserAndWork userName
    checkAuthS = do
      -- whenever a user logs in we check if there are stale users
      -- and delete them
      withConnection deleteStaleTemporaryUsersAndTheirWork

userAPI :: Proxy UserAPI
userAPI = Proxy

docsBS :: ByteString
docsBS =
  encodeUtf8
    . pack
    . markdown
    $ docsWithIntros [intro] userAPI
  where
    intro = DocIntro "Welcome" ["This is `glf`'s webservice's API."]

type DocsAPI =
  GlfAPI
    :<|> Raw

docsAPI :: Proxy DocsAPI
docsAPI = Proxy

server :: ServerT DocsAPI (App IO)
server =
  (userServer :<|> signUpServer)
    :<|> Tagged serveDocs
  where
    serveDocs _ doRespond =
      doRespond $ responseLBS ok200 [plain] docsBS
      where
        plain = ("Content-Type", "text/plain; charset=utf-8")

nt :: Env -> App IO a -> Handler a
nt c x = liftIO $ runApp' c (initializeState c) x

app :: ServerT DocsAPI (App IO) -> Env -> Application
app myserver env =
  myCors . serveWithContext docsAPI ctx $
    hoistServerWithContext
      docsAPI
      (Proxy :: Proxy '[BasicAuthCheck User])
      (nt env)
      myserver
  where
    ctx = checkBasicAuth env :. EmptyContext
    myCors = cors (const . Just $ corsPolicy)
      where
        corsPolicy =
          -- can't use simpleCors because we use authentication header
          simpleCorsResourcePolicy
            { corsRequestHeaders = ["Authorization", "Content-Type"]
            }

glfServe :: Env -> AppState -> LogLevel -> Port -> IO ()
-- ^ Serve 'UserAPI' under the given log level and port.
glfServe env st logLevel port = do
  let logger =
        -- IDEA: maybe log using Network.Wai.Logger?
        case logLevel of
          GLF.Base.None -> id
          Dev -> logStdoutDev
          Prod -> logStdout
      settings =
        setPort port defaultSettings
  liftIO . putStrLn $ "> Serve glf API at port " ++ show port
  runSettings settings . logger $ app myServer env
  where
    myServer =
      hoistServerWithContext
        docsAPI
        (Proxy :: Proxy '[BasicAuthCheck User])
        (liftIO . runApp' env st)
        server

-- | List of type definitions to be written to .elm files Each new
-- type from the domain model should be added there, otherwise the
-- root Elm module will fail to import some missing module, or will
-- refer to the type which definition was not written to file:
typeDefinitions :: [Definition.Definition]
typeDefinitions =
  concat
    [ jsonDefinitions @User,
      jsonDefinitions @RootId,
      jsonDefinitions @Precedence,
      jsonDefinitions @FormulaId,
      aliasDef "Api.NodeId" "Basics.Int",
      aliasDef "Api.FormulaId" "Api.NodeId",
      -- aliasDef "Api.GoalId" "Api.NodeId",
      aliasDef "Api.RootId" "Api.NodeId",
      jsonDefinitions @NodeInfo,
      jsonDefinitions @ProofNodeInfo,
      jsonDefinitions @AssumptionInfo,
      jsonDefinitions @RawProofInfo,
      jsonDefinitions @Arg,
      jsonDefinitions @RuleAction,
      -- jsonDefinitions @StrategyInfo,
      jsonDefinitions @DeductionInput,
      jsonDefinitions @PolyName,
      jsonDefinitions @Goal,
      jsonDefinitions @RuleApp,
      jsonDefinitions @SystemKindInfo,
      jsonDefinitions @DeductionRootRawInfo,
      jsonDefinitions @Associativity,
      jsonDefinitions @Arity,
      jsonDefinitions @Operator,
      jsonDefinitions @SystemInfo,
      -- jsonDefinitions @PossibleArguments,
      jsonDefinitions @NodeKind,
      jsonDefinitions @SubProof,
      --jsonDefinitions @ProofInfo,
      jsonDefinitions @RuleInfo,
      -- jsonDefinitions @Param,
      -- constantString "Api.parentArgName" parentArgName,
      -- constantString "Api.closesArgName" closesArgName,
      -- -- NOTE: name change when exporting elm code
      -- constantString "Api.rootArgName" rootNodeName,
      -- constantString "Api.goalArgName" goalNodeName,
      -- constantString "Api.resultFormulaArgName" resultFormulaArgName,
      -- constantString "Api.tableauxRevertChildrenGoalsParamName" tableauxRevertChildrenGoalsParamName,
      -- constantString "Api.tableauxRevertDataParentsParamName" tableauxRevertDataParentsParamName,
      constantInt "Api.userStaleAfterMilliseconds" (toInteger userStaleAfterMilliseconds)
    ]
  where
    constantInt name int = constantDef name "Basics.Int" (Expression.Int int)
    --constantString name str = constantDef name "Basics.String" (Expression.String str)
    constantDef qualifiedName existingQualifiedType expr =
      [ Definition.Constant
          qualifiedName
          0
          (Bound.toScope (Type.Global existingQualifiedType))
          expr
      ]
    aliasDef qualifiedName existingQualifiedType =
      [ Definition.Alias
          qualifiedName
          0
          (Bound.toScope (Type.Global existingQualifiedType))
      ]

generateElm :: FilePath -> IO ()
generateElm elmOutputDir = do
  let definitions =
        map (elmEndpointDefinition "Config.urlBase" ["Api"]) (elmEndpoints @GlfAPI)
          <> typeDefinitions
      modules =
        Pretty.modules $
          Simplification.simplifyDefinition <$> definitions

  forM_ (toList modules) (uncurry writeContentsToFile)
  where
    writeContentsToFile moduleName contents = do
      let elmFilepath = elmOutputDir </> FilePath.joinPath (fmap Text.unpack moduleName) <.> ".elm"
      -- It's important to check and create missing directories individually, because there may several directories
      createDirectoryIfMissing True $ FilePath.takeDirectory elmFilepath
      putStrLn $ "Writing file: " <> elmFilepath
      withFile elmFilepath WriteMode (`hPutDoc` contents)
