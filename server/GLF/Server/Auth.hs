{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

-- |
-- Module      : GLF.Server.Auth
-- Description : Authentication for the glf server
-- Copyright   : (c) bruno cuconato, 2020
-- License     : BSD-3
-- Maintainer  : bcclaro+haskell@gmail.com
-- Stability   : experimental
--
-- Define authentication for the glf server. Declares types for users,
-- passwords and user databases, plus functions for authenticating users.
module GLF.Server.Auth
  ( User (..),
    checkBasicAuth,
    hashPassword,
  )
where

import GHC.Stack (HasCallStack)
import Crypto.Hash
import Data.Aeson (FromJSON, ToJSON)
import qualified Data.Aeson as JSON
import qualified Data.ByteArray as ByteArray
import Data.ByteString (ByteString)
import Data.String.Interpolate (i)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
--import Debug.Trace (trace)
import GHC.Generics
import GLF.Base
import qualified Database.PostgreSQL.Simple as DB hiding (Connection)
import GLF.Server.ElmType
import qualified Generics.SOP as SOP
import Servant
  ( BasicAuth,
    BasicAuthCheck (..),
    BasicAuthResult (..),
    Proxy (..),
    basicAuthPassword,
    basicAuthUsername,
    (:>),
  )
import Servant.Foreign
  ( Arg (..),
    HasForeign (..),
    HasForeignType (..),
    HeaderArg (..),
    PathSegment (..),
    Req (..),
    foreignFor,
    typeFor,
  )

-- orphan instance for basic authentication in generated JS code
instance (HasForeign lang ftype api, HasForeignType lang ftype Text) => HasForeign lang ftype (BasicAuth a b :> api) where
  type Foreign ftype (BasicAuth a b :> api) = Foreign ftype api
  foreignFor lang proxy1 Proxy subR = foreignFor lang proxy1 (Proxy :: Proxy api) req
    where
      req = subR {_reqHeaders = HeaderArg arg : _reqHeaders subR}
      arg =
        Arg
          { _argName = PathSegment "Authorization",
            _argType =
              typeFor lang (Proxy :: Proxy ftype) (Proxy :: Proxy Text)
          }

-- | datatype for glf users
data User = User
  { user :: Text,
    email :: Text,
    -- some (hidden) polymorphism here, when client request contains user pass is
    -- the actual password, when stored in the backend pass is the password hashed
    -- with the salt
    pass :: Text,
    salt :: Text
  }
  deriving stock (Eq, Generic, Show)
  deriving anyclass (ToJSON, FromJSON, SOP.Generic, SOP.HasDatatypeInfo)
  deriving (HasElmType, HasElmDecoder JSON.Value, HasElmEncoder JSON.Value) via ElmType "Api.User" User

type HashPasswordAlgorithm = SHA3_384

hashPasswordAlgorithm :: HashPasswordAlgorithm
hashPasswordAlgorithm = undefined

hashPassword :: Text -> Text -> Digest HashPasswordAlgorithm
hashPassword salt password =
  let passwordWithSalt = password <> salt
      -- OPTIMIZE: could use stronger hash function, but don't want to use a
      -- hard one which might take too long to verify since we verify all
      -- the time (we could start using auth tokens too), and our threat
      -- model is not really that frightening
      hashedPasswordWithSalt = hashWith hashPasswordAlgorithm $ encodeUtf8 passwordWithSalt
   in hashedPasswordWithSalt

checkBasicAuth :: HasCallStack => Env -> BasicAuthCheck User

-- | checks user authentication against user database using Basic
-- Authentication
-- (<https://en.wikipedia.org/wiki/Basic_access_authentication>)
checkBasicAuth env = BasicAuthCheck $ \basicAuthData -> do
  let username = decodeUtf8 (basicAuthUsername basicAuthData)
  userData <- withConnectionEnv env $ getUserData username
  case userData of
    [(salt, hashpass, email)] ->
      let providedPassword = decodeUtf8 (basicAuthPassword basicAuthData)
          storedPassword' = decodeBase64 $ encodeUtf8 hashpass
       in case storedPassword' of
            Left {} -> return BadPassword
            Right (storedPassword :: ByteString) ->
              case digestFromByteString storedPassword :: Maybe (Digest HashPasswordAlgorithm) of
                Just providedPass ->
                  if ByteArray.constEq providedPass (hashPassword providedPassword salt)
                    then return (Authorized User{user = username, pass = hashpass, email = email, salt = salt})
                    else return BadPassword
                Nothing -> return BadPassword
    [] -> return NoSuchUser
    (_:_:_) -> error "Too many users found"
  where
    getUserData username conn = DB.query conn getUserDataQuery (DB.Only username)
      where
        getUserDataQuery =
          [i|SELECT salt, hashpass, user_email
FROM users
WHERE user_name = ?;|]
